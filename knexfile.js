const moment = require('moment');
module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host : '207.154.254.253',
      user : 'admin_opnmatU',
      password : '511JJzwwbK',
      database : 'admin_opnmatDB',
      timezone: 'UTC',
      typeCast: function (field, next) {
        if (field.type == 'DATETIME') {
          return moment(field.string()).format('YYYY-MM-DD HH:mm:ss');
        }
        return next();
      }
    },
    pool: {
      min: 2,
      max: process.env.RDS_MAX_CONNECTIONS || 10,
      evictionRunIntervalMillis: 1000
    },
    migrations: {
      tableName: 'knex_migrations'
    },
    debug: false
  },
  production: {
    client: 'mysql',
    connection: {
      host : '104.197.205.163',
      user : 'opn_u',
      password : 'kolza4PfaonctFvi',
      database : 'opnmat',
      timezone: 'UTC',
      typeCast: function (field, next) {
        if (field.type == 'DATETIME') {
          return moment(field.string()).format('YYYY-MM-DD HH:mm:ss');
        }
        return next();
      }
    },
    pool: {
      min: 2,
      max: process.env.RDS_MAX_CONNECTIONS || 10,
      evictionRunIntervalMillis: 1000
    },
    migrations: {
      tableName: 'knex_migrations'
    },
    debug: false
  }
}
