import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';

import { AcademiesService } from '../../../services/academies.service';

@Component({
  selector: 'opn-activations-academies',
  templateUrl: './activations.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class ActivationsComponent {

  settings = {
    edit: {
      editButtonContent: '<i class="nb-checkmark"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-locked"></i>',
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'name',
        type: 'string',
      },
      location: {
        title: 'Location',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      status_public: {
        title: 'Status public',
        type: 'string',
        filter: false,
      },
    },
    actions: {
      position: 'right',
      add: false,
    },
    mode: 'external',
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(private service: AcademiesService, private router: Router, private activeRoute: ActivatedRoute) {
    this.service.getAcademyByStatus('In_process,Accepted,Rejected').subscribe(res => {
      this.source.load(res);
    });
  }


  onConfirm(event) {
    this.service.updateAcademies(event.data.id, {status_public: 1}).subscribe(res => {
      if (res.status) {
        let data = event.data;
        data.status_public = 1;
        this.source.update(event.data, res);
      }
    });
  }

  onDelete(event) {
    this.service.updateAcademies(event.data.id, {status_public: 0}).subscribe(res => {
      if (res) {
        let data = event.data;
        data.status_public = 0;
        this.source.update(event.data, res);
      }
    });
  }
}
