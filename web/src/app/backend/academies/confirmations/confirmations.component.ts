import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';

import { AcademiesService } from '../../../services/academies.service';

@Component({
  selector: 'opn-confirmations-academies',
  templateUrl: './confirmations.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class ConfirmationsComponent {

  settings = {
    edit: {
      editButtonContent: '<i class="nb-checkmark"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-locked"></i>',
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'name',
        type: 'string',
      },
      location: {
        title: 'Location',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      status: {
        title: 'Status',
        type: 'string',
      },
    },
    actions: {
      position: 'right',
      add: false,
    },
    mode: 'external',
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(private service: AcademiesService, private router: Router, private activeRoute: ActivatedRoute) {
    this.service.getAcademyByStatus('In_process,Accepted,Rejected').subscribe(res => {
      this.source.load(res);
    });
  }


  onConfirm(event) {
    this.service.confirmAcademy( {id: event.data.id, status: 'Accepted'}).subscribe(res => {
      if (res) {
        let data = event.data;
        data.status = 'Accepted';
        this.source.update(event.data, data);
      }
    });
  }

  onDelete(event) {
    this.service.confirmAcademy( {id: event.data.id, status: 'Rejected'}).subscribe(res => {
      if (res) {
        let data = event.data;
        data.status = 'Rejected';
        this.source.update(event.data, data);
      }
    });
  }
}
