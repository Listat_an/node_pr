import {
    RouterModule,
    Routes
} from '@angular/router';
import {
    NgModule
} from '@angular/core';


import {
    PagesComponent
} from './pages.component';
import {
    DashboardComponent
} from './dashboard/dashboard.component';
import {
    CountStatisticMoreComponent
} from './dashboard/count-statistics/more-informations/more-informations.component';

const routes: Routes = [{
    path: '',
    component: PagesComponent,
    children: [{
        path: 'dashboard',
        component: DashboardComponent,
    }, {
      path: 'dashboard/count-detail',
      component: CountStatisticMoreComponent,
    }, {
        path: 'dashboard/user/:user_id',
        loadChildren: '../frontend/pages/user-dashboard/user-dashboard.module#UserDashboardModule',
    }, {
        path: 'dashboard/academiy-datail/:academy_id/dashboard',
        loadChildren: '../frontend/pages/academy-dashboard/academy-dashboard.module#AcademyDashboardModule',
    }, {
        path: 'ui-features',
        loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
    }, {
        path: 'academies',
        loadChildren: './academies/academies.module#AcademiesModule',
    }, {
        path: 'users',
        loadChildren: './users/users.module#UsersModule',
    }, {
        path: 'cms',
        loadChildren: './cms/cms.module#CmsModule',
    }, {
        path: 'teachers',
        loadChildren: './teachers/teachers.module#TeacherModule',
    }, {
        path: 'techniques',
        loadChildren: './techniques/techniques.module#TechniqueModule',
    }, {
        path: 'activities',
        loadChildren: './activity/activity.module#ActivityModule',
    }, {
        path: 'refunds',
        loadChildren: './refunds/refunds.module#RefundsModule',
    }, {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    }],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class BackendRoutingModule {}
