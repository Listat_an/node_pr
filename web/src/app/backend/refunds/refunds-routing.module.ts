import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RefundsComponent } from './refunds.component';
import { ConfirmationsComponent } from './confirmations/confirmations.component';




const routes: Routes = [{
  path: '',
  component: ConfirmationsComponent,
  children: [
    {
      path: 'confirmations',
      component: ConfirmationsComponent,
    },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RefundsRoutingModule { }

export const routedComponents = [
  RefundsComponent,
  ConfirmationsComponent,
];
