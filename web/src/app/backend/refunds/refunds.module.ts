import { TitleRenderComponent } from './title-render.component';
import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { RefundsRoutingModule, routedComponents } from './refunds-routing.module';
import { RefundService } from '../../services/refund.service';
import { ManagerRenderComponent } from './manager-render.component';
import { AmountRenderComponent } from './amount-render.component';
import { UserRenderComponent } from './user-render.component';


@NgModule({
  imports: [
    ThemeModule,
    RefundsRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
    ManagerRenderComponent,
    TitleRenderComponent,
    AmountRenderComponent,
    UserRenderComponent,
  ],
  entryComponents: [
    ManagerRenderComponent,
    TitleRenderComponent,
    AmountRenderComponent,
    UserRenderComponent,
  ],
  providers: [
    RefundService,
  ],
})
export class RefundsModule { }
