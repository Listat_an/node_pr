import { Component } from '@angular/core';

@Component({
  selector: 'opn-refunds',
  template: `<router-outlet></router-outlet>`,
})
export class RefundsComponent {
}
