import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';

import { RefundService } from '../../../services/refund.service';
import { ManagerRenderComponent } from '../manager-render.component';
import { TitleRenderComponent } from '../title-render.component';
import { AmountRenderComponent } from '../amount-render.component';
import { UserRenderComponent } from '../user-render.component';

@Component({
  selector: 'opn-confirmations-academies',
  templateUrl: './confirmations.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class ConfirmationsComponent {

  settings = {
    edit: {
      editButtonContent: '<i class="ion-checkmark-round"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="ion-close-round"></i>',
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      manager: {
        title: 'Manager',
        type: 'custom',
        renderComponent: ManagerRenderComponent,
      },
      user: {
        title: 'User',
        type: 'custom',
        renderComponent: UserRenderComponent,
      },
      transaction: {
        title: 'Title',
        type: 'custom',
        renderComponent: TitleRenderComponent,
      },
      amount: {
        title: 'Amount',
        type: 'custom',
        renderComponent: AmountRenderComponent,
      },
    },
    actions: {
      position: 'right',
      add: false,
    },
    mode: 'external',
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(
    private service: RefundService,
    private router: Router,
    private activeRoute: ActivatedRoute,
  ) {
    this.service.getRefunds().subscribe(resp => {
      console.log(resp);
      this.source.load(resp);
    });
  }


  onConfirm(event) {
    this.service.stripeRefund( {refund_id: event.data.id}).subscribe(res => {
      console.log('stripeRefund ', res);
      if (res.status) {
        this.source.remove(event.data);
      }
    });
  }

  onDelete(event) {
    this.service.declineRefund( {refund_id: event.data.id}).subscribe(res => {
      console.log('declineRefund ', res);
      if (res.status) {
        this.source.remove(event.data);
      }
    });
  }
}
