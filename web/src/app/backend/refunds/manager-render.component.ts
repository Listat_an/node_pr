import {
    Component,
    Input,
    OnInit,
} from '@angular/core';

import {
    ViewCell,
} from 'ng2-smart-table';

@Component({
    template: `
      <a>{{renderValue}}</a>
    `,
})
export class ManagerRenderComponent implements OnInit {

    renderValue: string;

    @Input() value: any;
    @Input() rowData: any;

    ngOnInit() {
        this.renderValue = this.value.full_name.toString();
    }
}
