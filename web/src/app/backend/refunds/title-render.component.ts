import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
    template: `
      <a>{{renderValue}}</a>
    `,
})
export class TitleRenderComponent implements OnInit {

      renderValue: string;

      @Input() value: any;
      @Input() rowData: any;

      ngOnInit() {
        this.renderValue = (this.value.activity) ? this.value.activity.name.toString() : '';
      }
    }