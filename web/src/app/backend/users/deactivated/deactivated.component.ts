import { Component, OnDestroy } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import { DateRenderComponent } from '../list/date-render.component';

import { UsersService } from '../../../services/users.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'opn-deactivated-users',
  templateUrl: './deactivated.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class ListDeactivatedUsersComponent implements OnDestroy {
  private ngUnsubscribe: Subject <any> = new Subject <any> ();

  settings = {
    delete: {
      deleteButtonContent: '<i class="fa fa-toggle-off"></i>',
      confirmDelete: true,
    },
    columns: {
      first_name: {
        title: 'First name',
        type: 'string',
      },
      last_name: {
        title: 'Last Name',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
        editable: false,
        editor: {
          type: 'text',
        },
      },
      created_at: {
        title: 'Registration date',
        type: 'custom',
        renderComponent: DateRenderComponent,
      },
    },
    actions: {
      position: 'right',
      add: false,
      delete: true,
      edit: false,
    },
    mode: 'external',
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(private service: UsersService, private router: Router, private activeRoute: ActivatedRoute) {
    this.service.getDeactivatedUsers()
    .takeUntil(this.ngUnsubscribe)
    .subscribe(res => {
      this.source.load(res.data);
    });
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to deactivate user?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onDelete(event) {
    this.service.userBlock({
      user_id: event.data.id,
      status: false,
    })
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        if (resp.status) {
          this.source.remove(event.data);
        }
      });
  }
  
  onSelect(event) {
    this.router.navigate(['/admin/users/detail/' + event.data.id]);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
