import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { ListUsersComponent } from './list/list.component';
import { DetailUserComponent } from './detail/detail.component';
import { ListDeactivatedUsersComponent } from './deactivated/deactivated.component';





const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [
    {
      path: 'list',
      component: ListUsersComponent,
    }, {
      path: 'deactivated',
      component: ListDeactivatedUsersComponent,
    }, {
      path: 'detail/:id',
      component: DetailUserComponent,
    },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcademiesRoutingModule { }

export const routedComponents = [
  UsersComponent,
  ListUsersComponent,
  DetailUserComponent,
  ListDeactivatedUsersComponent,
];
