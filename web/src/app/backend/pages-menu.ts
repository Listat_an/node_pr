import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/admin/dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'Academies',
    icon: 'nb-location',
    children: [
      {
        title: 'List of academies',
        link: '/admin/academies/list/',
      },
      {
        title: 'Confirm creation of academy',
        link: '/admin/academies/confirmations/',
      },
      {
        title: '[De]Activations',
        link: '/admin/academies/activations/',
      },
    ],
  },
  {
    title: 'Refunds',
    icon: 'nb-loop',
    children: [
      {
        title: 'List',
        link: '/admin/refunds/',
      },
    ],
  },
  {
    title: 'Users',
    icon: 'nb-person',
    children: [
      {
        title: 'List of users',
        link: '/admin/users/list/',
      },
      {
        title: 'List of deactivated users',
        link: '/admin/users/deactivated/',
      },
    ],
  },
  {
    title: 'CMS',
    icon: 'nb-compose',
    children: [
      {
        title: 'Pages',
        link: '/admin/cms/list/',
      },
    ],
  },
  {
    title: 'Teachers',
    icon: 'ion-university',
    children: [
      {
        title: 'List',
        link: '/admin/teachers/list/',
      },
    ],
  },
  {
    title: 'Techniques',
    icon: 'ion-android-walk',
    children: [
      {
        title: 'List',
        link: '/admin/techniques/list/',
      },
    ],
  },
  {
    title: 'Activities',
    icon: 'ion-ios-calendar-outline',
    children: [
      {
        title: 'List',
        link: '/admin/activities/list/',
      },
    ],
  },
];
