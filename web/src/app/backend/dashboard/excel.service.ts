import {
    Injectable,
} from '@angular/core';
import {
    saveAs
} from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/octet-stream';
const EXCEL_EXTENSION = '.xlsx';

export class Workbook {
  SheetNames: any = [];
  Sheets: any = {};
}

@Injectable()
export class ExcelService {

    constructor() {}

    saveExcel(param: any, filename: string) {
        let wb = new Workbook();

        let write = new Array;
        param.data.forEach(function (row, index) {

            let each = new Array;
            let keys = Object.keys(row); // all the keys
            if (index == 0) {
                // column headers
                for (let i = 0; i < keys.length; i++) {
                    each.push(param.titles[keys[i]]);
                }
                write.push(each); // write header
                each = [];
                for (let i = 0; i < keys.length; i++) {
                    each.push(row[keys[i]]);
                }
            } else {
                for (let i = 0; i < keys.length; i++) {
                    each.push(row[keys[i]]);
                }
            }
            write.push(each);
        }, this);

        let data = write;

        let ws_name = "Sheet 1";
        wb.SheetNames.push(ws_name);
        wb.Sheets[ws_name] = this.sheet_from_array_of_arrays(data);

        let wbout = XLSX.write(wb, {
            bookType: 'xlsx',
            type: 'binary'
        }); //bookSST: true,
        saveAs(new Blob([this.s2ab(wbout)], {
            type: EXCEL_TYPE,
        }), filename + new Date().getTime() + EXCEL_EXTENSION);
    }


    s2ab(s) {
        const buf = new ArrayBuffer(s.length);
        const view = new Uint8Array(buf);
        for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }

    sheet_from_array_of_arrays(data: any, opts?: any): any {
        let ws: any = {};
        let wscols = [];
        data[0].forEach(element => {
            wscols.push({
                wch: 20,
            });
        });
    
        let startCell = {
            c: 10000000,
            r: 10000000
        };
        let endCell = {
            c: 0,
            r: 0
        };

        let range = {
            s: startCell,
            e: endCell
        };
        for (let R = 0; R != data.length; ++R) {
            for (let C = 0; C != data[R].length; ++C) {
                if (range.s.r > R) range.s.r = R;
                if (range.s.c > C) range.s.c = C;
                if (range.e.r < R) range.e.r = R;
                if (range.e.c < C) range.e.c = C;
                //let cell = { v: data[R][C], t: 'n' };
                //if (cell.v == null) continue;
                //let cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                //if (typeof cell.v === 'number') cell.t = 'n';
                //else if (typeof cell.v === 'boolean') cell.t = 'b';
                //else cell.t = 's';

                //let cell = new Cell();
                let cell: any = {};
                cell.v = data[R][C];
                //console.log(cell);
                let cell_ref = XLSX.utils.encode_cell({
                    c: C,
                    r: R
                });
                //console.log(cell_ref);
                if (cell.v == null) continue;
                if (typeof cell.v === 'number') cell.t = 'n';
                else if (typeof cell.v === 'boolean') cell.t = 'b';
                else cell.t = 's';
                //console.log(cell);
                ws[cell_ref] = cell;
                //console.log(ws);
            }
        }
        if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(startCell, endCell);
        ws['!cols'] = wscols;
        return ws;
    }

    public exportAsExcelFile(json: any[], excelFileName: string) {
        const data = json;
        this.saveExcel(data, excelFileName);
    }

}