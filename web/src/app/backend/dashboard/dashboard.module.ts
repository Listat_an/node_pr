import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {NgDatepickerModule} from '../../../components/ng-datepicker';
import { DashboardComponent } from './dashboard.component';
import { CountStatisticModule } from './count-statistics/count-statistics.module';
import { DashboardReportsComponent } from './reports/reports.component';
import { ExcelService } from './excel.service';
import { PrintService } from './print.service';


@NgModule({
  imports: [
    ThemeModule,
    CountStatisticModule,
    AngularMultiSelectModule,
    NgDatepickerModule,
  ],
  declarations: [
    DashboardComponent,
    DashboardReportsComponent,
  ],
  entryComponents: [
    DashboardReportsComponent,
  ],
  providers: [
    ExcelService,
    PrintService,
  ],
})
export class DashboardModule { }
