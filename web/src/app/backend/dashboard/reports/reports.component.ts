import { Component, OnInit, OnDestroy } from '@angular/core';

import { AcademiesService } from '../../../services/academies.service';
import { SubscriptionsService } from '../../../services/subscriptions.service';
import { UsersService } from '../../../services/users.service';
import { DashboardService } from '../../../frontend/pages/academy-dashboard/dashboard/dashboard.service';
import { ExcelService } from '../excel.service';
import { Subject } from 'rxjs/Subject';
import * as _is from 'is_js';
import * as _ from 'lodash';
import * as _dt from 'date-fns';
import {
  DatepickerOptions,
} from 'ng2-datepicker';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'opn-dashboard-reports',
  styleUrls: ['./reports.component.scss'],
  templateUrl: './reports.component.html',
  providers: [
    SubscriptionsService,
    DashboardService,
  ],
})
export class DashboardReportsComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject <any> = new Subject <any> ();

  public toDateOptions: DatepickerOptions;

  public fromDateOptions: DatepickerOptions;


  public typeSettings: any = {
    singleSelection: true,
    text: 'Select type',
    enableSearchFilter: false,
  };

  public academyTypeSettings: any = {
    singleSelection: true,
    text: 'Select academy type',
    enableSearchFilter: false,
  };

  public academySettings: any = {
    singleSelection: true,
    text: 'Select academy',
    enableSearchFilter: true,
  };

  public transactionSettings: any = {
    singleSelection: true,
    text: 'Select activity',
    enableSearchFilter: false,
  };

  public subscriptionSettings: any = {
    singleSelection: false,
    text: 'Select subscriptions',
    selectAllText: 'Select all subscriptions',
    enableSearchFilter: true,
  };

  public studentsSettings: any = {
    singleSelection: true,
    text: 'Select student status',
    enableSearchFilter: false,
  };

  public studentSettings: any = {
    singleSelection: true,
    text: 'Select student',
    enableSearchFilter: true,
  };

  public selectType: Boolean = false;
  public selectAcademyType: Boolean = false;
  public selectAcademyId: Boolean = false;
  public selectTransactionType: Boolean = false;

  public runLoadStatistics: Boolean = false;

  public statisticType = [];
  public date_from = _dt.subMonths(new Date(), 1);
  public date_to = new Date();

  public dataModel: any = {
    type: [],
    academy_type: [],
    academy_id: [],
    activity: [],
    subscription_id: [],
    date_from: this.date_from,
    date_to: this.date_to,
  };

  public stydentModel: any = {
    type: [],
    student_status: [],
    student_id: [],
    activity: [],
    subscription_id: [],
    date_from: this.date_from,
    date_to: this.date_to,
  };


  public typeList: any = [
    {id: 'academy', itemName: 'Academies'},
    {id: 'student', itemName: 'Students'},
  ];
  public academyTypeList: any = [
    {id: 'all', itemName: 'All'},
    {id: 'regular', itemName: 'Regular'},
    {id: 'pro', itemName: 'Pro'},
  ];
  public academyList: any = [];

  public transactionList: any = [
    {id: 'all', itemName: 'All'},
    {id: 'subscription', itemName: 'Subscription'},
    {id: 'seminar', itemName: 'Seminar'},
    {id: 'opnmat', itemName: 'Opnmat'},
    {id: 'others', itemName: 'Others'},
    {id: 'class', itemName: 'Class'},
    {id: 'shop', itemName: 'Shop'},
  ];

  public subscriptionList: any = [];

  public stydentTypeList: any = [
    {id: 'all', itemName: 'All'},
    {id: 'Cancelled', itemName: 'Cancelled'},
    {id: 'Active', itemName: 'Active'},
  ];

  public studentsList = [];

  public selectStydentType: Boolean = false;
  public selectStydentId: Boolean = false;

  constructor(
    private academiesSrvc: AcademiesService,
    private subscriptionsSrvc: SubscriptionsService,
    private usersSrvc: UsersService,
    private dashboardSrvc: DashboardService,
    private excelSrvc: ExcelService,
    private toastr: ToastrService,
  ) {}

  ngOnInit() {
    this.fromDateOptions = this.toDateOptions = {
      minYear: 1970,
      maxYear: 2080,
      displayFormat: 'DD[/] MM[/] YYYY',
      barTitleFormat: 'MMMM YYYY',
      firstCalendarDay: 1,
    };
  }

  public pickFrom() {
    this.dataModel.date_from = this.date_from;
    this.stydentModel.date_from = this.date_from;
  }
  public pickTo() {
    this.dataModel.date_to = this.date_to;
    this.stydentModel.date_to = this.date_to;
  }

  private checkType(): void {
    let reloadChild = false;
    if (_is.not.empty(this.dataModel.type)) {
      this.selectType = true;
      reloadChild = true;
    }else {
      this.selectType = false;
    }
    this.checkAcademyType(this.selectType, reloadChild);
  }

  private checkAcademyType(type: Boolean, reload: Boolean): void {
    let reloadChild = false;
    if (reload) {
      this.dataModel.academy_type = [];
      this.runLoadStatistics = false;
    }
    if (!type) {
      this.selectAcademyType = false;
      this.academyList = [];
    }else if (_is.not.empty(this.dataModel.academy_type)) {
      this.selectAcademyType = true;
      reloadChild = true;
    }else {
      this.selectAcademyType = false;
      this.academyList = [];
    }
    this.checkAcademyId(this.selectAcademyType, reloadChild);
  }

  private checkAcademyId(academy_type: Boolean, reload: Boolean): void {
    let reloadChild = false;
    if (reload) {
      this.dataModel.academy_id = [];
      this.runLoadStatistics = false;
    }
    if (!academy_type) {
      this.selectAcademyId = false;
    }else if (_is.not.empty(this.dataModel.academy_id)) {
      this.selectAcademyId = true;
      reloadChild = true;
    }else {
      this.selectAcademyId = false;
    }
    this.checkTransaction(this.selectAcademyId, reloadChild);
  }

  private checkTransaction(academy_id: Boolean, reload: Boolean): void {
    let reloadChild = false;
    let tr = null;
    if (reload) {
      this.dataModel.activity = [];
      this.runLoadStatistics = false;
    }
    if (!academy_id) {
      this.selectTransactionType = false;
      this.subscriptionList = [];
      this.dataModel.activity = [];
      this.dataModel.subscription_id = [];
    }
    if (_is.not.empty(this.dataModel.activity)) {
      if (this.dataModel.activity[0].id === 'subscription') {
        this.selectTransactionType = true;
        tr = this.dataModel.activity[0].id;
        this.loadSubscriptions(this.dataModel.academy_id[0]);
        this.checkSubscription(this.selectTransactionType, reloadChild, tr);
      }else {
        this.selectTransactionType = false;
        this.runLoadStatistics = true;
        this.dataModel.subscription_id = [];
      }
      reloadChild = true;
    }else {
      this.selectTransactionType = false;
      this.subscriptionList = [];
      this.dataModel.activity = [];
      this.dataModel.subscription_id = [];
      this.runLoadStatistics = false;
    }
  }

  private checkSubscription(tranzaction_type: Boolean, reload: Boolean, type: String): void {
    if (reload) {
      this.dataModel.subscription_id = [];
      this.runLoadStatistics = false;
    }
    if (!tranzaction_type) {
      this.subscriptionList = [];
      this.runLoadStatistics = false;
    }else if (_is.not.empty(this.dataModel.subscription_id)) {
      this.runLoadStatistics = true;
    }else {
      this.subscriptionList = [];
      this.runLoadStatistics = false;
    }

    if (type && type != 'subscription') {
      this.runLoadStatistics = true;
    }
  }

  public onTypeSelect() {
    this.dataModel.type = this.statisticType;
    this.stydentModel.type = this.statisticType;
    this.checkType();
  }

  public onTypeDeSelect() {
    this.dataModel.type = this.statisticType;
    this.stydentModel.type = this.statisticType;
    this.checkType();
  }

  public onAcademyTypeSelect() {
    this.checkAcademyType(true, false);
    this.loadAcademies(this.dataModel.academy_type[0]);
  }

  public onAcademyTypeDeSelect() {
    this.checkAcademyType(false, true);
  }

  public onAcademyIdSelect() {
    this.checkAcademyId(true, false);
  }

  public onAcademyIdDeSelect() {
    this.checkAcademyId(false, true);
  }

  public onTransactionSelect() {
    this.checkTransaction(true, false);
  }

  public onTransactionDeSelect() {
    this.checkTransaction(false, true);
  }

  public onSubscriptionSelect() {
    this.checkSubscription(true, false, null);
  }

  public onSubscriptionDeSelect() {
    this.checkSubscription(true, false, null);
  }

  private loadAcademies({id}) {
    if (id) {
      this.academiesSrvc.getAcademyType(id)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(resp => {
          console.log(resp);
          this.academyList = resp;
        });
    }
  }

  private loadSubscriptions({id}) {
    if (id) {
      this.subscriptionsSrvc.getSybscriptionsByAcademy(id)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(resp => {
          this.subscriptionList = resp.map(el => {
            return {
                id: el.id,
                itemName: el.name,
            };
          });
        });
    }
  }

  private transformData(data): any {
    let sendData: any;
    _.forIn(data, (item, index) => {
      if (Array.isArray(item)) {
        sendData = {
          ...sendData,
            [index]: item.map(val => {
              return val.id;
          }).join(','),
        };
      }else if (item != '') {
        sendData = {
          ...sendData,
          [index]: item,
        };
      }else {
        sendData = {
          ...sendData,
          [index]: '',
        };
      }
    });
    return sendData;
  }

  private checkStydentType(type: Boolean, reload: Boolean): void {
    let reloadChild = false;
    if (reload) {
      this.stydentModel.student_status = [];
      this.runLoadStatistics = false;
    }
    if (!type) {
      this.selectStydentType = false;
      this.academyList = [];
    }else if (_is.not.empty(this.stydentModel.student_status)) {
      this.selectStydentType = true;
      reloadChild = true;
      this.getAllStudents(this.stydentModel.student_status[0]);
    }else {
      this.selectStydentType = false;
      this.studentsList = [];
    }
    this.checkStydentId(this.selectStydentType, reloadChild);
  }

  private checkStydentId(type: Boolean, reload: Boolean): void {
    let reloadChild = false;
    if (reload) {
      this.stydentModel.student_id = [];
      this.runLoadStatistics = false;
    }
    if (!type) {
      this.selectStydentId = false;
      this.studentsList = [];
    }else if (_is.not.empty(this.stydentModel.student_id)) {
      this.selectStydentId = true;
      reloadChild = true;
      this.getAllStudents(this.stydentModel.student_id[0]);
    }else {
      this.selectStydentId = false;
      this.studentsList = [];
    }
    console.log('this.selectStydentId ', this.selectStydentId);
    this.checkStTransaction(this.selectStydentId, reloadChild);
  }

  private checkStTransaction(astudent_id: Boolean, reload: Boolean): void {
    if (reload) {
      this.stydentModel.activity = [];
      this.runLoadStatistics = false;
    }
    if (!astudent_id) {
      this.runLoadStatistics = false;
      this.stydentModel.activity = [];
    }else if (_is.not.empty(this.stydentModel.activity)) {
      this.runLoadStatistics = true;
    }else {
      this.stydentModel.activity = [];
    }
  }


  public onStydentTypeSelect() {
    this.checkStydentType(true, false);
  }

  public onStydentTypeDeSelect() {
    this.checkStydentType(true, false);
  }

  public onStydentIdSelect() {
    this.checkStydentId(true, false);
  }

  public onStydentIdDeSelect() {
    this.checkStydentId(true, false);
  }

  public onStTransactionSelect() {
    this.checkStTransaction(true, false);
  }

  public onStTransactionDeSelect() {
    this.checkStTransaction(false, true);
  }

  getAllStudents({id}) {
    this.usersSrvc.getUsersForStatistic(id)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.studentsList = resp;
      });
  }

  public onLoadStatistics() {
    let sendData: any;
    if (this.statisticType[0].id == 'academy') {
      sendData = this.transformData(this.dataModel);
    }else if (this.statisticType[0].id == 'student') {
      sendData = this.transformData(this.stydentModel);
    }

    this.dashboardSrvc.getReportAdmin(sendData)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        if (resp.data.length > 0) {
          this.excelSrvc.exportAsExcelFile(resp, 'Statistic_' + sendData.type);
        }else {
            this.toastr.warning('No data for export.');
        }
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
