import {
    Injectable,
} from '@angular/core';
import {
    Printd,
} from 'printd';

const cssText = `
  table {
    font-size: 85%;
    font-family: sans-serif;
    border-spacing: 0;
    border-collapse: collapse;
  }
  table .h-btns{
      display: none;
  }
  .grid-data {
    width: 100%; }
    .grid-data tr {
      height: 54px;
      border-bottom: 1px solid #d3dce9; }
    .grid-data thead th {
      font-family: "MuseoSansCyrl";
      font-weight: 500;
      font-size: 13px;
      color: #172a47; }
      .grid-data thead th.select-t {
        display: none;
      }
      .grid-data thead th:after {
        display: none;
      }
      .grid-data thead th .filter {
        cursor: pointer; }
        .grid-data thead th .filter:after {
          width: 20px;
          text-align: right;
          content: '';
          color: #c2c2c2;
          font-size: 13px;
          display: inline-block;
          font-family: Ionicons;
          speak: none;
          font-style: normal;
          font-weight: 400;
          font-variant: normal;
          text-transform: none;
          text-rendering: auto;
          line-height: 1;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale; }
        .grid-data thead th .filter.ASC:after {
          content: "\F123"; }
        .grid-data thead th .filter.DESC:after {
          content: "\F126"; }
    .grid-data tbody td {
      font-family: "MuseoSansCyrl";
      font-weight: 300;
      font-size: 14px;
      color: #1c2836;
      vertical-align: center; }
    .grid-data tbody td.select-t {
      display: none;
    }
    .grid-data tbody td opn-view-avatar{
      display: none;
    }
    .grid-data tbody td span.price{
      color: #4ca1ff;
    }
    .grid-data tbody td.long{
      width: 25%;
    }
    .grid-data tbody .check-row label {
      width: 18px;
      height: 18px;
      border: 2px solid #babec9;
      position: relative;
      transition: 0.5s ease-in-out;
      cursor: pointer;
      margin: 0;
      top: 5px; }
      .grid-data tbody .check-row label:hover {
        border-color: #4ca1ff; }
      .grid-data tbody .check-row label:after {
        display: none;
        content: '';
        width: 8px;
        height: 8px;
        background-color: #4ca1ff;
        position: absolute;
        left: 3px;
        top: 3px; }
    .grid-data tbody .check-row input[type='checkbox'] {
      display: none; }
      .grid-data tbody .check-row input[type='checkbox']:checked + label {
        border-color: #4ca1ff; }
        .grid-data tbody .check-row input[type='checkbox']:checked + label:after {
          display: block;
        }
        .grid-data tbody .list-subscriptions, .grid-data tbody .list-subscriptions .child-list{
          list-style: none;
          padding: 0;
          margin: 0;
        }
        .grid-data tbody .list-subscriptions > .list-item > span{
          display: none;
        }
        .grid-data tbody .list-subscriptions .child-list .list-item{
          display: block;
          font-family: "MuseoSansCyrl";
          font-weight: 300;
          font-size: 14px;
          color: #1c2836;
        }
`;

const d = new Printd();


@Injectable()
export class PrintService {
    public printGrid(selectorId) {
        d.print(document.getElementById(selectorId), cssText);
    }
}
