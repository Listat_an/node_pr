import { Component, OnInit, OnDestroy } from '@angular/core';

import { AcademiesService } from '../../../../services/academies.service';
import { UsersService } from '../../../../services/users.service';
import { Subject } from 'rxjs/Subject';
import { LocalDataSource } from 'ng2-smart-table';
import { DateRenderComponent } from './date-render.component';
import { Router } from '@angular/router';
import { ExcelService } from './../../excel.service';

@Component({
  selector: 'opn-count-statistic-more',
  styleUrls: ['./more-informations.component.scss'],
  templateUrl: './more-informations.component.html',
})
export class CountStatisticMoreComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject <any> = new Subject <any> ();
  public usersCount: any;
  public academiesCount: any;
  public proAcademiesCount: any;

  userSettings = {
    add: {
      addButtonContent: '<i class="nb-arrow-retweet"></i>',
    },
    columns: {
      first_name: {
        title: 'First name',
        type: 'string',
      },
      last_name: {
        title: 'Last Name',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
        editable: false,
        editor: {
          type: 'text',
        },
      },
      created_at: {
        title: 'Registration date',
        type: 'custom',
        renderComponent: DateRenderComponent,
      },
    },
    actions: {
      position: 'right',
      add: true,
      delete: false,
      edit: false,
    },
    mode: 'external',
  };

  academySettings = {
    add: {
      addButtonContent: '<i class="nb-arrow-retweet"></i>',
    },
    columns: {
      name: {
        title: 'Name',
        type: 'string',
      },
      location: {
        title: 'Location',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      created_at: {
        title: 'Creation date',
        type: 'custom',
        renderComponent: DateRenderComponent,
      },
    },
    actions: {
      position: 'right',
      add: true,
      delete: false,
      edit: false,
    },
    mode: 'external',
  };

  public userSource: LocalDataSource = new LocalDataSource();
  public academySource: LocalDataSource = new LocalDataSource();
  public proAcademySource: LocalDataSource = new LocalDataSource();

  constructor(
    private usersSrvc: UsersService,
    private academiesSrvc: AcademiesService,
    private router: Router,
    private excelSrvc: ExcelService,
  ) {}

  ngOnInit() {
    this.getUsers();
    this.getAcademies();
  }
  private getUsers(): void {
    this.usersSrvc.getUsersForAdmin()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.userSource.load(resp.data);
        this.usersCount = resp.total;
      });
  }
  private getAcademies(): void {
    this.academiesSrvc.getNotProAcademiesForAdmin()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.academySource.load(resp.data);
        this.academiesCount = resp.total;
      });

    this.academiesSrvc.getProAcademiesForAdmin()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.proAcademySource.load(resp.data);
        this.proAcademiesCount = resp.total;
      });
  }

  public onUserExport() {
    this.usersSrvc.getUsersForExport()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.excelSrvc.exportAsExcelFile(resp, 'Report wUsers_');
      });
  }

  public onAcademyExport() {
    this.academiesSrvc.getAcademiesForExport(0)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.excelSrvc.exportAsExcelFile(resp, 'Report Academies_');
      });
  }

  public onProAcademyExport() {
    this.academiesSrvc.getAcademiesForExport(1)
    .takeUntil(this.ngUnsubscribe)
    .subscribe(resp => {
      this.excelSrvc.exportAsExcelFile(resp, 'Report Pro Academies_');
    });
  }

  public onUserSelect(user) {
    this.router.navigate(['admin', 'dashboard', 'user', user.data.id]);
  }
  public onAcademySelect(academy) {
    this.router.navigate(['admin', 'dashboard', 'academiy-datail', academy.data.id, 'dashboard']);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
