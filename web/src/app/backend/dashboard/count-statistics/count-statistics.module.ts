import { NgModule } from '@angular/core';

import { ThemeModule } from '../../../@theme/theme.module';
import { CountStatisticComponent } from './../count-statistics/count-statistics.component';
import { RoundProgressModule } from '../../../../components/angular-round-progressbar/index';
import { AcademiesService } from '../../../services/academies.service';
import { UsersService } from '../../../services/users.service';
import { CountStatisticMoreComponent } from './more-informations/more-informations.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToasterModule } from 'angular2-toaster';
import { DateRenderComponent } from './more-informations/date-render.component';


@NgModule({
  imports: [
    ThemeModule,
    RoundProgressModule,
    Ng2SmartTableModule,
    ToasterModule,
  ],
  declarations: [
    CountStatisticComponent,
    CountStatisticMoreComponent,
    DateRenderComponent,
  ],
  entryComponents: [
    CountStatisticComponent,
    CountStatisticMoreComponent,
    DateRenderComponent,
  ],
  exports: [
    CountStatisticComponent,
    CountStatisticMoreComponent,
  ],
  providers: [
    AcademiesService,
    UsersService,
  ],
})
export class CountStatisticModule { }
