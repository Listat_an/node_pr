import { Component, OnInit, OnDestroy } from '@angular/core';

import { AcademiesService } from '../../../services/academies.service';
import { UsersService } from '../../../services/users.service';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

@Component({
  selector: 'opn-count-statistic',
  styleUrls: ['./count-statistics.component.scss'],
  templateUrl: './count-statistics.component.html',
})
export class CountStatisticComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject <any> = new Subject <any> ();
  public usersCount: any = 0;
  public academiesCount: any = 0;
  public proAcademiesCount: any = 0;

  constructor(
    private usersSrvc: UsersService,
    private academiesSrvc: AcademiesService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.getUsers();
    this.getAcademies();
  }
  private getUsers(): void {
    this.usersSrvc.getUsers()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.usersCount = resp.total;
      });
  }
  private getAcademies(): void {
    this.academiesSrvc.getNotProAcademies()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.academiesCount = resp.total;
      });

    this.academiesSrvc.getProAcademies()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(resp => {
        this.proAcademiesCount = resp.total;
      });
  }

  public onMore() {
    this.router.navigate(['admin', 'dashboard', 'count-detail']);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
