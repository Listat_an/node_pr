import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

@Component({
  selector: 'opn-count-statistic-more',
  styleUrls: ['./detaile.component.scss'],
  templateUrl: './detaile.component.html',
})
export class CountStatisticDetaileComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject <any> = new Subject <any> ();

  constructor(
  ) {}

  ngOnInit() {
    
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
