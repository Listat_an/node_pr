import { NgModule } from '@angular/core';

import { ThemeModule } from '../../../../@theme/theme.module';
import { CountStatisticDetaileComponent } from './detaile.component';


@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    CountStatisticDetaileComponent,
  ],
  entryComponents: [
    CountStatisticDetaileComponent,
  ],
  exports: [
    CountStatisticDetaileComponent,
  ],
  providers: [
  ],
})
export class CountStatisticModule { }
