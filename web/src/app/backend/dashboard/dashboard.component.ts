import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {
  constructor(
    private title: Title,
  ) {
    this.title.setTitle('Administration dashboard');
  }
}
