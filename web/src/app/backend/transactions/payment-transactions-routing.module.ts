import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentTransactionsComponent } from './payment-transactions.component';
import { ListUsersComponent } from './list/list.component';
import { DetailUserComponent } from './detail/detail.component';





const routes: Routes = [{
  path: '',
  component: PaymentTransactionsComponent,
  children: [
    {
      path: 'list',
      component: ListUsersComponent,
    }, {
      path: 'detail/:id',
      component: DetailUserComponent,
    },
],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentTransactionsRoutingModule { }

export const routedComponents = [
  PaymentTransactionsComponent,
  ListUsersComponent,
  DetailUserComponent,
];
