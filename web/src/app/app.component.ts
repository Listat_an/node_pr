import { Component} from '@angular/core';

@Component({
  selector: 'opn-app',
  template: ` <div class="wrap-limiter m-auto">
              <router-outlet></router-outlet>
            </div>`,
})
export class AppComponent {
  constructor() {
  }
}
