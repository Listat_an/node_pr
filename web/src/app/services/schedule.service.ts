import {
    Injectable,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from './service.config';
import {
    JwtHelperService,
} from '@auth0/angular-jwt';
import {
    Broadcaster,
} from './broadcaster';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import * as _ from 'lodash';
import { format } from 'date-fns';

@Injectable()
export class ScheduleService {
    private options;
    private activate: Boolean;
    private user: any;

    constructor(
        private http: Http,
        public jwtHelper: JwtHelperService,
        private broadcaster: Broadcaster,
    ) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    public getServerUrl(): String {
        return ConfigService.URL_SERVER;
    }

    getMe(): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + `/users/${this.user.id}`,
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public scheduleDelete(data): Observable < any > {
        return this.http.post(
            encodeURI(ConfigService.STATIC_SERVER + `/schedules/delete`),
            data,
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }
    
    public getSchedulesForDay(date, academy): Observable < any > {
        // encodeURI
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/schedules?include=class&filter[date]=${date}&filter[academy_id]=${academy}`),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getSchedule(from, to, academy): Observable < any > {
        // encodeURI
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/activities/classes_shedule_manager?from=${from}&to=${to}&academy_id=${academy}`),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getScheduleForStudent(from, to, academy, subscriptions, payment, type, is_join?): Observable < any > {
        let url = '';
        if (subscriptions && subscriptions.length > 0) {
            const _subscriptions = subscriptions.map(subscrib => {
                return subscrib.id;
            });
            url += `&subscription_ids=${_subscriptions.join(',')}`;
        }
        if (payment && payment.length > 0) {
            const _payment = payment.map(paymentT => {
                return paymentT.id;
            });
            url += `&payment_status=${_payment.join(',')}`;
        }
        if (type && type.value) {
            url += `&type=${type.value}`;
        }else {
            url += `&type=class`;
        }

        if (is_join) {
            url += `&is_join=true`;
        }
        if (academy) {
            url += `&academy_id=${academy}`;
        }
    
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/activities/classes_shedule_student?from=${format(from, 'YYYY-MM-DD')}&to=${format(to, 'YYYY-MM-DD')}${url}`),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getHolidays(from, to, academy): Observable < any > {
        // encodeURI
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/activities/classes_shedule_manager_holiday?from=${from}&to=${to}&academy_id=${academy}`),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public createHoliday(data) {
        return this.http.post(
            encodeURI(ConfigService.STATIC_SERVER + `/schedules/create_holiday`),
            data,
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }

    public updateSchedule(data) {
        return this.http.post(
            encodeURI(ConfigService.STATIC_SERVER + `/schedules/update`),
            data,
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }


    private handleError(error: any) {
        return Observable.throw(error.message || JSON.parse(error._body).errors[0] || error);
    }
}
