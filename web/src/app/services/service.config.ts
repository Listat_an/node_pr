import { environment } from '../../environments/environment';

export class ConfigService {
    public static get STATIC_SERVER(): string { return environment.statics_server; }
    public static get URL_SERVER(): string { return environment.url_server; }
}
