import {Injectable} from '@angular/core';
import {ConfigService} from './service.config';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Broadcaster} from './broadcaster';
import {
    Observable,
  } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
    }),
};

@Injectable()
export class ShopService {
    user;

    constructor(private http: HttpClient,
                private jwtHelper: JwtHelperService,
                private broadcaster: Broadcaster) {
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    public getMe() {
        return this.user;
    }

    deleteProductById(id): Observable<any> {
        return this.http.delete(
            ConfigService.STATIC_SERVER + `/products/${id}`,
            httpOptions,
        ).catch(this.handleError);
    }

    public getUserMe(): Observable<any> {
        return this.http.get(
          ConfigService.STATIC_SERVER + '/users/me',
          httpOptions,
        )
        .map(res => res)
        .catch(this.handleError);
      }

    public getAllProducts() {
        return this.http.get(ConfigService.STATIC_SERVER + `/products?include=images`, httpOptions)
    }

    public getProducts(name, locationSearchData, maxPrice, minPrice, isPriceIncreasing, limit) {
        let url = `/products?include=images,shipping,academy&page[size]=${limit}&sort=${isPriceIncreasing ? '' : '-'}price&filter[title][like]=%${name}%`;
        if (minPrice) {
            url += `&filter[price][gte]=${minPrice}`
        }
        if (maxPrice) {
            url += `&filter[price][lte]=${maxPrice}`
        }
        if (locationSearchData) {
            url += `&latitude=${locationSearchData.lat}&longitude=${locationSearchData.lng}&distantion=${locationSearchData.searchLocationRadius}`
        }

        url += `&filter[show]=1`;
        return this.http.get(ConfigService.STATIC_SERVER + url, httpOptions)
    }

    public getProductById(productId): Observable<any> {
        return this.http.get(ConfigService.STATIC_SERVER + `/products/${productId}?include=images,shipping,academy`, httpOptions)
        .map(res => res);
    }

    public getAcademyProducts(academyId, name, isPriceIncreasing, limit) {
        let url = `/products?include=images,shipping,academy&page[size]=${limit}&sort=${isPriceIncreasing ? '' : '-'}price&filter[academy_id]=${academyId}&filter[title][like]=%${name}%`;
        url += `&filter[show]=1`;
        return this.http.get(ConfigService.STATIC_SERVER + url, httpOptions);
    }

    public getAcademyProductsWithPagination(academyId, productName, page, limit) {
        let url = `/products?include=images,shipping,academy&page[number]=${page}&page[size]=${limit}&filter[academy_id]=${academyId}&filter[title][like]=%${productName}%`;
        
        return this.http.get(encodeURI(ConfigService.STATIC_SERVER + url), httpOptions);
    }

    createProduct(body) {
        console.log(body);
        return this.http.post(ConfigService.STATIC_SERVER + `/products`, body, httpOptions);
    }

    updateProduct(id, body) {
        console.log(body);
        return this.http.put(ConfigService.STATIC_SERVER + `/products/${id}`, body, httpOptions);
    }

    addToCart(productId, photo, quantity) {
        const body = {
            product_id: productId,
            photo: photo,
            quantity: quantity,
        };
        this.broadcaster.broadcast(ShopService, 1);
        return this.http.post(ConfigService.STATIC_SERVER + `/carts`, body, httpOptions)
    }

    onPlusItem() {
        return this.broadcaster.on<any>(ShopService);
    }

    getUserCart() {
        let url = `/carts?page[size]=10000&filter[user_id]=${this.user.id}&include=product`;
        return this.http.get(
            ConfigService.STATIC_SERVER + url, 
            httpOptions,
        ).map(res => {
            res['data'].map(item => {
                item.error = {
                    timestamp: null,
                    details: {},
                };
                return item;
            });
            return res;
        });
    }

    savedCheckout() {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/shop/saved_checkout`,
            httpOptions,
        ).catch(this.handleError);
    }

    updateOrder(order_id, data) {
        return this.http.put(
            ConfigService.STATIC_SERVER + `/orders/${order_id}`,
            data,
            httpOptions,
        ).catch(this.handleError);
    }

    prepareCheckout(data) {
        return this.http.post(
            ConfigService.STATIC_SERVER + `/shop/prepare_checkout`,
            data,
            httpOptions,
        ).catch(this.handleError);
    }

    paymentOrder(data): Observable<any> {
        return this.http.post(
            ConfigService.STATIC_SERVER + `/shop/payment_order`,
            data,
            httpOptions,
        )
        .map(res => {
            if (res) {
                this.broadcaster.broadcast(ShopService, 0);
            }
            return res;
        })
        .catch(this.handleError);
    }

    deleteProductFromCart(id): Observable<any> {
        this.broadcaster.broadcast(ShopService, -1);
        return this.http.delete(
            ConfigService.STATIC_SERVER + `/carts/${id}?filter[user_id]=${this.user.id}`,
            httpOptions,
        ).catch(this.handleError);
    }

    changeQuantity(id, data): Observable<any> {
        return this.http.put(
            ConfigService.STATIC_SERVER + `/carts/${id}?filter[user_id]=${this.user.id}`,
            data,
            httpOptions,
        ).map(res => res)
        .catch(this.handleError);
    }

    cancelOrder(): Observable<any> {
        return this.http.post(
            ConfigService.STATIC_SERVER + `/shop/decline_order`,
            {},
            httpOptions,
        ).map(res => {
            if (res['status']) {
                this.broadcaster.broadcast(ShopService, 0);
            }
            return res;
        })
        .catch(this.handleError);
    }

    private handleError(errors: any) {
        return Observable.throw(errors.error || errors);
      }

}
