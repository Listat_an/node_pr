import {
    Injectable,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from './service.config';
import {
    JwtHelperService
} from '@auth0/angular-jwt';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { throttleTime } from 'rxjs/operators';

@Injectable()
export class UsersService {
    private options;
    private activate: Boolean;
    private user: Number;

    constructor(private http: Http, public jwtHelper: JwtHelperService) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    getMe(): any {
        return this.user;
    }

    getUsersForStatistic(type): Observable <any> {
        let url = '';
        switch (type) {
            case 'blocked': {
                url = '&filter[is_blocked]=1';
                break;
            }
            case 'unblocked': {
                url = '&filter[is_blocked]=0';
                break;
            }
        }
        return this.http.get(
                ConfigService.STATIC_SERVER + `/users?page[size]=10000000000000${url}`,
                this.options,
            )
            .map(res => {
                return res.json().data.map(el => {
                    return {
                        id: el.id,
                        itemName: el.full_name,
                    };
                });
            })
            .catch(this.handleError);
    }

    getServerUrl(): string {
        return ConfigService.URL_SERVER;
    }

    public userBlock(data): Observable <any> {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/users/blocked',
            data,
            this.options,
        ).map(res => res.json())
        .catch(this.handleError);
    }

    public getUserMe(): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + '/users/me',
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getActiveUsers(): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + '/users?filter[is_blocked]=0',
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }
    public getDeactivatedUsers(): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + '/users?filter[is_blocked]=1',
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUsers(): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + '/users',
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUsersForAdmin(): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + '/users?page[size]=10000000000000',
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUsersForExport(): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + '/users?page[size]=10000000000000&include=academy,belt_user.belt',
                this.options,
            )
            .map(res => {
                const arrayTmp: any = {
                  titles: {},
                  data: [],
                };
                arrayTmp.titles = {
                    full_name: 'Name',
                    email: 'Email',
                    address: 'Address',
                    academy: 'Academy name',
                    date: 'Date of birth',
                    age: 'Age',
                    belt: 'Belt',
                    stripe: 'Stripe',
                    weight: 'Weight',
                };

                arrayTmp.data = res.json().data.map(el => {
                    const user = {
                      full_name: (el.full_name) ? el.full_name : '',
                      email: (el.email) ? el.email : '',
                      address: (el.address) ? el.address : '',
                      academy: (el.academy) ? el.academy.name : '',
                      date: (el.date) ? el.date : '',
                      age: (el.age_type) ? el.age_type : '',
                      belt: (el.belt_user) ? el.belt_user.belt.name : '',
                      stripe: (el.belt_user) ?  el.belt_user.belt.stripe : '',
                      weight: (el.weight) ? el.weight : '',
                    };
                    return user;
                });
                return arrayTmp;
            })
            .catch(this.handleError);
    }

    public updateUsers(id, obj): Observable < any > {
        return this.http.put(
                ConfigService.STATIC_SERVER + '/users/' + id,
                JSON.stringify(obj),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUserById(id, include?) {
        let url = '';
        if (include) {
            url = include;
        }
        return this.http.get(
                ConfigService.STATIC_SERVER + '/users?filter[id]=' + id + url,
                this.options,
            )
            .map(res => res.json().data[0])
            .catch(this.handleError);
    }

    public resetPasword(obj): Observable < any > {
        return this.http.post(
                ConfigService.STATIC_SERVER + '/auth/forgot',
                JSON.stringify(obj),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Observable.throw(error.message || error);
    }
}
