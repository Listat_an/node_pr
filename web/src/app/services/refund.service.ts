import {
  Injectable,
} from '@angular/core';
import {
  Http,
  Headers,
  RequestOptions,
} from '@angular/http';
import {
  Observable,
} from 'rxjs/Observable';
import {
  ConfigService,
} from './service.config';
import { JwtHelperService } from '@auth0/angular-jwt';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class RefundService {
  private options;
  private activate: Boolean;
  private user: any;

  constructor(
    private http: Http,
    public jwtHelper: JwtHelperService,
    ) {
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token'),
    });
    this.options = new RequestOptions({
      headers: headers,
    });
    this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
  }

  getMe(): any {
    return this.user;
  }

  public addRefund(obj): Observable<any> {
    return this.http.post(
      ConfigService.STATIC_SERVER + `/refunds`,
      obj,
      this.options,
    )
    .map(res => res.json())
    .catch(this.handleError);
  }

  public getRefunds(): Observable<any> {
    return this.http.get(
      ConfigService.STATIC_SERVER + `/refunds?include=manager,transaction.activity,transaction.user&filter[status]=0`,
      this.options,
    )
    .map(res => res.json().data.map(el => {
      let data = {
        ...el,
        user: el.transaction.user,
      };
      return data;
    }))
    .catch(this.handleError);
  }

  public declineRefund(obj): Observable<any> {
    return this.http.post(
      ConfigService.STATIC_SERVER + `/refunds/decline`,
      obj,
      this.options,
    )
    .map(res => res.json())
    .catch(this.handleError);
  }
  public stripeRefund(obj): Observable<any> {
    return this.http.post(
      ConfigService.STATIC_SERVER + `/refunds/stripe`,
      obj,
      this.options,
    )
    .map(res => res.json())
    .catch(this.handleError);
  }

  private handleError(error: any) {
    return Observable.throw(error.message || error);
  }
}
