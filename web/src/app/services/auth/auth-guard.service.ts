import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    public auth: AuthService,
    public router: Router,
  ) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    const activeUser = localStorage.getItem('active_token');
    
    if (!this.auth.isAuthenticated()) {
      if (route.queryParams['show-p']) {
        localStorage.setItem('referal-p', JSON.stringify({
          'show-p': route.queryParams['show-p'],
        }));
      }
      this.router.navigate(['/auth/login']);
      return false;
    }
    if (activeUser != '') {
      if (route.queryParams['show-p']) {
        localStorage.setItem('referal-p', JSON.stringify({
          'show-p': route.queryParams['show-p'],
        }));
      }
      this.router.navigate(['/auth/success']);
      return false;
    }
    return true;
  }
}
