import {
    Injectable,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from './service.config';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Broadcaster} from './broadcaster';
import {
    format,
    addDays,
} from 'date-fns';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import * as _ from 'lodash';

import { ScheduleService } from './schedule.service';

@Injectable()
export class ActivitiesService {
    private options;
    private activate: Boolean;
    private user: any;

    constructor(
        private http: Http,
        public jwtHelper: JwtHelperService,
        private broadcaster: Broadcaster,
    ) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    fire(data: string): void {
        this.broadcaster.broadcast(ActivitiesService, data);
    }

    on(): Observable<any> {
        return this.broadcaster.on<any>(ActivitiesService);
    }

    fireHoliday(data: string): void {
        this.broadcaster.broadcast(ScheduleService, data);
    }

    onHoliday(): Observable<any> {
        return this.broadcaster.on<any>(ScheduleService);
    }

    userId() {
        return this.user.id;
    }

    public getServerUrl(): String {
        return ConfigService.URL_SERVER;
    }

    getMe(): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/users/${this.user.id}`,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getScheduleClass(class_id): Observable < any > {
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/activities/${class_id}?include=shedules,techniques,subscriptions,teachers`),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public addCoverPhoto(obj): Observable<any> {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/activities/add_cover_photo',
            obj,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }


    public payActivity(obj): Observable<any> {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/activities/pay',
            obj,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public unJoinActivity(joinId): Observable<any> {
        return this.http.delete(
            ConfigService.STATIC_SERVER + `/join_activity/${joinId}`,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public joinActivity(obj): Observable<any> {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/join_activity',
            obj,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUserSubscriptions(): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/user_subscriptions?filter[user_id]=${this.user.id}`,
            this.options,
            )
            .map(res => {
                return res.json().data.map(el => {
                    return el.subscription_id;
                });
            })
            .catch(this.handleError);
    }

    public getActivitiesByAcademy(size, number, academy_id, filterDate, name?: String): Observable<any> {
        let url = `/activities/?filter[type][in]=seminar,mat_event,others&filter[academy_id]=${academy_id}&page[size]=${size}&page[number]=${number}&include=join_activities,subscriptions`;
        if (name) {
            url += `&filter[name][like]=%${name}%`;
        }

        if (filterDate && filterDate.start != -1 && filterDate.end != -1) {

            const [start_d, start_m, start_y] = filterDate.start.split('.');
            const [end_d, end_m, end_y] = filterDate.end.split('.');
            url += `&filter[start_date][gte]=${start_y}-${start_m}-${start_d}&filter[end_date][lte]=${end_y}-${end_m}-${end_d}`;
        }
        console.log(url);
        return this.http.get(
            encodeURI(ConfigService.STATIC_SERVER + url),
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getClassesByAcademy(size, number, academy_id, filterDate, name?: String): Observable<any> {
        let url = `/activities/?filter[type]=class&filter[academy_id]=${academy_id}&page[size]=${size}&page[number]=${number}&include=join_activities,subscriptions,subscriptions.subscription`;
        if (name) {
            url += `&filter[name][like]=%${name}%`;
        }
        if (filterDate) {
            url += `&filter[start_date][lte]=${format(addDays(filterDate, 1), 'YYYY-MM-DD')}&filter[end_date][gte]=${format(filterDate, 'YYYY-MM-DD')}`;
        }
        console.log(url);
        return this.http.get(
            encodeURI(ConfigService.STATIC_SERVER + url),
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getAllClassesByAcademy(academy_id): Observable<any> {
        const url = `/activities/?filter[type]=class&filter[academy_id]=${academy_id}`;
        return this.http.get(
            encodeURI(ConfigService.STATIC_SERVER + url),
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getSubscriptionsByAcademy(academy_id): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/subscriptions?filter[academy_id]=${academy_id}`,
            this.options,
            )
            .map(res => {
                return res.json().data.map(subscription => {
                    return {
                        id: subscription.id,
                        itemName: subscription.name,
                    };
                });
            })
            .catch(this.handleError);
    }

    public getTeachersByAcademy(academy_id): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/teachers?filter[academy_id]=${academy_id}&filterOr[academy_id]=0`,
            this.options,
            )
            .map(res => {
                return res.json().data.map(teacher => {
                    return {
                        id: teacher.id,
                        itemName: `${teacher.first_name} ${teacher.last_name}`,
                    };
                });
            })
            .catch(this.handleError);
    }

    public getTechniquesByAcademy(academy_id): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/techniques?filter[academy_id]=${academy_id}&filterOr[academy_id]=0`,
            this.options,
            )
            .map(res => {
                return res.json().data.map(techniques => {
                    return {
                        id: techniques.id,
                        itemName: techniques.name,
                    };
                });
            })
            .catch(this.handleError);
    }

    public getComments(comment_id): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/activity_comments?filter[activity_id]=${comment_id}&include=user`,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public addComment(comment): Observable<any> {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/activity_comments',
            comment,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public removeComment(id): Observable<any> {
        return this.http.delete(
            ConfigService.STATIC_SERVER + `/activity_comments/${id}`,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUserMe(): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + '/users/me',
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getActivityById(a_id): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/activities/${a_id}?include=join_activities.user.followers,techniques,subscriptions,teachers,academy.managers,join_activities.user.academy,join_activities.subscription`,
            this.options,
            )
            .map(res => {
                let data = {
                    ...res.json(),
                    isManager: false,
                };
                if (res.json().academy && res.json().academy.managers) {
                    if (_.findIndex(res.json().academy.managers, {user_id: this.user.id}) >= 0) {
                        data.isManager = true;
                    }
                }
                return data;
            })
            .catch(this.handleError);
    }

    public removeActivity(id): Observable<any> {
        return this.http.delete(
            ConfigService.STATIC_SERVER + '/activities/' + id,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public createActivity(obj): Observable<any> {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/activities',
            JSON.stringify(obj),
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public updateActivity(obj, id): Observable<any> {
        return this.http.put(
            ConfigService.STATIC_SERVER + `/activities/${id}`,
            JSON.stringify(obj),
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getAcademies(): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + '/academies?sort=id&filter[status_public]=1',
            this.options,
            )
            .map(res => {
                const academies = [
                    {
                        id: '0',
                        text: 'Academy',
                    },
                ];
                res.json().data.forEach(function (el, i, arr) {
                    academies.push({id: el.id, text: el.name});
                });
                return academies;
            })
            .catch(this.handleError);
    }

    public getActivities(): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + '/activities?include=academy',
            this.options,
            )
            .map(res => res.json().data)
            .catch(this.handleError);
    }

    public getActivitiesForCalendar(academy): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/activities?filter[academy_id]=${academy}&include=subscriptions.subscription,join_activities`,
            this.options,
            )
            .map(res => res.json().data)
            .catch(this.handleError);
    }

    public getEventsFilter(miles, page: number = 1): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/activities?filter[type][in]=seminar,mat_event,others&page[number]=${page}&page[size]=5&distantion=${miles}&sort=start_date&filter[start_date][gte]=${format(new Date(), 'YYYY-MM-DD')}`,
            this.options,
            )
            .map(res => {
                return res.json().data.map(el => {
                    el.date = format(el.start_date, 'MM/DD/YYYY');
                    el.time = format(el.start_date, 'Hms');
                    return el;
                });
            })
            .catch(this.handleError);
    }

    public getEventsByAcademy(academy, page: number = 1): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/activities?filter[academy_id]=${academy}&page[number]=${page}&page[size]=5&sort=start_date&filter[start_date][gte]=${format(new Date(), 'YYYY-MM-DD')}&include=subscriptions.subscription`,
            this.options,
            )
            .map(res => {
                return res.json().data.map(el => {
                    el.date = format(el.start_date, 'MM/DD/YYYY');
                    el.time = format(el.start_date, 'Hms');
                    return el;
                });
            })
            .catch(this.handleError);
    }

    public getUpcomingFilter(miles, page: number = 1): Observable<any> {
        return this.http.get(
            ConfigService.STATIC_SERVER + `/activities?include=subscriptions.subscription&join_activity=true&page[number]=${page}&page[size]=5&distantion=${miles}&sort=-start_date&filter[start_date][gte]=${format(new Date(), 'YYYY-MM-DD')}&filter[type][not]=class`,
            this.options,
            )
            .map(res => {
                return res.json().data.map(el => {
                    el.date = format(el.start_date, 'MM/DD/YYYY');
                    el.time = format(el.start_date, 'Hms');
                    return el;
                });
            })
            .catch(this.handleError);
    }

    public checkInJoinedUser(userId, activityId) {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/checkins',
            {user_id: userId, activity_id: activityId},
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public uncheckJoinedUser(checkInId) {
        return this.http.delete(
            ConfigService.STATIC_SERVER + `/checkins/${checkInId}`,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getStudentsForCheckIn(data) {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/activities/student_checkins?include=academies',
            data,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public checkInForManager(data) {
        return this.http.post(
            ConfigService.STATIC_SERVER + '/checkins/manager',
            data,
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }


    private handleError(error: any) {
        return Observable.throw(error.message || JSON.parse(error._body).errors[0] || error);
    }
}
