import {
    Injectable,
} from '@angular/core';
import {
    Socket,
} from 'ngx-socket-io';
import {DataService} from './data.service';
import {SocketService} from './socket.service';

@Injectable()
export class AcademyMessagesService {
    private connect = false;

    constructor(private dataService: DataService,
                private socket: Socket,
                private socketService: SocketService
    ){
        this.connectSocket();
    }

    private connectSocket() {
        if (!this.connect) {
            this.socket.connect();
            this.connect = !this.connect;
        }

        this.socket.on('disconnect', () => {
            console.log('Client disconnect ', new Date().getTime());
        });

        this.socket.on('error', (error: string) => {
            console.log(`ERROR: ${error}`);
        });
    }

    setLogin(id) {
        this.socket.emit('login', {id: id}, res => {
            console.log('login: ', res);
        });
    }

    onSocketConected() {
        return this.socket
            .fromEvent('connect')
            .map(data => new Date().getTime());
    }

    public getUnreadMessages(academyId){
        return this.dataService.postResponse('/conversations/academy/unread_messages', { academy_id: academyId })
    }

    public connectToUnreadMessages(userId, academyId){
        this.socket.emit('post_unread_conversation_academy', {user_id: userId, academy_id: academyId});
    }

    public onUnreadMessages(){
        return this.socket
            .fromEvent('unread_conversation_academy')
            .map(data => data)
    }

    public getUnread(userId, academyId){
        this.socketService.emit('post_unread_conversation_academy', {user_id: userId, academy_id: academyId})
    }

    public onUnread(){
        return this.socketService.onAcademyMessage();
    }
}
