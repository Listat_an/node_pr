import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Observable} from 'rxjs/Observable';
import {ConfigService} from './service.config';

@Injectable()
export class AcademyBeltService {
    private options;
    private user: any;

    constructor(private http: Http, public jwtHelper: JwtHelperService) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    public getBeltSettings(academyId): Observable<any> {
        let url = `/belt_settings?filter[academy_id]=${academyId}&page[size]=500&include=belt`;
        return this.http.get(
            ConfigService.STATIC_SERVER + url,
            this.options,
            )
            .map(res => res.json().data)
            .catch(this.handleError);
    }

    public editBeltSettings(belt, count): Observable<any> {
        let url = `/belt_settings/${belt.id}`;
        return this.http.put(
            ConfigService.STATIC_SERVER + url,
            {
                academy_id: belt.academy_id,
                belt_id: belt.belt_id,
                time_stripe_count: count,
                class_stripe_count: count,
            },
            this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Observable.throw(error.message || error);
    }
}
