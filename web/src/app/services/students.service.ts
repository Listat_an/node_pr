import {
    Injectable,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from './service.config';
import {
    JwtHelperService
} from '@auth0/angular-jwt';
import {
    format,
    addDays
} from 'date-fns';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class StudentsService {
    private options;
    private activate: Boolean;
    private user: any;

    constructor(private http: Http, public jwtHelper: JwtHelperService) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    getMe(): any {
        return this.user;
    }

    public setAcademyStatus(st_id, obj): Observable < any > {
        return this.http.put(
                ConfigService.STATIC_SERVER + `/users/${st_id}`,
                obj,
                this.options,
            )
            .map(res => res.json().data)
            .catch(this.handleError);
    }

    public getStudents(academyId, size, number, status ? , name ? ): Observable < any > {
        let sqlStatus = '';
        let sqlName = '';
        if (status) {
            sqlStatus = `&filter[academy_status]=${status}`;
        }
        if (name) {
            sqlName = `&filter[first_name][like]=%${name}%`;
        }
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/users?include=belt_user.belt&filter[academy_id]=${academyId}${sqlStatus}${sqlName}&page[size]=${size}&page[number]=${number}&filter[is_student]=1`),
                this.options,
            )
            .map(res => {
                return {
                    limit: res.json().limit,
                    skip: res.json().skip,
                    total: res.json().total,
                    data: res.json().data.map((el) => {
                        return {
                            ...el,
                            belt_img: ConfigService.URL_SERVER + el.belt_user.belt.sourse,
                        };
                    }),
                };
            })
            .catch(this.handleError);
    }

    public getStudentById(studentId) {
        return this.http.get(
                ConfigService.STATIC_SERVER + `/users/${studentId}?include=belt_user.belt`,
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public addStudentBelt(studentId, beltId) {
        const body = {
            user_id: studentId,
            belt_id: beltId
        };
        return this.http.post(
                ConfigService.STATIC_SERVER + `/users/belt`,
                body,
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getStudentSubscriptions(studentId) {
        return this.http.get(ConfigService.STATIC_SERVER + `/user_subscriptions?filter[user_id]=${studentId}&include=subscription&filter[status][not]=0`,
                this.options)
            .map(res => res.json().data)
            .catch(this.handleError);
    }

    public getUserActivities(studentId, limit ? ) {
        return this.http.get(ConfigService.STATIC_SERVER + `/user_timelines?filter[user_id]=${studentId}&page[size]=${limit || 5000}`,
                this.options)
            .map(res => res.json().data)
            .catch(this.handleError);
    }

    public getStudentActivities(studentId, academyId, limit ? ) {
        return this.http.get(ConfigService.STATIC_SERVER + `/user_timelines?filter[user_id]=${studentId}&filter[academy_id]=${academyId}&page[size]=${limit || 5000}&include=transaction`,
                this.options)
            .map(res => {
                return res.json().data.map(el => {
                    let data = {
                        ...el,
                        refundLoader: false,
                    };
                    return data;
                });
            })
            .catch(this.handleError);
    }

    public getUserAttendance(userId, fromDate, toDate, academyId ? ) {
        let url = `/activities/counts?filter[user_id]=${userId}`;
        if (fromDate && toDate) {
            url += `&filter[created_at][gte]=${format(fromDate, 'YYYY-MM-DD')}&filter[created_at][lte]=${format(addDays(toDate, 1), 'YYYY-MM-DD')}`
        }
        if (academyId) {
            url += `&filter[academy_id]=${academyId}`
        }
        return this.http.get(
                ConfigService.STATIC_SERVER + url,
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUserCards(userId): Observable < any > {
        return this.http.post(
                ConfigService.STATIC_SERVER + '/users/cards', {
                    user_id: userId
                },
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUserData(): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + '/users/me',
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getSybscriptionsByAcademy(id): Observable < any > {
        return this.http.get(
                ConfigService.STATIC_SERVER + `/subscriptions?filter[academy_id]=${id}`,
                this.options,
            )
            .map(res => res.json().data)
            .catch(this.handleError);
    }

    public signUp(obj): Observable < any > {
        return this.http.post(
                ConfigService.STATIC_SERVER + `/user_subscriptions`,
                obj,
                this.options,
            )
            .map(res => res.json().data)
            .catch(this.handleError);
    }

    public getStudentStripeLevel(userId, academyId) {
        let body = {
            'user_id': userId,
            'academy_id': academyId
        };
        return this.http.post(
                ConfigService.STATIC_SERVER + `/users/next_level`,
                body,
                this.options
            )
            .map(res => res.json())
            .catch(this.handleError);
    }


    private handleError(error: any) {
        return Observable.throw(error.message || error);
    }
}
