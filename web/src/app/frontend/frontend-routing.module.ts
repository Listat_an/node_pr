import {
    RouterModule,
    Routes,
    CanActivate,
} from '@angular/router';
import {
    NgModule,
} from '@angular/core';

import {
    AuthGuardService as AuthGuard,
} from '../services/auth/auth-guard.service';

import {
    AcademyGuardService as AcademyGuard,
} from '../services/auth/academy-guard.service';
import {
    AcademyProService as AcademyPro,
} from '../services/auth/academy-pro.service';
import { FrontendComponent } from './frontend.component';

const routes: Routes = [
    {
        path: 'auth',
        loadChildren: 'app/frontend/auth/auth.module#AuthModule',
    },

    {
        path: '',
        component: FrontendComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                loadChildren: 'app/frontend/pages/feed-page/feed-page.module#FeedPageModule',
            },
            {
                path: 'profile/:id',
                loadChildren: 'app/frontend/pages/profile-page/profile-page.module#ProfilePageModule',
            },
            {
                path: 'profile/:id/followers',
                loadChildren: 'app/frontend/pages/followers/followers-page.module#FollowersPageModule',
            },
            {
                path: 'profile/:id/followings',
                loadChildren: 'app/frontend/pages/followings/followings-page.module#FollowingsPageModule',
            },
            {
                path: 'academies',
                loadChildren: 'app/frontend/pages/academies/academy-page.module#AcademyPageModule',
            },
            {
                path: 'academiy-datail/:id',
                loadChildren: 'app/frontend/pages/academy-detail/academy-detail.module#AcademyDetailPageModule',
            }, {
                path: 'academiy-datail/:id/messages',
                loadChildren: 'app/frontend/pages/academy-messages/academy-messages.module#AcademyMessagesModule',
            },
            {
                path: 'academiy-datail/:id/settings',
                loadChildren: 'app/frontend/pages/settings/settings-page.module#SettingsPageModule',
                canActivate: [AcademyGuard],
            },
            {
                path: 'academiy-datail/:id/create-subscriptions',
                loadChildren: 'app/frontend/pages/create-subscriptions/create-subscriptions.module#CreateSubscriptionsPageModule',
                canActivate: [AcademyPro, AcademyGuard],
            },
            {
                path: 'academiy-datail/:id/teachers-techniques',
                loadChildren: 'app/frontend/pages/teachers-techniques/teachers-techniques.module#TeachersTechniquesPageModule',
                canActivate: [AcademyPro, AcademyGuard],
            },
            {
                path: 'academiy-datail/:id/classes',
                loadChildren: 'app/frontend/pages/classes/classes-page.module#ClassesPageModule',
                canActivate: [AcademyPro, AcademyGuard],
            },
            {
                path: 'academiy-datail/:id/schedule',
                loadChildren: 'app/frontend/pages/schedule/schedule-page.module#SchedulePageModule',
                canActivate: [AcademyPro, AcademyGuard],
            },
            {
                path: 'academiy-datail/:id/activities',
                loadChildren: 'app/frontend/pages/activities/activities-page.module#ActivitiesPageModule',
                canActivate: [AcademyGuard],
            }, {
                path: 'academiy-datail/:id/belt-settings',
                loadChildren: 'app/frontend/pages/belt-settings/belt-settings.module#BeltSettingsModule',
                canActivate: [AcademyGuard],
            },
            {
                path: 'academiy-datail/:id/students',
                loadChildren: 'app/frontend/pages/students/students.module#StudentsModule',
                canActivate: [AcademyGuard],
            },
            {
                path: 'academiy-datail/:id/dashboard',
                loadChildren: 'app/frontend/pages/academy-dashboard/academy-dashboard.module#AcademyDashboardModule',
                canActivate: [AcademyPro, AcademyGuard],
            },
            {
                path: 'academiy-datail/:id/contacts',
                loadChildren: 'app/frontend/pages/contacts/contacts-page.module#ContactsPageModule',
            },
            {
                path: 'academiy-datail/:id/shop',
                loadChildren: 'app/frontend/pages/academy-shop/academy-shop.module#AcademyShopModule',
                canActivate: [AcademyPro],
            },
            {
                path: 'academiy-datail/:id/product/:productId',
                loadChildren: 'app/frontend/pages/academy-product-page/academy-product-page.module#AcademyProductPageModule',
            },
            {
                path: 'academiy-datail/:id/upgrade',
                loadChildren: 'app/frontend/pages/upgrade/upgrade-page.module#UpgradePageModule',
                canActivate: [AcademyGuard],
            },
            {
                path: 'activity/:id',
                loadChildren: 'app/frontend/pages/activity-view/activity-view.module#ActivityViewPageModule',
            },
            {
                path: 'message',
                loadChildren: 'app/frontend/pages/message/message-page.module#MessagePageModule',
            },
            {
                path: 'notification',
                loadChildren: 'app/frontend/pages/notification/notification-page.module#NotificationPageModule',
            },
            {
                path: 'my-technique-roadmap',
                loadChildren: 'app/frontend/pages/roadmap/roadmap-page.module#RoadmapPageModule',
            },
            {
                path: 'events-near-me',
                loadChildren: 'app/frontend/pages/events/events-page.module#EventsPageModule',
            },
            {
                path: 'check-in',
                loadChildren: 'app/frontend/pages/checkin/checkin-page.module#CheckinPageModule',
            },
            {
                path: 'subscriptions',
                loadChildren: 'app/frontend/pages/subscriptions/subscriptions-page.module#SubscriptionsPageModule',
            },
            {
                path: 'search',
                loadChildren: 'app/frontend/pages/search/search-page.module#SearchPageModule',
            },
            {
                path: 'dashboard',
                loadChildren: 'app/frontend/pages/user-dashboard/user-dashboard.module#UserDashboardModule',
            },
            {
                path: 'shop',
                loadChildren: 'app/frontend/pages/shop/shop.module#ShopModule',
            },
            {
                path: 'redirect',
                loadChildren: 'app/frontend/helpers/redirect/redirect.module#RedirectModule',
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class FrontendRoutingModule {
}
