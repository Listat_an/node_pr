import {
    Component,
    OnDestroy,
} from '@angular/core';
import {
    SocketService,
} from '../services/socket.service';
import {
    Router,
    NavigationEnd,
} from '@angular/router';

import { Subject } from 'rxjs/Subject';


@Component({
    selector: 'opn-frontend-root',
    template: `<opn-left-sidebar></opn-left-sidebar>
            <router-outlet></router-outlet>
            <opn-right-sidebar [isManager]="isManager"></opn-right-sidebar>`,
})
export class FrontendComponent implements OnDestroy {
    private ngUnsubscribe: Subject < void > = new Subject < void > ();
    private isManager: Boolean = false;

    constructor(
        private router: Router,
    ) {
        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
                this.router.navigated = false;
                if (this.router.url.indexOf('academiy-datail') >= 0) {
                  this.isManager = true;
                } else {
                  this.isManager = false;
                }
            }
        });
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
