import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ActiveComponent } from './login/active.component';
import { RegistrationComponent } from './registration/registration.component';
import { SuccessComponent } from './success/success.component';
import { ForgotComponent } from './forgot/forgot.component';
import { ResetComponent } from './reset/reset.component';
import { AcademyComponent } from './academy/academy.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'reset/:kode',
        component: ResetComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      }, {
        path: 'welcome',
        component: WelcomeComponent,
      }, {
        path: 'login/:kode',
        component: ActiveComponent,
      }, {
        path: 'registration',
        component: RegistrationComponent,
      }, {
        path: 'success',
        component: SuccessComponent,
      }, {
        path: 'forgot',
        component: ForgotComponent,
      }, {
        path: 'academy',
        component: AcademyComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
}
