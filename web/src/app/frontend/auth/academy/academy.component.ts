import {
    Component,
    OnInit,
    OnDestroy,
} from '@angular/core';
import {
    Select2TemplateFunction,
    Select2OptionData,
} from 'ng2-select2';
import {
    DatepickerOptions,
} from '../../../../components/ng-datepicker';
import {
    ClolorsBelt,
    Belts,
} from '../../../../components/belts';
import {
    TooltipModule,
} from 'ng2-tooltip';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    AbstractControl,
} from '@angular/forms';
import {
    Router,
} from '@angular/router';
import {
    Title,
} from '@angular/platform-browser';
import {
    ToastrService,
} from 'ngx-toastr';

import {
    AuthService,
} from '../../../services/auth/auth.service';
import {
    SocketService,
} from '../../../services/socket.service';

import {
    indexOf,
} from 'lodash';

export class PasswordValidation {
    static MatchPassword(AC: AbstractControl) {
        const password = AC.get('password').value; // to get value in input tag
        const confirmPassword = AC.get('passwordConfirm').value; // to get value in input tag
        if (password !== confirmPassword) {
            AC.get('passwordConfirm').setErrors({
                MatchPassword: true,
            });
        } else {
            return null;
        }
    }
}

@Component({
    selector: 'opn-academy',
    templateUrl: './academy.component.html',
    styleUrls: ['./academy.component.d.scss'],
})

export class AcademyComponent implements OnInit, OnDestroy {

    public title = 'Registration';
    public belts: Array < Select2OptionData > ;
    public academies: Array < Select2OptionData > ;
    public weights: Array < Select2OptionData > ;
    public optionsBelt: Select2Options;
    public optionsAcademy: Select2Options;
    public optionsWeight: Select2Options;
    private selected = {};

    public birthDay: Date;
    public dateOptions: DatepickerOptions;
    public form1: FormGroup;
    public form2: FormGroup;
    public form3: FormGroup;
    private belt: any = '0';
    private academy: any;
    private academyModel: any;
    private weight: any;
    private ageType: String = 'junior';
    private location: any;
    private latitude: any;
    private longitude: any;
    private address: any;
    private addressError: any;
    private shortAddress: any;

    public step: any = 1;
    private stepAction: String = 'login';

    public slides: Array < any > = [];

    colorsBelt: Array < ClolorsBelt > ;
    arrBelts: Array < Belts > ;

    private gaAutocompleteStings: any = {
        showSearchButton: false,
        showCurrentLocation: false,
        currentLocIconUrl: 'https://cdn4.iconfinder.com/data/icons/proglyphs-traveling/512/Current_Location-512.png',
        locationIconUrl: 'http://www.myiconfinder.com/uploads/iconsets/369f997cef4f440c5394ed2ae6f8eecd.png',
        recentStorageName: 'componentData4',
        noOfRecentSearchSave: 8,
        inputPlaceholderText: 'Address',
        inputString: '',
    };

    private loadIn: Boolean = false;
    private loadUp: Boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private titleService: Title,
        private router: Router,
        private toastr: ToastrService,
        private socketService: SocketService,
    ) {
        this.titleService.setTitle(this.title);
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        localStorage.removeItem('active_token');
    }

    private addNewSlide() {
        this.slides.push({
            text: 'Lorem Ipsum is simply dummy text of the printing and typesettin',
            title: 'You are what you opn mat 1',
        }, {
            text: 'Lorem Ipsum is simply dummy text of the printing and typesettin',
            title: 'You are what you opn mat 2',
        }, {
            text: 'Lorem Ipsum is simply dummy text of the printing and typesettin',
            title: 'You are what you opn mat 3',
        });
    }

    ngOnInit() {
        this.addNewSlide();
        this.authService.getBeltColors(`all,${this.ageType}`).subscribe(res => {
            this.colorsBelt = res;
            this.authService.getBeltStipiesBycolorId(res[0].id).subscribe(belts => {
                this.arrBelts = belts;
                this.belt = belts[0].id;
            });
        });
        this.academies = [];
        this.titleService.setTitle(this.title);
        this.form1 = this.formBuilder.group({
            name: [null, [Validators.required]],
            // location: [null, Validators.required],
            // email: [null, [Validators.required, Validators.email]],
            phone: [null, Validators.required],
        });

        this.form2 = this.formBuilder.group({
            firstName: [null, [Validators.required]],
            lastName: [null, Validators.required],
            email: [null, [Validators.required, Validators.email]],
            weight: [null, Validators.required],
            password: [null, [Validators.required, Validators.pattern('((?=.*[0-9])(?=.*[a-zA-Z]).{8,50})')]],
            passwordConfirm: [null, [Validators.required, Validators.pattern('((?=.*[0-9])(?=.*[a-zA-Z]).{8,50})')]],
        }, {
            validator: PasswordValidation.MatchPassword,
        });

        this.form3 = this.formBuilder.group({
            email: [null, [Validators.required, Validators.email]],
            password: [null, [Validators.required, Validators.pattern('((?=.*[0-9])(?=.*[a-zA-Z]).{8,50})')]],
        });

        this.dateOptions = {
            minYear: 1970,
            maxYear: 2080,
            displayFormat: 'M[/] D[/] YYYY',
            barTitleFormat: 'MMMM YYYY',
            firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
        };

        this.birthDay = new Date();

        this.belts = [{
                id: '0',
                text: 'Template 1',
                additional: {
                    image: 'assets/images/belt.png',
                    winner: '4',
                },
            },
            {
                id: '1',
                text: 'Template 2',
                additional: {
                    image: 'assets/images/belt.png',
                    winner: '3',
                },
            },
        ];
        this.weights = [{
            id: '0',
            text: 'Weight',
        }];

        for (let i = 1; i <= 250; i++) {
            this.weights.push({
                id: `${i}`,
                text: `${i}`,
            });
        }

        this.optionsBelt = {
            templateResult: this.templateResult,
            templateSelection: this.templateSelection,
            minimumResultsForSearch: -1,
        };

        this.optionsAcademy = {
            minimumResultsForSearch: 1,
            dropdownCssClass: 'academy-dropdown',
            disabled: true,
        };

        this.optionsWeight = {
            minimumResultsForSearch: 1,
            dropdownCssClass: 'academy-dropdown',
            placeholder: {
                id: '0', // the value of the option
                text: 'Weight',
            },
        };
    }

    onChangeAge(e) {
        this.ageType = e.value;
        this.authService.getBeltColors(`all,${e.value}`).subscribe(res => {
            this.colorsBelt = res;
            this.authService.getBeltStipiesBycolorId(res[0].id).subscribe(belts => {
                this.arrBelts = belts;
                this.belt = belts[0].id;
            });
        });
    }

    autoCompleteCallback1(event) {
        if (event.data) {
            this.location = event.data.formatted_address;
            const location = event.data.geometry.location;
            this.latitude = location.lat;
            this.longitude = location.lng;
        }
    }

    autoCompleteCallback(event) {
        if (event.data) {
            this.address = event.data.formatted_address;
            this.latitude = event.data.geometry.location.lat;
            this.longitude = event.data.geometry.location.lng;
            this.setShortAddres(event.data);
            this.addressError = false;
        } else {
            this.addressError = true;
        }
    }

    setShortAddres(data) {
        console.log(data.address_components);
        data.address_components.forEach(el => {
            if (indexOf(['locality', 'administrative_area_level_1', 'postal_code'], el.types[0]) >= 0) {
                this.shortAddress += this.formatAddress(this.shortAddress, el.short_name);
            }
        });
    }

    private formatAddress(short_address, short_name): String {
        return (short_address == '') ? short_name : `, ${short_name}`;
    }

    // function for result template
    public templateResult: Select2TemplateFunction = (state: Select2OptionData): JQuery | string => {
        if (!state.id) {
            return state.text;
        }
        let image = '<span class="image"></span>';
        if (state.additional.image) {
            image = '<span class="image select-belt"><img src="' + state.additional.image + '"/></span>';
        }
        return jQuery(image);
    }

    // function for selection template
    public templateSelection: Select2TemplateFunction = (state: Select2OptionData): JQuery | string => {
        let image = '<span class="image"></span>';
        if (!state.id) {
            return state.text;
        }
        if (state.additional.image) {
            image = '<span class="image select-belt"><img src="' + state.additional.image + '"/></span>';
        }
        return jQuery(image);
    }

    public changeBelt = function ($e) {
        this.belt = $e.value;
    };

    public changeAcademy = function ($e) {
        this.academy = $e.value;
    };

    public changeWeight = function ($e) {
        this.weight = $e.value;
    };

    public changeBeltColor = function (id) {
        this.belt = id;
    };
    isFieldValid(field: string) {
        return !this.form3.get(field).valid && this.form3.get(field).touched;
    }

    displayFieldCss(field: string) {
        return {
            'has-error': this.isFieldValid(field),
        };
    }

    isFieldValid1(field: string) {
        return !this.form1.get(field).valid && this.form1.get(field).touched;
    }

    displayFieldCss1(field: string) {
        return {
            'has-error': this.isFieldValid1(field),
        };
    }

    isFieldValid2(field: string) {
        return !this.form2.get(field).valid && this.form2.get(field).touched;
    }

    displayFieldCss2(field: string) {
        return {
            'has-error': this.isFieldValid2(field),
        };
    }

    onSubmit1() {
        if (this.form1.valid) {
            this.academyModel = {
                // email: this.form1.get('email').value,
                name: this.form1.get('name').value,
                location: this.location,
                latitude: this.latitude,
                longitude: this.longitude,
                phone: this.form1.get('phone').value,
            };
            this.academies.push({
                id: '0',
                text: this.form1.get('name').value,
            });
            this.step = 2;
        } else {
            this.validateAllFormFields(this.form1);
        }
    }

    onSubmit2() {
        this.addressError = !(this.address && this.address.trim() !== '');
        if (this.form2.valid && !this.addressError) {
            this.loadUp = true;
            this.authService.signup({
                user: {
                    email: this.form2.get('email').value,
                    password: this.form2.get('password').value,
                    first_name: this.form2.get('firstName').value,
                    last_name: this.form2.get('lastName').value,
                    belt_id: this.belt,
                    academy_id: 0,
                    weight: this.form2.get('weight').value,
                    date: this.birthDay,
                    address: this.address,
                    longitude: this.longitude,
                    latitude: this.latitude,
                    short_address: this.shortAddress,
                },
                academy: this.academyModel,
            })
            .finally(() => {
                this.loadUp = false;
            })
            .subscribe(res => {
                    if (res.access_token) {
                        if (this.authService.setToken(res.access_token, res.user.active_kode)) {
                            localStorage.setItem('email', res.user.email);
                            localStorage.setItem('user', JSON.stringify(res.user));
                            this.router.navigate(['/auth/success']);
                        }
                    }
                },
                err => {
                    const errore = JSON.parse(err._body).errors;
                    const self = this;
                    errore.forEach(function (el, i, arr) {
                        if (el.status === '400') {
                            self.toastr.info(el.description, 'Warning!');
                        }
                    });
                });
        } else {
            this.validateAllFormFields(this.form2);
        }
    }

    onSubmit3() {
        if (this.form3.valid) {
            this.loadIn = true;
            this.authService.loginWithAcademy({
                user: {
                    email: this.form3.get('email').value,
                    password: this.form3.get('password').value,
                },
                academy: this.academyModel,
            })
            .finally(() => {
                this.loadIn = false;
            })
            .subscribe(
                res => {
                    if (res.access_token) {
                        if (this.authService.setToken(res.access_token, res.user.active_kode)) {
                            localStorage.setItem('user', JSON.stringify(res.user));
                            this.socketService.connect(res.user.id);
                            const intervalId = setInterval(() => {
                                if (localStorage.getItem('token') != null) {
                                    this.router.navigate(['/']);
                                    clearTimeout(intervalId);
                                }
                            }, 500);
                        }
                    }
                },
                err => {
                    const errore = JSON.parse(err._body).errors;
                    const self = this;
                    errore.forEach(function (el, i, arr) {
                        if (el.status == '400') {
                            self.toastr.info(el.description, 'Warning!');
                        }
                    });
                });
        } else {
            this.validateAllFormFields(this.form3);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({
                    onlySelf: true,
                });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    onChangeColor(e) {
        this.authService.getBeltStipiesBycolorId(e).subscribe(res => {
            console.log('getBeltStipiesBycolorId: ', res);
            this.arrBelts = res;
        });
    }

    toAcademyLogin() {
        this.step = 3;
        this.stepAction = 'login';
    }
    toAcademyRegistration() {
        this.step = 3;
        this.stepAction = 'registration';
    }

    ngOnDestroy() {}
}
