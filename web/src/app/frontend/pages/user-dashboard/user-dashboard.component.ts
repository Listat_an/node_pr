import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {StudentsService} from '../../../services/students.service';
import {ConfigService} from '../../../services/service.config';
import {UsersService} from '../../../services/users.service';
import {cancelSubscription} from '../../../providers/cancelSubscription';
import {ActivatedRoute} from '@angular/router';


@Component({
    selector: 'opn-user-dashboard',
    templateUrl: './user-dashboard.html',
    styleUrls: ['./user-dashboard.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        StudentsService,
        UsersService,
    ],
})
export class UserDashboardComponent implements OnInit, OnDestroy {
    public subscrips: Subscription[] = [];
    public studentSubscriptions = [];
    public studentActivities = [];
    public baseUrl = ConfigService.URL_SERVER;
    public student: any;
    private isManager: Boolean = false;
    viewMore = false;
    initialLimit = 7;

    public userId;

    constructor(
        private usersService: UsersService,
        private studentsService: StudentsService,
        private activeRoute: ActivatedRoute,
    ) {
        this.activeRoute.params.subscribe(param => {
            console.log('param ', param);
            if (param.user_id) {
                this.userId = param.user_id;
            }
        });
    }

    ngOnInit() {
        this.getStudent();
    }

    getStudent() {
        if (!this.userId) {
            this.subscrips.push(
                this.usersService.getUserMe().subscribe(res => {
                    this.student = res;
                    this.getUserSubscriptions(this.student.id);
                    this.getUserActivities(this.student.id, this.initialLimit);
                }),
            );
        }else if (this.userId) {
            this.subscrips.push(
                this.usersService.getUserById(this.userId, '&include=academy,belt_user,belt').subscribe(res => {
                    this.student = res;
                    this.getUserSubscriptions(this.student.id);
                    this.getUserActivities(this.student.id, this.initialLimit);
                }),
            );
        }
    }

    private getUserSubscriptions(studentId) {
        this.subscrips.push(
            this.studentsService.getStudentSubscriptions(studentId).subscribe(res => {
                this.studentSubscriptions = res;
            }),
        );
    }

    private getUserActivities(studentId, limit?) {
        this.subscrips.push(
            this.studentsService.getUserActivities(studentId, limit).subscribe(res => {
                this.studentActivities = res;
            })
        )
    }

    onLoadMore() {
        this.viewMore = true;
        this.getUserActivities(this.student.id);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }

    onCheckManager(status) {
        this.isManager = status;
    }
}
