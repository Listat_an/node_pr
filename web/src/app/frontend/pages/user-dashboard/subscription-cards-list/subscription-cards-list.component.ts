import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ChangeCardComponent} from '../change-card/change-card.component';
import {PaymentService} from '../../../../services/payment.service';
import {find} from 'lodash';

@Component({
    selector: 'opn-subscription-cards-list',
    templateUrl: './subscription-cards-list.html',
    styleUrls: ['./subscription-cards-list.scss'],
    providers: [PaymentService]
})
export class SubscriptionCardsListComponent implements OnInit {
    @Input() userId: number;
    cards = [];
    defaultCard;

    @ViewChild(ChangeCardComponent)
    public paymentStripe: ChangeCardComponent;

    constructor(private paymentService: PaymentService) {
    }

    ngOnInit() {
        this.paymentService.getCards().subscribe(res => {
            this.cards = res;
            this.defaultCard = find(this.cards, (card) => card.defaul)
        });
    }

    openCardSelection() {
        this.paymentStripe.showPayment()
    }

    onOutToken(res) {
        this.paymentService.changeDefaultUserCard(this.userId, res.id).subscribe(res => {
            this.defaultCard = res.card[0];
        })
    }

}

