import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubscriptionCardsListComponent} from './subscription-cards-list.component';
import {SubscriptionListModule} from '../../parts/subscription-list/subscription-list.module';
import {ChangeCardComponent} from '../change-card/change-card.component';

@NgModule({
    imports: [
        CommonModule,
        SubscriptionListModule
    ],
    declarations: [
        SubscriptionCardsListComponent,
        ChangeCardComponent,
    ],
    exports: [
        SubscriptionCardsListComponent
    ]
})
export class SubscriptionCardsListModule {

}
