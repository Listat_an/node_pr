import {NgModule} from '@angular/core';
import {UserDashboardComponent} from './user-dashboard.component';
import {RouterModule} from '@angular/router';
import {PagesModule} from '../pages.module';
import {CommonModule} from '@angular/common';
import {AcademyComponentsModule} from '../components/academy/academy.module';
import {FormsModule} from '@angular/forms';
import {AttendedModule} from '../components/attended/attended.module';
import {LevelStripeModule} from '../components/level-stripe/level-stripe.module';
import {SubscriptionCardsListModule} from './subscription-cards-list/subscription-cards-list.module';
import {UiBackBtnModule} from './../../../../components/ui/ui-back-btn/ui-back-btn.module'


@NgModule({
    imports: [
        RouterModule.forChild([{path: '', component: UserDashboardComponent}]),
        CommonModule,
        FormsModule,
        PagesModule,
        AcademyComponentsModule,
        LevelStripeModule,
        SubscriptionCardsListModule,
        AttendedModule,
        UiBackBtnModule,
    ],
    declarations: [
        UserDashboardComponent,
    ],
    exports: [
        UserDashboardComponent,
    ],
})
export class UserDashboardModule {

}
