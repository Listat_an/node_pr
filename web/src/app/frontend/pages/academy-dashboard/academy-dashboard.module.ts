import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {AcademyDashboardComponent} from './academy-dashboard.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PromotionDetailComponent} from './dashboard/promotion/promotion.component';
import {AcademyDashboardRoutingModule} from './academy-dashboard.routing.module';
import {PagesModule} from '../pages.module';
import {AcademyComponentsModule} from '../components/academy/academy.module';
import {UiPerPageModule} from '../../../../components/ui/ui-per-page/ui-per-page.module';

import { TotalClasesModule } from './dashboard/components/total-classes/total-classes.module';
import { PromotionViewModule } from './dashboard/components/promotion/promotion.module';
import { MarketingViewModule } from './dashboard/components/marketing/marketing.module';
import { RevenueViewModule } from './dashboard/components/revenue/revenue.module';
import {UiCheckbox2Module} from '../../../../components/ui/ui-checkbox-2/ui-checkbox-2.module';
import { SubscriptionListPopupComponent } from './dashboard/components/subscription-list/subscription-list.component';
import {RevenueDetailComponent} from './dashboard/revenue/revenue.component';
import {NgDatepickerModule} from 'ng2-datepicker';
import {ToDashboardComponent} from './dashboard/components/to-dashboard/to-dashboard.component';
import {MarketingDetailComponent} from './dashboard/marketing/marketing.component';
import { UiBackBtnModule } from './../../../../components/ui/ui-back-btn/ui-back-btn.module'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AcademyDashboardRoutingModule,
        PagesModule,
        AcademyComponentsModule,
        TotalClasesModule,
        PromotionViewModule,
        MarketingViewModule,
        RevenueViewModule,
        UiPerPageModule,
        UiCheckbox2Module,
        NgDatepickerModule,
        UiBackBtnModule,
    ],
    declarations: [
        AcademyDashboardComponent,
        DashboardComponent,
        PromotionDetailComponent,
        SubscriptionListPopupComponent,
        RevenueDetailComponent,
        ToDashboardComponent,
        MarketingDetailComponent,
    ],
    exports: [
        NgDatepickerModule,
    ],
})
export class AcademyDashboardModule {
}
