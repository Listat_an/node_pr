import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { AcademiesService } from '../../../services/academies.service';

@Component({
  selector: 'opn-academy-dashboard',
  templateUrl: './academy-dashboard.component.html',
  styleUrls: ['./academy-dashboard.component.scss', '../@theme/scss/theme.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [AcademiesService],
})
export class AcademyDashboardComponent implements OnInit {
  private academyId: any;
  private isManager: Boolean = false;
  public isAdmin = false;

  constructor(
    private academyService: AcademiesService,
    private activeRoute: ActivatedRoute,
  ) {
    this.activeRoute.params.subscribe(params => {
      console.log(params);
      if (params['id']) {
        this.academyId = params['id'];
      }else if (params['academy_id']) {
        this.academyId = params['academy_id'];
        this.isAdmin = true;
      }

    });
  }

  ngOnInit() {
  }

  onCheckManager(status) {
    this.isManager = status;
  }
}
