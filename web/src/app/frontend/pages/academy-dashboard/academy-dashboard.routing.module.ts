import {
  NgModule,
} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  AcademyDashboardComponent,
} from './academy-dashboard.component';

import {
  DashboardComponent,
} from './dashboard/dashboard.component';

import {
  PromotionDetailComponent,
} from './dashboard/promotion/promotion.component';

import {RevenueDetailComponent} from './dashboard/revenue/revenue.component';
import {MarketingDetailComponent} from './dashboard/marketing/marketing.component';

const StudentsRoutes: Routes = [
  {
    path: '',
    component: AcademyDashboardComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: ':id/promotion-deteil',
        component: PromotionDetailComponent,
      },
      {
        path: ':id/revenue-deteil',
        component: RevenueDetailComponent,
      },
      {
        path: ':id/marketing-deteil',
        component: MarketingDetailComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(StudentsRoutes),
  ],
  exports: [
    RouterModule,
  ],
})

export class AcademyDashboardRoutingModule {}
