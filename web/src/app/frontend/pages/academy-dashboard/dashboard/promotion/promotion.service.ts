import {
    Injectable,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from '../../../../../services/service.config';
import {
    JwtHelperService,
} from '@auth0/angular-jwt';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as _ from 'lodash';

import { format, addDays } from 'date-fns';

@Injectable()
export class PromotionService {
    private options;
    private activate: Boolean;
    private user: any;

    constructor(private http: Http, public jwtHelper: JwtHelperService) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    public getPromotionDeteil(size, page, academyId, filter): Observable < any > {
        let filterStr: string = '';
        if (filter) {
            _.forIn(_.values(filter), (v) => {
                if (v['order'] === 'ASC') {
                    filterStr += (filterStr !== '' ? ',' : '') + v['field'];
                }
                if (v['order'] === 'DESC') {
                    filterStr += (filterStr !== '' ? ',-' : '-') + v['field'];
                }
            });
        }
        if (filterStr !== '') {
            filterStr = '&sort=' + filterStr;
        }
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/user_promotions?include=user,user.subscriptions.subscription&filter[academy_id]=${academyId}&page[size]=${size}&page[number]=${page}${filterStr}`),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    private subscriptionsJoin(subscriptions) {
        if (subscriptions.length > 0) {
            let subscribe = [];
            subscriptions.forEach(el => {
                subscribe.push(el.subscription.name);
            });
            return subscribe.join(', ');
        }else {
            return '-';
        }
    }

    public getData(academyId, promotion): Observable < any > {
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/user_promotions?include=user,user.subscriptions.subscription&filter[academy_id]=${academyId}&page[size]=10000`),
                this.options,
            )
            .map(res => {
                let arrayTmp = res.json();
                arrayTmp.titles = {
                    full_name: 'Student name',
                    student_level: 'Student level',
                    status: 'Required for promo',
                    next_promotion: 'Promotion type',
                    subscription: 'Subscription',
                };
                if (promotion == 'time') {
                    arrayTmp.titles.time_attended = 'Time attended';
                }else {
                    arrayTmp.titles.class_attended = 'Class attended';
                }
                arrayTmp.data = res.json().data.map((el, i) => {
                    let student = {
                        full_name: el.full_name,
                        student_level: el.student_level,
                        status: el.status,
                        next_promotion: el.next_promotion,
                        subscription: this.subscriptionsJoin(el.user.subscriptions),
                    };
                    if (promotion == 'time') {
                        student['time_attended'] = el.time_attended;
                    }else {
                        student['class_attended'] = el.class_attended;
                    }
                    return student;
                });
                return arrayTmp;
            })
            .catch(this.handleError);
    }
    private handleError(error: any) {
        return Observable.throw(error.message || error);
    }
}
