import {
    Component,
    OnInit,
    ViewEncapsulation,
    OnDestroy,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import {
    cancelSubscription,
} from '../../../../../providers/cancelSubscription';
import {
    Subscription,
} from 'rxjs/Subscription';
import {
    ExcelService,
} from '../../excel.service';

import {
    PromotionService,
} from './promotion.service';

import {
    PrintService,
} from '../print.service';
import * as _ from 'lodash';
import {
    AcademiesService,
} from '../../../../../services/academies.service';

@Component({
    selector: 'opn-dashboard-promotion',
    templateUrl: './promotion.component.html',
    styleUrls: ['./promotion.component.scss', '../../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        AcademiesService,
        PromotionService,
        ExcelService,
        PrintService,
    ],
})
export class PromotionDetailComponent implements OnInit, OnDestroy {
    private academyId: Number;
    public subscrips: Subscription[] = [];
    private promotionUsers: Array < any > = [];

    private studentId: Number = null;
    private isAddBelt: Boolean = false;
    listCheckboxes: Array < any > = [];
    row_index: number = -1;
    private per_page: any = 10;
    private page: any = 1;
    private pagintion: any;
    private load: Boolean = true;
    private academyInfo: any;

    private filter = {
        name: {
            order: '',
            field: 'full_name',
        },
        subscription: {
            order: '',
            field: 'subscription_name',
        },
        level: {
            order: '',
            field: 'student_level',
        },
        attended: {
            order: '',
            field: '',
        },
        status: {
            order: '',
            field: 'status',
        },
        next: {
            order: '',
            field: 'next_promotion',
        },
    };

    constructor(
        private academySrv: AcademiesService,
        private promotionSrv: PromotionService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private excelSrv: ExcelService,
        private printSrv: PrintService,
    ) {
        this.pagintion = {
            show: false,
            pages: [],
        };
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.academyId = params['id'];
        });

        this.getpromotionUsers();
        this.getAcademyInfo();
    }

    private getAcademyInfo() {
        this.subscrips.push(
            this.academySrv.getAcademyById(this.academyId).subscribe(academyInfo => {
                this.academyInfo = academyInfo;
                console.info('academyInfo', academyInfo);
            }),
        );
    }

    private getpromotionUsers() {
        this.load = false;
        this.subscrips.push(
            this.promotionSrv.getPromotionDeteil(this.per_page, this.page, this.academyId, this.filter)
            .finally(() => {
                this.load = true;
            })
            .subscribe(deteils => {
                this.formatData(deteils.data);
                this.Paginations(deteils.total);
            }),
        );
    }

    private formatData(data) {
        this.promotionUsers = data;
        this.listCheckboxes = data.map(el => {
            return {
                name: 'checkbox_' + el.id,
                checked: false,
                value: el.user_id,
            };
        });
    }

    onChangeFilter(nameFilter) {
        if (nameFilter !== 'attended') {
            if (this.filter[nameFilter].order === '' || this.filter[nameFilter].order === 'DESC') {
                this.filter[nameFilter].order = 'ASC';
                this.getpromotionUsers();
                return;
            }
            if (this.filter[nameFilter].order === 'ASC') {
                this.filter[nameFilter].order = 'DESC';
                this.getpromotionUsers();
                return;
            }
        }
        if (nameFilter === 'attended') {
            let fName: string;
            switch (this.academyInfo.promotion_method) {
                case 'time': {
                    fName = 'time_attended';
                    break;
                }
                case 'attendance': {
                    fName = 'class_attended';
                    break;
                }
            }
            if (this.filter[nameFilter].order === '' || this.filter[nameFilter].order === 'DESC') {
                this.filter[nameFilter].order = 'ASC';
                this.filter[nameFilter].field = fName;
                this.getpromotionUsers();
                return;
            }
            if (this.filter[nameFilter].order === 'ASC') {
                this.filter[nameFilter].order = 'DESC';
                this.filter[nameFilter].field = fName;
                this.getpromotionUsers();
            }
        }
    }

    Paginations(total) {
        if (total > this.per_page) {
            this.pagintion.pages = _.range(1, _.ceil(total / this.per_page) + 1, 1);
            this.pagintion.show = true;
        } else {
            this.pagintion.show = false;
        }
    }

    onLoadPage(page) {
        this.page = page;
        this.getpromotionUsers();
    }

    public onChangePerPage(e) {
        this.per_page = e.value;
        this.getpromotionUsers();
    }

    onCheckRow(e, i) {
        if (e.target.checked) {
            this.listCheckboxes.forEach((el, index) => {
                if (index !== i) {
                    this.listCheckboxes[index].checked = false;
                }else {
                    this.listCheckboxes[index].checked = true;
                }
            });
            this.studentId = this.listCheckboxes[i].value;
            this.row_index = i;
        } else {
            this.studentId = null;
            this.row_index = -1;
        }
    }

    exportToExcel() {
        this.subscrips.push(
        this.promotionSrv.getData(this.academyId,  this.academyInfo.promotion_method).subscribe(res => {
            this.excelSrv.exportAsExcelFile(res, 'Promotions_');
        }),
    );
    }
    printGrid() {
        this.printSrv.printGrid('data-grid');
    }
    onAddBelt() {
        if (this.studentId) {
            this.isAddBelt = true;
        }
    }

    closeAddBelt() {
        this.isAddBelt = false;
    }
    onBeltAdded(e) {
        console.info('belt added');
    }

    toUserDashboar(user) {
        this.router.navigate(['academiy-datail', this.academyId, 'students', user.id, 'detail']);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
