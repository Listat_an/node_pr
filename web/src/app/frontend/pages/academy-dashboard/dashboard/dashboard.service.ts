import {
    Injectable,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from '../../../../services/service.config';
import {
    JwtHelperService,
} from '@auth0/angular-jwt';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as _ from 'lodash';

import { format, addDays, isWithinRange, addMonths } from 'date-fns';

@Injectable()
export class DashboardService {
    private options;
    private activate: Boolean;
    private user: any;

    constructor(private http: Http, public jwtHelper: JwtHelperService) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    public getReportAdmin(data) {
        if (data.date_from && data.date_to) {
            data.date_from = format(data.date_from, 'YYYY-MM-DD');
            data.date_to = format(data.date_to, 'YYYY-MM-DD');
        }

        return this.http.post(
                encodeURI(ConfigService.STATIC_SERVER + `/dashboard/report_admin`),
                data,
                this.options,
            )
            .map(res => {
                const arrayTmp: any = {
                    titles: {},
                    data: [],
                };
                arrayTmp.titles = {
                    student: 'Student Name',
                    name: 'Academy name',
                    status: (data.type == 'academy') ? 'Academy status' : 'User status',
                    type: 'Activity type',
                    from: 'From date',
                    to: 'To date',
                    date: 'Transaction date',
                    price: 'Price',
                };

                arrayTmp.data = res.json().data.map(el => {
                    const user = {
                        student: (el.student_name) ? el.student_name : '',
                        name: (el.academy_name) ? el.academy_name : '',
                        status: (data.type == 'academy') ? (el.academy_status) ? el.academy_status : '' : (el.status) ? el.status : '',
                        type: (el.activity_type) ? el.activity_type : '',
                        from: (el.date_from) ? el.date_from : '',
                        to: (el.date_to) ? el.date_to : '',
                        date: (el.date_transaction) ? el.date_transaction : '',
                        price: (el.amount) ? el.amount : '',
                    };
                    return user;
                });
                return arrayTmp;
            })
            .catch(this.handleError);
    }

    public getTotalEvents(fromDate, toDate, academyId): Observable < any > {
        let url = `/dashboard/total_class`;
        if (fromDate && toDate) {
            url += `?filter[created_at][gte]=${format(fromDate, 'YYYY-MM-DD')}&filter[created_at][lte]=${format(toDate, 'YYYY-MM-DD')}`;
        }
        if (academyId) {
            url += `&filter[academy_id]=${academyId}`;
        }
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + url),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getPromotionView(academyId): Observable < any > {
        return this.http.get(
            encodeURI(ConfigService.STATIC_SERVER + `/dashboard/promotion_view?filter[academy_id]=${academyId}`),
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }
 
    public getTotalMarketing(fromDate, toDate, academyId): Observable < any > {
        let url = `/dashboard/marketing`;
        if (fromDate && toDate) {
            url += `?filter[created_at][gte]=${format(fromDate, 'YYYY-MM-DD')}&filter[created_at][lte]=${format(toDate, 'YYYY-MM-DD')}`;
        }
        if (academyId) {
            url += `&filter[academy_id]=${academyId}`;
        }
        return this.http.get(
            encodeURI(ConfigService.STATIC_SERVER + url),
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }

    rangeMonthData(startDate, endDate) {
        let date = startDate;
        let dataSet: any = {
            labels: [],
            values: [],
        };
        while (isWithinRange(date, startDate, endDate)) {
            dataSet.labels.push(format(date, 'MMM, YYYY'));
            dataSet.values.push(0);
            date = addMonths(date, 1);
        }
        return dataSet;
    }

    private revenuLabels(data) {
        let labels = [];
        data.forEach(element => {
            labels.push(format(element.date, 'MMM, YYYY'));
        });
        return labels;
    }

    private revenuValues(data) {
        let values = [];
        data.forEach(element => {
            values.push(element.total);
        });
        return values;
    }


    public getTotalRevenue(fromDate, toDate, academyId): Observable < any > {
        const monthData = this.rangeMonthData(fromDate, toDate);
        const bgColor = {
            subscription: '#4ca1ff',
            shop: '#f18849',
            event: '#ff4f81',
        };
        let url = `/dashboard/revenue`;
        if (fromDate && toDate) {
            url += `?date_from=${format(fromDate, 'YYYY-MM-DD')}&date_to=${format(toDate, 'YYYY-MM-DD')}`;
        }
        if (academyId) {
            url += `&academy_id=${academyId}`;
        }
        return this.http.get(
            encodeURI(ConfigService.STATIC_SERVER + url),
            this.options,
        )
        .map(res => {
            let data = {};
            _.forIn(res.json(), (v, k) => {
                data[k] = {
                    labels: _.union(monthData.labels, this.revenuLabels(v)) ,
                    datasets: [{
                        data: _.union(monthData.values, this.revenuValues(v)),
                        label: '$',
                        backgroundColor: bgColor[k],
                    }],
                };
            });
            return data;
        })
        .catch(this.handleError);
    }
 
    private handleError(error: any) {
        return Observable.throw(error.message || error);
    }
}
