import {
    Injectable,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from '../../../../../services/service.config';
import {
    JwtHelperService,
} from '@auth0/angular-jwt';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as _ from 'lodash';

import { format, addDays } from 'date-fns';

@Injectable()
export class RevenueService {
    private options;
    private activate: Boolean;
    private user: any;

    constructor(private http: Http, public jwtHelper: JwtHelperService) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    public getRevenueDeteil(size, page, academyId, filter, fromDate, toDate): Observable < any > {
        let filterStr: string = '';
        let dateStr: string = '';
        if (filter) {
            _.forIn(_.values(filter), (v) => {
                if (v['order'] === 'ASC') {
                    filterStr += (filterStr !== '' ? ',' : '') + v['field'];
                }
                if (v['order'] === 'DESC') {
                    filterStr += (filterStr !== '' ? ',-' : '-') + v['field'];
                }
            });
        }
        if (filterStr !== '') {
            filterStr = '&sort=' + filterStr;
        }
        if (fromDate && toDate) {
            dateStr += `&filter[created_at][gte]=${format(fromDate, 'YYYY-MM-DD')}&filter[created_at][lte]=${format(toDate, 'YYYY-MM-DD')}`;
        }
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/user_revenue?include=user&filter[academy_id]=${academyId}&page[size]=${size}&page[number]=${page}${filterStr}${dateStr}`),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getData(academyId): Observable < any > {
        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/user_revenue?include=user&page[size]=10000&filter[academy_id]=${academyId}`),
                this.options,
            )
            .map(res => {
                let arrayTmp = res.json();
                arrayTmp.titles = {
                    full_name: 'Student name',
                    subscription_name: 'Subscription',
                    class: 'Class revenue',
                    shop: 'Shop revenue',
                    event: 'Event revenue',
                    subscription: 'Subscription revenue',
                };
               
                arrayTmp.data = res.json().data.map((el, i) => {
                    let student = {
                        full_name: el.full_name,
                        subscription_name: el.subscription_name,
                        class: el.class,
                        shop: el.shop,
                        event: el.event,
                        subscription: el.subscription,
                    };
                    return student;
                });
                return arrayTmp;
            })
            .catch(this.handleError);
    }
    private handleError(error: any) {
        return Observable.throw(error.message || error);
    }
}
