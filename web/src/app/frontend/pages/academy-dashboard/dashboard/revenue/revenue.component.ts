import {
    Component,
    OnInit,
    ViewEncapsulation,
    OnDestroy,
    ViewChild,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import {
    cancelSubscription,
} from '../../../../../providers/cancelSubscription';
import {
    Subscription,
} from 'rxjs/Subscription';
import {
    ExcelService,
} from '../../excel.service';

import {
    RevenueService,
} from './revenue.service';

import {
    PrintService,
} from '../print.service';
import * as _ from 'lodash';
import {
    addDays,
    isBefore,
    subYears,
} from 'date-fns';
import {DatepickerOptions} from 'ng2-datepicker';
import {
    AcademiesService,
} from '../../../../../services/academies.service';

@Component({
    selector: 'opn-dashboard-promotion',
    templateUrl: './revenue.component.html',
    styleUrls: ['./revenue.component.scss', '../../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        AcademiesService,
        RevenueService,
        ExcelService,
        PrintService,
    ],
})
export class RevenueDetailComponent implements OnInit, OnDestroy {
    private academyId: Number;
    public subscrips: Subscription[] = [];
    private promotionUsers: Array < any > = [];

    private studentId: Number = null;
    private isAddBelt: Boolean = false;
    listCheckboxes: Array < any > = [];
    row_index: Number = -1;
    private per_page: any = 10;
    private page: any = 1;
    private pagintion: any;
    private load: Boolean = true;
    private academyInfo: any;

    private filter = {
        name: {
            order: '',
            field: 'full_name',
        },
        subscription_name: {
            order: '',
            field: 'subscription_name',
        },
        class: {
            order: '',
            field: 'class',
        },
        shop: {
            order: '',
            field: 'shop',
        },
        event: {
            order: '',
            field: 'event',
        },
        subscription: {
            order: '',
            field: 'subscription',
        },
    };

    private fromDate = subYears(new Date(), 1);
    private toDate = new Date();

    @ViewChild('fromDateComponent') fromDateComponent;

    toDateOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
        // maxDate: new Date(),
    };

    fromDateOptions: DatepickerOptions = {
        ...this.toDateOptions,
    };


    constructor(
        private academySrv: AcademiesService,
        private revenueSrv: RevenueService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private excelSrv: ExcelService,
        private printSrv: PrintService,
    ) {
        this.pagintion = {
            show: false,
            pages: [],
        };
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.academyId = params['id'];
        });

        this.getRevenueUsers();
        this.getAcademyInfo();
    }

    private getAcademyInfo() {
        this.subscrips.push(
            this.academySrv.getAcademyById(this.academyId).subscribe(academyInfo => {
                this.academyInfo = academyInfo;
                console.info('academyInfo', academyInfo);
            }),
        );
    }

    onToDateChange(toDate) {
        if (isBefore(toDate, this.fromDate)) {
            this.fromDate = toDate;
        }
        this.fromDateOptions.maxDate = toDate;
        this.fromDateComponent.init();
        this.getRevenueUsers();
    }

    private getRevenueUsers() {
        this.load = false;
        this.subscrips.push(
            this.revenueSrv.getRevenueDeteil(this.per_page, this.page, this.academyId, this.filter, this.fromDate, addDays(this.toDate, 1))
            .finally(() => {
                this.load = true;
            })
            .subscribe(deteils => {
                console.log('reenue ', deteils);
                this.formatData(deteils.data);
                this.Paginations(deteils.total);
            }),
        );
    }

    private formatData(data) {
        this.promotionUsers = data;
        this.listCheckboxes = data.map(el => {
            return {
                name: 'checkbox_' + el.id,
                checked: false,
                value: el.user_id,
            };
        });
    }

    onChangeFilter(nameFilter) {
        if (this.filter[nameFilter].order === '' || this.filter[nameFilter].order === 'DESC') {
            this.filter[nameFilter].order = 'ASC';
            this.getRevenueUsers();
            return;
        }
        if (this.filter[nameFilter].order === 'ASC') {
            this.filter[nameFilter].order = 'DESC';
            this.getRevenueUsers();
            return;
        }
    }

    Paginations(total) {
        if (total > this.per_page) {
            this.pagintion.pages = _.range(1, _.ceil(total / this.per_page) + 1, 1);
            this.pagintion.show = true;
        } else {
            this.pagintion.show = false;
        }
    }

    onLoadPage(page) {
        this.page = page;
        this.getRevenueUsers();
    }

    public onChangePerPage(e) {
        this.per_page = e.value;
        this.getRevenueUsers();
    }

    exportToExcel() {
        this.subscrips.push(
            this.revenueSrv.getData(this.academyId).subscribe(res => {
                this.excelSrv.exportAsExcelFile(res, 'Revenue_');
            }),
        );
    }
    printGrid() {
        this.printSrv.printGrid('data-grid');
    }

    toUserDashboar(user) {
        this.router.navigate(['academiy-datail', this.academyId, 'students', user.id, 'detail']);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
