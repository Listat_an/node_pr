import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { cancelSubscription } from '../../../../providers/cancelSubscription';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

import { AcademiesService } from '../../../../services/academies.service';

@Component({
  selector: 'opn-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss', '../../@theme/scss/theme.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [
    AcademiesService,
  ],
})
export class DashboardComponent implements OnInit, OnDestroy {
  public subscrips: Subscription[] = [];
  private academy: any = {
    id: null,
    name: null,
    photo: null,
    location: null,
  };

  public reload = true;
  public isAdmin = false;

  constructor(
    private academySrv: AcademiesService,
    private activeRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.activeRoute.params.subscribe(params => {
      if (params['id']) {
        this.academy.id = params['id'];
      }else if (params['academy_id']) {
        this.academy.id = params['academy_id'];
        this.isAdmin = true;
      }
    });

    // this.router.routeReuseStrategy.shouldReuseRoute = function () {
    //   return false;
    // };
    this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
            this.router.navigated = false;
            window.scrollTo(0, 0);
            this.reload = false;
            this.getAcademyInfo();
        }
    });
  }

  ngOnInit() {
    this.getAcademyInfo();
  }

  private getAcademyInfo() {
    this.subscrips.push(
      this.academySrv.getAcademyById(this.academy.id)
      .finally(() => {
        this.reload = true;
      })
      .subscribe( academy => {
        this.academy = _.pick(academy, _.keys(this.academy));
      }),
    );
  }

  toAcademy() {
    this.router.navigate(['academiy-datail', this.academy.id]);
  }

  ngOnDestroy() {
    cancelSubscription(this.subscrips);
  }

}
