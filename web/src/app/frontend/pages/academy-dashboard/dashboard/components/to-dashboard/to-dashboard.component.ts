import {Component, Input} from '@angular/core';

import { Location } from '@angular/common';

export interface PromotionsInterface {
    stripe: Number;
    belt: Number;
}
@Component({
    selector: 'opn-to-dashboard',
    templateUrl: './to-dashboard.html',
    styleUrls: ['./to-dashboard.scss'],
})
export class ToDashboardComponent {

    @Input() academyId: Number;
    constructor(
        private _location: Location,
    ) {
    }

    toDashboard() {
        this._location.back();
    }
}

