import {
    Component,
    Input,
    OnInit,
    HostListener,
    ElementRef,
    ViewChild,
  } from '@angular/core';

  @Component({
    selector: 'opn-subscription-list',
    templateUrl: './subscription-list.component.html',
    styleUrls: ['./subscription-list.component.scss'],
  })
  export class SubscriptionListPopupComponent implements OnInit {

    @Input() subscriptions: Array<any> = [];
    private firstaName: String = '-';
    private list: Array<string> = [];
    private open: Boolean = false;

    @ViewChild('listEl', {
        read: ElementRef,
      }) listRef: ElementRef;

    @HostListener('document:click', ['$event'])
    onMouseUp() {
        if (this.listRef.nativeElement.classList.value.indexOf('open') >= 0 && this.open) {
            this.closeTimePicker();
        }
    }

    constructor() {
    }

    ngOnInit() {
        if (this.subscriptions.length > 0) {
            this.transformArray(this.subscriptions);
        }
    }

    private transformArray(list) {
        for (let i = 0; i < list.length; i++) {
            let el = list[i].subscription.name;
            if (i === 0) {
                this.firstaName = el;
            }
            this.list.push(el);
        }
    }
    onToggleList(e) {
        this.open = !this.open;
        if (this.open) {
            this.listRef.nativeElement.classList.add('open');
        }else {
            this.listRef.nativeElement.classList.remove('open');
        }
        e.stopPropagation();
    }
    closeTimePicker() {
        this.listRef.nativeElement.classList.remove('open');
        this.open = false;
    }
  }
