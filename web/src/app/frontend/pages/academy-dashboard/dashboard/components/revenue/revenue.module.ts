import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { RouterModule } from '@angular/router';
import {RoundProgressModule} from '../../../../../../../components/angular-round-progressbar/index';
import {RevenueViewComponent} from './revenue.component';
import {NgDatepickerModule} from 'ng2-datepicker';
import { ChartModule } from 'angular2-chartjs';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        RoundProgressModule,
        NgDatepickerModule,
        ChartModule,
    ],
    declarations: [
        RevenueViewComponent,
    ],
    exports: [
        RevenueViewComponent,
    ],
})
export class RevenueViewModule {

}
