import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { RouterModule } from '@angular/router';
import {RoundProgressModule} from '../../../../../../../components/angular-round-progressbar/index';
import {MarketingViewComponent} from './marketing.component';
import {NgDatepickerModule} from 'ng2-datepicker';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        RoundProgressModule,
        NgDatepickerModule,
    ],
    declarations: [
        MarketingViewComponent,
    ],
    exports: [
        MarketingViewComponent,
    ],
})
export class MarketingViewModule {

}
