import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RoundProgressModule} from '../../../../../../../components/angular-round-progressbar/index';
import {TotalClassesComponent} from './total-classes.component';
import {NgDatepickerModule} from 'ng2-datepicker';

@NgModule({
    imports: [
        CommonModule,
        RoundProgressModule,
        NgDatepickerModule,
    ],
    declarations: [
        TotalClassesComponent,
    ],
    exports: [
        TotalClassesComponent,
    ],
})
export class TotalClasesModule {

}
