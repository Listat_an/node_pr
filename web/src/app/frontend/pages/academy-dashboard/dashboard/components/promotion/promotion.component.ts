import {Component, Input, OnInit, OnDestroy} from '@angular/core';

import { cancelSubscription } from '../../../../../../providers/cancelSubscription';
import { Subscription } from 'rxjs/Subscription';

import { DashboardService } from '../../dashboard.service';

export interface PromotionsInterface {
    stripe: Number;
    belt: Number;
}
@Component({
    selector: 'opn-promotion-view',
    templateUrl: './promotion.html',
    styleUrls: ['./promotion.scss'],
    providers: [
        DashboardService,
    ],
})
export class PromotionViewComponent implements OnInit, OnDestroy {
    public subscrips: Subscription[] = [];

    private promotion: PromotionsInterface = {
        stripe: 0,
        belt: 0,
    };

    @Input() academyId?: number;

    constructor(
        private dashboardSrv: DashboardService,
    ) {
    }

    ngOnInit() {
        this.getTotalEnvents();
    }

    private getTotalEnvents() {
        this.subscrips.push(
          this.dashboardSrv.getPromotionView(this.academyId).subscribe(promotion => {
            this.promotion = promotion;
          }),
        );
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}

