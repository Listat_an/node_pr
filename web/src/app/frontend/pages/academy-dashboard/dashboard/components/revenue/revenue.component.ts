import {
    Component,
    Input,
    OnInit,
    OnDestroy,
    ViewChild,
} from '@angular/core';

import {
    DatepickerOptions,
} from 'ng2-datepicker';
import {
    subYears,
    differenceInYears,
    isBefore,
    addMonths,
    addDays,
} from 'date-fns';
import {
    cancelSubscription,
} from '../../../../../../providers/cancelSubscription';
import {
    Subscription,
} from 'rxjs/Subscription';

import {
    DashboardService,
} from '../../dashboard.service';

@Component({
    selector: 'opn-revenue-view',
    templateUrl: './revenue.html',
    styleUrls: ['./revenue.scss'],
    providers: [
        DashboardService,
    ],
})
export class RevenueViewComponent implements OnInit, OnDestroy {
    public subscrips: Subscription[] = [];

    @Input() academyId?: number;

    private fromDate = subYears(new Date(), 1);
    private toDate = new Date();

    dataCharts: any = null;
    options: any;
    tab: any = 'subscription';

    @ViewChild('fromDateComponent') fromDateComponent;

    toDateOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
        // maxDate: new Date(),
    };

    fromDateOptions: DatepickerOptions = {
        ...this.toDateOptions,
    };


    constructor(
        private dashboardSrv: DashboardService,
    ) {}

    ngOnInit() {
        this.options = {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: false,
                labels: {
                    fontColor: '#90949c',
                },
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false,
                        color: '#d4e0ec',
                    },
                    ticks: {
                        fontColor: '#90949c',
                    },
                }],
                yAxes: [{
                    gridLines: {
                        borderDash: [4, 2],
                        display: true,
                        color: '#d4e0ec',
                    },
                    ticks: {
                        fontColor: '#90949c',
                    },
                }],
            },
        };
        this.getTotalRevenue();
    }

    onToDateChange(toDate) {
        if (isBefore(toDate, this.fromDate)) {
            this.fromDate = toDate;
        }
        this.fromDateOptions.maxDate = toDate;
        this.fromDateComponent.init();
        this.getTotalRevenue();
    }

    private getTotalRevenue() {
        this.subscrips.push(
          this.dashboardSrv.getTotalRevenue(this.fromDate, addDays(this.toDate, 1), this.academyId).subscribe(dataSet => {
              console.log(dataSet);
            this.dataCharts = dataSet;
          }),
        );
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
