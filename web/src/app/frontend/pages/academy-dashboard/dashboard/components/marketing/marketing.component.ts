import {Component, Input, OnInit, OnDestroy, ViewChild} from '@angular/core';

import {DatepickerOptions} from 'ng2-datepicker';
import {
    addDays,
    isBefore,
} from 'date-fns';
import { cancelSubscription } from '../../../../../../providers/cancelSubscription';
import { Subscription } from 'rxjs/Subscription';

import { DashboardService } from '../../dashboard.service';

export interface Marketing {
    cancelled: Number;
    inactive: Number;
    free_trial: Number;
    events: Number;
}
@Component({
    selector: 'opn-marketing-view',
    templateUrl: './marketing.html',
    styleUrls: ['./marketing.scss'],
    providers: [
        DashboardService,
    ],
})
export class MarketingViewComponent implements OnInit, OnDestroy {
    public subscrips: Subscription[] = [];

    private marketing: Marketing = {
        cancelled: 0,
        inactive: 0,
        free_trial: 0,
        events: 0,
    };

    @Input() academyId?: number;

    private fromDate = addDays(new Date(), -30);
    private toDate = new Date();

    @ViewChild('fromDateComponent') fromDateComponent;

    toDateOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
        // maxDate: new Date(),
    };

    fromDateOptions: DatepickerOptions = {
        ...this.toDateOptions,
    };


    constructor(
        private dashboardSrv: DashboardService,
    ) {
    }

    ngOnInit() {
        this.getTotalMarketing();
    }

    onToDateChange(toDate) {
        if (isBefore(toDate, this.fromDate)) {
            this.fromDate = toDate;
        }
        this.fromDateOptions.maxDate = toDate;
        this.fromDateComponent.init();
        this.getTotalMarketing();
    }

    private getTotalMarketing() {
        this.subscrips.push(
          this.dashboardSrv.getTotalMarketing(this.fromDate, this.toDate, this.academyId).subscribe(total => {
            this.marketing = total;
          }),
        );
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}

