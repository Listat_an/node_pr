import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { RouterModule } from '@angular/router';
import {RoundProgressModule} from '../../../../../../../components/angular-round-progressbar/index';
import {PromotionViewComponent} from './promotion.component';
import {NgDatepickerModule} from 'ng2-datepicker';

@NgModule({
    imports: [
        CommonModule,
        RoundProgressModule,
        RouterModule,
        NgDatepickerModule,
    ],
    declarations: [
        PromotionViewComponent,
    ],
    exports: [
        PromotionViewComponent,
    ],
})
export class PromotionViewModule {

}
