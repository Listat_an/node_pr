import {Component, Input, OnInit, OnDestroy, ViewChild} from '@angular/core';

import {DatepickerOptions} from 'ng2-datepicker';
import {
    addDays,
    isBefore,
} from 'date-fns';
import { cancelSubscription } from '../../../../../../providers/cancelSubscription';
import { Subscription } from 'rxjs/Subscription';

import { DashboardService } from '../../dashboard.service';

export interface TotalClasses {
    others: Object;
    class: Object;
    mat_event: Object;
    seminar: Object;
}
@Component({
    selector: 'opn-total-classes',
    templateUrl: './total-classes.html',
    styleUrls: ['./total-classes.scss'],
    providers: [
        DashboardService,
    ],
})
export class TotalClassesComponent implements OnInit, OnDestroy {
    public subscrips: Subscription[] = [];

    private total_events: TotalClasses = {
        others: {
            otal: 0,
            active_total: 0,
        },
        class: {
            total: 0,
            active_total: 0,
        },
        mat_event: {
            total: 0,
            active_total: 0,
        },
        seminar: {
            total: 0,
            active_total: 0,
        },
    };

    @Input() academyId?: number;

    private fromDate = addDays(new Date(), -30);
    private toDate = new Date();

    @ViewChild('fromDateComponent') fromDateComponent;

    toDateOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
        // maxDate: new Date(),
    };

    fromDateOptions: DatepickerOptions = {
        ...this.toDateOptions,
    };


    constructor(
        private dashboardSrv: DashboardService,
    ) {
    }

    ngOnInit() {
        this.getTotalEnvents(this.fromDate, this.toDate, this.academyId);
    }

    onToDateChange(toDate) {
        if (isBefore(toDate, this.fromDate)) {
            this.fromDate = toDate;
        }
        this.fromDateOptions.maxDate = toDate;
        this.fromDateComponent.init();
        this.getTotalEnvents(this.fromDate, this.toDate, this.academyId);
    }

    private getTotalEnvents(fromDate, toDate, academyId) {
        this.subscrips.push(
          this.dashboardSrv.getTotalEvents(fromDate, toDate, academyId).subscribe(total => {
            this.total_events = total;
          }),
        );
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}

