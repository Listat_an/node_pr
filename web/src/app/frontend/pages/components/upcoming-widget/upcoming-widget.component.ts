import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnDestroy,
    AfterViewInit,
    ViewChild,
    ElementRef,
    DoCheck,
    IterableDiffers,
} from '@angular/core';

import {
    Router,
} from '@angular/router';
import {
    ActivitiesService,
} from '../../../../services/activities.service';

import {
    ScheduleService,
} from '../../../../services/schedule.service';

import * as _d from 'date-fns';
import * as _is from 'is_js';
import * as _ from 'lodash';

import {
    GroupByPipe,
    OrderByPipe,
} from 'ngx-pipes';
import {
    values,
    keys,
    orderBy,
    forEach,
} from 'lodash';
import {
    Subject,
} from 'rxjs/Subject';
import {
    Observable,
} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
    selector: 'opn-events-upcoming',
    templateUrl: './upcoming-widget.component.html',
    styleUrls: ['./upcoming-widget.component.scss'],
    providers: [GroupByPipe, OrderByPipe, ActivitiesService, ScheduleService],
})
export class UpcomingEventsComponent implements OnInit, AfterViewInit, DoCheck, OnDestroy {
    private events: Array < any > = [];
    private dates: Array < any > ;
    private page = 2;
    public load: Boolean = true;

    private differ: any;

    @ViewChild('eventsList') eventsListRef: ElementRef;
    @Output() loaded: EventEmitter < any > = new EventEmitter < any > ();

    private ngUnsubscribe: Subject < void > = new Subject < void > ();
    private scheduleSource: any = [];
    private dataSource: any = [];

    constructor(
        private router: Router,
        private groupBy: GroupByPipe,
        private orderPipe: OrderByPipe,
        private activityService: ActivitiesService,
        differs: IterableDiffers,
        private scheduleSrvc: ScheduleService,
    ) {
        this.differ = differs.find([]).create(null);
    }

    ngOnInit() {
        this.getUpcoming();
    }

    private getUpcoming() {
        const from = _d.startOfWeek(new Date(), {
            weekStartsOn: 1,
        });

        const to = _d.endOfWeek(new Date(), {
            weekStartsOn: 1,
        });
        Observable.forkJoin([
            this.activityService.getUpcomingFilter(50),
            this.scheduleSrvc.getScheduleForStudent(from, to, null, null, null, null, true),
        ])
        .finally(() => {
            this.load = false;
        })
        .takeUntil(this.ngUnsubscribe)
        .subscribe(t => {
            this.dataSource = t[0];
            this.parseUpcoming(t[1].data);
        });
    }

    private checkHoliday(schedyle, date_2): Boolean {
        const date_1 = new Date(schedyle.date);
        if (_d.isEqual(_d.setHours(_d.setMinutes(date_1, 0), 0), _d.setHours(_d.setMinutes(date_2, 0), 0))) {
            return true;
        }
        return false;
    }

    private parseUpcoming(data) {
        let _holidayDays: Array<any> = [];
        forEach(data, (_classes, index) => {
            _classes.forEach(_class => {
                _class.shedules.forEach(_shedule => {
                    const [h1, m1] = _shedule.time_start.split('.');
                    if (_shedule.holiday == 1 && this.checkHoliday(_shedule, new Date(index))) {
                        _holidayDays.push({day: _shedule.day, date: index});
                    }
                    this.dataSource.push({
                        ..._shedule,
                        name: _class.name,
                        type: _class.type,
                        start_date: _d.format(_d.setHours(_d.setMinutes(new Date(index), m1), h1), 'YYYY-MM-DD HH:mm:ss'),
                        date: _d.format(new Date(index), 'MM/DD/YYYY'),
                    });
                });
            });
        });

        let _schedules = [];
        _holidayDays = _.uniqBy(_holidayDays, 'day');
        if (_is.not.empty(_holidayDays)) {
            _holidayDays.forEach(_day => {
                _schedules = this.dataSource.filter(ISchedule => ISchedule.day == _day.day && (ISchedule.holiday == 0 || !this.checkHoliday(ISchedule, new Date(_day.date))));
            });
            this.dataSource  = _.difference(this.dataSource, _schedules);
        }else {
            this.dataSource = this.dataSource.filter(ISchedule => ISchedule.holiday != 1);
        }
        this.loaded.emit(this.dataSource.length);
        this.orderData(this.dataSource);
    }

    ngAfterViewInit() {
        if (this.eventsListRef) {
            const element = this.eventsListRef.nativeElement;
            const height = element.offsetHeight;
        }
    }

    ngDoCheck() {
        /*const change = this.differ.diff(this.dataSource);
        if (change) {
            this.orderData(change._collection);
        }*/
    }
    
    private oderDates(data): any {
        data = data.map(el => {
            return el = _d.getTime(new Date(el));
        });
        return data.sort().map(date => _d.format(new Date(date), 'MM/DD/YYYY'));
    }
    orderData(arr) {
        const events = this.groupBy.transform(arr, 'date');
        this.oderDates(keys(events));
        this.dates = this.oderDates(keys(events));
        console.log('dates ', this.dates);
        this.events = events;
        this.dates.forEach(el => {
            this.events[el] = this.orderPipe.transform(events[el], 'time');
        });
    }
    private onRedirectTo(id, type, academy_id) {
        if (type == 'class') {
            this.router.navigate(['academiy-datail', academy_id], { queryParams: { tab: 'activiti' } });
            return;
        }
        this.router.navigate([`activity/${id}`]);
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
