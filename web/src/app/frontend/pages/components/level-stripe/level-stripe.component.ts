import {Component, Input, OnInit} from '@angular/core';
import {StudentsService} from '../../../../services/students.service';

@Component({
    selector: 'opn-level-stripe',
    templateUrl: './level-stripe.html',
    styleUrls: ['./level-stripe.scss'],
    providers: [StudentsService]
})
export class LevelStripeComponent implements OnInit{
    @Input() studentId;
    @Input() academyId;
    userScore;
    maxScore;

    constructor(private studentsService: StudentsService){

    }

    ngOnInit(){
        this.studentsService.getStudentStripeLevel(this.studentId, this.academyId).subscribe(res => {
            this.userScore =  res.next_level;
            this.maxScore =  res.total_level;
        })
    }
}

