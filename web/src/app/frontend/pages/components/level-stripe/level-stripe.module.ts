import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RoundProgressModule} from '../../../../../components/angular-round-progressbar/index';
import {LevelStripeComponent} from './level-stripe.component';

@NgModule({
    imports: [
        CommonModule,
        RoundProgressModule,
    ],
    declarations: [
        LevelStripeComponent
    ],
    exports: [
        LevelStripeComponent
    ]
})
export class LevelStripeModule {

}
