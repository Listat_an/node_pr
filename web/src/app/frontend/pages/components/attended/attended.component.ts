import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {StudentsService} from '../../../../services/students.service';
import {DatepickerOptions} from 'ng2-datepicker';
import {
    addDays,
    isBefore
} from 'date-fns';

@Component({
    selector: 'opn-attended',
    templateUrl: './attended.html',
    styleUrls: ['./attended.scss'],
    providers: [StudentsService]
})
export class AttendedComponent implements OnInit {
    @Input() userId: number;
    @Input() academyId: number;
    joins;
    checkins;
    fromDate = addDays(new Date(), -30);
    toDate = new Date();
    @ViewChild('fromDateComponent') fromDateComponent;

    toDateOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
        maxDate: new Date(),
    };
    fromDateOptions: DatepickerOptions = {
        ...this.toDateOptions
    };


    constructor(private studentsService: StudentsService) {
    }

    ngOnInit() {
        this.getUserAttendance()
    }

    getUserAttendance(){
        this.studentsService.getUserAttendance(this.userId, this.fromDate, this.toDate, this.academyId).subscribe(data => {
            this.joins = data.join;
            this.checkins = data.checkin;
        })
    }

    onToDateChange(toDate) {
        if (isBefore(toDate, this.fromDate)) {
            this.fromDate = toDate;
        }
        this.fromDateOptions.maxDate = toDate;
        this.fromDateComponent.init();
        this.getUserAttendance();
    }
}

