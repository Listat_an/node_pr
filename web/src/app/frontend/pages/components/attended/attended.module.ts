import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RoundProgressModule} from '../../../../../components/angular-round-progressbar/index';
import {AttendedComponent} from './attended.component';
import {NgDatepickerModule} from 'ng2-datepicker';

@NgModule({
    imports: [
        CommonModule,
        RoundProgressModule,
        NgDatepickerModule
    ],
    declarations: [
        AttendedComponent
    ],
    exports: [
        AttendedComponent
    ]
})
export class AttendedModule {

}
