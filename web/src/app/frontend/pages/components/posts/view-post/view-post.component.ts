import {
    MetascraperService
} from './../../../../../services/metascraper.service';
import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnDestroy,
    OnChanges,
    AfterViewInit,
    ViewChild,
    ElementRef,
    HostListener,
    SimpleChanges,
} from '@angular/core';

import {
    PlatformLocation,
} from '@angular/common';

import {
    Router,
} from '@angular/router';
import {
    PostService,
} from '../../../../../services/posts.service';

import {
    Broadcaster,
} from '../../../../../services/broadcaster';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ISubscription,
    Subscription,
} from 'rxjs/Subscription';
import {
    cancelSubscription
} from '../../../../../providers/cancelSubscription';

import {
    ModalConfirmComponent,
} from '../../academy/modal-confirm/modal-confirm.component';
import {
    LinkyPipe
} from 'angular-linky';

@Component({
    selector: 'opn-view-post',
    templateUrl: './view-post.component.html',
    styleUrls: ['./view-post.component.scss'],
    providers: [
        LinkyPipe,
        MetascraperService,
    ],
})

export class ViewPostComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
    public subscrips: Subscription[] = [];
    @Input() me: any;
    @Input() post: any;
    @Input() academyId: any;

    @ViewChild('feedContent') feedContent: ElementRef;
    @ViewChild('showMore') showMore: ElementRef;
    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;

    @Output() onRemove: EventEmitter < any > = new EventEmitter < any > ();
    @Output() openModal: EventEmitter < any > = new EventEmitter < any > ();

    @HostListener('document:click', ['$event'])

    click(event: MouseEvent): void {
        if (this.shareToggle) {
            this.shareToggle = false;
        }
    }

    private offset: Number;
    private edit: Boolean = false;
    private serverUrl: String;

    private comments: Array < any > = [];
    private lastComment: any;
    private limit: any = 10;
    private page: any = 1;
    private maxPage: Number;
    private total: Number;

    private commentToggle: Boolean = false;
    private shareToggle: Boolean = false;
    private userAvatar: string;

    private _height: Number;
    private _toggleMore: Boolean = false;
    private _updatePost: Boolean = false;
    private contentBox: Boolean = false;
    private _gridFiles: Object = {
        1: {
            0: {
                col: 12,
            },
        },
        2: {
            0: {
                col: 6,
            },
            1: {
                col: 6,
            },
        },
        3: {
            0: {
                col: 12,
            },
            1: {
                col: 6,
            },
            2: {
                col: 6,
            },
        },
        4: {
            0: {
                col: 6,
            },
            1: {
                col: 6,
            },
            2: {
                col: 6,
            },
            3: {
                col: 6,
            },
        },
    };
    private countFiles: Number = 1;
    private postSubscribe: ISubscription;

    private addComment: Boolean = true;
    private userView: any;
    private confirmOpen: Boolean = false;
    private confirmAction: String = '';

    private shareUrl: any = '';
    public htmlContent = '';
    public editorConfig = {
        editable: true,
        spellcheck: true,
        height: 'auto',
        minHeight: '3rem',
        placeholder: '',
        translate: 'no',
        toolbar: [
            ['bold', 'italic', 'underline', 'strikeThrough', 'superscript', 'subscript'],
        ],
    };

    public openGraph: any;
    public openGraph_load: Boolean = false;

    constructor(
        private postService: PostService,
        private broadcaster: Broadcaster,
        private router: Router,
        private platformLocation: PlatformLocation,
        private linkyP: LinkyPipe,
        private metascraperSrvc: MetascraperService,
    ) {
        this.serverUrl = postService.getServerUrl();
    }

    ngOnInit() {
        this.shareUrl = (this.platformLocation as any).location.origin + `/#/?show-p=${this.post.id}`;
        const user_id = this.userView = this.postService.getMe().id;
        if (this.post.content != '') {
            this.contentBox = true;
            this.htmlContent = this.linkyP.transform(this.post.content);
            this.parseData(this.htmlContent);
        }
        if ((this.post.user && user_id == this.post.user.id) || (this.post.academy && this.post.academy.id == this.academyId)) {
            this._updatePost = true;
        }
        if (this.post.user) {
            this.userAvatar = this.post.user.avatar;
        }

        if (this.post.files && this.post.files.length > 0) {
            this.countFiles = this.post.files.length;
        }

        this.registerPostBroadcast();
        this.subscrips.push(
            this.postService.getCommentsPost(1, 1, this.post.id).subscribe(res => {
                if (res.data) {
                    this.lastComment = res.data[0];
                }
            }),
        );
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.post && changes.post.currentValue) {
            this.post = changes.post.currentValue;
        }
        if (changes.post && changes.post.currentValue && changes.post.currentValue.files && changes.post.currentValue.files.length > 0) {
            this.post = changes.post.currentValue;
            this.countFiles = changes.post.currentValue.files.length;
        }
    }

    registerPostBroadcast() {
        this.subscrips.push(
            this.postSubscribe = this.broadcaster.on < string > (`post${this.post.id}`)
            .subscribe(postData => {
                console.log(`post_${this.post.id}`, postData);
            }),
        );
    }

    heightContent() {
        if (this.post.content) {
            const element = this.feedContent.nativeElement;
            this.offset = element.offsetHeight - element.clientHeight;
            const height = element.scrollHeight + this.offset;
            this._height = height;
            this._toggleMore = true;
            if (height <= 140) {
                element.style.height = height + 'px';
                this.showMore.nativeElement.style.display = 'none';
            } else {
                element.style.height = '140px';
                this.showMore.nativeElement.style.display = 'block';
            }
        } else {
            this._toggleMore = false;
            this.showMore.nativeElement.style.display = 'none';
        }
    }

    ngAfterViewInit() {
        this.heightContent();
    }

    onClickMedia(post, file_index) {
        this.openModal.emit({
            post: this.post,
            fileIndex: file_index,
        });
    }

    toggleMore(event) {
        if (this._toggleMore) {
            this.feedContent.nativeElement.style.height = this._height + 'px';
        } else {
            this.feedContent.nativeElement.style.height = '140px';
        }
        this._toggleMore = !this._toggleMore;
    }

    onControllAction(event) {
        switch (event.action) {
            case 'update':
                {
                    this.subscrips.push(
                        this.postService.updatePost({
                            share_from: event.share_from,
                            content: this.post.content,
                            title: this.post.title,
                        }, this.post.id).subscribe(res => {
                            this.post.share_from = res.post.share_from;
                        }),
                    );
                    break;
                }
            case 'remove':
                {
                    this.confirmAction = 'remove';
                    this.modalConfirm.modalTitle = 'Delete your Post?';
                    this.modalConfirm.modalDescription = 'This action will remove your form system and you can not receover the post anymore.';
                    this.modalConfirm.btnsTitle = {
                        accept: 'Yes',
                        declide: 'no',
                    };
                    this.modalConfirm.open();
                    break;
                }
            case 'edit':
                {
                    this.contentBox = true;
                    // this.feedContent.nativeElement.classList.add('editing');
                    // this.feedContent.nativeElement.readOnly = false;
                    this.edit = true;
                    break;
                }
        }
    }

    public logKeuUp() {
        console.log('logKeuUp');
    }

    onEdit(e, post) {
        this.htmlContent = this.linkyP.transform(this.htmlContent);
        this.subscrips.push(
            this.postService.updatePost({
                content: this.htmlContent,
                title: post.title,
            }, post.id)
            .subscribe(res => {
                this.edit = !this.edit;
                if (res.post.content == '') {
                    this.contentBox = false;
                    this._toggleMore = false;
                    this.showMore.nativeElement.style.display = 'none';
                } else {
                    // this.heightContent();
                }
                this.parseData(this.htmlContent);
            }),
        );
        e.stopPropagation();
    }

    onConfirm(confirm) {
        if (confirm) {
            switch (this.confirmAction) {
                case 'remove':
                    {
                        this.subscrips.push(
                            this.postService.removePost(this.post.id).subscribe(res => {
                                if (res.status) {
                                    this.onRemove.emit(this.post.id);
                                    this.confirmAction = '';
                                }
                            }),
                        );
                        break;
                    }
                case 'share':
                    {
                        this.subscrips.push(
                            this.postService.sharePostTimline({
                                post_id: this.post.id,
                            }).subscribe(res => {
                                if (res.status) {
                                    // alert('This has been shared to your Timeline!');
                                }
                            }),
                        );
                        break;
                    }
            }
        }
    }

    onLike(event, is_like, post) {
        this.subscrips.push(
            this.postService.postLike(is_like, post.id).subscribe(res => {
                if (res.status) {
                    switch (res.action) {
                        case 'unlike':
                            {
                                post.count_like -= 1;
                                post.is_like = !is_like;
                                event.toElement.classList.remove('is-like');
                                event.toElement.classList.remove('hand-gesture-blue');
                                event.toElement.classList.add('hand-gesture');
                                break;
                            }
                        case 'like':
                            {
                                post.count_like += 1;
                                post.is_like = !is_like;
                                event.toElement.classList.add('is-like');
                                event.toElement.classList.remove('hand-gesture');
                                event.toElement.classList.add('hand-gesture-blue');
                                break;
                            }
                    }
                }
            }),
        );
    }

    onCommentToggle(event, post) {
        this.commentToggle = !this.commentToggle;
        if (this.commentToggle) {
            event.toElement.classList.add('active');
            this.subscrips.push(
                this.postService.getCommentsPost(this.limit, this.page, this.post.id).subscribe(res => {
                    this.total = res.total;
                    this.maxPage = Math.ceil(res.total / this.limit);
                    this.comments = this.comments.concat(res.data);
                }),
            );
        } else {
            this.comments = [];
            event.toElement.classList.remove('active');
        }
    }

    onSaveComment(inputElement, post) {
        if (inputElement.value != '') {
            this.subscrips.push(
                this.postService.setCommentToPost({
                    post_id: post.id,
                    content: inputElement.value,
                }).subscribe(res => {
                    if (res.status) {
                        post.count_comments += 1;
                        inputElement.value = '';
                        this.comments.unshift(res.comment);
                        this.lastComment = res.comment;
                        this.addComment = true;
                    }
                }),
            );
        }
    }
    onRemoveComment(modalConfirm) {
        modalConfirm.style.display = 'block';
    }
    onConfirmComment(comment, confirm, modal) {
        if (confirm) {
            this.subscrips.push(
                this.postService.removeComment(comment.id).subscribe(res => {
                    if (res.status) {
                        this.comments = this.comments.filter(iComment => iComment !== comment);
                        this.post.count_comments -= 1;
                    }
                }),
            );
        } else {
            modal.style.display = 'none';
        }
    }

    onScrollDown() {}

    onShareTimline(id) {
        this.confirmAction = 'share';
        this.modalConfirm.modalTitle = 'Share your Post?';
        this.modalConfirm.modalDescription = 'This action will share your post.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'No',
        };
        this.modalConfirm.open();
    }

    onCloseSahre(social) {
        alert(`This has been shared to your ${social}!`);
        this.shareToggle = !this.shareToggle;
    }

    onShareToggle(event) {
        event.stopPropagation();
        this.shareToggle = !this.shareToggle;
    }
    public parseData(content) {
        const pattern = new RegExp("(https|http)?:\/\/([a-zA-Z0-9-_\.]+)?(\.[a-zA-Z0-9]{2,})?([-a-zA-Z0-9:%_\+.~#?&//=]*)?", 'gi');
        let links = content.match(pattern);
        if (links && links[0]) {
            this.openGraph_load = true;
            this.subscrips.push(
                this.metascraperSrvc.getOpenGraph({
                    url: links[0],
                })
                .finally(() => {
                    this.openGraph_load = false;
                })
                .subscribe(resp => {
                    if (resp.code != 'ETIMEDOUT') {
                        this.openGraph = resp;
                    }
                }),
            );
        }
    }

    ngOnDestroy() {
        this.postSubscribe.unsubscribe();
        cancelSubscription(this.subscrips);
    }

    toProfile(user_id) {
        this.router.navigate(['profile', user_id]);
    }
}
