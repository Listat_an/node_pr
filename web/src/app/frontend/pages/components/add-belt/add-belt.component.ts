import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthService} from '../../../../services/auth/auth.service';
import {StudentsService} from '../../../../services/students.service';

@Component({
    selector: 'add-belt',
    templateUrl: './add-belt.html',
    styleUrls: ['./add-belt.scss'],
    providers: [StudentsService, AuthService]
})
export class AddBeltComponent implements OnInit{

    user;
    userAgeType;
    colorsBelt = [];
    arrBelts = [];
    beltId;
    isAdded = false;

    @Input() studentId;
    @Output() onAdd: EventEmitter<any> = new EventEmitter<any>();
    @Output() close: EventEmitter<any> = new EventEmitter<any>();

    constructor(private studentsService: StudentsService,
                private authService: AuthService){

    }

    ngOnInit() {
        if (this.studentId) {
            this.studentsService.getStudentById(this.studentId).subscribe(user => {
                this.user = user;
                this.userAgeType = user.belt.belt_color.type === 'all'? 'junior' : user.belt.belt_color.type;
                this.authService.getBeltColors(`all,${this.userAgeType}`).subscribe(beltColors => {
                    this.colorsBelt = beltColors;
                    this.authService.getBeltStipiesBycolorId(this.user.belt.belt_color_id).subscribe(belts => {
                        this.arrBelts = belts;
                    });
                });
            });
        }
    }

    onChangeAge(e) {
        this.authService.getBeltColors(`all,${e.value}`).subscribe(res => {
            this.colorsBelt = res;
            this.authService.getBeltStipiesBycolorId(res[0].id).subscribe(belts => {
                this.arrBelts = belts;
            });
        });
    }

    onChangeColor(e) {
        this.authService.getBeltStipiesBycolorId(e).subscribe(res => {
            this.arrBelts = res;
        });
    }
    public changeBeltColor = function (id) {
        this.beltId = id;
    };

    public addBelt(){
        this.studentsService.addStudentBelt(this.studentId, this.beltId).subscribe(res => {
            this.onAdd.emit(res);
            this.isAdded = true;
        });
    }

    onClose(){
        this.isAdded = false;
        this.close.emit();
    }
}
