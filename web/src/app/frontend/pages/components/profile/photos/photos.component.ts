import { Component, OnInit, ViewChild, Input, Output, EventEmitter  } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from 'ngx-image-gallery';

import { ProfileService } from '../../../../../services/profile.service';
import { PostService } from '../../../../../services/posts.service';

import {
  ModalConfirmComponent,
} from '../../academy/modal-confirm/modal-confirm.component';

import * as _ from 'lodash';


@Component({
  selector: 'opn-profile-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss'],
})
export class PhotosProfileComponent implements OnInit {
  private photos: Array<any> = [];
  private limit: any = 10;
  private page: any = 1;
  private maxPage: Number;
  private total: Number;
  private load: Boolean = true;

  private user: any;
  private serverUrl: String;

  @ViewChild(NgxImageGalleryComponent) ngxImageGallery: NgxImageGalleryComponent;
  @Input() me?: any;
  @Output() removePost: EventEmitter<any> = new EventEmitter();
  @ViewChild(ModalConfirmComponent)
  public modalConfirm: ModalConfirmComponent;

  conf: GALLERY_CONF = {
    imageOffset: '0px',
    showDeleteControl: false,
    showImageTitle: false,
  };
  images: GALLERY_IMAGE[] = [];

  private postTmpId: any;

  constructor(
    private profileService: ProfileService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private postService: PostService,
  ) {
    this.activeRoute.params.subscribe(params => {
      this.user = params['id'];
    });
  }
  ngOnInit() {
    this.load = false;
    this.profileService.getUserPhotos(this.user, this.limit, this.page).subscribe(photos => {
      this.total = photos.total;
      this.maxPage = Math.ceil(photos.total / this.limit);
      this.photos = this.photos.concat(photos.data);
      console.log('photos ', photos);
      this.load = true;
      photos.data.forEach(image => {
        this.images.push({
          url: `${this.profileService.getServerUrl() + image.url}`,
          thumbnailUrl: `${this.profileService.getServerUrl() + image.url}?w=60`,
        });
      });
    });
    this.serverUrl = this.profileService.getServerUrl();
  }
  openGallery(index: number = 0) {
    console.log('index ', index);
    this.ngxImageGallery.open(index);
  }
  onScrollDown () {
    if (this.page <= this.maxPage) {
      this.load = false;
      this.page += 1;
      this.profileService.getUserPhotos(this.user, this.limit, this.page).subscribe(photos => {
        this.photos = this.photos.concat(photos.data);
        this.load = true;
        photos.data.forEach(image => {
          this.images.push({
            url: `${this.profileService.getServerUrl() + image.url}`,
            thumbnailUrl: `${this.profileService.getServerUrl() + image.url}?w=60`,
          });
        });
      });
    }
  }
  removeFiles(postId) {
    this.modalConfirm.modalTitle = 'Delete your Photo?';
    this.modalConfirm.modalDescription = 'This action will remove your photo.';
    this.modalConfirm.btnsTitle = {
        accept: 'Yes',
        declide: 'no',
    };
    this.modalConfirm.open();
    this.postTmpId = postId;
  }
  onConfirm (confirm) {
    if (confirm) {
      this.postService.removePost(this.postTmpId).subscribe(resp => {
        if (resp.status) {
          this.photos = _.filter(this.photos, (o) => {
            return o.post_id != this.postTmpId;
          });
          this.removePost.emit(this.postTmpId);
        }
      });
    }
  }
}
