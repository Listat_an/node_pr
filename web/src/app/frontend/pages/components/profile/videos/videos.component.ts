import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { ProfileService } from '../../../../../services/profile.service';
import { PostService } from '../../../../../services/posts.service';

import * as _ from 'lodash';

import {
  ModalConfirmComponent,
} from '../../academy/modal-confirm/modal-confirm.component';

@Component({
  selector: 'opn-profile-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss'],
})
export class VideosProfileComponent implements OnInit {
  private videos: Array<any> = [];
  private limit: any = 10;
  private page: any = 1;
  private maxPage: Number;
  private total: Number;
  private load: Boolean = true;

  private user: any;
  private serverUrl: String;

  @Input() me?: any;
  @Output() removePost: EventEmitter<any> = new EventEmitter();
  @ViewChild(ModalConfirmComponent)
  public modalConfirm: ModalConfirmComponent;
  private postTmpId: any;

  constructor(
    private profileService: ProfileService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private postService: PostService,
  ) {
    this.activeRoute.params.subscribe(params => {
      this.user = params['id'];
    });
  }
  ngOnInit() {
    this.load = false;
    this.profileService.getUserVideos(this.user, this.limit, this.page).subscribe(videos => {
      this.total = videos.total;
      this.maxPage = Math.ceil(videos.total / this.limit);
      this.videos = this.videos.concat(videos.data);
      console.log('videos ', videos);
      this.load = true;
    });
    this.serverUrl = this.profileService.getServerUrl();
  }
  onScrollDown () {
    if (this.load && this.page <= this.maxPage) {
      this.page += 1;
      this.load = false;
      this.profileService.getUserVideos(this.user, this.limit, this.page).subscribe(videos => {
        this.videos = this.videos.concat(videos.data);
        this.load = true;
      });
    }
  }
  removeFiles(postId) {
    this.modalConfirm.modalTitle = 'Delete your Video?';
    this.modalConfirm.modalDescription = 'This action will remove your video.';
    this.modalConfirm.btnsTitle = {
        accept: 'Yes',
        declide: 'no',
    };
    this.modalConfirm.open();
    this.postTmpId = postId;
  }

  onConfirm (confirm) {
    if (confirm) {
      this.postService.removePost(this.postTmpId).subscribe(resp => {
        if (resp.status) {
          this.videos = _.filter(this.videos, (o) => {
            return o.post_id != this.postTmpId;
          });
          this.removePost.emit(this.postTmpId);
        }
      });
    }
  }
}
