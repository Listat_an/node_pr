import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateTechniquesComponent } from './update-technique.component';

@NgModule({
  declarations: [
    UpdateTechniquesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    UpdateTechniquesComponent,
  ],
  providers: [
  ],
  // entryComponents: [
  //   UpdateTeachersComponent,
  // ],
})
export class UpdateTechniquesModule { }
