import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    AbstractControl,
} from '@angular/forms';
import { TechniquesService } from '../../../../../services/techniques.service';

@Component({
    selector: 'opn-update-techniques',
    templateUrl: './update-technique.component.html',
    styleUrls: ['./update-technique.component.scss'],
    providers: [
        TechniquesService,
    ],
})
export class UpdateTechniquesComponent implements OnInit {

    public formTechnique: FormGroup;
    private open = false;
    private step = 1;
    private technique;

    @Input() academyId: Number;
    public added: EventEmitter < any > ;
    constructor(
        private techniquesService: TechniquesService,
        private fb: FormBuilder,
    ) {
        this.added = new EventEmitter();
    }

    ngOnInit() {
        this.buildformTechnique();
    }

    private buildformTechnique() {
        this.formTechnique = this.fb.group({
            name: ['', Validators.required],
        });
    }

    openModal() {
        document.body.classList.add('openModal');
        this.open = true;
    }

    closeModal() {
        document.body.classList.remove('openModal');
        this.open = false;
        this.step = 1;
        this.formTechnique.patchValue({
            name: '',
        });
    }

    onClose() {
        this.closeModal();
    }

    private onCreateTeacher(teacher) {
        if (this.formTechnique.valid) {
            this.technique = this.formTechnique.get('name').value;
            const obj = {
                academy_id: this.academyId,
                name: this.technique,
            };
            this.techniquesService.createTechnique(obj).subscribe(
                res => {
                    if (res.status) {
                        this.step = 2;
                        this.added.emit(obj);
                    }
                },
                err => {
                    console.log(err);
                    const errore = JSON.parse(err._body).errors;
                    const self = this;
                    errore.forEach(function (el, i, arr) {
                        if (el.status == '400') {
                            //self.toastr.info(el.description, 'Warning!');
                        }
                    });
                },
            );
        }else {
            this.validateAllFormFields(this.formTechnique);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({
                    onlySelf: true,
                });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
}
