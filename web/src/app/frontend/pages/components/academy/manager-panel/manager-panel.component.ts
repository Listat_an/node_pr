import {
    Component,
    OnInit,
    OnDestroy,
    Input,
    Output,
    ViewEncapsulation,
    EventEmitter
} from '@angular/core';
import {
    Router
} from '@angular/router';

import {
    AcademiesService
} from '../../../../../services/academies.service';
import {
    DataService
} from '../../../../../services/data.service';
import {
    AcademyMessagesService
} from '../../../../../services/academy-messages.service';
import {
    SocketService
} from '../../../../../services/socket.service';
import {
    UpgradeService
} from '../../../upgrade/upgrade.service';

import {
    Subscription,
} from 'rxjs/Subscription';
import {
    cancelSubscription,
} from '../../../../../providers/cancelSubscription';

@Component({
    selector: 'opn-manager-panel',
    templateUrl: './manager-panel.component.html',
    styleUrls: ['./manager-panel.component.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        AcademiesService,
        AcademyMessagesService,
        SocketService,
    ],
})
export class ManagerPanelComponent implements OnInit, OnDestroy {
    private openPro: Boolean = false;
    private isManager: Boolean = false;
    private isPro: Boolean = false;
    private academyLink: String = '';
    public newMessagesCount: Number = 0;

    @Input() academyId?: any;

    @Output() checkManager: EventEmitter < any > = new EventEmitter < any > ();
    @Output() onCheckPro: EventEmitter < any > = new EventEmitter < any > ();

    public subscrips: Subscription[] = [];

    constructor(
        private academyService: AcademiesService,
        private router: Router,
        private dataServices: DataService,
        private academyMessages: AcademyMessagesService,
        private upgradeService: UpgradeService,
    ) {}

    ngOnInit() {
        this.academyLink = `/academiy-datail/${this.academyId}`;
        this.statusManager();
        this.checkPro();
        this.subscrips.push(
            this.academyMessages.getUnreadMessages(this.academyId).subscribe(unreadMessages => {
                this.newMessagesCount = unreadMessages.count;
            }),
        );
        this.subscrips.push(
            this.upgradeService.upgradeEvent.subscribe(
                resp => {
                    this.isPro = resp;
                    console.log('***** ', resp);
                },
            ),
        );
    }

    public onEventMessage() {
        this.dataServices.eventMessage.emit({
            type: 'manager',
            academy_id: this.academyId,
        });
    }

    checkPro() {
        this.subscrips.push(
            this.academyService.checkPro(this.academyId).subscribe(res => {
                if (res.is_pro == 1) {
                    this.isPro = true;
                    this.onCheckPro.emit(true);
                } else {
                    this.onCheckPro.emit(false);
                }
            }),
        );
    }

    statusManager() {
        this.subscrips.push(
            this.academyService.checkUserInManager(this.academyId).subscribe(res => {
                if (res) {
                    this.isManager = true;
                    this.checkManager.emit(true);
                }
            }),
        );
    }

    onOpenPro() {
        this.router.navigate([`academiy-datail/${this.academyId}/upgrade`]);
    }

    goToLink(link) {
        this.router.navigate([this.academyLink + link]);
    }

    onSetPro() {
        this.isPro = true;
        this.onCheckPro.emit(true);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
