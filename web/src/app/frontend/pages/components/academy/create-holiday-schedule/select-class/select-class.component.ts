import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  SimpleChange,
} from '@angular/core';


@Component({
  selector: 'opn-select-class',
  templateUrl: './select-class.component.html',
  styleUrls: ['./select-class.component.scss'],
})
export class SelectClassComponent implements OnChanges {

  @Input() classes: Array<any>;
  @Input() public isOpen: Boolean = false;

  @Output() selected: EventEmitter<any> = new EventEmitter<any>();
  @Output() toggle: EventEmitter<any> = new EventEmitter<any>();

  constructor(
  ) {}

  public onSelect(item) {
    this.selected.emit(item);
  }

  ngOnChanges(changes: SimpleChanges) {
    const isOpen: SimpleChange = changes.isOpen;
    this.isOpen = isOpen.currentValue;
  }
  public onOutside() {
      this.toggle.emit();
  }

}
