import {
    NgModule,
} from '@angular/core';
import {
    CommonModule,
} from '@angular/common';
import {
    FormsModule,
} from '@angular/forms';
import {
    CreateHolidayScheduleComponent,
} from './create-holiday-schedule.component';
import {
    SelectModule,
} from 'ng-select';
import {
    AngularMultiSelectModule,
} from '../../../../../../components/angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {
    ClickOutsideModule,
} from 'ng4-click-outside';
import {
    UIDatepickerModule,
} from '../../../../../../components/ui/ui-datepicker/';
import {
    UiTimeModule,
} from '../../../../../../components/ui/ui-time/ui-time.module';
import {
    UpdateTeachersModule,
} from '../update-teachers/update-teachers.module';
import {
    UpdateTechniquesModule,
} from '../update-technique/update-technique.module';
import {
    ColorPickerModule,
} from 'ngx-color-picker';
import {
    NgDatepickerModule,
} from './ng2-datapicker/module/ng-datepicker.module';
import {
    PerfectScrollbarModule,
} from 'ngx-perfect-scrollbar';
import {
    PERFECT_SCROLLBAR_CONFIG,
} from 'ngx-perfect-scrollbar';
import {
    PerfectScrollbarConfigInterface,
} from 'ngx-perfect-scrollbar';
import { SelectClassComponent } from './select-class/select-class.component';
import { ModalConfirmModule } from '../modal-confirm/modal-confirm.module';
import { DirectivesModule } from '../../../../../../directives/directives.module';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: false,
};

@NgModule({
    entryComponents: [
        CreateHolidayScheduleComponent,
        SelectClassComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        SelectModule,
        UIDatepickerModule,
        AngularMultiSelectModule,
        ClickOutsideModule,
        UiTimeModule,
        UpdateTeachersModule,
        UpdateTechniquesModule,
        ColorPickerModule,
        NgDatepickerModule,
        PerfectScrollbarModule,
        ModalConfirmModule,
        DirectivesModule,
    ],
    declarations: [
        CreateHolidayScheduleComponent,
        SelectClassComponent,
    ],
    exports: [
        CreateHolidayScheduleComponent,
    ],
    providers: [
        {
        provide: PERFECT_SCROLLBAR_CONFIG,
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    }],
})
export class CreateHolidayScheduleModule {

}
