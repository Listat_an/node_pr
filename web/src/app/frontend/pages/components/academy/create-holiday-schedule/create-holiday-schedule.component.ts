import { filter } from 'rxjs/operator/filter';
import {
    Component,
    Input,
    OnInit,
    ViewChild,
    OnDestroy,
    Compiler,
} from '@angular/core';

import * as _dt from 'date-fns';

import {
    ActivitiesService,
} from '../../../../../services/activities.service';
import {
    ScheduleService,
} from '../../../../../services/schedule.service';

import {
    Subject,
} from 'rxjs/Subject';
import { DatepickerOptions } from './ng2-datapicker/component/ng-datepicker.component';

import * as _is from 'is_js';
import {
    ModalConfirmComponent,
} from '../modal-confirm/modal-confirm.component';

@Component({
    selector: 'opn-create-holiday-schedule',
    templateUrl: './create-holiday-schedule.component.html',
    styleUrls: ['./create-holiday-schedule.component.scss'],
    providers: [ActivitiesService, ScheduleService],
    entryComponents: [],
})

export class CreateHolidayScheduleComponent implements OnInit, OnDestroy {
    private step: any = 0;
    public openType = 'Create';
    public open: Boolean = false;
    public openTime: Boolean = false;
    private ngUnsubscribe: Subject <void> = new Subject <void> ();
    public acceptCreate: Boolean = false;
    public classes: Array<any> = [];
    public openClasses: Boolean = false;
    public selectClass: any;
    public selectDate: any;

    @Input() academyId: any;

    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;

    public date: Date = new Date(Date.now());
    public options: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2030,
        displayFormat: 'MMM D[,] YYYY',
        barTitleFormat: 'MMMM YYYY',
        dayNamesFormat: 'dd',
        firstCalendarDay: 1,
        barTitleIfEmpty: 'Click to select a date',
        useEmptyBarTitle: false, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown 
    };

    public time: any = {
        start: new Date(),
        end: _dt.addHours(new Date(), 1),
    };

    public holidayClasses: any = [];
    private tmpClass: any;
    private removeItem: String;
    public createProcess: Boolean = false;

    constructor(
        private activitiSrvc: ActivitiesService,
        private scheduleSrvc: ScheduleService,
        private compiler: Compiler,
    ) {}

    ngOnInit() {
        this.getClases();
    }

    private parseShedules(data) {
        data.forEach(schedule => {
            const [h1, m1] = schedule.time_start.split('.');
            const [h2, m2] = schedule.time_end.split('.');
            const time_start = _dt.setHours(_dt.setMinutes(new Date(), m1), h1);
            const time_end = _dt.setHours(_dt.setMinutes(new Date(), m2), h2);
            this.holidayClasses.push({
                openClass: false,
                openTime: false,
                class_id: schedule.class_id,
                class_name: schedule.class.name,
                days: [{
                    id: schedule.id,
                    day: schedule.day,
                    time_start: time_start,
                    time_end: time_end,
                }],
            });
        });
        this.acceptCreate = true;
    }

    private getClases() {
        if (this.academyId) {
            this.activitiSrvc.getAllClassesByAcademy(this.academyId)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(resp => {
                this.classes = resp.data;
                this.selectClass = resp.data[0];
            });
        }
    }

    private getSchedules(event) {
        this.scheduleSrvc.getSchedulesForDay(event.index, this.academyId)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(resp => {
                this.parseShedules(resp.data);
            });
    }

    private clearForm() {
        this.step = 0;
        this.holidayClasses = [];
        this.acceptCreate = false;
        this.openTime = false;
        this.compiler.clearCache();
    }

    public openModal() {
        this.step = 1;
        this.time = {
            start: new Date(),
            end: _dt.addHours(new Date(), 1),
        };
        this.open = true;
        document.body.classList.add('openModal');
    }

    public openUpdateModal(event) {
        document.body.classList.add('openModal');
        this.date = new Date(event.index + ' 00:00:00');
        this.selectDate = this.date;
        this.getSchedules(event);
        this.step = 2;
       
        this.time = {
            start: new Date(),
            end: _dt.addHours(new Date(), 1),
        };
        this.open = true;
        this.openType = 'Update';
    }

    public closeModal() {
        this.step = 0;
        this.date = new Date();
        this.clearForm();
        this.open = false;
        this.selectDate = null;
        document.body.classList.remove('openModal');
        this.openType = 'Create';
    }

    public onNextStep() {
        console.log(this.date);
        switch (this.step) {
            case 1:
                {
                    if (this.date) {
                        this.step++;
                    }
                    break;
                }
        }
    }
    private convertTime12to24(time12h) {
        const [time, modifier] = time12h.split(' ');
        let [hours, minutes] = time.split(':');
        if (modifier === 'pm') {
            if ( hours == 12) {
                hours = 12;
            }else {
                hours = parseInt(hours, 10) + 12;
            }
        }
        if (modifier === 'am' && hours == 12) {
            hours = 24;
        }
        return hours + '.' + minutes;
    }

    public onAddHoliday() {
        this.holidayClasses.push({
            openClass: false,
            openTime: false,
            class_id: this.selectClass.id,
            class_name: this.selectClass.name,
            days: [{
                day: _dt.format(this.date, 'ddd'),
                time_start: this.time.start,
                time_end: this.time.end,
            }],
        });
        this.acceptCreate = true;
    }

    public onERemoveClass(_class) {
        this.modalConfirm.modalTitle = 'Delete your class?';
        this.modalConfirm.modalDescription = 'This action will remove your class.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
        this.tmpClass = _class;
        this.removeItem = 'EClass';
    }

    onConfirm(confirm) {
        if (confirm) {
            switch (this.removeItem) {
                case 'EClass': {
                    this.holidayClasses = this.holidayClasses.filter(IClass => IClass != this.tmpClass);
                    this.tmpClass = null;
                    if (this.holidayClasses.length == 0) {
                        this.acceptCreate = false;
                    }
                    break;
                }
                case 'Class': {
                    this.scheduleSrvc.scheduleDelete({date: _dt.format(this.date, 'YYYY-MM-DD')})
                    .takeUntil(this.ngUnsubscribe)
                    .subscribe(resp => {
                        this.closeModal();
                        this.activitiSrvc.fireHoliday(resp);
                    });
                    break;
                }
            }
        }
    }

    public toggleeditClasses(e, _holiday) {
        _holiday.openClass = !_holiday.openClass;
        e.stopPropagation();
    }
    public toggleEditTime(e, _holiday) {
        _holiday.openTime = !_holiday.openTime;
        e.stopPropagation();
    }

    public onCreateHolidays() {
        let _data = this.holidayClasses.map(el => {
            let data = {
                academy_id: this.academyId,
                class_ids: el.class_id.toString(),
                date: _dt.format(this.date, 'YYYY-MM-DD'),
                days: el.days.map(day => {
                    return {
                        id: day.id,
                        day: day.day,
                        time_start: this.convertTime12to24(_dt.format(day.time_start, 'hh:mm a')),
                        time_end: this.convertTime12to24(_dt.format(day.time_end, 'hh:mm a')),
                        holiday: 1,
                    };
                }),
            };

            return data;
        });
        if (this.openType == 'Create') {
            this.createProcess = true;
            this.scheduleSrvc.createHoliday(_data)
            .takeUntil(this.ngUnsubscribe)
            .finally(() => {
                this.createProcess = false;
            })
            .subscribe(resp => {
                if (resp.status) {
                    this.step = 3;
                    this.activitiSrvc.fireHoliday(resp);
                }
            });
        }else if (this.openType == 'Update') {
            this.createProcess = true;
            this.scheduleSrvc.updateSchedule(_data)
            .takeUntil(this.ngUnsubscribe)
            .finally(() => {
                this.createProcess = false;
            })
            .subscribe(resp => {
                if (resp.status) {
                    this.step = 3;
                    this.activitiSrvc.fireHoliday(resp);
                }
            });
        }
    }

    public toggleCreateClasses(e) {
        this.openClasses = !this.openClasses;
        e.stopPropagation();
    }

    public closeCreateClasses() {
        this.openClasses = false;
    }

    public onCSelected(item) {
        this.openClasses = false;
        this.selectClass = item;
    }

    public onESelected(item, _holiday) {
        _holiday.class_id = item.id;
        _holiday.class_name = item.name;
        _holiday.openClass = false;
        console.log('this.holidayClasses ', this.holidayClasses);
    }

    setTimeToStartDate(time, _holiday?, _i?) {
        const [hours, minutes, seconds] = time.split(':');
        const date = new Date();
        if (_holiday) {
            _holiday.days[_i].time_start = _dt.setHours(_dt.setMinutes(_holiday.days[_i].time_start, minutes), hours);
            if (_dt.differenceInMinutes(_holiday.days[_i].time_start, _holiday.days[_i].time_end) > 0) {
                _holiday.days[_i].time_end = _dt.addHours(_holiday.days[_i].time_start, 1);
            }
            return;
        }

        this.time.start = _dt.setHours(_dt.setMinutes(this.time.start, minutes), hours);
        if (_dt.differenceInMinutes(this.time.start, this.time.end) > 0) {
            this.time.end = _dt.addHours(this.time.start, 1);
        }
    }

    setTimeToEndDate(time, _holiday?, _i?) {
        const [hours, minutes, seconds] = time.split(':');
        const date = new Date();
        if (_holiday) {
            _holiday.days[_i].time_end = _dt.setHours(_dt.setMinutes(_holiday.days[_i].time_end, minutes), hours);
            if (_dt.differenceInMinutes(_holiday.days[_i].time_end, _holiday.days[_i].time_start) < 0) {
                _holiday.days[_i].time_start = _dt.subHours(_holiday.days[_i].time_end, 1);
            }
            return;
        }

        this.time.end = _dt.setHours(_dt.setMinutes(this.time.end, minutes), hours);
        if (_dt.differenceInMinutes(this.time.end, this.time.start) < 0) {
            this.time.start = _dt.subHours(this.time.end, 1);
        }
    }

    onChangeStartTime(time) {
        this.setTimeToStartDate(time);
    }
    onChangeEndTime(time) {
        this.setTimeToEndDate(time);
    }

    onEChangeStartTime(time, _holiday, _i) {
        this.setTimeToStartDate(time, _holiday, _i);
    }
    onEChangeEndTime(time, _holiday, _i) {
        this.setTimeToEndDate(time, _holiday, _i);
    }

    public toggleTime(e) {
        this.openTime = !this.openTime;
        e.stopPropagation();
    }

    public onOutside(e) {
        this.toggleTime(e);
    }

    public onEditOutside(e, hd) {
        this.toggleEditTime(e, hd);
    }

    public onRemove() {
        this.removeItem = 'Class';
        this.modalConfirm.modalTitle = 'Delete your holiday?';
        this.modalConfirm.modalDescription = 'This action will remove your holiday.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
