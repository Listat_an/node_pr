import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ConfigService} from '../../../../../../services/service.config';
import {ShopService} from '../../../../../../services/shop.service';

@Component({
    selector: 'opn-academy-product-card',
    templateUrl: './academy-product-card.html',
    styleUrls: ['./academy-product-card.scss'],
})
export class AcademyProductCardComponent implements OnInit {
    @Input() product;
    baseUrl = ConfigService.URL_SERVER;
    noPhoto = '/assets/images/no-photo-product.png';

    constructor(private router: Router,
                private shopService: ShopService) {
    }

    ngOnInit() {
    }

    addProductToCart() {
        this.shopService.addToCart(this.product.id, this.product.images[0] ? this.product.images[0].url : '', 1).subscribe(res => {
            this.product.isAdded = true;
        });
    }

    goToProduct() {
        this.router.navigate([`academiy-datail/${this.product.academy.id}/product/${this.product.id}`])
    }
}
