import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ShopService} from '../../../../../services/shop.service';

@Component({
    selector: 'opn-shop-tab',
    templateUrl: './shop-tab.component.html',
    styleUrls: ['./shop-tab.component.scss'],
    providers: [ShopService],
    encapsulation: ViewEncapsulation.None
})
export class ShopTabComponent implements OnInit {
    academyId;
    productName: string = '';
    products;
    cardsPerPagination = 8;
    limit = this.cardsPerPagination;
    productsCount = 0;
    isPriceIncreasing: Boolean = true;

    constructor(private shopService: ShopService,
                private activeRoute: ActivatedRoute,) {
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.academyId = params['id'];
            this.activeRoute.queryParams.subscribe(res => {
                this.productName = res['productName'] || '';
                this.searchProducts()
            });
        });
    }

    searchProducts() {
        this.shopService.getAcademyProducts(this.academyId, this.productName, this.isPriceIncreasing, this.limit).subscribe(productsRes => {
            this.shopService.getUserCart().subscribe(cartRes => {
                const cart = cartRes['data'];
                this.products = productsRes['data'];
                this.products.forEach(product => {
                    product['isAdded'] = cart.find(cartProduct => product.id === cartProduct.product_id)
                });
                this.productsCount = productsRes['total'];
            });
        });
    }

    toggleSortByPrice() {
        this.isPriceIncreasing = !this.isPriceIncreasing;
        this.searchProducts();
    }

    onViewMore() {
        this.limit += this.cardsPerPagination;
        this.searchProducts()
    }

}
