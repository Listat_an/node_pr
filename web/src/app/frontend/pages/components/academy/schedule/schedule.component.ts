import {
    Component,
    ChangeDetectionStrategy,
    ViewChild,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from '@angular/core';
import {
    isSameDay,
    isSameMonth,
} from 'date-fns';
import {
    Subject,
} from 'rxjs/Subject';
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarEventTitleFormatter,
    CalendarMonthViewDay,
} from 'angular-calendar';

import {
    Router,
} from '@angular/router';

import { ScheduleService } from '../../../../../services/schedule.service';
import {
    ISubscription,
} from 'rxjs/Subscription';

import {
    ModalConfirmComponent,
} from '../modal-confirm/modal-confirm.component';
import * as _ from 'lodash';


@Component({
    selector: 'opn-academy-schrdule-tab',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './schedule.component.html',
    styleUrls: ['./schedule.component.scss'],
    providers: [
        ScheduleService,
    ],
})
export class ScheduleAcademyComponent implements OnInit, OnDestroy {

    private ngUnsubscribe: Subject < void > = new Subject < void > ();
    constructor(
        private scheduleSrvc: ScheduleService,
        private router: Router,
    ) {}

    ngOnInit() {}


    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
