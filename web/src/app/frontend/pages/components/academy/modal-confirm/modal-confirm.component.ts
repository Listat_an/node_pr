import { Component, HostListener, Input, Output, OnInit, EventEmitter } from '@angular/core';
/**
 * ModalComponent - This class represents the modal component.
 * @requires Component
 */

@Component({
  selector: 'opn-confirm-modal',
  styleUrls: ['./modal-confirm.component.scss'],
  templateUrl: './modal-confirm.component.html',
})

export class ModalConfirmComponent implements OnInit {

  isOpen: Boolean = false;

  @Input() public modalTitle?: string;
  @Input() public modalDescription?: String = 'This action will remove your form system and you can not receover the post anymore.';
  @Input() btnsTitle?: any = {
    accept: 'Yse',
    declide: 'No',
  };

  @Output() confirm: EventEmitter <boolean> = new EventEmitter <boolean>();
  @HostListener('document:keyup', ['$event'])

  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.isOpen = false;
    }
  }

  constructor() { }

  ngOnInit() {}

  onConfirm(confirm): void {
    this.confirm.emit(confirm);
    this.close();
  }

  open(): void {
    this.isOpen = true;
  }

  close(): void {
    this.isOpen = false;
  }
}
