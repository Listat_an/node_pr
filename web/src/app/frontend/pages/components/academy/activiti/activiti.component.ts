import {
    Component,
    ChangeDetectionStrategy,
    ViewChild,
    OnInit,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from '@angular/core';
import * as _d from 'date-fns';
import {
    Subject,
} from 'rxjs/Subject';


import {
    Router,
} from '@angular/router';
import {
    ScheduleService,
} from '../../../../../services/schedule.service';
import {
    ActivitiesService,
} from '../../../../../services/activities.service';
import {
    SubscriptionsService,
} from '../../../../../services/subscriptions.service';
import {
    Broadcaster,
} from '../../../../../services/broadcaster';
import * as _ from 'lodash';

import {
    IOption,
} from 'ng-select';

const types: Object = {
    'class': 'Class',
    'seminar': 'Seminar',
    'mat_event': 'Opn Mat',
    'others': 'Others',
};

@Component({
    selector: 'opn-academy-activiti',
    // changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './activiti.component.html',
    styleUrls: ['./activiti.component.scss'],
    providers: [
        ActivitiesService,
        SubscriptionsService,
    ],
})
export class ActivitiAcademyComponent implements OnInit, OnDestroy {
    public nowDate = new Date();
    private ngUnsubscribe: Subject < void > = new Subject < void > ();
    public scheduleSource: Array < any > ;
    public scheduleLoad = false;
    public subscriptions: Array < any > = [];
    public selectedSubscriptions = [];
    public selectedPayment = [];
    public selectedType: any = {
        title: 'Class',
        value: 'class',
    };
    public currentType: String = 'class';

    subscriptionsSelectSettings = {
        text: 'All Subscriptions',
        primaryKey: 'id',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        badgeShowLimit: 1,
        classes: 'myclass custom-class-search',
    };

    priceSelectSettings = {
        text: 'All Price',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: false,
        singleSelection: true,
        classes: 'myclass custom-class-search',
    };

    @Input() academyId: any;
    @Input() isManager: any;
    @Output() openUpdate: EventEmitter < any > = new EventEmitter < any > ();

    public typesOptions: Array < any > = [{
            title: 'Event',
            value: 'mat_event',
        },
        {
            title: 'Seminar',
            value: 'seminar',
        },
        {
            title: 'Class',
            value: 'class',
        },
        {
            title: 'Others',
            value: 'others',
        },
    ];
    public priceOptions = [
        {
            itemName: 'Free',
            id: 'free',
        },
        {
            itemName: 'Paid',
            id: 'fee',
        },
    ];

    constructor(
        private activitiService: ActivitiesService,
        private broadcaster: Broadcaster,
        private router: Router,
        private scheduleSrvc: ScheduleService,
        private subscriptionsSrvc: SubscriptionsService,
    ) {}

    ngOnInit() {
        if (this.academyId) {
            this.initSchedule();
            this.getSubscriptions();
        }
    }
    getItems() {
        this.scheduleLoad = false;
        this.initSchedule();
    }
    private initSchedule() {
        const from = _d.startOfWeek(new Date(), {
            weekStartsOn: 1,
        });

        const to = _d.endOfWeek(new Date(), {
            weekStartsOn: 1,
        });
        this.scheduleSrvc.getScheduleForStudent(from, to, this.academyId, this.selectedSubscriptions, this.selectedPayment, this.selectedType)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(resp => {
                if (resp.status) {
                    this.scheduleSource = resp.data;
                    this.scheduleLoad = true;
                }
            });
    }

    getSubscriptions() {
        this.subscriptionsSrvc.getSybscriptionsByAcademy(this.academyId)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(resp => {
                console.log('getSubscriptions ', resp);
                resp.map(el => {
                    this.subscriptions.push({
                        id: el.id,
                        itemName: el.name,
                    });
                });
            });
    }

    public onTypeSelect(type) {
        this.currentType = type.value;
        this.selectedType = type;
        this.getItems();
    }

    public onEventClick(event) {
        if (event && event.role == 'student' && event.type != 'class') {
            this.router.navigate(['activity', event.id]);
        }
    }

    ngOnDestroy() {
        console.log('ngOnDestroy');
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
