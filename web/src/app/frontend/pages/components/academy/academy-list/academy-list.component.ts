import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  ElementRef,
} from '@angular/core';

import {
  Router,
} from '@angular/router';

import { AcademiesService } from '../../../../../services/academies.service';

@Component({
  selector: 'opn-list-academy',
  templateUrl: './academy-list.component.html',
  styleUrls: ['./academy-list.component.scss'],
  providers: [AcademiesService],
})
export class ViewListAcademyComponent implements OnInit {

  @Input() academy: any;
  @Output() onSelect: EventEmitter<any> = new EventEmitter<any> ();

  private ServerUrl: String;
  private photoUrl: String;
  constructor(
    private service: AcademiesService,
    private router: Router,
  ) {
    this.ServerUrl = this.service.getServerUrl();
  }

  ngOnInit() {
    console.log('Academy list: ', this.academy);
    if (!this.academy.photo || this.academy.photo == '') {
      this.photoUrl = `url(/assets/images/academy-logo.jpg)`;
    }else {
      this.photoUrl = `url(${this.ServerUrl}${this.academy.photo}?${new Date().getTime()})`;
    }
  }
  onToAcademy() {
    this.onSelect.emit(true);
    if ( this.academy.is_pro ) {
      this.router.navigate(['academiy-datail', this.academy.id, 'dashboard']);
      return;
    }
    this.router.navigate(['academiy-datail', this.academy.id]);
  }
}
