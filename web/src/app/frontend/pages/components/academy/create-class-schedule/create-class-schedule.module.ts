import {
    NgModule,
} from '@angular/core';
import {
    CommonModule,
} from '@angular/common';
import {
    FormsModule,
} from '@angular/forms';
import {
    CreateClassScheduleComponent,
} from './create-class-schedule.component';
import {SelectModule} from 'ng-select';
import {
    AngularMultiSelectModule,
} from '../../../../../../components/angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {
    ClickOutsideModule,
} from 'ng4-click-outside';
import {
    UIDatepickerModule,
} from '../../../../../../components/ui/ui-datepicker/';
import {
    UiTimeModule,
} from '../../../../../../components/ui/ui-time/ui-time.module';
import { UpdateTeachersModule } from '../update-teachers/update-teachers.module';
import { UpdateTechniquesModule } from '../update-technique/update-technique.module';
import { ColorPickerModule } from 'ngx-color-picker';
import { ModalConfirmModule } from '../modal-confirm/modal-confirm.module';

@NgModule({
    entryComponents: [
        CreateClassScheduleComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        SelectModule,
        UIDatepickerModule,
        AngularMultiSelectModule,
        ClickOutsideModule,
        UiTimeModule,
        UpdateTeachersModule,
        UpdateTechniquesModule,
        ColorPickerModule,
        ModalConfirmModule,
    ],
    declarations: [
        CreateClassScheduleComponent,
    ],
    exports: [
        CreateClassScheduleComponent,
    ],
})
export class CreateClassScheduleModule {

}
