import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    OnDestroy,
    ViewChild,
    ElementRef,
} from '@angular/core';
import {
    DatePipe,
} from '@angular/common';

import {
    addHours,
    setHours,
    setMinutes,
    subHours,
    format,
    differenceInMinutes,
} from 'date-fns';

import {
    ActivitiesService,
} from '../../../../../services/activities.service';
import {
    ScheduleService,
} from '../../../../../services/schedule.service';
import {
    AcademiesService,
} from '../../../../../services/academies.service';
import {
    Broadcaster,
} from '../../../../../services/broadcaster';
import {
    UiSelectComponent,
} from '../../../../../../components/ui/ui-select/ui-select.component';

import {
    Subject,
} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import {
    UpdateTeachersComponent,
} from '../update-teachers/update-teachers.component';
import {
    UpdateTechniquesComponent,
} from '../update-technique/update-technique.component';
import {
    IOption,
} from 'ng-select';

import * as _is from 'is_js';
import {
    ModalConfirmComponent,
} from '../modal-confirm/modal-confirm.component';

@Component({
    selector: 'opn-create-class-schedule',
    templateUrl: './create-class-schedule.component.html',
    styleUrls: ['./create-class-schedule.component.scss'],
    providers: [ActivitiesService, AcademiesService, DatePipe, ScheduleService],
    entryComponents: [UiSelectComponent],
})

export class CreateClassScheduleComponent implements OnInit, OnDestroy {
    private step: any = 1;
    private openType: any = 'Create';
    private dateOptions: Object;
    private startDate = new Date();

    private subjectField2: Subject < string > = new Subject();
    private subjectField3: Subject < string > = new Subject();

    private createDisabled: Boolean = false;


    @Input() academyId: any;

    @Output() close: EventEmitter < any > = new EventEmitter < any > ();

    @ViewChild(UpdateTeachersComponent)
    public updateTeachersComponent: UpdateTeachersComponent;
    @ViewChild(UpdateTechniquesComponent)
    public updateTechniquesComponent: UpdateTechniquesComponent;
    @ViewChild('elDays') elDays: ElementRef;

    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;

    private ngUnsubscribe: Subject < void > = new Subject < void > ();

    modelEvent: any;
    acceptStep2: Boolean = false;
    acceptCreate: Boolean = false;
    time: any = {
        start: new Date(),
        end: addHours(new Date(), 1),
    };

    teacherList = [];
    teachersSettings = {};

    subscriptionsList = [];
    subscriptionSettings = {};

    techniqueList = [];
    techniquesSettings = {};
    public open = false;
    public academy: any;
    public colorBorder: any = 'rgba(159, 231, 173)';
    public colorBg: any = 'rgba(159, 231, 173, 0.65)';

    public days: Array < IOption > = [{
            label: 'Mon',
            value: 'Mon',
        },
        {
            label: 'Tue',
            value: 'Tue',
        },
        {
            label: 'Wed',
            value: 'Wed',
        },
        {
            label: 'Thu',
            value: 'Thu',
        },
        {
            label: 'Fri',
            value: 'Fri',
        },
        {
            label: 'Sat',
            value: 'Sat',
        },
        {
            label: 'Sun',
            value: 'Sun',
        },
    ];

    public disableAddDay = true;
    public clearDaySelect = false;
    public createProcess = false;

    constructor(
        private activitiService: ActivitiesService,
        private academySrvc: AcademiesService,
        private scheduleSrvc: ScheduleService,
    ) {}

    ngOnInit() {
        this.getAcademyInfo();
        this.modelEvent = {
            type: 'class',
            name: '',
            subscriptions: [],
            amount: '1000000',
            start: this.time.start,
            end: this.time.end,
            teachers: [],
            techniques: [],
            days: [],
            color_border: this.colorBorder,
            color_bg: this.colorBg,
        };

        this.dateOptions = {
            minYear: 1970,
            maxYear: 2080,
            displayFormat: 'M[/] D[/] YYYY',
            barTitleFormat: 'MMMM YYYY',
            firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
        };

        this.getSubscriptions();
        this.modelEvent.subscriptions = [];
        this.subscriptionSettings = {
            singleSelection: false,
            text: '',
            enableSearchFilter: true,
        };

        this.getTeachers();
        this.teachersSettings = {
            singleSelection: false,
            text: '',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: 'myclass custom-class',
        };

        this.getTechniques();
        this.techniquesSettings = {
            singleSelection: false,
            text: '',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: 'myclass custom-class',
        };

        this.subjectField2.debounceTime(500).subscribe((searchValue) => {
            this.acceptStep2 = this.checkStep2();
        });
        this.subjectField3.debounceTime(500).subscribe((searchValue) => {
            this.acceptCreate = this.checkStep3();
        });

        this.updateTeachersComponent.added.subscribe(teacher => {
            this.teacherList.push({
                id: teacher.id,
                itemName: teacher.first_name + ' ' + teacher.last_name,
            });
        });

        this.updateTechniquesComponent.added.subscribe(technique => {
            this.techniqueList.push({
                id: technique.id,
                itemName: technique.name,
            });
        });
    }

    private clearData() {
        this.time = {
            start: new Date(),
            end: addHours(new Date(), 1),
        };
        this.modelEvent.start = this.time.start;
        this.modelEvent.end = this.time.end;
        this.modelEvent.teachers = [];
        this.modelEvent.techniques = [];
        this.modelEvent.days = [];
        this.modelEvent.subscriptions = [];
        this.modelEvent.name = '';

        delete this.modelEvent.class_ids;
        this.colorBorder = 'rgba(159, 231, 173)';
        this.colorBg = 'rgba(159, 231, 173, 0.65)';

        this.step = 1;
        this.acceptStep2 = false;
        this.acceptCreate = false;
    }

    public onSelectColor(color: String) {
        this.modelEvent.color_border = this.colorBorder = color;
        this.modelEvent.color_bg = this.colorBg = color.slice(0, -1) + ', 0.6)';
    }

    private getAcademyInfo() {
        this.academySrvc.getAcademyById(this.academyId).subscribe(academy => {
            this.academy = academy;
            this.modelEvent = {
                ...this.modelEvent,
                academy_id: this.academy.id,
                location: this.academy.location,
                longitude: this.academy.longitude,
                latitude: this.academy.latitude,
            };
        });
    }
    onDaySelect(e) {
        this.disableAddDay = false;
    }

    onAddDay(elDay) {
        this.modelEvent.days.push({
            day: elDay.value,
            time_start: format(this.time.start, 'h:mm a'),
            time_end: format(this.time.end, 'h:mm a'),
        });

        this.disableAddDay = true;
        elDay.clear();
        this.subjectField2.next();
    }

    onRemoveDay(i) {
        this.modelEvent.days.splice(i, 1);
        this.subjectField2.next();
    }

    getSubscriptions() {
        this.activitiService.getSubscriptionsByAcademy(this.academyId).subscribe(subscriptions => {
            this.subscriptionsList = subscriptions;
            console.log('subscriptions ', subscriptions);
        });
    }

    getTeachers() {
        this.activitiService.getTeachersByAcademy(this.academyId).subscribe(teachers => {
            this.teacherList = teachers;
        });
    }

    getTechniques() {
        this.activitiService.getTechniquesByAcademy(this.academyId).subscribe(techniques => {
            this.techniqueList = techniques;
        });
    }

    onSubscriptionSelect(item: any) {
        this.subjectField2.next();
    }
    OnSubscriptionDeSelect(item: any) {
        this.subjectField2.next();
    }

    onTeacherSelect(item: any) {
        this.subjectField3.next();
    }
    OnTeacherDeSelect(item: any) {
        this.subjectField3.next();
    }
    onSelectAllTeacher(items: any) {
        this.subjectField3.next();
    }
    onDeSelectAllTeacher(items: any) {
        this.subjectField3.next();
    }

    onTechniqueSelect(item: any) {
        this.subjectField3.next();
    }
    OnTechniqueDeSelect(item: any) {
        this.subjectField3.next();
    }
    onSelectAllTechnique(items: any) {
        this.subjectField3.next();
    }
    onDeSelectAllTechnique(items: any) {
        this.subjectField3.next();
    }

    checkStep2() {
        if (_is.not.empty(this.modelEvent.days) && _is.not.empty(this.modelEvent.subscriptions) && this.modelEvent.name != '') {
            return true;
        } else {
            return false;
        }
    }

    checkStep3() {
        if (this.modelEvent.teachers.length > 0 && this.modelEvent.techniques.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    onChangeField2() {
        this.subjectField2.next();
    }

    onChangeField3() {
        this.subjectField3.next();
    }
    onNextStep() {
        switch (this.step) {
            case 1:
                {
                    if (this.acceptStep2) {
                        this.step++;
                    }
                    break;
                }
        }
    }

    onPevStep() {
        this.step--;
        if (this.step == 2) {
            this.acceptStep2 = this.checkStep2();
        }
    }

    setTimeToStartDate(time) {
        const [hours, minutes, seconds] = time.split(':');
        const date = new Date();
        this.time.start = setHours(setMinutes(this.time.start, minutes), hours);
        if (differenceInMinutes(this.time.start, this.time.end) > 0) {
            this.time.end = addHours(this.time.start, 1);
        }
    }

    setTimeToEndDate(time) {
        const [hours, minutes, seconds] = time.split(':');
        const date = new Date();
        this.time.end = setHours(setMinutes(this.time.end, minutes), hours);
        if (differenceInMinutes(this.time.end, this.time.start) < 0) {
            this.time.start = subHours(this.time.end, 1);
        }
    }

    onChangeStartTime(time) {
        console.log('onChangeStartTime ', time);
        this.setTimeToStartDate(time);
    }
    onChangeEndTime(time) {
        this.setTimeToEndDate(time);
    }

    private convertTime12to24(time12h) {
        const [time, modifier] = time12h.split(' ');
        let [hours, minutes] = time.split(':');
        if (modifier === 'pm') {
            if ( hours == 12) {
                hours = 12;
            }else {
                hours = parseInt(hours, 10) + 12;
            }
        }
        if (modifier === 'am' && hours == 12) {
            hours = 24;
        }
        return hours + '.' + minutes;
    }

    onCreateActivity() {
        if (this.acceptCreate) {
            this.createDisabled = true;
            delete this.modelEvent.start;
            delete this.modelEvent.end;

            this.modelEvent.days = this.modelEvent.days.map(el => {
                return {
                    id: el.id,
                    day: el.day,
                    time_start: this.convertTime12to24(el.time_start),
                    time_end: this.convertTime12to24(el.time_end),
                };
            });

            if (this.openType == 'Create') {
                this.createProcess = true;
                this.activitiService.createActivity(this.modelEvent)
                    .takeUntil(this.ngUnsubscribe)
                    .finally(() => {
                        this.createProcess = false;
                    })
                    .subscribe(res => {
                        if (res.status) {
                            this.activitiService.fire(res.activity);
                            this.step = 3;
                        }
                    });
            }

            if (this.openType == 'Update') {
                this.createProcess = true;
                this.scheduleSrvc.updateSchedule(this.modelEvent)
                    .takeUntil(this.ngUnsubscribe)
                    .finally(() => {
                        this.createProcess = false;
                    })
                    .subscribe(res => {
                        const id = this.modelEvent.class_ids;
                        delete this.modelEvent.class_ids;
                        delete this.modelEvent.days;
                        this.modelEvent.start_date = '2018-11-08 23:26:17';
                        this.modelEvent.end_date = '2018-11-08 23:26:17';
                        this.activitiService.updateActivity(this.modelEvent, id)
                        .takeUntil(this.ngUnsubscribe)
                        .subscribe(resp => {
                            if (resp.status) {
                                this.activitiService.fire(res.activity);
                                this.step = 3;
                            }
                        });
                    });
            }
        }
    }

    public onOpen() {
        document.body.classList.add('openModal');
        this.open = true;
    }

    onRemove() {
        this.modalConfirm.modalTitle = 'Delete your class?';
        this.modalConfirm.modalDescription = 'This action will remove your class.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
    }

    public onConfirm(confirm) {
        if (confirm) {
            this.activitiService.removeActivity(this.modelEvent.class_ids)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(resp => {
                this.onClose();
                this.activitiService.fire(resp);
            });
        }
    }

    private getClass(classId) {
        this.activitiService.getScheduleClass(classId)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(resp => {
                if (resp) {
                    this.colorBorder = resp.color_border;
                    this.colorBg = resp.color_bg;
                    this.modelEvent.name = resp.name;
                    this.modelEvent.class_ids = <String>resp.id.toString();
                    if (resp.subscriptions) {
                        resp.subscriptions.map(subscription => {
                            this.modelEvent.subscriptions.push({
                                id: subscription.id,
                                itemName: subscription.name,
                            });
                        });
                    }
                    if (resp.teachers) {
                        resp.teachers.map(teacher => {
                            this.modelEvent.teachers.push({
                                id: teacher.id,
                                itemName: `${teacher.first_name} ${teacher.last_name}`,
                            });
                        });
                    }

                    if (resp.techniques) {
                        resp.techniques.map(technique => {
                            this.modelEvent.techniques.push({
                                id: technique.id,
                                itemName: technique.name,
                            });
                        });
                    }

                    this.parseDays(resp.shedules);

                    this.acceptStep2 = true;
                    this.acceptCreate = true;
                    this.createDisabled = false;
                }
            });
    }

    private parseDays(days) {
        days.forEach(day => {
            if (!day.holiday) {
                const [h1, m1] = day.time_start.split('.');
                const [h2, m2] = day.time_end.split('.');
                const time_start = setHours(setMinutes(new Date(), m1), h1);
                const time_end = setHours(setMinutes(new Date(), m2), h2);
                this.modelEvent.days.push({
                    id: day.id,
                    day: day.day,
                    time_start: format(time_start, 'h:mm a'),
                    time_end: format(time_end, 'h:mm a'),
                });
            }
        });
    }

    public onOpenUpdate(id) {
        this.openType = 'Update';
        this.getClass(id);
        document.body.classList.add('openModal');
        this.open = true;
    }

    onClose() {
        document.body.classList.remove('openModal');
        this.open = false;
        this.clearData();
        this.openType = 'Create';
    }

    openNewTeacher() {
        this.updateTeachersComponent.openModal();
    }
    openNewTechnique() {
        this.updateTechniquesComponent.openModal();
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
