import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UpdateTeachersComponent } from './update-teachers.component';

@NgModule({
  declarations: [
    UpdateTeachersComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    UpdateTeachersComponent,
  ],
  providers: [
  ],
  // entryComponents: [
  //   UpdateTeachersComponent,
  // ],
})
export class UpdateTeachersModule { }
