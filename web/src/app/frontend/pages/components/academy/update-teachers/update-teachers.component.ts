import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
} from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    AbstractControl,
} from '@angular/forms';
import { TeachersService } from '../../../../../services/teachers.service';

@Component({
    selector: 'opn-update-teachers',
    templateUrl: './update-teachers.component.html',
    styleUrls: ['./update-teachers.component.scss'],
    providers: [
        TeachersService,
    ],
})
export class UpdateTeachersComponent implements OnInit {

    public formTeacher: FormGroup;
    private open = false;
    private step = 1;
    private teacher;

    @Input() academyId: Number;
    public added: EventEmitter < any > ;
    constructor(
        private teacherService: TeachersService,
        private fb: FormBuilder,
    ) {
        this.added = new EventEmitter();
    }

    ngOnInit() {
        this.buildFormTeacher();
    }

    private buildFormTeacher() {
        this.formTeacher = this.fb.group({
            name: ['', Validators.required],
        });
    }

    openModal() {
        document.body.classList.add('openModal');
        this.open = true;
    }

    closeModal() {
        document.body.classList.remove('openModal');
        this.open = false;
        this.step = 1;
        this.formTeacher.patchValue({
            name: '',
        });
    }

    onClose() {
        this.closeModal();
    }

    private onCreateTeacher(teacher) {
        if (this.formTeacher.valid) {
            this.teacher = this.formTeacher.get('name').value;
            const [first_name, last_name] = this.teacher.replace(' ', '|').split('|');
            const obj = {
                academy_id: this.academyId,
                first_name: first_name,
                last_name: (last_name) ? last_name : '',
            };
            this.teacherService.createTeacher(obj).subscribe(
                res => {
                    if (res.status) {
                        this.step = 2;
                        this.added.emit(obj);
                    }
                },
                err => {
                    console.log(err);
                    const errore = JSON.parse(err._body).errors;
                    const self = this;
                    errore.forEach(function (el, i, arr) {
                        if (el.status == '400') {
                            //self.toastr.info(el.description, 'Warning!');
                        }
                    });
                },
            );
        }else {
            this.validateAllFormFields(this.formTeacher);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({
                    onlySelf: true,
                });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
}
