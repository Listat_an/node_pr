import {Component, OnInit} from '@angular/core';
import {ShopService} from '../../../../services/shop.service';

@Component({
    selector: 'opn-cart-informer',
    templateUrl: './cart-informer.component.html',
    styleUrls: ['./cart-informer.component.scss'],
    providers: [ShopService]
})
export class CartInformerComponent implements OnInit {
    cartProductsCount: number = 0;

    constructor(private shopService: ShopService) {
        this.shopService.getUserCart().subscribe(res => {
            this.cartProductsCount = res['total'];
        });
    }

    ngOnInit() {
        this.shopService.onPlusItem().subscribe(res => {
            if (res == 0) {
                this.cartProductsCount = 0;
            }else {
                this.cartProductsCount += res;
            }
        });
    }
}
