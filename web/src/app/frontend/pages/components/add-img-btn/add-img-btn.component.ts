import {Component, EventEmitter, OnInit, Output, Input, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'opn-add-img-btn',
    templateUrl: './add-img-btn.component.html',
    styleUrls: ['./add-img-btn.component.scss'],
})
export class AddImgBtnComponent implements OnInit {
    plusIcon = '/assets/images/icons/plus-icon.png';

    @Input() stylesBtn?: any;
    @Input() stylesIco?: any;
    @Output() onImg: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    ngOnInit() {
    }

    onImage(e) {
        let file: File = e.target.files[0];
        let myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
            this.onImg.emit(myReader.result);
        };
        if(file)myReader.readAsDataURL(file);
        e.target.value = '';
    }
}
