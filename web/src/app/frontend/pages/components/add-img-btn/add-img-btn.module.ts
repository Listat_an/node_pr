import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddImgBtnComponent} from './add-img-btn.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [AddImgBtnComponent],
    exports: [AddImgBtnComponent]
})
export class AddImgBtnModule {
}
