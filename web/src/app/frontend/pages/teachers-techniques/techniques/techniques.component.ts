import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ModalConfirmComponent } from '../../components/academy/modal-confirm/modal-confirm.component';

import { TechniquesService } from '../../../../services/techniques.service';
import { findIndex } from 'lodash';

@Component({
  selector: 'opn-techniques',
  templateUrl: './techniques.component.html',
  styleUrls: ['./techniques.component.scss'],
  providers: [
    TechniquesService,
  ],
})
export class TechniquesComponent implements OnInit {

  public formTechniques: FormGroup;
  public formTechnique: FormGroup;
  private techniquesArr: any = [];

  @Input() academyId: Number;
  private updateTechnique: Boolean = false;
  private newTechniquesShow: Boolean = false;

  @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;
  private techniqueTmp: any;
  private action: string;
  private load: Boolean = false;

  constructor(
    private techniquesService: TechniquesService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) {

    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    };
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
         this.router.navigated = false;
         window.scrollTo(0, 0);
      }
    });
  }
  ngOnInit() {
    this.buildFormTechniques();
    this.getTechniques(this.academyId);
    this.buildFormTechnique({});
  }

  getTechniques(academy_id) {
    this.load = true;
    this.techniquesService.getTechniquesByAcademy(academy_id).subscribe(Techniques => {
      console.log('Techniques', Techniques);
      this.techniquesArr = Techniques;
      this.load = false;
    });
  }


  private buildFormTechniques() {
    this.formTechniques = this.fb.group({
      name: ['', Validators.required],
    });
  }

  private buildFormTechnique(technique) {
    this.formTechnique = this.fb.group({
      id: [(technique.id)?technique.id:0],
      name: [(technique.name)?technique.name:'', Validators.required],
    });
  }

  private onOpenEdit(e, technique) {
    const top = e.toElement.parentElement.offsetTop - 13;    
    this.buildFormTechnique(technique);
    this.updateTechnique = true;
    document.getElementById("update-technique").style.cssText  = `top: ${top}px; height: auto;`;
  }

  private onRemove(technique) {
    this.techniqueTmp = technique;
    this.modalConfirm.modalTitle = 'Delete your Technique?';
    this.modalConfirm.modalDescription = 'This action will remove your technique.';
    this.modalConfirm.btnsTitle = {
        accept: 'Yes',
        declide: 'no',
    };
    this.action = 'remove';
    this.modalConfirm.open();
  }

  private onCreateTechnique(technique) {
    this.techniqueTmp = technique;
    this.action = 'create';
    this.modalConfirm.modalTitle = 'Create your Technique?';
    this.modalConfirm.modalDescription = 'This action will create your technique.';
    this.modalConfirm.btnsTitle = {
        accept: 'Yes',
        declide: 'no',
    };
    this.modalConfirm.open();
  }
  private onUpdateTechnique(technique) {
    this.techniqueTmp = technique;
    this.action = 'update';
    this.modalConfirm.modalTitle = 'Update your Technique?';
    this.modalConfirm.modalDescription = 'This action will update your technique.';
    this.modalConfirm.btnsTitle = {
        accept: 'Yes',
        declide: 'no',
    };
    this.modalConfirm.open();
  }

  onConfirm(confirm) {
    if (confirm) {
        switch (this.action) {
          case 'create': {
            const obj = {
              academy_id: this.academyId,
              name: this.techniqueTmp.value.name,
            };
            this.techniquesService.createTechnique(obj).subscribe(res => {
              if (res.status) {
                this.techniquesArr.unshift(res.data);
                this.buildFormTechniques();
              }
            });
            break;
          }
          case 'update': {
            const obj = {
              academy_id: this.academyId,
              name: this.techniqueTmp.value.name,
            };
            this.techniquesService.updateTechnique(this.techniqueTmp.value.id, obj).subscribe(res => {
              if (res.status) {
                const i = findIndex(this.techniquesArr, {id: this.techniqueTmp.value.id});
                this.techniquesArr[i].name = this.techniqueTmp.value.name;
                document.getElementById('update-technique').style.cssText  = `top: ${top}px; height: 0;`;
                this.buildFormTechnique({});
              }
            });
            break;
          }
          case 'remove': {
            const self = this;
            this.techniquesService.removeTechnique(this.techniqueTmp.id).subscribe(res => {
              if (res.status) {
                self.techniquesArr = self.techniquesArr.filter(iTechnique => iTechnique !== this.techniqueTmp);
              }
            });
            break;
          }
          default: {
            this.techniqueTmp = {};
            break;
          }
        }
    }
  }
}
