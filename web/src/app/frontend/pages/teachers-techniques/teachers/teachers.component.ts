import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ModalConfirmComponent } from '../../components/academy/modal-confirm/modal-confirm.component';

import { TeachersService } from '../../../../services/teachers.service';
import { findIndex } from 'lodash';

@Component({
  selector: 'opn-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss'],
  providers: [
    TeachersService,
  ],
})
export class TeachersComponent implements OnInit {

  public formTeachers: FormGroup;
  public formTeacher: FormGroup;
  private teachersArr: any = [];

  @Input() academyId: Number;
  private updateTeacher: Boolean = false;
  private newTeacherShow: Boolean = false;
  private teacherTmp: any;
  private action: string;
  private load: Boolean = false;

  @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;

  constructor(
    private teacherService: TeachersService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) {

    this.router.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    };
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
         this.router.navigated = false;
         window.scrollTo(0, 0);
      }
    });
  }
  ngOnInit() {
    this.buildFormTeachers();
    this.getTeachers(this.academyId);
    this.buildFormTeacher({});
  }

  getTeachers(academy_id) {
    this.load = true;
    this.teacherService.getTeachersByAcademy(academy_id).subscribe(teachers => {
      this.teachersArr = teachers;
      this.load = false;
    });
  }


  private buildFormTeachers() {
    this.formTeachers = this.fb.group({
      name: ['', Validators.required],
    });
  }

  private buildFormTeacher(teacher) {
    this.formTeacher = this.fb.group({
      id: [(teacher.id) ? teacher.id : 0],
      name: [(teacher.first_name) ? `${teacher.first_name} ${teacher.last_name}` : '', Validators.required],
    });
  }

  private onOpenEdit(e, teacher) {
    const top = e.toElement.parentElement.offsetTop - 13;
    this.buildFormTeacher(teacher);
    this.updateTeacher = true;
    document.getElementById('update-teacher').style.cssText  = `top: ${top}px; height: auto;`;
  }

  private onRemove(teacher) {
    this.teacherTmp = teacher;
    this.modalConfirm.modalTitle = 'Delete your Teacher?';
    this.modalConfirm.modalDescription = 'This action will remove your teacher.';
    this.modalConfirm.btnsTitle = {
        accept: 'Yes',
        declide: 'no',
    };
    this.action = 'remove';
    this.modalConfirm.open();
  }

  onConfirm(confirm) {
    if (confirm) {
        switch (this.action) {
          case 'create': {
            const [first_name, last_name] = this.teacherTmp.value.name.replace(' ', '|').split('|');
            const obj = {
              academy_id: this.academyId,
              first_name: first_name,
              last_name: (last_name) ? last_name : '',
            };
            this.teacherService.createTeacher(obj).subscribe(res => {
              if (res.status) {
                this.teachersArr.unshift(res.data);
                this.buildFormTeachers();
              }
            });
            break;
          }
          case 'update': {
            const [first_name, last_name] = this.teacherTmp.value.name.replace(' ', '|').split('|');
            const obj = {
              academy_id: this.academyId,
              first_name: first_name,
              last_name: (last_name) ? last_name : '',
            };
            this.teacherService.updateTeacher(this.teacherTmp.value.id, obj).subscribe(res => {
              if (res.status) {
                const i = findIndex(this.teachersArr, {id: this.teacherTmp.value.id});
                this.teachersArr[i].first_name = first_name;
                this.teachersArr[i].last_name = (last_name) ? last_name : '';
                document.getElementById('update-teacher').style.cssText  = `top: ${top}px; height: 0;`;
                this.buildFormTeacher({});
              }
            });
            break;
          }
          case 'remove': {
            const self = this;
            this.teacherService.removeTeacher(this.teacherTmp.id).subscribe(res => {
              if (res.status) {
                self.teachersArr = self.teachersArr.filter(iTeacher => iTeacher !== this.teacherTmp);
              }
            });
            break;
          }
          default: {
            this.teacherTmp = {};
            break;
          }
        }
    }
  }

  private onCreateTeacher(teacher) {
    this.teacherTmp = teacher;
    this.action = 'create';
    this.modalConfirm.modalTitle = 'Create your Teacher?';
    this.modalConfirm.modalDescription = 'This action will create your teacher.';
    this.modalConfirm.btnsTitle = {
        accept: 'Yes',
        declide: 'no',
    };
    this.modalConfirm.open();
  }
  private onUpdateTeacher(teacher) {
    this.teacherTmp = teacher;
    this.action = 'update';
    this.modalConfirm.modalTitle = 'Update your Teacher?';
    this.modalConfirm.modalDescription = 'This action will update your teacher.';
    this.modalConfirm.btnsTitle = {
        accept: 'Yes',
        declide: 'no',
    };
    this.modalConfirm.open();
  }
}
