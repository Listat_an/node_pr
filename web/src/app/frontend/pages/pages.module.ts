import { ConfigService } from '../../services/service.config';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
// import { ShareButtonsModule } from 'ngx-sharebuttons';
import {ShareButtonModule} from '@ngx-share/button';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {TooltipModule} from 'ng2-tooltip';
import {Ng4GeoautocompleteModule} from 'ng4-geoautocomplete';
import {NgPipesModule} from 'ngx-pipes';

import {Select2Module} from 'ng2-select2';
import {NgDatepickerModule} from '../../../components/ng-datepicker';
import {BeltsModule} from '../../../components/belts';
import {SelectAcademiesModule} from '../../../components/select-acadimies';
import {MomentModule} from 'angular2-moment';
import {ImageCropperComponent} from 'ng2-img-cropper';
import {SocketIoModule, SocketIoConfig} from 'ngx-socket-io';
import {NgxMaskModule} from 'ngx-mask';
import { PerfectScrollbarModule, PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import {PARTS_COMPONENTS} from './parts';
import {NewPostFormComponent} from './components/posts/new-post/new-post.component';
import {ViewPostComponent} from './components/posts/view-post/view-post.component';
import {PopupPostComponent} from './components/posts/popup-post/popup-post.component';
import {DropdownVisibilityComponent} from './components/posts/dropdown/dropdown-visibility.component';
import {AccountVisibilityComponent} from './components/posts/dropdown.account/account-visibility.component';
import {ControllsPostComponent} from './components/posts/controll.post/controll.component';
import {NewAcademyComponent} from './components/academy/new-academy/new-academy.component';
import {NotificationInformerComponent} from './components/notification-informer/notification-informer.component';

import {niceDateFormatPipe} from './pipes/DateFormatPipe.pipe';
import {AvatarPipe} from './pipes/avatarPipe.pipe';
import {ViewAvatarComponent} from './components/view-avatar/view-avatar.component';
import {ViewLogoAcademyComponent} from './components/view-logo-academy/view-logo-academy.component';
import {ViewWidgetAcademyComponent} from './components/academy/academy-widget/academy-view.component';
import {ViewListAcademyComponent} from './components/academy/academy-list/academy-list.component';
import {CropCoverPhotoComponent} from './components/crop-cover-photo/crop-cover-photo.component';
import {CropLogoPhotoComponent} from './components/crop-logo-photo/crop-logo-photo.component';
import {Broadcaster} from '../../services/broadcaster';
// import { SocketService } from '../../services/socket.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from './components/modal/modal.component';
import {ModalService} from './components/modal/modal.service';
import {OpnActiveRouteDirective} from '../../../directives/link-active.directive';
import {UiSelectTypeSettingComponent} from '../../../components/ui/ui-select-type.1/ui-select.component';
import {CropAcademyLogoComponent} from './components/crop-academy-logo/crop-academy-logo.component';
import {AddBeltComponent} from './components/add-belt/add-belt.component';
import {AttendedComponent} from './components/attended/attended.component';
import {RoundProgressModule} from '../../../components/angular-round-progressbar/index';
import { UserSettingsComponent } from './parts/sidebars/left/user-settings/user-settings.component';
import { MyAcademiesComponent } from './parts/sidebars/left/my-academies/my-academies.component';
import {CartInformerComponent} from './components/cart-informer/cart-informer.component';
import {ModalConfirmModule} from './components/academy/modal-confirm/modal-confirm.module';
import { UiCheckboxModule } from 'components/ui/ui-checkbox/ui-checkbox.module';
import { LinkyModule } from 'angular-linky';
import { NgxEditorModule } from 'ngx-editor';
import { environment } from '../../../environments/environment';
const config: SocketIoConfig = {
    url: environment.url_server,
    options: {},
};

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true,
  };

@NgModule({
    declarations: [
        ...PARTS_COMPONENTS,
        NewPostFormComponent,
        ViewPostComponent,
        PopupPostComponent,
        AddBeltComponent,
        DropdownVisibilityComponent,
        AccountVisibilityComponent,
        niceDateFormatPipe,
        AvatarPipe,
        ViewAvatarComponent,
        ViewLogoAcademyComponent,
        ViewWidgetAcademyComponent,
        ViewListAcademyComponent,
        ControllsPostComponent,
        NewAcademyComponent,
        CropCoverPhotoComponent,
        CropLogoPhotoComponent,
        CropAcademyLogoComponent,
        ImageCropperComponent,
        NotificationInformerComponent,
        CartInformerComponent,
        ModalComponent,
        OpnActiveRouteDirective,
        UiSelectTypeSettingComponent,
        UserSettingsComponent,
        MyAcademiesComponent,
    ],
    entryComponents: [
        ViewAvatarComponent,
        ViewLogoAcademyComponent,
        ViewWidgetAcademyComponent,
        ViewListAcademyComponent,
        CropCoverPhotoComponent,
        ModalComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        InfiniteScrollModule,
        HttpClientModule,
        HttpModule,
        TooltipModule,
        Select2Module,
        ReactiveFormsModule,
        NgDatepickerModule,
        BeltsModule,
        SelectAcademiesModule,
        Ng4GeoautocompleteModule.forRoot(),
        ShareButtonModule.forRoot(),
        NgbModule.forRoot(),
        SocketIoModule.forRoot(config),
        NgxMaskModule.forRoot(),
        NgPipesModule,
        MomentModule,
        PerfectScrollbarModule,
        ModalConfirmModule,
        UiCheckboxModule,
        LinkyModule,
        NgxEditorModule,
    ],
    exports: [
        ...PARTS_COMPONENTS,
        NewPostFormComponent,
        ViewPostComponent,
        CropLogoPhotoComponent,
        CropAcademyLogoComponent,
        PopupPostComponent,
        AddBeltComponent,
        DropdownVisibilityComponent,
        AccountVisibilityComponent,
        niceDateFormatPipe,
        AvatarPipe,
        ViewAvatarComponent,
        ViewLogoAcademyComponent,
        ViewWidgetAcademyComponent,
        ViewListAcademyComponent,
        ControllsPostComponent,
        NewAcademyComponent,
        CropCoverPhotoComponent,
        NotificationInformerComponent,
        ModalComponent,
        OpnActiveRouteDirective,
        UiSelectTypeSettingComponent,
        PerfectScrollbarComponent,
        MyAcademiesComponent,
    ],
    providers: [
        CookieService,
        Broadcaster,
        ModalService,
        // SocketService,
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
        },
    ],
})
export class PagesModule {
}
