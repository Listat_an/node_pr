import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {SubscriptionsService} from '../../../../services/subscriptions.service';
import {Subscription} from 'rxjs/Subscription';
import {cancelSubscription} from '../../../../providers/cancelSubscription';
import {
    differenceInDays,
    addDays
} from 'date-fns';

@Component({
    selector: 'opn-subscription',
    templateUrl: './subscription.component.html',
    styleUrls: ['./subscription.component.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [SubscriptionsService],
})
export class SubscriptionComponent implements OnInit {
    public subscrips: Subscription[] = [];
    private subscriptions: Array<any> = [];
    public membership: Object = {
        '1_day': 'One day',
        '1_month': 'Month',
        '3_month': '3 month',
        '6_month': '6 month',
        '1_year': 'Year',
    };

    private load: Boolean = true;

    constructor(private subscribeSrv: SubscriptionsService,) {
    }

    ngOnInit() {
        this.getMeSubscriptions();
    }

    getMeSubscriptions() {
        this.load = false;
        this.subscrips.push(
            this.subscribeSrv.getSubscriptionsByUser().subscribe(subscribes => {
                this.subscriptions = subscribes;
                this.load = true;
            }),
        );
    }

    onCancel(subscribe) {
        const self = this;
        this.subscrips.push(
            this.subscribeSrv.cancelSabscription({subscription_id: subscribe.id}).subscribe(res => {
                console.log('Cenceled process: ', res);
                if (res) {
                    self.subscriptions = self.subscriptions.filter(iSubscribe => iSubscribe.subscription !== subscribe);
                }
            }),
        );
    }

    public getTrialDaysLeft(createdAt, trialDays) {
        return trialDays - differenceInDays(new Date(), new Date(createdAt));
    }
}
