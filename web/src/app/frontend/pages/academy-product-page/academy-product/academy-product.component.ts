import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'opn-academy-product',
    templateUrl: './academy-product.html',
    styleUrls: ['./academy-product.scss'],
})
export class AcademyProductComponent {
    productName: string = '';
    academyId;

    constructor(private router: Router,
                private activeRoute: ActivatedRoute) {
        this.activeRoute.parent.params.subscribe(params => {
            this.academyId = params['id'];
        });
    }

    searchProducts() {
        this.router.navigate([`academiy-datail/${this.academyId}`], {queryParams: {productName: this.productName}})
    }
}
