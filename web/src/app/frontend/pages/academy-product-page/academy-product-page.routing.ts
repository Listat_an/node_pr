import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AcademyProductComponent} from './academy-product/academy-product.component';
import {AcademyProductPageComponent} from './academy-product-page.component';


const routes: Routes = [{
    path: '',
    component: AcademyProductPageComponent,
    children: [
        {
            path: '',
            component: AcademyProductComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AcademyProductPageRouting {
}
