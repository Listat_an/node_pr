import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'opn-academy-product-page',
    templateUrl: './academy-product-page.component.html',
    styleUrls: ['./academy-product-page.component.scss', '../@theme/scss/theme.scss'],
})
export class AcademyProductPageComponent implements OnInit {
    isManager: Boolean = false;
    academyId;

    constructor(private activeRoute: ActivatedRoute,) {
        this.activeRoute.params.subscribe(params => {
            this.academyId = params['id'];
        });
    }

    ngOnInit() {
    }

    onCheckManager(status) {
        this.isManager = status;
    }
}
