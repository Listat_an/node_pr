import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {UiBackBtnModule} from '../../../../components/ui/ui-back-btn/ui-back-btn.module';
import {ProductInfoModule} from '../shop/product/product-info.module';
import {RouterModule, Routes} from '@angular/router';
import {AcademyProductPageComponent} from './academy-product-page.component';
import {AcademyProductComponent} from './academy-product/academy-product.component';
import {AcademyComponentsModule} from '../components/academy/academy.module';
import {PagesModule} from '../pages.module';
import {AcademyProductPageRouting} from './academy-product-page.routing';

@NgModule({
    imports: [
        AcademyProductPageRouting,
        AcademyComponentsModule,
        CommonModule,
        FormsModule,
        ProductInfoModule,
        PagesModule,
        UiBackBtnModule,
    ],
    declarations: [
        AcademyProductPageComponent,
        AcademyProductComponent
    ],
})
export class AcademyProductPageModule {
}
