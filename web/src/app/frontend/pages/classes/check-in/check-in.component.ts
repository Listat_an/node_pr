import {
    Component,
    OnInit,
    OnDestroy,
    ViewEncapsulation,
    ViewChild,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';


import {
    ActivitiesService,
} from '../../../../services/activities.service';
import {
    ScheduleService,
} from '../../../../services/schedule.service';
import {
    AcademiesService,
} from '../../../../services/academies.service';
import {
    Title,
} from '@angular/platform-browser';

import {
    Broadcaster,
} from '../../../../services/broadcaster';
import {
    Subject,
} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import {
    DatepickerOptions,
} from '../../../../../components/ng-datepicker/ng-datepicker.component';
import {
    ModalConfirmComponent,
} from '../../components/academy/modal-confirm/modal-confirm.component';
import * as _d from 'date-fns';
import * as _ from 'lodash';
import * as _is from 'is_js';

@Component({
    selector: 'opn-check-in',
    templateUrl: './check-in.component.html',
    styleUrls: ['./check-in.component.scss', '../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        ActivitiesService,
        AcademiesService,
    ],
    // entryComponents: [UiPerPageComponent],
})
export class CheckInComponent implements OnInit, OnDestroy, OnDestroy {
    public academy_id: any;

    public academyProfile: any;
    public filerDate = new Date();
    public isManager: Boolean = false;
    public datePickerOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
    };


    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;

    private ngUnsubscribe: Subject < void > = new Subject < void > ();

    public listClasses: Array<any> = [];
    public tab = 'checkin';

    constructor(
        private scheduleSrvc: ScheduleService,
        private academyService: AcademiesService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private broadcaster: Broadcaster,
        private titleService: Title,
    ) {
        this.titleService.setTitle('Check In');
        this.activeRoute.parent.params.subscribe(params => {
            this.academy_id = params['id'];
        });
    }

    ngOnInit() {
        this.getAcademyInfo();
        this.getClases(this.filerDate);
    }

    public onDateSelect() {
        this.getClases(this.filerDate);
    }

    getClases(filerDate) {
        if (this.academy_id) {
            const from = filerDate;
            const to = filerDate;

            this.scheduleSrvc.getSchedule(from, to, this.academy_id)
                .takeUntil(this.ngUnsubscribe)
                .subscribe(resp => {
                    console.log('getClases', resp);
                    if (resp.status) {
                        this.parseShedules(resp.data);
                    }
                });
        }
    }

    private checkHoliday(schedyle, date_2): Boolean {
        const date_1 = new Date(schedyle.date);
        if (_d.isEqual(date_1, date_2)) {
            return true;
        }
        return false;
    }

    private parseShedules(data) {
        const self = this;
        let schedule: Array < any > = [];
        let _holidayDays: Array<any> = [];

        _.forEach(data, (classes, index) => {
            classes.forEach(_class => {
                _class.shedules.forEach(_shedule => {
                    const [h1, m1] = _shedule.time_start.split('.');
                    const [h2, m2] = _shedule.time_end.split('.');
                    const time_start = _d.setHours(_d.setMinutes(new Date(), m1), h1);
                    const time_end = _d.setHours(_d.setMinutes(new Date(), m2), h2);

                    if (_shedule.holiday == 1 && this.checkHoliday(_shedule, new Date(index))) {
                        _holidayDays.push({day: _shedule.day, date: index});
                    }
                    schedule.push({
                        ..._shedule,
                        name: _class.name,
                        subscriptions: _class.subscriptions,
                        colorBorder: _class.color_border,
                        colorBg: _class.color_bg,
                        time_start: _d.format(time_start, 'h:mm a'),
                        time_end: _d.format(time_end, 'h:mm a'),
                        isOpen: false,
                        meta: {
                            type: 'class',
                            role: 'manager',
                            id: _class.id,
                        },
                    });
                });
            });
        });

        let _schedule;
        _holidayDays = _.uniqBy(_holidayDays, 'day');
        console.log(_is.not.empty(_holidayDays), _holidayDays);
        if (_is.not.empty(_holidayDays)) {
            _holidayDays.forEach(_day => {
                _schedule = _.union(_schedule, schedule.filter(ISchedule => ISchedule.day == _day.day && (ISchedule.holiday == 0 || !this.checkHoliday(ISchedule, new Date(_day.date)))));
            });
            this.listClasses = schedule = _.difference(schedule, _schedule);
        }else {
            this.listClasses = schedule.filter(ISchedule => ISchedule.holiday != 1);
        }

        console.log(this.listClasses);
    }

    private getAcademyInfo() {
        this.academyService.getAcademyById(this.academy_id)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(academy => {
                this.academyProfile = academy;
            });
    }

    public onSubsOpen(event, _class: any) {
        event.stopPropagation();
        _class.isOpen = !_class.isOpen;
    }

    public onChangeFilterStatus(tab) {
        switch (tab) {
            case 'schedule':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes']);
                    break;
                }
            case 'holiday':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'holiday']);
                    break;
                }
            case 'checkin':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'check-in']);
                    break;
                }
        }
    }

    private convertTime12to24(time12h) {
        const [time, modifier] = time12h.split(' ');
        let [hours, minutes] = time.split(':');
        if (modifier === 'pm') {
            if ( hours == 12) {
                hours = 12;
            }else {
                hours = parseInt(hours, 10) + 12;
            }
        }
        if (modifier === 'am' && hours == 12) {
            hours = 24;
        }
        return hours + '.' + minutes;
    }

    toStunentsList(class_id, sheudle_id, start, end) {
        this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'check-in', class_id], {
            queryParams: {
                date: _d.format(this.filerDate, 'YYYY-MM-DD'),
                sch_id: sheudle_id,
                sch_start: this.convertTime12to24(start),
            },
        });
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
