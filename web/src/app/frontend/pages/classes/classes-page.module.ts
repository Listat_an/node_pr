import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ClassesPageComponent} from './classes-page.component';
import {ClassesPageRouting} from './classes-page.routing';
import {PagesModule} from '../pages.module';
import {AcademyComponentsModule} from '../components/academy/academy.module';
import {UiPerPageModule} from '../../../../components/ui/ui-per-page/ui-per-page.module';
import {ClassesListComponent} from './classes-list/classes-list.component';
import {ClassViewSubscriptionsComponent} from './classes-list/subscriptions-list/subscriptions-list.component';
import {ClassesStudentsListComponent} from './classes-students-list/classes-students-list.component';
import {UiBackBtnModule} from '../../../../components/ui/ui-back-btn/ui-back-btn.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgDatepickerModule} from 'ng2-datepicker';
import { ModalConfirmModule } from '../components/academy/modal-confirm/modal-confirm.module';
import { CreateClassScheduleModule } from '../components/academy/create-class-schedule/create-class-schedule.module';
import { CreateHolidayScheduleModule } from '../components/academy/create-holiday-schedule/create-holiday-schedule.module';
import {
    PerfectScrollbarModule,
    PERFECT_SCROLLBAR_CONFIG,
    PerfectScrollbarConfigInterface,
} from 'ngx-perfect-scrollbar';
import {HolidayListComponent} from './holiday-list/holiday-list.component';
import {CheckInComponent} from './check-in/check-in.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: false,
    suppressScrollY: false,
};

@NgModule({
    imports: [
        CommonModule,
        ClassesPageRouting,
        PagesModule,
        AcademyComponentsModule,
        UiPerPageModule,
        UiBackBtnModule,
        NgDatepickerModule,
        ModalConfirmModule,
        CreateClassScheduleModule,
        CreateHolidayScheduleModule,
        PerfectScrollbarModule,
    ],
    declarations: [
        ClassesPageComponent,
        ClassesListComponent,
        ClassesStudentsListComponent,
        ClassViewSubscriptionsComponent,
        HolidayListComponent,
        CheckInComponent,
    ],
    entryComponents: [
        ClassViewSubscriptionsComponent,
    ],
    providers: [{
        provide: PERFECT_SCROLLBAR_CONFIG,
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    }],
})
export class ClassesPageModule {
}
