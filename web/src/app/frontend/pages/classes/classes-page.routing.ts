import { RouterModule, Routes, CanActivate } from '@angular/router';
import { NgModule } from '@angular/core';

import { ClassesPageComponent } from './classes-page.component';
import {ClassesListComponent} from './classes-list/classes-list.component';
import {ClassesStudentsListComponent} from './classes-students-list/classes-students-list.component';
import {HolidayListComponent} from './holiday-list/holiday-list.component';
import {CheckInComponent} from './check-in/check-in.component';

const routes: Routes = [{
  path: '',
  component: ClassesPageComponent,
  children: [
    {
      path: '',
      component: ClassesListComponent,
    },
    {
      path: 'holiday',
      component: HolidayListComponent,
    },
    {
      path: 'check-in',
      component: CheckInComponent,
    },
    {
      path: 'check-in/:class',
      component: ClassesStudentsListComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClassesPageRouting {
}
