import {
    Component,
    OnInit,
    OnDestroy,
    ViewEncapsulation,
    ViewChild,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';


import {
    ActivitiesService,
} from '../../../../services/activities.service';
import {
    ScheduleService,
} from '../../../../services/schedule.service';
import {
    AcademiesService,
} from '../../../../services/academies.service';
import {
    Title,
} from '@angular/platform-browser';

import {
    Broadcaster,
} from '../../../../services/broadcaster';
import {
    Subject,
} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import {
    DatepickerOptions,
} from '../../../../../components/ng-datepicker/ng-datepicker.component';
import {
    ModalConfirmComponent,
} from '../../components/academy/modal-confirm/modal-confirm.component';
import * as _d from 'date-fns';
import * as _ from 'lodash';
import {
    CreateHolidayScheduleComponent,
} from '../../components/academy/create-holiday-schedule/create-holiday-schedule.component';

@Component({
    selector: 'opn-holiday-list',
    templateUrl: './holiday-list.component.html',
    styleUrls: ['./holiday-list.component.scss', '../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        ActivitiesService,
        AcademiesService,
    ],
    // entryComponents: [UiPerPageComponent],
})
export class HolidayListComponent implements OnInit, OnDestroy, OnDestroy {
    public academy_id: any;

    public academyProfile: any;
    public filterBy: String = 'date';
    public filerDate = new Date();
    public isManager: Boolean = false;
    public datePickerOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
    };


    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;
    @ViewChild(CreateHolidayScheduleComponent)
    public createHolidaySchedule: CreateHolidayScheduleComponent;

    private ngUnsubscribe: Subject < void > = new Subject < void > ();

    public listClasses: Array<any> = [];
    public tab = 'holiday';

    constructor(
        private activityService: ActivitiesService,
        private scheduleSrvc: ScheduleService,
        private academyService: AcademiesService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private broadcaster: Broadcaster,
        private titleService: Title,
    ) {
        this.titleService.setTitle('Holiday list');
        this.activeRoute.parent.params.subscribe(params => {
            this.academy_id = params['id'];
        });
    }

    ngOnInit() {
        this.getAcademyInfo();
        this.getClases();
        this.registerActivitiBroadcast();
    }

    public onOpenCreateClass(event) {
        this.createHolidaySchedule.openUpdateModal(event);
    }

    registerActivitiBroadcast() {
        this.activityService.onHoliday()
            .takeUntil(this.ngUnsubscribe)
            .subscribe(obj => {
                this.getClases();
            });
    }

    getClases() {
        if (this.academy_id) {
            const from = _d.startOfWeek(new Date(), {
                weekStartsOn: 1,
            });

            const to = _d.endOfWeek(new Date(), {
                weekStartsOn: 1,
            });
            this.scheduleSrvc.getHolidays(from, to, this.academy_id)
                .takeUntil(this.ngUnsubscribe)
                .subscribe(resp => {
                    console.log('getClases', resp);
                    if (resp.status) {
                        this.parseShedules(resp.data);
                    }
                });
        }
    }

    private parseShedules(data) {
        const self = this;
        let schedule: Array < any > = [];
        _.forEach(data, (classes, index) => {

            classes.forEach(_class => {

                _class.shedules.forEach(_shedule => {
                    if (_shedule.holiday == 1) {
                        const [h1, m1] = _shedule.time_start.split('.');
                        const [h2, m2] = _shedule.time_end.split('.');
                        const time_start = _d.setHours(_d.setMinutes(new Date(), m1), h1);
                        const time_end = _d.setHours(_d.setMinutes(new Date(), m2), h2);
                        schedule.push({
                            ..._shedule,
                            name: _class.name,
                            subscriptions: _class.subscriptions,
                            colorBorder: _class.color_border,
                            colorBg: _class.color_bg,
                            time_start: _d.format(time_start, 'h:mm a'),
                            time_end: _d.format(time_end, 'h:mm a'),
                            isOpen: false,
                            meta: {
                                type: 'holiday',
                                role: 'manager',
                                id: _class.id,
                                index: index,
                            },
                        });
                    }
                });
            });
        });
        console.log(schedule);
        this.listClasses = schedule;
    }

    private getAcademyInfo() {
        this.academyService.getAcademyById(this.academy_id)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(academy => {
                this.academyProfile = academy;
            });
    }

    public onSubsOpen(event, _class: any) {
        event.stopPropagation();
        _class.isOpen = !_class.isOpen;
    }

    public onChangeFilterStatus(tab) {
        switch (tab) {
            case 'schedule':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes']);
                    break;
                }
            case 'holiday':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'holiday']);
                    break;
                }
            case 'checkin':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'check-in']);
                    break;
                }
        }
    }


    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
