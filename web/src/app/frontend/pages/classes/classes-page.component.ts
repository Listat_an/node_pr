import {
    Component,
    OnInit,
    OnDestroy,
    ViewEncapsulation,
} from '@angular/core';
import {
    ActivatedRoute,
} from '@angular/router';

import {
    ActivitiesService,
} from '../../../services/activities.service';
import {
    AcademiesService,
} from '../../../services/academies.service';
import 'rxjs/add/operator/debounceTime';

@Component({
    selector: 'opn-classes',
    templateUrl: './classes-page.component.html',
    styleUrls: ['./classes-page.component.scss', '../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        ActivitiesService,
        AcademiesService,
    ],
    // entryComponents: [UiPerPageComponent],
})
export class ClassesPageComponent implements OnInit {
    public isManager: Boolean = false;
    public academy_id;
    

    constructor(
        private activeRoute: ActivatedRoute,
    ) {
        this.activeRoute.params.subscribe(params => {
            this.academy_id = params['id'];
        });
    }

    ngOnInit() {}

    onCheckManager(status) {
        this.isManager = status;
    }
}
