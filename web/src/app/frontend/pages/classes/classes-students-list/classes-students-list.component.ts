import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {ActivitiesService} from '../../../../services/activities.service';
import { Subject } from 'rxjs/Subject';
import * as _d from 'date-fns';
import * as _is from 'is_js';

@Component({
    selector: 'opn-classes-students-list',
    templateUrl: './classes-students-list.component.html',
    styleUrls: ['./classes-students-list.component.scss', '../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        ActivitiesService,
    ],
})
export class ClassesStudentsListComponent implements OnInit, OnDestroy {
    academy_id: any;
    activity_id: any;
    private date: any;
    private schedule_id: any;
    private schedule_start: any;
    private schedule_end: any;
    public tab = '';
    private ngUnsubscribe: Subject <any> = new Subject <any> ();
    public usersList: any = [];


    constructor(
        private activitiesSrvc: ActivitiesService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private titleService: Title,
    ) {
        this.activeRoute.params.subscribe(params => {
            this.activity_id = parseInt(params['class'], 10);
        });
        this.activeRoute.queryParams.subscribe(queryParams => {
            this.date = queryParams['date'];
            this.schedule_id = parseInt(queryParams['sch_id'], 10);
            this.schedule_start = queryParams['sch_start'];
        });

        this.activeRoute.parent.parent.params.subscribe(params => {
            this.academy_id = params['id'];
        });
        this.titleService.setTitle('Students List');
    }
    
    ngOnInit() {
        this.getStudents();
    }

    private getStudents() {
        this.activitiesSrvc.getStudentsForCheckIn({
            activity_id: this.activity_id,
            schedule_id: this.schedule_id,
            date: this.date,
        })
        .takeUntil(this.ngUnsubscribe)
        .subscribe(resp => {
            if (resp.status) {
                this.usersList = resp.data.map(item => {
                    return {
                        ...item,
                        checkLoad: false,
                    };
                });
            }
            console.log('Students ', this.usersList);
        });
    }

    public onChangeFilterStatus(tab) {
        switch (tab) {
            case 'schedule':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes']);
                    break;
                }
            case 'holiday':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'holiday']);
                    break;
                }
            case 'checkin':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'check-in']);
                    break;
                }
        }
    }

    public onChangeFilterStatus2(tab) {
        switch (tab) {
            case 'schedules':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'schedule']);
                    break;
                }
            case 'classes':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes']);
                    break;
                }
            case 'subscriptions':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'create-subscriptions']);
                    break;
                }
        }
    }

    public isCheckin(checkin): Boolean {
        if ( _is.not.null(checkin) && _is.not.empty(checkin) ) {
            return true;
        }
        return false;
    }

    public checkIn(user) {
        user.checkLoad = true;
        const [hs, ms] = this.schedule_start.split('.');
        this.activitiesSrvc.checkInForManager({
            user_id: user.id,
            activity_id: this.activity_id,
            academy_id: this.academy_id,
            schedule_id: this.schedule_id,
            checkin_date: _d.format(_d.setMinutes(_d.setHours(new Date(this.date), hs), ms), 'YYYY-MM-DD HH:mm:ss'),
        })
        .takeUntil(this.ngUnsubscribe)
        .finally(() => {
            user.checkLoad = false;
        })
        .subscribe(resp => {
            if (resp.status) {
                user.checkin = resp.data;
            }
            console.log(resp);
        });
    }

    public uncheck(user) {
        this.activitiesSrvc.uncheckJoinedUser(user.checkin.id)
        .takeUntil(this.ngUnsubscribe)
        .subscribe(resp => {
            if (resp.status) {
                user.checkin = null;
            }
            console.log(resp);
        });
        return false;
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

}
