import {
    Component,
    OnInit,
    OnDestroy,
    ViewEncapsulation,
    ViewChild,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
    NavigationEnd,
} from '@angular/router';


import {
    ActivitiesService,
} from '../../../../services/activities.service';
import {
    ScheduleService,
} from '../../../../services/schedule.service';
import {
    AcademiesService,
} from '../../../../services/academies.service';
import {
    Title,
} from '@angular/platform-browser';

import {
    Broadcaster,
} from '../../../../services/broadcaster';
import {
    Subject,
} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import {
    DatepickerOptions,
} from '../../../../../components/ng-datepicker/ng-datepicker.component';
import {
    ModalConfirmComponent,
} from '../../components/academy/modal-confirm/modal-confirm.component';
import * as _d from 'date-fns';
import * as _ from 'lodash';
import {
    CreateClassScheduleComponent,
} from '../../components/academy/create-class-schedule/create-class-schedule.component';

@Component({
    selector: 'opn-classes-list',
    templateUrl: './classes-list.component.html',
    styleUrls: ['./classes-list.component.scss', '../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        ActivitiesService,
        AcademiesService,
    ],
    // entryComponents: [UiPerPageComponent],
})
export class ClassesListComponent implements OnInit, OnDestroy, OnDestroy {
    public academy_id: any;

    public academyProfile: any;
    public filterBy: String = 'date';
    public filerDate = new Date();
    public isManager: Boolean = false;
    public datePickerOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
    };


    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;
    @ViewChild(CreateClassScheduleComponent)
    public createClassSchedule: CreateClassScheduleComponent;

    private ngUnsubscribe: Subject < void > = new Subject < void > ();

    public listClasses: Array<any> = [];
    public tab = 'schedule';
    public tab2 = 'classes';

    constructor(
        private activityService: ActivitiesService,
        private scheduleSrvc: ScheduleService,
        private academyService: AcademiesService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private broadcaster: Broadcaster,
        private titleService: Title,
    ) {
        this.titleService.setTitle('List classes');
        this.activeRoute.params.subscribe(params => {
            this.academy_id = params['id'];
        });
    }

    ngOnInit() {
        this.getAcademyInfo();
        this.getClases();
        this.registerActivitiBroadcast();
    }

    public onOpenCreateClass(event) {
        this.createClassSchedule.onOpenUpdate(event.id);
    }

    registerActivitiBroadcast() {
        this.activityService.on()
            .takeUntil(this.ngUnsubscribe)
            .subscribe(obj => {
                this.getClases();
            });
    }

    getClases() {
        if (this.academy_id) {
            const from = _d.startOfWeek(new Date(), {
                weekStartsOn: 1,
            });

            const to = _d.endOfWeek(new Date(), {
                weekStartsOn: 1,
            });
            this.scheduleSrvc.getSchedule(from, to, this.academy_id)
                .takeUntil(this.ngUnsubscribe)
                .subscribe(resp => {
                    console.log('getClases', resp);
                    if (resp.status) {
                        this.parseShedules(resp.data);
                    }
                });
        }
    }

    private parseShedules(data) {
        const self = this;
        let schedule: Array < any > = [];
        _.forEach(data, (classes, index) => {

            classes.forEach(_class => {

                _class.shedules.forEach(_shedule => {
                    if (_shedule.holiday == 0) {
                        const [h1, m1] = _shedule.time_start.split('.');
                        const [h2, m2] = _shedule.time_end.split('.');
                        const time_start = _d.setHours(_d.setMinutes(new Date(), m1), h1);
                        const time_end = _d.setHours(_d.setMinutes(new Date(), m2), h2);
                        schedule.push({
                            ..._shedule,
                            name: _class.name,
                            subscriptions: _class.subscriptions,
                            colorBorder: _class.color_border,
                            colorBg: _class.color_bg,
                            time_start: _d.format(time_start, 'h:mm a'),
                            time_end: _d.format(time_end, 'h:mm a'),
                            isOpen: false,
                            meta: {
                                type: 'class',
                                role: 'manager',
                                id: _class.id,
                            },
                        });
                    }
                });
            });
        });
        this.listClasses = schedule;
    }

    private getAcademyInfo() {
        this.academyService.getAcademyById(this.academy_id)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(academy => {
                this.academyProfile = academy;
            });
    }

    public onSubsOpen(e, _class: any) {
        _class.isOpen = !_class.isOpen;
        e.stopPropagation();
    }

    public onChangeFilterStatus(tab) {
        switch (tab) {
            case 'schedule':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes']);
                    break;
                }
            case 'holiday':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'holiday']);
                    break;
                }
            case 'checkin':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes', 'check-in']);
                    break;
                }
        }
    }
    public onChangeFilterStatus2(tab) {
        switch (tab) {
            case 'schedules':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'schedule']);
                    break;
                }
            case 'classes':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'classes']);
                    break;
                }
            case 'subscriptions':
                {
                    this.router.navigate(['academiy-datail', this.academy_id, 'create-subscriptions']);
                    break;
                }
        }
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
