import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';
import * as _is from 'is_js';

@Component({
  selector: 'opn-class-subs-list',
  templateUrl: './subscriptions-list.component.html',
  styleUrls: ['./subscriptions-list.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ClassViewSubscriptionsComponent implements OnInit {

  @Input() subscriptions: Array<any>;
  @Input() pos: String;

  constructor() { }

  ngOnInit() {
  }
}
