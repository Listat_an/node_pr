import { RouterModule, Routes, CanActivate } from '@angular/router';
import { NgModule } from '@angular/core';

import { SchedulePageComponent } from './schedule-page.component';

const routes: Routes = [{
  path: '',
  component: SchedulePageComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SchedulePageRouting {
}
