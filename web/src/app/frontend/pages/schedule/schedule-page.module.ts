import {
    NgModule,
} from '@angular/core';
import {
    CommonModule,
} from '@angular/common';
import {
    FormsModule,
} from '@angular/forms';
import {
    HttpModule,
} from '@angular/http';
// import {ShareButtonsModule} from 'ngx-sharebuttons';
import {
    HttpClientModule,
} from '@angular/common/http';

import {
    SchedulePageRouting,
} from './schedule-page.routing';
import {
    SchedulePageComponent,
} from './schedule-page.component';

import {
    PagesModule,
} from '../pages.module';
import {
    AcademyComponentsModule,
} from '../components/academy/academy.module';
import {
    OpnWeekCalendarModule,
} from 'components/opn-week-calendar/opn-week-calendar.module';
import { ScheduleService } from '../../../services/schedule.service';
import { CreateClassScheduleModule } from '../components/academy/create-class-schedule/create-class-schedule.module';
import { CreateHolidayScheduleModule } from '../components/academy/create-holiday-schedule/create-holiday-schedule.module';


@NgModule({
    declarations: [
        SchedulePageComponent,
    ],
    imports: [
        SchedulePageRouting,
        CommonModule,
        FormsModule,
        HttpClientModule,
        HttpModule,
        PagesModule,
        AcademyComponentsModule,
        OpnWeekCalendarModule,
        CreateClassScheduleModule,
        CreateHolidayScheduleModule,
    ],
    providers: [
        ScheduleService,
    ],
})
export class SchedulePageModule {}
