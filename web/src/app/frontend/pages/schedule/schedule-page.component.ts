import {
    CreateClassScheduleComponent,
} from './../components/academy/create-class-schedule/create-class-schedule.component';
import {
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
} from '@angular/core';
import {
    Title,
} from '@angular/platform-browser';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import * as _d from 'date-fns';
import {
    ScheduleService,
} from '../../../services/schedule.service';
import {
    Subject,
} from 'rxjs/Subject';
import {
    ActivitiesService,
} from '../../../services/activities.service';
import {
    takeUntil,
} from 'rxjs/operator/takeUntil';
import { CreateHolidayScheduleComponent } from '../components/academy/create-holiday-schedule/create-holiday-schedule.component';
import * as _ from 'lodash';

@Component({
    selector: 'opn-schedule-page',
    templateUrl: './schedule-page.component.html',
    styleUrls: ['./schedule-page.component.scss', '../@theme/scss/theme.scss'],
    providers: [ActivitiesService],
})
export class SchedulePageComponent implements OnInit, OnDestroy {
    public academyId: any;
    public isManager: Boolean = false;
    public nowDate = new Date();
    private ngUnsubscribe: Subject <void> = new Subject <void> ();
    public scheduleSource: Array <any>;
    public holidaysSource: Array <any>;
    public scheduleLoad = false;
    public holidaysLoad = false;
    public tab: String = 'schedules';

    @ViewChild(CreateClassScheduleComponent)
    public createClassSchedule: CreateClassScheduleComponent;
    @ViewChild(CreateHolidayScheduleComponent)
    public createHolidaySchedule: CreateHolidayScheduleComponent;

    constructor(
        private titleService: Title,
        private activeRoute: ActivatedRoute,
        private scheduleSrvc: ScheduleService,
        private activitiService: ActivitiesService,
        private router: Router,
    ) {
        this.titleService.setTitle('Schedule');
        this.activeRoute.params.subscribe(params => {
            this.academyId = params['id'];
        });
    }
    ngOnInit() {
        this.initSchedule();
        this.initHolidays();
    }

    public onClassClick(event) {
        this.createClassSchedule.onOpenUpdate(event.id);
        console.log('class click');
    }

    public onHolidayClick(event) {
        this.createHolidaySchedule.openUpdateModal(event);
        console.log('holiday click: ', event);
    }
    public isEmpty(obj) {
        return _.isEmpty(obj);
    }

    registerActivitiBroadcast() {
        this.activitiService.on()
            .takeUntil(this.ngUnsubscribe)
            .subscribe(obj => {
                this.scheduleLoad = false;
                this.initSchedule();
                console.log('activitiService.on');
            });

        this.activitiService.onHoliday()
            .takeUntil(this.ngUnsubscribe)
            .subscribe(obj => {
                this.holidaysLoad = false;
                this.initHolidays();
                console.log('activitiService.onHoliday');
            });
    }

    private initSchedule() {
        const from = _d.startOfWeek(new Date(), {
            weekStartsOn: 1,
        });

        const to = _d.endOfWeek(new Date(), {
            weekStartsOn: 1,
        });
        this.scheduleSrvc.getSchedule(from, to, this.academyId)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(resp => {
                if (resp.status) {
                    this.scheduleSource = resp.data;
                    this.scheduleLoad = true;
                    this.registerActivitiBroadcast();
                }
            });
    }

    private initHolidays() {
        const from = _d.startOfWeek(new Date(), {
            weekStartsOn: 1,
        });

        const to = _d.endOfWeek(new Date(), {
            weekStartsOn: 1,
        });

        this.scheduleSrvc.getHolidays(from, to, this.academyId)
            .takeUntil(this.ngUnsubscribe)
            .subscribe(resp => {
                if (resp.status) {
                    this.holidaysSource = resp.data;
                    this.holidaysLoad = true;
                }
            });
    }
    public onOpenCreateClass() {
        this.createClassSchedule.onOpen();
    }
    public onOpenCreateHoliday() {
        this.createHolidaySchedule.openModal();
    }
    onCheckManager(status) {
        this.isManager = status;
    }

    public onChangeFilterStatus(tab) {
        switch (tab) {
            case 'schedule':
                {
                    this.router.navigate(['academiy-datail', this.academyId, 'schedule']);
                    break;
                }
            case 'classes':
                {
                    this.router.navigate(['academiy-datail', this.academyId, 'classes']);
                    break;
                }
            case 'subscriptions':
                {
                    this.router.navigate(['academiy-datail', this.academyId, 'create-subscriptions']);
                    break;
                }
        }
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }
}
