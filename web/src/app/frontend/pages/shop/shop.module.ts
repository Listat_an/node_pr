import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

import {ShopRoutingModule} from './shop.routing.module';
import {PagesModule} from '../pages.module';
import {UserShopComponent} from './user-shop/user-shop.component';
import {ShopComponent} from './shop.component';
import {Ng4GeoautocompleteModule} from 'ng4-geoautocomplete';
import {ProductPageComponent} from './product-page/product-page.component';
import {UiBackBtnModule} from '../../../../components/ui/ui-back-btn/ui-back-btn.module';
import {ProductInfoModule} from './product/product-info.module';
import {ProductCardComponent} from './product-card/product-card.component';
import {CartComponent} from './cart/cart.component';
import {UserCartComponent} from './cart/user-cart/user-cart.component';
import {UiQtyInpModule} from '../../../../components/ui/ui-qty-inp/ui-qty-inp.module';
import {ShopOrdersComponent} from './orders/orders.component';
import {ShopOrderDetailComponent} from './orders/order-detail/order-detail.component';
import {NgDatepickerModule} from 'ng2-datepicker';
import {AngularMultiSelectModule} from '../../../../components/angular2-multiselect-dropdown';
import {UiPerPageModule} from '../../../../components/ui/ui-per-page/ui-per-page.module';
import {NgPipesModule} from 'ngx-pipes';
import {NgxMaskModule} from 'ngx-mask';

import {CartProductComponent} from './cart/cart-product/cart-product.component';
import {CartYourDetailsComponent} from './cart/your-details/your-details.component';
import {PaymentCartComponent} from './cart/payment/payment-cart.component';
import {CartBillingAddressComponent} from './cart/billing-address/billing-address.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ShopRoutingModule,
        PagesModule,
        Ng4GeoautocompleteModule,
        UiQtyInpModule,
        UiBackBtnModule,
        ProductInfoModule,
        NgDatepickerModule,
        AngularMultiSelectModule,
        UiPerPageModule,
        NgPipesModule,
        NgxMaskModule.forRoot(),
    ],
    declarations: [
        ShopComponent,
        CartComponent,
        UserCartComponent,
        CartProductComponent,
        CartYourDetailsComponent,
        UserShopComponent,
        ProductPageComponent,
        ProductCardComponent,
        ShopOrdersComponent,
        ShopOrderDetailComponent,
        PaymentCartComponent,
        CartBillingAddressComponent,
    ],
})
export class ShopModule {
}
