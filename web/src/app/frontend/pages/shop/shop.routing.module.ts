import {
    NgModule,
} from '@angular/core';
import {RouterModule, Routes, CanActivate} from '@angular/router';
import {UserShopComponent} from './user-shop/user-shop.component';
import {ShopComponent} from './shop.component';
import {ProductPageComponent} from './product-page/product-page.component';
import {CartComponent} from './cart/cart.component';

import {ShopOrdersComponent} from './orders/orders.component';
import {ShopOrderDetailComponent} from './orders/order-detail/order-detail.component';

const shopRoutings: Routes = [
    {
        path: '',
        component: ShopComponent,
        children: [
            {
                path: '',
                component: UserShopComponent,
            },
            {
                path: 'cart',
                component: CartComponent,
            },
            {
                path: 'product/:productId',
                component: ProductPageComponent,
            },
            {
                path: 'orders',
                component: ShopOrdersComponent,
            },
            {
                path: 'orders/:orderId',
                component: ShopOrderDetailComponent,
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(shopRoutings),
    ],
    exports: [
        RouterModule,
    ],
})

export class ShopRoutingModule {
}
