import {
    Component,
    OnInit,
    ViewEncapsulation,
    OnDestroy,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import {
    cancelSubscription,
} from '../../../../../providers/cancelSubscription';

import {
    OrdersService,
} from '../orders.service';

import {DataService} from '../../../../../services/data.service';

import {
    AcademiesService,
} from '../../../../../services/academies.service';
import {ConfigService} from '../../../../../services/service.config';

import * as _ from 'lodash';

@Component({
    selector: 'opn-shop-orders-detail',
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.scss', '../../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        AcademiesService,
        OrdersService,
    ],
})
export class ShopOrderDetailComponent implements OnInit, OnDestroy {
    private academyId: Number;
    private orderId: Number;
    private userId: Number;
    private orderData: any = null;
    private load: Boolean = true;
    private academyInfo: any;
    private subscrips = [];
    private academies = [];

    private viewAcademy = 0;

    private baseUrl = ConfigService.URL_SERVER;
    private noPhoto = '/assets/images/no-photo-product.png';

    protected statusLabels: object = {
        new: 'New',
        in_process: 'In process',
        shipped: 'Shipped',
    };

    private statusBar = {
        data: [],
        count: 0,
    };

    protected paidLabels: object = {
        paid: 'Paid',
        not_paid: 'Not paid',
    };

    totalsData: any;

    constructor(
        private academySrv: AcademiesService,
        private ordersSrv: OrdersService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private dataServices: DataService,
    ) {
        _.forIn(this.statusLabels, (v, k) => {
            this.statusBar.data.push({
                id: k,
                label: v,
                active: false,
                hover: false,
            });
            this.statusBar.count += 1;
        });
    }



    ngOnInit() {

        this.userId = this.ordersSrv.getMe().id;
        this.activeRoute.params.subscribe(params => {
            this.orderId = params['orderId'];
        });

        this.getOrder();
    }

    private getOrder() {
        this.load = false;
        this.subscrips.push(
            this.ordersSrv.getUserOrderById(this.orderId).finally(() => {
                this.load = true;
            }).subscribe(order => {
                console.log('Order deteils: ', order);
                this.totalsData = {
                    payment_status: order.payment_status,
                    subtotal: order.subtotal,
                    shipping_cost: order.shipping_cost,
                    tax: order.tax,
                    total: order.total,
                };
                this.orderData = order.academy_orders;
                this.parseAcademy(order.academy_orders);
            }),
        );
    }
    parseAcademy(items) {
        items.forEach(el => {
            if (_.findIndex(this.academies, (academy) => {
                return academy.id === el.academy.id;
            }) < 0) {
                this.academies.push(el.academy);
            }
        });
    }

    public onEventMessage(academyId) {
        this.dataServices.eventMessage.emit({
            user_id: this.userId,
            academy_id: academyId,
            type: 'academy',
        });
    }
    onPrevSeler() {
        if (this.viewAcademy > 0) {
            this.viewAcademy --;
        }else {
            this.viewAcademy = this.academies.length - 1;
        }
    }

    onNextSeler() {
        if (this.viewAcademy < this.academies.length - 1) {
            this.viewAcademy ++;
        }else {
            this.viewAcademy = 0;
        }
    }

    toOrders() {
        this.router.navigate(['shop', 'orders']);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
