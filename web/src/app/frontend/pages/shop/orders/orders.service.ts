import {
    Injectable,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from '../../../../services/service.config';
import {
    JwtHelperService,
} from '@auth0/angular-jwt';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as _ from 'lodash';

import { format, addDays } from 'date-fns';

@Injectable()
export class OrdersService {
    private options;
    private activate: Boolean;
    private user: any;

    constructor(private http: Http, public jwtHelper: JwtHelperService) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    public getMe(): any {
        return this.user;
    }

    public getUserOrders(size, page, userId, sort, filter): Observable < any > {
        let sortStr: string = '';
        let filterStr: string = '';
        if (sort) {
            _.forIn(_.values(sort), (v) => {
                if (v['order'] === 'ASC') {
                    sortStr += (sortStr !== '' ? ',' : '') + v['field'];
                }
                if (v['order'] === 'DESC') {
                    sortStr += (sortStr !== '' ? ',-' : '-') + v['field'];
                }
            });
        }

        if (sortStr !== '') {
            sortStr = '&sort=' + sortStr;
        }

        if (filter.status && filter.status.length > 0) {
            const types = filter.status.map(status => {
                return status.id;
            });
            filterStr += `&filter[status][in]=${types.join(',')}`;
        }

        if (filter.id) {
            filterStr += `&filter[id]=${filter.id}`;
        }
        if (filter.date) {
            filterStr += `&filter[created_at][gte]=${format(filter.date.fromDate, 'YYYY-MM-DD')}&filter[created_at][lte]=${format(filter.date.toDate, 'YYYY-MM-DD')}`;
        }

        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/orders?filter[type]=user&filter[user_id]=${userId}&page[size]=${size}&page[number]=${page}${sortStr}${filterStr}`),
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }

    public getUserOrdersCount(userId): Observable < any > {

        return this.http.get(
                encodeURI(ConfigService.STATIC_SERVER + `/orders?filter[type]=user&filter[user_id]=${userId}&page[size]=100000`),
                this.options,
            )
            .map(res => {
                let statusArr: any = {
                    new: [],
                    in_process: [],
                    shipped: [],
                };
                return _.map(_.assign(statusArr, _.groupBy(res.json().data, 'status')), (val: Array<any>, key) => {
                    return {
                        label: key,
                        count: val.length,
                    }
                });
            })
            .catch(this.handleError);
    }

    public getOrderById( orderId ): Observable < any > {
        return this.http.get(
            encodeURI(ConfigService.STATIC_SERVER + `/orders/${orderId}/?include=items,items.academy,user,user.belt_user.belt&filter[type]=academy`),
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }

    public getUserOrderById( orderId ): Observable < any > {
        return this.http.get(
            encodeURI(ConfigService.STATIC_SERVER + `/orders/${orderId}/?include=items.academy,user,academy_orders.items,academy_orders.academy&filter[type]=user`),
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }

    public setOrderStatus(orderId, data) {
        return this.http.put(
            encodeURI(ConfigService.STATIC_SERVER + `/orders/${orderId}`),
            data,
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }

    private handleError(error: any) {
        return Observable.throw(error.message || error);
    }
}
