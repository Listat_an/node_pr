import {
    Component,
    OnInit,
    ViewEncapsulation,
    OnDestroy,
    ViewChild,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
    NavigationEnd,
} from '@angular/router';
import {
    cancelSubscription,
} from '../../../../providers/cancelSubscription';
import {
    Subscription,
} from 'rxjs/Subscription';

import {
    OrdersService,
} from './orders.service';

import * as _ from 'lodash';
import {
    addDays,
    isBefore,
    subMonths,
} from 'date-fns';
import {
    DatepickerOptions,
} from 'ng2-datepicker';

import {
    Subject,
} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import {
    AcademiesService,
} from '../../../../services/academies.service';

@Component({
    selector: 'opn-shop-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss', '../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        AcademiesService,
        OrdersService,
    ],
})
export class ShopOrdersComponent implements OnInit, OnDestroy {
    public subscrips: Subscription[] = [];
    private orders: Array < any > = [];

    private studentId: Number = null;
    private isAddBelt: Boolean = false;
    row_index: number = -1;
    private per_page: any = 10;
    private page: any = 1;
    private pagintion: any;
    private load: Boolean = true;
    private userId = null;

    private filter = {
        status: [],
        id: '',
        date: {
            fromDate: subMonths(new Date(), 1),
            toDate: addDays(new Date(), 1),
        },
    };

    private sort = {
        id: {
            order: 'DESC',
            field: 'id',
        },
        date: {
            order: '',
            field: 'created_at',
        },
        full_name: {
            order: '',
            field: 'full_name',
        },
        status: {
            order: '',
            field: 'status',
        },
        total: {
            order: '',
            field: 'total',
        },
    };

    protected statusLabels: object = {
        in_process: 'In process',
        new: 'New',
        shipped: 'Shipped',
    };

    @ViewChild('fromDateComponent') fromDateComponent;

    toDateOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
        maxDate: addDays(new Date(), 1),
    };

    fromDateOptions: DatepickerOptions = {
        ...this.toDateOptions,
    };

    statusSelectSettings = {
        text: 'Status',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: false,
        badgeShowLimit: 1,
        classes: 'myclass custom-class-search',
    };

    statusOptions = [];

    private subjectFilter: Subject < string > = new Subject();
    private tab: String = 'orders';
    private statusArr: Array<Object>;
    public fragment = 'products';

    constructor(
        private ordersSrv: OrdersService,
        private router: Router,
        private ar: ActivatedRoute,
    ) {
        this.pagintion = {
            show: false,
            pages: [],
        };

        _.forIn(this.statusLabels, (v: string, k: string) => {
            this.statusOptions.push({
                itemName: v,
                id: k,
            }, );
        });
    }



    ngOnInit() {

        this.subscrips.push(
            this.ar.fragment.subscribe(fragment => {
                if (fragment) {
                    this.fragment = fragment;
                }
            }),
        );

        this.userId = this.ordersSrv.getMe().id;

        this.getUserOrders();

        this.subjectFilter.debounceTime(500).subscribe(() => {
            this.getUserOrders();
        });
        this.getOrdersStatusCount(this.userId);
    }

    getOrdersStatusCount(userId) {
        this.ordersSrv.getUserOrdersCount(userId).subscribe(statuses => {
            this.statusArr = statuses;
        });
    }

    onOrderSearch() {
        this.subjectFilter.next();
    }

    onToDateChange(toDate) {
        if (isBefore(toDate, this.filter.date.fromDate)) {
            this.filter.date.fromDate = toDate;
        }
        this.fromDateOptions.maxDate = toDate;
        this.fromDateComponent.init();
        this.getUserOrders();
    }

    selectStatus(options) {
        console.log('options ', options);
    }

    private getUserOrders() {
        this.load = false;
        this.subscrips.push(
            this.ordersSrv.getUserOrders(this.per_page, this.page, this.userId, this.sort, this.filter)
            .finally(() => {
                this.load = true;
            })
            .subscribe(orders => {
                this.formatData(orders.data);
                this.Paginations(orders.total);
            }),
        );
    }

    private formatData(data) {
        this.orders = data;
    }

    onChangeFilter(nameFilter) {
        if (this.sort[nameFilter].order === '' || this.sort[nameFilter].order === 'DESC') {
            this.sort[nameFilter].order = 'ASC';
            this.getUserOrders();
            return;
        }
        if (this.sort[nameFilter].order === 'ASC') {
            this.sort[nameFilter].order = 'DESC';
            this.getUserOrders();
            return;
        }
    }

    Paginations(total) {
        if (total > this.per_page) {
            this.pagintion.pages = _.range(1, _.ceil(total / this.per_page) + 1, 1);
            this.pagintion.show = true;
        } else {
            this.pagintion.show = false;
        }
    }

    onLoadPage(page) {
        this.page = page;
        this.getUserOrders();
    }

    public onChangePerPage(e) {
        this.per_page = e.value;
        this.getUserOrders();
    }

    toOrder(orderId) {
        this.router.navigate(['shop', 'orders', orderId]);
    }

    onChangeTab(tab) {
        switch (tab) {
            case 'products':
                {
                    this.router.navigate(['shop']);
                    break;
                }
            case 'orders':
                {
                    this.router.navigate(['shop', 'orders']);
                    break;
                }
            case 'cart':
                {
                    this.router.navigate(['shop', 'cart']);
                    break;
                }
        }
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
