import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'opn-product-page',
    templateUrl: './product-page.html',
    styleUrls: ['./product-page.scss'],
})
export class ProductPageComponent{
    productName: string = '';
    academyId;

    constructor(private router: Router,
                private activeRoute: ActivatedRoute) {
        this.activeRoute.parent.params.subscribe(params => {
            this.academyId = params['id'];
        });
    }

    searchProducts() {
        this.router.navigate([`shop`], {queryParams: {productName: this.productName}})
    }
}
