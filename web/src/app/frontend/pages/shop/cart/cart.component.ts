import {Component, OnInit} from '@angular/core';
import {ShopService} from '../../../../services/shop.service';

import {Router} from '@angular/router';

@Component({
    selector: 'opn-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss', '../../@theme/scss/theme.scss'],
    providers: [ShopService],
})
export class CartComponent implements OnInit {

    private stepOrder = 1;
    private totals: any = {};
    status: String = 'cart';
    orderId: any = null;
    public tab = 'cart';

    constructor(
        private shopService: ShopService,
        private router: Router,
    ) {
    }

    ngOnInit() {
    }

    onchangeTotals(total) {
        this.totals = total;
    }
    onPay(token) {
        this.shopService.paymentOrder({
           ...token,
           order_id: this.orderId,
          }).subscribe(res => {
            if (res.status) {
                this.stepOrder = 4;
            }
        });
    }

    goShop() {
        this.router.navigate(['shop']);
    }

    onChangeStep(step) {
        this.stepOrder = step;
    }
    onChangeTab(tab) {
        switch (tab) {
            case 'cart': {
                this.router.navigate(['shop', 'cart']);
                break;
            }
            case 'orders': {
                this.router.navigate(['shop', 'orders'], { fragment: 'cart' });
            }
        }
    }
}
