import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ShopService} from '../../../../../services/shop.service';
import {ConfigService} from '../../../../../services/service.config';
import {FormGroup, FormBuilder, Validators, FormControl, AbstractControl} from '@angular/forms';
import * as _ from 'lodash';
import {AutoCompleteSearchService} from 'ng4-geoautocomplete/auto-complete.service';

@Component({
    selector: 'opn-cart-your-details',
    templateUrl: './your-details.component.html',
    styleUrls: ['./your-details.component.scss'],
    providers: [ShopService],
})
export class CartYourDetailsComponent implements OnInit {
    products;

    baseUrl = ConfigService.URL_SERVER;
    noPhoto = '/assets/images/no-photo-product.png';
    public form: FormGroup;
    private addres = {
        street: {
            street_number: '',
            route: '',
        },
        locality: '',
        state: '',
    };
    data = {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        state: '',
        sity: '',
        street: '',
    };

    private gaAutocompleteStings: any = {
        showSearchButton: false,
        showCurrentLocation: false,
        currentLocIconUrl: 'https://cdn4.iconfinder.com/data/icons/proglyphs-traveling/512/Current_Location-512.png',
        locationIconUrl: 'http://www.myiconfinder.com/uploads/iconsets/369f997cef4f440c5394ed2ae6f8eecd.png',
        recentStorageName: 'cart-data',
        noOfRecentSearchSave: 8,
        geoTypes: ['address'],
        inputPlaceholderText: '',
        inputString: '',
    };

    private billingData: any;
    addressText: String;

    @Output() changeStep: EventEmitter<any> = new EventEmitter();
    @Output() changeTotals: EventEmitter<any> = new EventEmitter();
    @Output() outOrderId: EventEmitter<any> = new EventEmitter();

    constructor(
        private shopService: ShopService,
        private fb: FormBuilder,
        private acSearchService: AutoCompleteSearchService,
    ) {
    }

    ngOnInit() {
        this.buildForm();
        this.savedCheckout();
    }

    savedCheckout() {
        this.shopService.savedCheckout().subscribe(res => {
            console.log('savedCheckout ', res);
            if (res.order) {
                this.billingData = _.pick(res.order, ['billing_first_name', 'billing_last_name', 'billing_email', 'billing_phone', 'billing_address', 'billing_state', 'billing_sity', 'billing_street']);
                this.setFormValues(_.assign(this.data, _.pick(res.order, _.keys(this.data))));
                (<HTMLInputElement>document.getElementById('search_places')).value = res.order.address;
                this.addressText = res.order.address;
                this.changeTotals.emit({
                    subtotal: res.order.subtotal,
                    shipping: res.order.shipping_cost,
                    tax: res.order.tax,
                    total: res.order.total,
                });
                this.outOrderId.emit(res.order.id);
            }else {
                this.getUserData();
                const sh = _.sumBy(res.shipping_details, 'shipping_cost');
                this.changeTotals.emit({
                    shipping: sh,
                });
            }
        });
    }

    getUserData() {
        this.shopService.getUserMe().subscribe(res => {
            this.setFormValues(_.assign(this.data, _.pick(res, _.keys(this.data))));
            (<HTMLInputElement>document.getElementById('search_places')).value = res.address;
            this.addressText = res.address;
            this.acSearchService.getGeoLatLngDetail({lat: parseFloat(res.latitude), lng: parseFloat(res.longitude)}).then(data => {
                this.setShortAddres(data);
            });
        });
    }

    public buildForm() {
        this.form = this.fb.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            phone: ['', Validators.required],
            state: ['', Validators.required],
            sity: ['', Validators.required],
            street: ['', Validators.required],
        });
    }

    setFormValues(data) {
        _.forEach(data, (v, k) => {
            this.form.controls[k].setValue(v);
        });
    }

    autoCompleteCallback1(event) {
        if (event.data) {
            this.addressText = event.data.formatted_address;
            this.addres = {
                street: {
                    street_number: '',
                    route: '',
                },
                locality: '',
                state: '',
            };
            this.setShortAddres(event.data);
        }
    }

    setShortAddres(data) {
        data.address_components.forEach(el => {
            if (_.indexOf(['administrative_area_level_1', 'locality', 'street_number', 'route'], el.types[0]) >= 0) {
                this.completeAddress(el.types[0], el.short_name);
            }
        });
    }
    completeAddress(type, value) {
        if ( type == 'street_number' ) {
            this.addres.street[type] = value;
        }else if (type == 'route') {
            this.addres.street[type] = value;
            this.form.controls['street'].setValue(`${this.addres.street.street_number} ${this.addres.street.route}`);
        }else if ( type == 'locality' ) {
            this.addres.locality = value;
            this.form.controls['sity'].setValue(value);
        }else if ( type == 'administrative_area_level_1' ) {
            this.addres.state = value;
            this.form.controls['state'].setValue(value);
        }
    }

    prevOrder() {
        this.changeStep.emit(1);
    }

    onSaveYourData() {
        if (this.form.valid) {
            this.shopService.prepareCheckout({
                ..._.forIn(this.form.controls, (v, k, o) => {
                    o[k] = v.value;
                }),
                address: this.addressText,
                ...this.billingData,
            }).subscribe(res => {
                this.changeTotals.emit({
                    subtotal: res.subtotal,
                    shipping: res.shipping_cost,
                    tax: res.tax,
                    total: res.total,
                });
                this.changeStep.emit(3);
                this.outOrderId.emit(res.id);
            });
        }
    }
}
