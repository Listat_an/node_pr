import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {ShopService} from '../../../../../services/shop.service';
import {ConfigService} from '../../../../../services/service.config';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'opn-user-cart',
    templateUrl: './user-cart.component.html',
    styleUrls: ['./user-cart.component.scss'],
    providers: [ShopService],
})
export class UserCartComponent implements OnInit {
    products: Array<any> = [];

    totals = 0;
    productErrore: any;

    baseUrl = ConfigService.URL_SERVER;
    noPhoto = '/assets/images/no-photo-product.png';

    @Output() changeStep: EventEmitter<any> = new EventEmitter();
    constructor(
        private shopService: ShopService,
        private toastr: ToastrService,
    ) {
    }

    ngOnInit() {
        this.getCart();
    }

    calcTotal() {
        this.totals = _.sumBy(this.products, (el) => {
            return el.price * el.quantity + el.product.shipping_price;
        });
    }

    getCart() {
        this.shopService.getUserCart().subscribe(res => {
            console.log(res['data']);
            this.products = res['data'];
            this.calcTotal();
        });
    }

    deleteProduct(product) {
        this.shopService.deleteProductFromCart(product.id).subscribe(res => {
            _.remove(this.products, product);
            this.calcTotal();
        });
    }

    onChangeQuantity(q, product) {
        const i = _.indexOf(this.products, product);
        if (q == 0) {
            this.deleteProduct(product);
            return;
        }
        this.shopService.changeQuantity(product.id, {quantity: q}).subscribe(
            res => {
                if (res) {
                    this.products[i] = res;
                    this.calcTotal();
                }
            },
            err => {
                this.products[i].error = {
                    details: err.errors[0].details,
                    timestamp: Date.now(),
                };
                console.info(this.products[i]);
                this.toastr.info(err.errors[0].description, 'Warning!');
            },
        );
    }
    cancelOrder() {
        this.shopService.cancelOrder().subscribe(res => {
            if (res.status) {
                this.products = [];
            }
        });
    }
    nextStep() {
        this.shopService.savedCheckout().subscribe(res => {
            if (!res.order) {
                this.changeStep.emit(2);
            }else {
                this.shopService.prepareCheckout(_.pick(res.order, ['first_name', 'last_name', 'email', 'phone', 'address', 'state', 'sity', 'street', 'billing_first_name', 'billing_last_name', 'billing_email', 'billing_phone', 'billing_address', 'billing_state', 'billing_sity', 'billing_street'])).subscribe(order => {
                    if (order) {
                        this.changeStep.emit(2);
                    }
                });
            }
        });
        
    }
}
