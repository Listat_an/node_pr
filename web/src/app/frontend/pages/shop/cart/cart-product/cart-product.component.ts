import {Component, OnInit, Input} from '@angular/core';
import {ShopService} from '../../../../../services/shop.service';
import {ConfigService} from '../../../../../services/service.config';
import * as _ from 'lodash';

@Component({
    selector: 'opn-cart-product',
    templateUrl: './cart-product.component.html',
    styleUrls: ['./cart-product.component.scss'],
    providers: [ShopService],
})
export class CartProductComponent implements OnInit {
    products;

    baseUrl = ConfigService.URL_SERVER;
    noPhoto = '/assets/images/no-photo-product.png';

    @Input() totals?: any;

    tmpSubTotal: any;
    tmpTotal: any;

    constructor(private shopService: ShopService) {
    }

    ngOnInit() {
        this.getCart();
    }

    calcTotal(products) {
        this.tmpSubTotal = this.tmpTotal = _.sumBy(products, (el: any) => {
            return el.price * el.quantity;
        });
    }

    getCart() {
        this.shopService.getUserCart().subscribe(res => {
            console.log('getUserCart ', res['data']);
            this.products = res['data'];
            this.calcTotal(res['data']);
        });
    }
}
