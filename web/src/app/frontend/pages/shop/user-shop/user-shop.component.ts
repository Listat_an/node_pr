import {ShopService} from '../../../../services/shop.service';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { OrdersService } from '../orders/orders.service';

export interface LocationSearchData {
    lat: any;
    lng: any;
    searchLocationRadius: any;
}

@Component({
    selector: 'opn-user-shop',
    templateUrl: './user-shop.html',
    styleUrls: ['./user-shop.scss', '../../@theme/scss/theme.scss'],
    providers: [
        ShopService,
        OrdersService,
    ],
})
export class UserShopComponent implements OnInit {
    products;
    productName: String = '';
    minPrice: number;
    maxPrice: number;

    cardsPerPagination = 8;
    limit = this.cardsPerPagination;
    productsCount = 0;
    isPriceIncreasing = true;

    locationSearchData: LocationSearchData;

    gaAutoCompleteSettings: any = {
        showSearchButton: false,
        currentLocIconUrl: 'https://cdn4.iconfinder.com/data/icons/proglyphs-traveling/512/Current_Location-512.png',
        locationIconUrl: 'http://www.myiconfinder.com/uploads/iconsets/369f997cef4f440c5394ed2ae6f8eecd.png',
        recentStorageName: 'componentData4',
        noOfRecentSearchSave: 8,
        inputPlaceholderText: 'Location',
        geoTypes: ['(regions)'],
        inputString: '',
    };

    private tab: String = 'search';
    private userId: any = null;
    private load: Boolean = false;

    constructor(
        private shopService: ShopService,
        private activatedRoute: ActivatedRoute,
        private ordersSrv: OrdersService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.userId = this.ordersSrv.getMe().id;
        this.activatedRoute.queryParams.subscribe(res => {
            this.productName = res['productName'] || '';
            this.searchProducts();
        });
    }

    searchProducts() {
        this.load = true;
        this.shopService.getProducts(this.productName, this.locationSearchData, this.maxPrice, this.minPrice, this.isPriceIncreasing, this.limit)
        .subscribe(productsRes => {
            this.shopService.getUserCart().subscribe(cartRes => {
                const cart = cartRes['data'];
                this.products = productsRes['data'];
                this.products.forEach(product => {
                    product['isAdded'] = cart.find(cartProduct => product.id === cartProduct.product_id)
                });
                this.load = false;
                this.productsCount = productsRes['total'];
            });
        });
    }

    onViewMore() {
        this.limit += this.cardsPerPagination;
        this.searchProducts()
    }

    toggleSortByPrice() {
        this.isPriceIncreasing = !this.isPriceIncreasing;
        this.searchProducts();
    }

    autoCompleteCallback(e) {
        if (e.data) {
            const searchLocationRadius = this.findRadius(e.data.geometry);
            const location = e.data.geometry.location;
            this.locationSearchData = {
                lat: location.lat,
                lng: location.lng,
                searchLocationRadius: searchLocationRadius,
            }
        } else {
            this.locationSearchData = null;
        }
    }

    findRadius(data) {
        const lat = data.location.lat;
        const lng = data.location.lng;
        const center = new google.maps.LatLng(lat, lng);
        let distances = [];
        distances.push(google.maps.geometry.spherical.computeDistanceBetween(center, new google.maps.LatLng(lat, data.viewport.east)));
        distances.push(google.maps.geometry.spherical.computeDistanceBetween(center, new google.maps.LatLng(lat, data.viewport.west)));
        distances.push(google.maps.geometry.spherical.computeDistanceBetween(center, new google.maps.LatLng(data.viewport.north, lng)));
        distances.push(google.maps.geometry.spherical.computeDistanceBetween(center, new google.maps.LatLng(data.viewport.south, lng)));
        return this.mToMi(Math.max(...distances));
    }

    mToMi = (m) => m * 0.00062137;

    onChangeTab(tab) {
        switch (tab) {
            case 'search': {
                this.router.navigate(['shop']);
                break;
            }
            case 'orders': {
                this.router.navigate(['shop', tab]);
            }
        }
    }
}
