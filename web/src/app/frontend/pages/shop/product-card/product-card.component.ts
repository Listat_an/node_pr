import {Component, Input, OnInit} from '@angular/core';
import {ConfigService} from '../../../../services/service.config';
import {Router} from '@angular/router';
import {ShopService} from '../../../../services/shop.service';

@Component({
    selector: 'opn-product-card',
    templateUrl: './product-card.html',
    styleUrls: ['./product-card.scss'],
})
export class ProductCardComponent implements OnInit {
    @Input() product;
    baseUrl = ConfigService.URL_SERVER;
    noPhoto = '/assets/images/no-photo-product.png';

    constructor(private router: Router,
                private shopService: ShopService) {
    }

    ngOnInit() {
    }

    addProductToCart() {
        this.shopService.addToCart(this.product.id, this.product.images[0] ? this.product.images[0].url : '', 1).subscribe(res => {
            this.product.isAdded = true;
        });
    }

    goToProduct() {
        this.router.navigate([`shop/product/${this.product.id}`])
    }
}
