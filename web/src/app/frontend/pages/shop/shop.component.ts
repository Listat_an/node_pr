import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'opn-shop',
    templateUrl: './shop.component.html',
    styleUrls: ['./shop.component.scss', '../@theme/scss/theme.scss'],
})
export class ShopComponent implements OnInit {
    constructor() {
    }

    ngOnInit() {
    }
}
