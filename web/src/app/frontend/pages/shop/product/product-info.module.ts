import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '../../../../backend/forms/forms.module';
import {ProductInfoComponent} from './product-info.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: [
        ProductInfoComponent
    ],
    exports: [
        ProductInfoComponent
    ],
})
export class ProductInfoModule{
}
