import {ShopService} from '../../../../services/shop.service';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ConfigService} from '../../../../services/service.config';
import {DataService} from '../../../../services/data.service';

@Component({
    selector: 'opn-product-info',
    templateUrl: './product-info.html',
    styleUrls: ['./product-info.scss'],
    providers: [ShopService]
})
export class ProductInfoComponent implements OnInit {
    productId;
    product;
    mainImageIndex = 0;
    productAdded: boolean = false;
    baseUrl = ConfigService.URL_SERVER;
    noPhoto = '/assets/images/no-photo-product.png';
    images = [];
    loaded: Boolean = true;
    userId: any;

    constructor(private activatedRoute: ActivatedRoute,
                private shopService: ShopService,
                private dataServices: DataService,
            ) {
    }

    ngOnInit() {
        this.userId = this.shopService.getMe().id;
        this.activatedRoute.params.subscribe(res => {
            this.loaded = false;
            this.productId = res['productId'];
            this.shopService.getProductById(this.productId).subscribe(products => {
                this.shopService.getUserCart().subscribe(cart => {
                    this.product = products;
                    this.productAdded = cart['data'].find(product => this.product.id === product.product_id);
                    this.images = this.product.images;
                    this.loaded = true;
                });

            });
        });
    }

    checkProductAdded() {

    }

    public onEventMessage(academyId) {
        this.dataServices.eventMessage.emit({
            user_id: this.userId,
            academy_id: academyId,
            type: 'academy',
        });
    }

    addProductToCart() {
        this.shopService.addToCart(this.product.id, this.product.images[0] ? this.product.images[0].url : '', 1).subscribe(res => {
            this.productAdded = true;
        });
    }

    nextImg() {
        this.mainImageIndex === this.images.length - 1 ? this.mainImageIndex = 0 : this.mainImageIndex += 1;
    }

    prevImg() {
        this.mainImageIndex === 0 ? this.mainImageIndex = this.images.length - 1 : this.mainImageIndex -= 1;
    }
}
