import 'rxjs/add/operator/debounceTime';
import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import * as _ from 'lodash';

import {cancelSubscription} from '../../../../providers/cancelSubscription';
import {ActivitiesService} from '../../../../services/activities.service';
import {AcademiesService} from '../../../../services/academies.service';
import {
    UiPerPageComponent,
} from '../../../../../components/ui/ui-per-page/ui-per-page.component';
import {Title} from '@angular/platform-browser';

import {
    Broadcaster,
} from '../../../../services/broadcaster';
import {
    Subject,
} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import {DatepickerOptions} from '../../../../../components/ng-datepicker/ng-datepicker.component';
import { ModalConfirmComponent } from '../../components/academy/modal-confirm/modal-confirm.component';
import {
    NgbDateStruct,
    NgbCalendar,
} from '@ng-bootstrap/ng-bootstrap';

import * as dfns from 'date-fns';

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
    one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day ?
        false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day ?
        false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
    selector: 'opn-activity-list',
    templateUrl: './activity-list.component.html',
    styleUrls: ['./activity-list.component.scss', '../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        ActivitiesService,
        AcademiesService,
    ],
    // entryComponents: [UiPerPageComponent],
})
export class ActivityListComponent implements OnInit, OnDestroy {
    private academy_id: any;
    private activitiesData: Array<any> = [];
    public subscrips: Subscription[] = [];
    private per_page: any = 10;
    private page: any = 1;
    public academyProfile: any;
    public filterBy: String = 'date';
    public filerDate = {
        start: dfns.format(new Date(), 'DD.MM.YYYY'),
        end:   dfns.format(dfns.addMonths(new Date(), 1), 'DD.MM.YYYY'),
    };
    public isCreateActivity: Boolean = false;
    public isUpdateActivity: Boolean = false;
    public isManager: Boolean = false;
    private editClassSource: any;
    private datePickerOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
    };

    private pagintion: any;
    private subjectFilter: Subject<string> = new Subject();

    private tmpData: any;
    private types: Object = {
        'class': 'Class',
        'seminar': 'Seminar',
        'mat_event': 'Opn Mat',
        'others': 'Others',
    };

    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;

    constructor(
        private activityService: ActivitiesService,
        private academyService: AcademiesService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private broadcaster: Broadcaster,
        private titleService: Title,
    ) {
        this.titleService.setTitle('List classes');
        this.activeRoute.params.subscribe(params => {
            this.academy_id = params['id'];
        });

        this.pagintion = {
            show: false,
            pages: [],
        };
    }

    ngOnInit() {
        this.getActivities();
        this.getAcademyInfo();

        this.subjectFilter.debounceTime(500).subscribe((searchValue) => {
            this.page = 1;
            this.getActivities(searchValue);
        });
        this.registerActivitiBroadcast();
        this.registerNewActivitiesListener();
    }

    private registerActivitiBroadcast() {
        this.broadcaster.on<any>('update_event')
            .subscribe(updatedObj => {
                this.updateObjectClass(updatedObj);
            });
    }

    private registerNewActivitiesListener() {
        this.activityService.on()
            .subscribe(activityObj => {
                if (!this.activitiesData) {
                    this.activitiesData = [];
                    this.activitiesData.push(activityObj);
                } else {
                    this.activitiesData.unshift(activityObj);
                }
            });
    }

    updateObjectClass(obj) {
        this.activitiesData[_.findIndex(this.activitiesData, {id: obj.id})] = obj;
    }

    private getAcademyInfo() {
        this.academyService.getAcademyById(this.academy_id).subscribe(academy => {
            this.academyProfile = academy;
        });
    }

    private getActivities(filter?) {
        this.subscrips.push(
            this.activityService.getActivitiesByAcademy(this.per_page, this.page, this.academy_id, this.filerDate, filter).subscribe(res => {
                console.log(res.data);
                this.activitiesData = res.data;
                this.Paginations(res.total);
            }),
        );
    }

    Paginations(total) {
        if (total > this.per_page) {
            this.pagintion.pages = _.range(1, _.ceil(total / this.per_page) + 1, 1);
            this.pagintion.show = true;
        } else {
            this.pagintion.show = false;
        }
    }

    onLoadPage(page) {
        this.page = page;
        this.getActivities();
    }

    public onChangePerPage(e) {
        this.per_page = e.value;
        this.getActivities();
    }

    public onFilter(name) {
        this.subjectFilter.next(name);
    }

    onEditClass(data) {
        this.editClassSource = data;
        document.body.classList.add('openModal');
        this.isUpdateActivity = true;
    }

    onRemoveClass(data) {
        this.tmpData = data;
        this.modalConfirm.modalTitle = 'Delete your Activity?';
        this.modalConfirm.modalDescription = 'This action will remove your activity.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
    }

    onConfirm(confirm) {
        if (confirm) {
            const self = this;
            this.activityService.removeActivity(this.tmpData.id).subscribe(res => {
                self.activitiesData = self.activitiesData.filter(iEvent => iEvent !== this.tmpData);
                delete(this.tmpData);
            });
        }
    }

    onCloseUpdateClass() {
        document.body.classList.remove('openModal');
        this.isUpdateActivity = false;
    }

    toActivity(id) {
        this.router.navigate([`academiy-datail/${this.academy_id}/activities/${id}`]);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }

    onCheckManager(status) {
        this.isManager = status;
    }

    public closeCreateActivity() {
        document.body.classList.remove('openModal');
        this.isCreateActivity = false;
    }

    public openCreateActivity() {
        document.body.classList.add('openModal');
        this.isCreateActivity = true;
    }

    public getAllActivities() {
        this.filterBy = 'date';
        this.filerDate = null;
        this.getActivities();
    }

    public filterByDate() {
        this.filterBy = 'date';
    }

    public onSelectRange(range) {
        this.filerDate = range;
        this.getActivities();
    }
}
