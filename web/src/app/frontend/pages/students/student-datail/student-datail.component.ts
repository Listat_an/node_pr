import { RefundService } from './../../../../services/refund.service';
import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';


import {cancelSubscription} from '../../../../providers/cancelSubscription';

import {StudentsService} from '../../../../services/students.service';
import {AuthService} from '../../../../services/auth/auth.service';
import {ConfigService} from '../../../../services/service.config';
import { DataService } from '../../../../services/data.service';

@Component({
    selector: 'opn-student-datail',
    templateUrl: './student-datail.component.html',
    styleUrls: ['./student-datail.component.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        StudentsService,
        AuthService,
        RefundService,
    ],
})
export class StudentDatailComponent implements OnInit, OnDestroy {
    studentId;
    academyId;
    public subscrips: Subscription[] = [];
    public studentSubscriptions = [];
    public studentActivities = [];
    public baseUrl = ConfigService.URL_SERVER;
    public student: any;
    isAddBelt = false;
    viewMore = false;
    initialLimit = 7;

    constructor(
        private activeRoute: ActivatedRoute,
        private studentsService: StudentsService,
        private refundService: RefundService,
        private dataServices: DataService,
    ) {
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.studentId = params['studentId'];
            this.getStudent();
        });
        this.activeRoute.parent.params.subscribe(params => {
            this.academyId = params['id'];
        });
    }
    

    getStudent() {
        this.subscrips.push(
            this.studentsService.getStudentById(this.studentId).subscribe(res => {
                this.student = res;
                this.getUserSubscriptions(this.student.id);
                this.getUserActivities(this.student.id, this.academyId, this.initialLimit);
            }),
        );
    }

    private getUserSubscriptions(studentId) {
        this.subscrips.push(
            this.studentsService.getStudentSubscriptions(studentId).subscribe(res => {
                this.studentSubscriptions = res;
            }),
        );
    }

    private getUserActivities(studentId, academyId, limit?) {
        this.subscrips.push(
            this.studentsService.getStudentActivities(studentId, academyId, limit).subscribe(res => {
                this.studentActivities = res;
                console.log('studentActivities ', res);
            }),
        );
    }

    onLoadMore() {
        this.viewMore = true;
        this.getUserActivities(this.studentId, this.academyId);
    }

    closeAddBelt() {
        this.isAddBelt = false;
    }

    public onRefund(activity) {
        const refund = {
            transaction_id: activity.transaction.id,
            amount: activity.transaction.amount,
        };
        activity.refundLoader = true;
        this.subscrips.push(
            this.refundService.addRefund(refund)
            .finally(() => {
                activity.refundLoader = false;
            })
            .subscribe(resp => {
                if (resp.status) {
                    activity.is_change_refund = 1;
                }
            }),
        );
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }

    public onEventMessage() {
        if (!this.studentId) {
            return false;
        }
        this.dataServices.eventMessage.emit({
            type: 'user',
            user_id: this.studentId,
        });
    }


    onBeltAdded(res) {
        const timelineBelt = {
            created_at: new Date(),
            type: 'belt',
            data: 'New belt received',
            details: res.belt,
        };
        this.studentActivities.unshift(timelineBelt);
    }
}
