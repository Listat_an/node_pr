import {Component, Input, OnInit} from '@angular/core';
import {StudentsService} from '../../../../../services/students.service';
import {find} from 'lodash';

@Component({
    selector: 'opn-student-subscriptions',
    templateUrl: './student-subscriptions.html',
    styleUrls: ['./student-subscriptions.scss'],
    providers: [StudentsService]
})
export class StudentSubscriptionsComponent implements OnInit {
    @Input() userId: number;
    defaultCard;

    constructor(private studentsService: StudentsService) {
    }

    ngOnInit() {
        this.studentsService.getUserCards(this.userId).subscribe(cards => {
            this.defaultCard = find(cards, (card)=> card.defaul)
        })
    }

}

