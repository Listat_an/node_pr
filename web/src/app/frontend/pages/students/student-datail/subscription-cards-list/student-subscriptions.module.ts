import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StudentSubscriptionsComponent} from './student-subscriptions.component';
import {SubscriptionListModule} from '../../../parts/subscription-list/subscription-list.module';

@NgModule({
    imports: [
        CommonModule,
        SubscriptionListModule
    ],
    declarations: [
        StudentSubscriptionsComponent
    ],
    exports: [
        StudentSubscriptionsComponent
    ]
})
export class StudentSubscriptionsModule {

}
