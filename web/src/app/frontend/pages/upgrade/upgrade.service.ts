import {
    Injectable,
    EventEmitter,
} from '@angular/core';
import {
    Http,
    Headers,
    RequestOptions,
} from '@angular/http';
import {
    Observable,
} from 'rxjs/Observable';
import {
    ConfigService,
} from '../../../services/service.config';
import {
    JwtHelperService,
} from '@auth0/angular-jwt';
import * as _ from 'lodash';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class UpgradeService {
    private options;
    private activate: Boolean;
    private user: any;

    public upgradeEvent: EventEmitter<any> = new EventEmitter();

    constructor(private http: Http, public jwtHelper: JwtHelperService) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token'),
        });
        this.options = new RequestOptions({
            headers: headers,
        });
        this.user = jwtHelper.decodeToken(localStorage.getItem('token'));
    }

    getMe(): any {
        return this.user;
    }

    getPackages() {
        return this.http.get(
                ConfigService.STATIC_SERVER + `/academy_subscriptions`,
                this.options,
            )
            .map(res => {
                return _.groupBy(res.json().data, 'name');
            })
            .catch(this.handleError);
    }

    payPackage(obj) {
        return this.http.post(
                ConfigService.STATIC_SERVER + `/academy_subscriptions/pay`,
                obj,
                this.options,
            )
            .map(res => res.json())
            .catch(this.handleError);
    }
    cancelPackage(obj) {
        return this.http.post(
            ConfigService.STATIC_SERVER + `/academy_subscriptions/canceled`,
            obj,
            this.options,
        )
        .map(res => res.json())
        .catch(this.handleError);
    }

    private handleError(error: any) {
        return Observable.throw(error.message || error);
    }
}
