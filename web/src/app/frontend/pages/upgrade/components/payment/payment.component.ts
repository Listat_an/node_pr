import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    ViewEncapsulation,
    HostListener,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators,
} from '@angular/forms';

import {
    StripeService,
    Elements,
    Element as StripeElement,
    ElementsOptions,
} from 'ngx-stripe';
import {
    PaymentService,
} from '../../../../../../app/services/payment.service';

@Component({
    selector: 'opn-payment-upgrade',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss'],
    providers: [PaymentService],
    encapsulation: ViewEncapsulation.Emulated,
})
export class PaymentUpgradeComponent implements OnInit {
    elements: Elements;
    card: StripeElement;
    @ViewChild('card') cardRef: ElementRef;
    @ViewChild('error') errorRef: ElementRef;
    @ViewChild('current') currentRef: ElementRef;
    @ViewChild('selectOptions') selectOptionsRef: ElementRef;

    private showPay: Boolean = false;

    elementsOptions: ElementsOptions = {
        locale: 'en',
    };
    stripePayment: FormGroup;

    @Input() payment?: any;
    @Output() outToken: EventEmitter < any > = new EventEmitter < any > ();

    private meCards: any = [];
    private addStatus: Boolean = true;
    private openSelect: Boolean = false;
    private paymentType: String = '';
    private cardToken: String = '';
    private process: Boolean = false;
    private total: Number = 0;
    private load = false;

    constructor(
        private fb: FormBuilder,
        private stripeService: StripeService,
        private paymentSrv: PaymentService,
    ) {}

    ngOnInit() {
        this.selectOptionsRef.nativeElement.style = 'display: none';
        this.addStatus = false;
        if (this.paymentType === '') {
            this.currentRef.nativeElement.textContent = 'Select your card';
        }
        this.stripeService.elements(this.elementsOptions)
            .subscribe(elements => {
                this.elements = elements;
                // Only mount the element the first time
                if (!this.card) {
                    this.card = this.elements.create('card', {
                        hidePostalCode: true,
                        style: {
                            base: {
                                iconColor: '#666EE8',
                                color: '#202f3b',
                                lineHeight: '50px',
                                fontWeight: 300,
                                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                                fontSize: '15px',
                            },
                        },
                    });
                }
            });
        this.getMyCards();
    }

    totalAmount(list) {
        list.forEach(element => {
            this.total = Number(this.total) + Number(element.amount);
        });
    }

    getMyCards() {
        this.paymentSrv.getCards().subscribe(cards => {
            this.meCards = cards;
            this.checkMetod(cards);
        });
    }
    checkMetod(cards) {
        if (cards.length == 0) {
            this.onNewCard();
        }
    }

    public showPayment() {
        this.getMyCards();
        this.errorRef.nativeElement.textContent = '';
        document.body.classList.add('openModal');
        if (this.addStatus) {
            this.card.mount(this.cardRef.nativeElement);
        }
    }


    onSelectCard(card) {
        this.currentRef.nativeElement.textContent = `${card.brand} **** **** **** ${card.last4}`;
        this.addStatus = false;
        this.card.unmount();
        this.paymentType = 'select';
        this.cardToken = card.id;
    }

    onNewCard() {
        this.currentRef.nativeElement.textContent = 'New Card';
        this.addStatus = true;
        this.card.mount(this.cardRef.nativeElement);
        this.paymentType = 'new';
    }
    onToggle(e) {
        e.preventDefault();
        this.openSelect = !this.openSelect;
        this.statusSelect(this.openSelect);
    }
    statusSelect(visible) {
        this.openSelect = visible;
        this.selectOptionsRef.nativeElement.style = `display: ${(visible) ? 'block' : 'none'}`;
    }
    public getToken() {
        this.load = true;
        switch (this.paymentType) {
            case 'new':
                {
                    this.process = true;
                    this.stripeService
                    .createToken(this.card, {})
                    .subscribe(result => {
                        if (result.token) {
                            this.outToken.emit({
                                type: 'token',
                                source: result.token.id,
                            });
                        } else if (result.error) {
                            this.errorRef.nativeElement.textContent = result.error.message;
                        }
                        this.process = false;
                    });
                    break;
                }
            case 'select':
                {
                    this.outToken.emit({
                        type: 'card',
                        source: this.cardToken,
                    });
                    break;
                }
        }
    }
}
