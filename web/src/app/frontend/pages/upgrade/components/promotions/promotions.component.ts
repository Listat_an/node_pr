import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  ElementRef,
} from '@angular/core';

import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  AbstractControl,
} from '@angular/forms';

import { AcademiesService } from '../../../../../services/academies.service';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'opn-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.scss'],
  providers: [ AcademiesService ],
})
export class PromotionsAcademyComponent implements OnInit {
  @Input() academyId: any;
  @Input() isPro: Boolean;
  @Input() academyData: any;

  private promotion: String = '';
  @Output() promotionChange: EventEmitter<any>;

  private time: any = '';
  private attendance: any = '';
  public validPromotions: Boolean = true;

  constructor(private academyService: AcademiesService) {
    this.promotionChange = new EventEmitter();
  }

  ngOnInit() {
    if (this.isPro) {
      this.setAcademyData(this.academyData);
    }
  }
  setAcademyData(academy) {
      this.promotion = academy.promotion_method;
      this.onChangeTypeEvent('', this.promotion);
  }
  onChangeTypeEvent(event, type) {
    this.promotion = type;
    this.validPromotions = true;
    this.promotionChange.emit(this.promotion);
  }
}
