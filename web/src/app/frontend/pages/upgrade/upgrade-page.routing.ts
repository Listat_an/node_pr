import { RouterModule, Routes, CanActivate } from '@angular/router';
import { NgModule } from '@angular/core';

import { UpgradePageComponent } from './upgrade-page.component';

const routes: Routes = [{
  path: '',
  component: UpgradePageComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpgradePageRouting {
}
