import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { ReactiveFormsModule } from '@angular/forms';
import {AcademyComponentsModule} from '../components/academy/academy.module';

import { PagesModule } from '../pages.module';
import { UpgradePageComponent } from './upgrade-page.component';
import { PaymentUpgradeComponent } from './components/payment/payment.component';
import { PromotionsAcademyComponent } from './components/promotions/promotions.component';
import { UpgradePageRouting } from './upgrade-page.routing';
import {
  ModalAcademyService,
} from '../components/academy/modal/modal.service';

@NgModule({
  declarations: [
    UpgradePageComponent,
    PaymentUpgradeComponent,
    PromotionsAcademyComponent,
  ],
  imports: [
    ReactiveFormsModule,
    AcademyComponentsModule,
    FormsModule,
    CommonModule,
    HttpClientModule,
    UpgradePageRouting,
    HttpModule,
    PagesModule,
    RouterModule,
  ],
  providers: [
    CookieService,
    ModalAcademyService,
  ],
})
export class UpgradePageModule { }
