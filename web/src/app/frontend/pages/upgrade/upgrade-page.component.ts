import {
    Component,
    OnInit,
    ViewChild,
    OnDestroy,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import {
    Title,
} from '@angular/platform-browser';

import {
    Subscription,
} from 'rxjs/Subscription';
import {
    cancelSubscription,
} from '../../../providers/cancelSubscription';

import {
    PaymentStripeComponent,
} from '../../../../components/payment-stripe/payment-stripe.component';

import {
    PromotionsAcademyComponent,
} from './components/promotions/promotions.component';
import {
    ToastrService,
} from 'ngx-toastr';

import {
    UpgradeService,
} from './upgrade.service';

import {
    AcademiesService,
} from '../../../services/academies.service';

export interface ListData {
    name?: String;
    amount?: Number;
}

@Component({
    selector: 'opn-upgrade-page',
    templateUrl: './upgrade-page.component.html',
    styleUrls: ['../@theme/scss/theme.scss', './upgrade-page.component.scss'],
    providers: [
        AcademiesService,
    ],
})
export class UpgradePageComponent implements OnInit, OnDestroy {
    private academyId: any;
    private isManager: Boolean = false;

    @ViewChild(PaymentStripeComponent)
    public paymentStripe: PaymentStripeComponent;

    @ViewChild(PromotionsAcademyComponent)
    public promotionsComponent: PromotionsAcademyComponent;

    public subscrips: Subscription[] = [];

    private totalData: Array < ListData > ;
    private selectPckg: any = {
        id: 0,
        name: '',
        price: 0,
        payLoader: false,
    };
    private stepUpgrade = 0;
    private isPro: Boolean = false;
    private packages: any;
    private academyData: any;
    private currentPackage = {
        free: false,
        pro: false,
    };
    private promotion: string;
    private load: Boolean = false;

    constructor(
        private titleService: Title,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private upgradeService: UpgradeService,
        private academyService: AcademiesService,
        private toastr: ToastrService,
    ) {
        this.titleService.setTitle('Upgrade academy');
        this.activeRoute.params.subscribe(params => {
            this.academyId = params['id'];
            this.getAcademy();
        });
    }

    ngOnInit() {}

    getAcademy() {
        this.load = true;
        this.subscrips.push(
            this.academyService.getAcademyById(this.academyId).subscribe(
                resp => {
                    this.academyData = resp;
                    this.getPackages(resp);
                },
            ),
        );
    }

    getPackages(academy) {
        this.subscrips.push(
            this.upgradeService.getPackages().subscribe(
                resp => {
                    this.packages = resp;
                    this.currentPackage = {
                        free: !academy.academy_subscription_id || academy.academy_subscription_id === resp.free[0].id,
                        pro: academy.academy_subscription_id === resp.pro[0].id,
                    };
                    this.isPro = this.currentPackage.pro;
                    if (this.isPro) {
                        this.selectPckg = Object.assign({
                            id: resp.pro[0].id,
                            name: resp.pro[0].name,
                            price: resp.pro[0].price,
                        });
                        this.stepUpgrade = 2;
                    } else {
                        this.stepUpgrade = 1;
                    }
                    this.load = false;
                },
            ),
        );
    }

    onCheckManager(status) {
        this.isManager = status;
    }

    onSelectPackcge(pckg) {
        this.stepUpgrade = 2;
        this.selectPckg = Object.assign({
            id: pckg.id,
            name: pckg.name,
            price: pckg.price,
        });
    }

    onPay(token) {
        if (this.promotion && this.promotion != '') {
            this.subscrips.push(
                this.upgradeService.payPackage({
                    subscription_id: this.selectPckg.id,
                    academy_id: this.academyId,
                    source: token.source,
                    type: token.type,
                    promotion: this.promotion,
                }).subscribe(
                    resp => {
                        this.upgradeService.upgradeEvent.emit(true);
                        this.stepUpgrade = 3;
                    },
                    err => {
                        const errore = JSON.parse(err._body).errors;
                        const self = this;
                        errore.forEach(function (el, i, arr) {
                            if (el.status == '400') {
                                self.toastr.info(el.description, 'Warning!');
                            }
                        });
                        this.router.navigate(['academiy-datail', this.academyId]);
                    },
                ),
            );
        } else {
            this.promotionsComponent.validPromotions = false;
            this.toastr.info('Pick your promotion type', 'Warning!');
        }
    }

    onCancelPro() {
        if (this.isPro) {
            this.subscrips.push(
                this.upgradeService.cancelPackage({
                    academy_id: this.academyId,
                }).subscribe(
                    resp => {
                        this.upgradeService.upgradeEvent.emit(false);
                        this.stepUpgrade = 4;
                    },
                ),
            );
        }else {
            this.stepUpgrade = 1;
        }
    }

    onChangePckg() {
        this.stepUpgrade = 1;
    }

    promotionChange(promotion) {
        this.promotion = promotion;
    }
    toDashboardPage() {
        this.router.navigate([`academiy-datail/${this.academyId}/dashboard`]);
    }
    toAcademyPage() {
        this.router.navigate([`academiy-datail/${this.academyId}`]);
    }
    toFeedPage() {
        this.router.navigate([`/`]);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
