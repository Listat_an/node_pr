import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SearchPageService } from '../search-page.service';
import { ConfigService } from '../../../../services/service.config';
import {AcademiesService} from '../../../../services/academies.service';
import {UsersService} from '../../../../services/users.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-search-users',
    templateUrl: './search-users.component.html',
    styleUrls: ['./search-users.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [AcademiesService, UsersService]
})
export class SearchUsersComponent implements OnInit {
    public baseUrl;
    public me;
    public noImg = '/assets/images/user-dafault.png';
    public isLoading: boolean = false;
    public isLoadingNew: boolean = false;
    public academies = [];
    public users = [];
    public total = 0;
    public skip = 1;
    public limit = 20;
    public search = '';
    public sort = 1;
    radius: number = 0;
    lat: number;
    lng: number;
    public activeSlider:boolean = false;
    public sortField = 'full_name';
    public allItems: boolean = false;
    public requestUrl = 'users';
    public requestIncludes = 'followers,belt_user.belt';
    public selectedAcademies = [];
    public sliderOptions = {
        length: 150,
        color: '#4ca1ff',
        emptyColor: '#c5cacf',
        thumbColor: '#4ca1ff',
        thumbSize: 10,
        lineWidth: 2,
        toggleDisableText: 'disable',
        minValue: 1,
        maxValue: 80,
        initialValue: 5
    };
    public newRadius: number;

    academiesSelectSettings = {
        text: 'Academies',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        singleSelection: false,
        enableSearchFilter: true,
        classes: 'academies-multiselect',
        badgeShowLimit: 1,
    };

    private tab = 'users';
    constructor(
        private searchPageService: SearchPageService,
        private academiesService: AcademiesService,
        private usersService: UsersService,
        private router: Router,
    ) {
        this.academiesService.getAcademies().subscribe(res => {
            this.academies = res.data.map((el) => {
                el.itemName = el.name;
                return el;
            });
        });
        this.usersService.getUserMe().subscribe(user => {
            this.me = user;
            this.lat = this.me.latitude;
            this.lng = this.me.longitude;
        });
    }

    ngOnInit() {
        this.baseUrl = ConfigService.URL_SERVER;
        this.getItems();
    }

    public getItems() {
        this.skip = 1;
        this.isLoading = true;
        this.searchPageService.getItems(this.requestUrl, this.selectedAcademies, this.newRadius, this.lat, this.lng, this.skip, this.limit, this.search, this.sort, this.requestIncludes, this.sortField)
            .then((res) => {
                console.log(res);
                this.users = res.data;
                this.total = res.total;
                this.skip++;
                this.isLoading = false;
                this.allItems = res.total <=  res.data.length + res.skip;
                this.activeSlider = false;
            }, (err) => {
                console.log(err);
                this.isLoading = false;
            })

    }

    public loadMoreItems() {
        this.isLoadingNew = true;
        this.searchPageService.getItems(this.requestUrl, this.selectedAcademies, this.newRadius, this.lat, this.lng, this.skip, this.limit, this.search, this.sort,
            this.requestIncludes, this.sortField)
            .then((res) => {
                this.skip++;
                this.users.push(...res.data);
                this.isLoadingNew = false;
                this.allItems = res.total <=  res.data.length + res.skip;
            }, (err) => {
                console.log(err);
                this.isLoadingNew = false;
            })
    }

    public sortItems(): void {
        if (this.sort % 3 !== 0) {
            this.sort = this.sort + 1;
        } else {
            this.sort = 1;
        }
        this.allItems = false;
        this.skip = 1;
        this.getItems();
    }

    public searchItems(value: string) {
        this.search = value.trim();
        this.skip = 1;
        this.allItems = false;
        this.getItems();
    }

    public followUser(id: number, type: string){
        this.searchPageService.followUser(id, type)
            .then((res) => {
                console.log(res);
            }, (err) => {
                console.log(err);
            })
    }

    public onAcademySelect(){
        this.skip = 1;
        this.allItems = false;
        this.getItems();
    }

    public onChangeRadius(event) {
        this.newRadius = event;
    }

    clearRadius(){
        this.newRadius = 0;
        this.getItems();
    }
    onChangeTab(page) {
        this.router.navigate(['search', page]);
    }
}
