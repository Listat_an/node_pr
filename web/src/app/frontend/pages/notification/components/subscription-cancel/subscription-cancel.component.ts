import {
    Component,
    OnInit,
    Input,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
    NavigationEnd,
} from '@angular/router';


import {
    Subscription,
} from 'rxjs/Subscription';
import {
    cancelSubscription,
} from '../../../../../providers/cancelSubscription';
import {
    SubscriptionsService,
} from '../../../../../services/subscriptions.service';
import {
    NotificationsService,
} from '../../../../../services/notifications.service';

import {
    ModalFeeComponent
} from './modal-fee/modal-fee.component';


@Component({
    selector: 'opn-subscription-cancel',
    templateUrl: './subscription-cancel.component.html',
    styleUrls: ['./subscription-cancel.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [
        SubscriptionsService,
        NotificationsService,
    ],
})
export class SubscriptionCancelComponent implements OnInit {

    public subscrips: Subscription[] = [];

    @Input() dataSource: any = [];
    @ViewChild(ModalFeeComponent)
    private modalFeeComponent: ModalFeeComponent;

    constructor(
        private subscribeSrv: SubscriptionsService,
        private notificationSrv: NotificationsService,
    ) {}

    ngOnInit() {
        this.dataSource.joinLoaderC = false;
        this.dataSource.joinLoaderD = false;
        this.dataSource.changed = false;
        console.log('Notification ', this.dataSource);
    }

    onConfirm(event) {
        if (event.confirm) {
            const subscribe_id = event.notification.details.subscription_id;
            this.subscrips.push(
                this.subscribeSrv.feeNotification({
                    subscription_id: subscribe_id,
                    user_id: this.dataSource.user_id,
                    fee: event.fee,
                }).subscribe(res => {
                    if (res.status) {
                        this.dataSource.joinLoaderC = false;
                        this.changeType(event.notification.id);
                    }
                }),
            );
        }
    }

    onCencel(notification) {
        // const subscribe_id = notification.details.subscription_id;
        // this.dataSource.joinLoaderC = true;
        this.modalFeeComponent.open(notification);
        /*
        {
            "subscription_id": 1,
            "user_id": 15,
            "fee": 5
        }
        */
        // this.subscrips.push(
        //     this.subscribeSrv.cancelSabscription({
        //         subscription_id: subscribe_id,
        //         user_id: this.dataSource.user_id,
        //     }).subscribe(res => {
        //         if (res.status) {
        //             this.dataSource.joinLoaderC = false;
        //             this.changeType(notification.id);
        //         }
        //     }),
        // );
    }

    onDeclineSubscription(notification) {
        const subscribe_id = notification.details.subscription_id;
        this.dataSource.joinLoaderD = true;
        this.subscrips.push(
            this.subscribeSrv.declineSubscription({
                subscription_id: subscribe_id,
            }).subscribe(res => {
                if (res.status) {
                    this.dataSource.joinLoaderD = false;
                    this.changeType(notification.id);
                }
            }),
        );
    }

    private changeType(subscribe_id) {
        this.subscrips.push(
            this.notificationSrv.changeTypeNotifications(subscribe_id).subscribe(res => {
                this.dataSource = {
                    ...res,
                    changed: true,
                };
            }),
        );
    }
}
