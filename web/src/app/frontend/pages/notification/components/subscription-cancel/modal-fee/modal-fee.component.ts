import { Component, HostListener, Input, Output, OnInit, EventEmitter } from '@angular/core';
/**
 * ModalComponent - This class represents the modal component.
 * @requires Component
 */

@Component({
  selector: 'opn-modal-fee',
  styleUrls: ['./modal-fee.component.scss'],
  templateUrl: './modal-fee.component.html',
})

export class ModalFeeComponent implements OnInit {

  isOpen: Boolean = false;
  private fee: any = 0;
  private disable: false;
  private notification: any;

  @Input() btnsTitle?: any = {
    accept: 'Accept',
    decline: 'Decline',
  };

  @Output() confirm: EventEmitter <any> = new EventEmitter <any>();
  @HostListener('document:keyup', ['$event'])

  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.isOpen = false;
    }
  }

  constructor() { }

  ngOnInit() {}

  onConfirm(confirm): void {
    this.confirm.emit({
      confirm: confirm,
      fee: this.fee,
      notification: this.notification,
    });
    this.close();
  }

  open(notification): void {
    this.notification = notification;
    this.isOpen = true;
  }

  close(): void {
    this.isOpen = false;
  }
}
