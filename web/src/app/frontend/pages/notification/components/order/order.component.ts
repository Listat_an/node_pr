import {
    Component,
    OnInit,
    AfterViewInit,
    Input,
    ViewEncapsulation,
} from '@angular/core';
import {
    Router,
} from '@angular/router';

import {OrdersService} from '../../../academy-shop/orders/orders.service';


@Component({
    selector: 'opn-order-notification',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [
        OrdersService,
    ],
})
export class OrderNotificationComponent implements AfterViewInit {

    @Input() dataSource: any = [];

    private academyId;

    constructor(
        private router: Router,
        private orderSrv: OrdersService,
    ) {}

    ngAfterViewInit() {
        this.getOrder();
    }

    getOrder() {
        if (this.dataSource.type == 'manager') {
            this.orderSrv.getOrderId(this.dataSource.details.order_id).subscribe(order => {
                this.academyId = order.academy_id;
            });
        }
    }


    toOrder() {
        switch (this.dataSource.type) {
            case 'user': {
                this.router.navigate(['shop', 'orders', this.dataSource.details.order_id]);
                break;
            }
            case 'manager': {
                this.router.navigate(['academiy-datail', this.academyId, 'shop', 'orders', this.dataSource.details.order_id]);
                break;
            }
        }
    }
}
