import {
    Component,
    OnInit,
    Input,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
    NavigationEnd,
} from '@angular/router';


import {
    Subscription,
} from 'rxjs/Subscription';
import {
    cancelSubscription,
} from '../../../../../providers/cancelSubscription';
import {
    SubscriptionsService,
} from '../../../../../services/subscriptions.service';
import {
    NotificationsService,
} from '../../../../../services/notifications.service';
import {
    ModalService,
} from '../../../../pages/components/modal/modal.service';
import {
    PaymentStripeComponent,
} from '../../../../../../components/payment-stripe/payment-stripe.component';

export interface ListData {
    name?: String;
    amount?: Number;
}

@Component({
    selector: 'opn-subscription-cancel-fee',
    templateUrl: './subscription-cancel-fee.component.html',
    styleUrls: ['./subscription-cancel-fee.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [
        SubscriptionsService,
        NotificationsService,
    ],
})
export class SubscriptionCancelFeeComponent implements OnInit {

    public subscrips: Subscription[] = [];

    @Input() dataSource: any = [];

    @ViewChild(PaymentStripeComponent)
    public paymentStripe: PaymentStripeComponent;
    private totalData: Array < ListData > = [];
    public modalAlert: any = '';

    constructor(
        private subscribeSrv: SubscriptionsService,
        private notificationSrv: NotificationsService,
        private modalSrv: ModalService,
    ) {}

    ngOnInit() {
        this.dataSource.joinLoaderC = false;
        this.dataSource.changed = false;
        console.log('Notification ', this.dataSource);
        this.totalData = [].concat({
            name: 'Fee',
            amount: this.dataSource.details.fee,
        });
    }

    onCencel(notification) {
        this.paymentStripe.showPayment();
    }

    onRePay() {
        this.totalData = [].concat({
            name: 'Fee',
            amount: this.dataSource.details.fee,
        });
        this.paymentStripe.showPayment();
        this.modalSrv.close('payment-fail');
    }

    onPay(token) {
        this.dataSource.joinLoaderC = true;
        this.subscribeSrv.payFeeCanceled({
                subscription_id: this.dataSource.details.subscription_id,
                user_id: this.dataSource.details.user_id,
                fee: this.dataSource.details.fee,
                source: token.id,
                type: token.type,
            })
            .finally(() => {
                this.dataSource.joinLoaderC = false;
            })
            .subscribe(
                res => {
                    new Promise((resolve) => {
                        resolve();
                    }).then(() => {
                        this.changeType(this.dataSource.id);
                        this.modalSrv.open('payment-success');
                    });
                },
                err => {
                    if (err.status == 400) {
                        new Promise((resolve) => {
                            this.modalAlert = err.description;
                            resolve();
                        }).then(() => {
                            this.modalSrv.open('payment-fail');
                        });
                    }
                });
    }

    private changeType(subscribe_id) {
        this.subscrips.push(
            this.notificationSrv.changeTypeNotifications(subscribe_id).subscribe(res => {
                this.dataSource = {
                    ...res,
                    changed: true,
                };
            }),
        );
    }
}
