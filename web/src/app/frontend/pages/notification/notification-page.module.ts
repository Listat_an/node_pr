import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
// import {ShareButtonsModule} from 'ngx-sharebuttons';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { NotificationPageRouting } from './notification-page.routing';
import { NoficationPageComponent } from './notification-page.component';
import { PostService } from '../../../services/posts.service';

import { Select2Module } from 'ng2-select2';
import { ReactiveFormsModule } from '@angular/forms';
import { NgDatepickerModule } from '../../../../components/ng-datepicker';
import { BeltsModule } from '../../../../components/belts';
import { SelectAcademiesModule } from '../../../../components/select-acadimies';
import { TooltipModule } from 'ng2-tooltip';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';
import {CurrencyMaskModule} from 'ng2-currency-mask';

import { PagesModule } from '../pages.module';
import { AcademyComponentsModule } from '../components/academy/academy.module';
import { EscapeHtmlPipe } from '../pipes/keep-html.pipe';
import { SubscriptionCancelComponent } from './components/subscription-cancel/subscription-cancel.component';
import { OrderNotificationComponent } from './components/order/order.component';
import { ModalFeeComponent } from './components/subscription-cancel/modal-fee/modal-fee.component';
import { SubscriptionCancelFeeComponent } from './components/subscription-cancel-fee/subscription-cancel-fee.component';

@NgModule({
  declarations: [
    NoficationPageComponent,
    EscapeHtmlPipe,
    SubscriptionCancelComponent,
    OrderNotificationComponent,
    ModalFeeComponent,
    SubscriptionCancelFeeComponent,
  ],
  imports: [
    Select2Module,
    ReactiveFormsModule,
    NgDatepickerModule,
    BeltsModule,
    SelectAcademiesModule,
    TooltipModule,
    NotificationPageRouting,
    CommonModule,
    InfiniteScrollModule,
    HttpClientModule,
    HttpModule,
    Ng4GeoautocompleteModule.forRoot(),
    // ShareButtonsModule.forRoot(),
    PagesModule,
    CurrencyMaskModule,
    AcademyComponentsModule,
  ],
  providers: [
    PostService,
    CookieService,
  ],
})
export class NotificationPageModule { }
