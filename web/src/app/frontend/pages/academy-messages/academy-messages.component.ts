import {Component, OnInit, EventEmitter, Input, Output, ViewChild, ElementRef} from '@angular/core';
import {SocketService} from '../../../services/socket.service';
import {AcademyMessagesService} from './academy-messages.service';
import {ConfigService} from '../../../services/service.config';
import {
    MessagePageService,
} from '../message/message-page.service';

import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import {UsersService} from '../../../services/users.service';
import {ActivatedRoute} from '@angular/router';

import {
    AcademiesService,
} from '../../../services/academies.service';

import {
    ModalConfirmComponent,
} from '../components/academy/modal-confirm/modal-confirm.component';

import * as _ from 'lodash';

@Component({
    selector: 'opn-academy-messages-page',
    templateUrl: './academy-messages.component.html',
    styleUrls: ['./academy-messages.component.scss', '../@theme/scss/theme.scss'],
    providers: [MessagePageService],
})

export class AcademyMessagesComponent implements OnInit {
    public dialogs: any = [];
    public isDialog = 0;
    public baseUrl;
    public isLoading: Boolean = false;
    public noImg = '/assets/images/user-dafault.png';
    public me;
    academyId;
    activeDialogId;
    private isManager: Boolean = false;
    public listSearch: Array<any> = [];
    public activeDialog: Subject<any> = new Subject();
    @ViewChild('search') public inputSearch: ElementRef;
    private subjectSearch: Subject<string> = new Subject();

    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;
    private dialogTmpId: any;

    constructor(private socketService: SocketService,
                private activatedRoute: ActivatedRoute,
                private academyMessagesService: AcademyMessagesService,
                private messagePageService: MessagePageService,
                private usersService: UsersService) {
        this.usersService.getUserMe().subscribe(user => {
                this.me = user;
                this.subjectSearch.debounceTime(500)
                    .subscribe((searchValue) => {
                        this.searchUsers(searchValue);
                    });

                this.socketService.onGetMessages()
                    .subscribe((data) => {
                        if ((data.user && data.user.id == this.me.id) || data.sender_id == this.me.id) {
                            let element = this.dialogs.find((el) => {
                                return el.id == data.conversation_id;
                            });
                            const index = this.dialogs.indexOf(element);
                            let newDialog;
                            if (index !== -1) {
                                if (this.dialogs[index].last_message) {
                                    this.dialogs[index].last_message.content = data.content;
                                }
                                newDialog = this.dialogs[index];
                                this.dialogs = this.dialogs.filter((el) => {
                                    return el.id != data.conversation_id;
                                });
                                this.dialogs.unshift(newDialog);
                            } else {
                                const newData = {
                                    id: data.conversation_id,
                                    updated_at: data.updated_at,
                                    users: [
                                        {
                                            user: data.user_sender,
                                            user_id: data.user_sender,
                                        },
                                        {
                                            user: data.user,
                                            user_id: data.user,
                                        },
                                    ],
                                    last_message: {
                                        conversation_id: data.id,
                                        sender_id: data.user_sender.id,
                                        user_id: data.user.id,
                                        content: data.content
                                    }
                                };
                                this.dialogs.unshift(newData);
                                this.onChangeDialog(newData);
                            }
                        }
                    }, (error) => {
                        console.log('Error', error);
                    });
            }
        );
    };

    public ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.academyId = params['id'];
            this.getDialogs(this.academyId);
        });
        this.baseUrl = ConfigService.URL_SERVER;
    }

    public getDialogs(academyId) {
        this.isLoading = true;
        this.academyMessagesService.getDialogs(academyId)
            .then((res) => {
                this.dialogs = res.data;
                if (this.dialogs.length) {
                    this.activeDialog.next(this.dialogs[0]);
                    this.activeDialogId = this.dialogs[0].id;
                    this.isDialog = this.dialogs[0];
                    this.academyMessagesService.setViewsMessages(this.dialogs[0].id)
                    .then((res) => {
                        console.log('setViewsMessages ', res);
                    }, (error) => {
                        console.log('setViewsMessages ', error);
                    });
                }
                this.isLoading = false;
            }, (err) => {
                console.log(err);
                this.isLoading = false;
            })
    }

    public onChangeDialog(dialog) {
        this.activeDialogId = dialog.id;
        this.activeDialog.next(dialog);
        this.isDialog = dialog;
        this.academyMessagesService.setViewsMessages(dialog.id)
            .then((res) => {
                console.log('setViewsMessages ', res);
            }, (error) => {
                console.log('setViewsMessages ', error);
            });
    }

    public toChat(user) {
        const data = {
            'user_id': user.id
        };
        this.inputSearch.nativeElement.value = '';
        this.listSearch = [];
        this.academyMessagesService.getConversation(data)
            .then((res) => {
                let dialog = res;
                const index = this.dialogs.filter((el) => {
                    return el.id == res.id
                });
                if (!index.length) {
                    this.dialogs.unshift(dialog);
                }
                this.isDialog = dialog.id;
                this.onChangeDialog(dialog);
            }, (err) => {
                console.log(err);
            })
    }

    public onSearch(value: string) {
        this.subjectSearch.next(value);
    }

    public searchUsers(value: string) {
        if (value.trim()) {
            this.academyMessagesService.getUsersWithoutMe(value.trim(), this.me.id)
                .then((res) => {
                    this.listSearch = res.data;
                }, (err) => {
                    console.log(err);
                })
        } else {
            this.listSearch = [];
        }
    }

    onCheckManager(status) {
        this.isManager = status;
    }
    onRemoveDialog(dialog_id) {
        this.modalConfirm.modalTitle = 'Delete your dialog?';
        this.modalConfirm.modalDescription = 'This action will remove your dialog.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
        this.dialogTmpId = dialog_id;
    }

    onConfirm(confirm) {
        if (confirm) {
            this.messagePageService.rmConversationById(this.dialogTmpId).then(resp => {
                const self = this;
                const dialogFilter = new Promise((resolve: any) => resolve(self.dialogs.filter(IDialog => IDialog.id != this.dialogTmpId)));
                dialogFilter.then(dialogs => {
                    self.dialogs = dialogs;
                    console.log('self.dialogs.length > 0 ', self.dialogs.length > 0);
                    if (self.dialogs.length > 0) {
                        self.activeDialog.next(dialogs[0]);
                        self.isDialog = dialogs[0];
                    } else {
                        self.activeDialog.next(null);
                        self.isDialog = null;
                    }
                });
            });
        }
    }
}
