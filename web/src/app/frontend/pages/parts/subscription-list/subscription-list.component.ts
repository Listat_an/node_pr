import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    SubscriptionsService
} from '../../../../services/subscriptions.service';
@Component({
    selector: 'opn-subscription-list',
    templateUrl: './subscription-list.html',
    styleUrls: ['./subscription-list.scss'],
    providers: [SubscriptionsService]
})
export class SubscriptionListComponent implements OnInit {
    @Input() userId: number;
    subscriptions = [];

    constructor(private subscriptionsService: SubscriptionsService) {}

    ngOnInit() {
        this.subscriptionsService.getSubscriptionsByUserId(this.userId).subscribe(subscriptions => {
            console.log('getSubscriptionsByUserId ', subscriptions);
            this.subscriptions = subscriptions;
        });
    }

    onCancelNotification(subscribe) {
        subscribe.joinLoader = true;
        this.subscriptionsService.CancelNotification({
            subscription_id: subscribe.subscription.id,
        }).subscribe(res => {
            if (res.status) {
                subscribe.is_cancel = true;
                subscribe.joinLoader = false;
            }
        });
    }

}
