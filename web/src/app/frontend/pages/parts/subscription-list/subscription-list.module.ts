import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SubscriptionListComponent} from './subscription-list.component';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        SubscriptionListComponent
    ],
    exports: [
        SubscriptionListComponent
    ]
})
export class SubscriptionListModule {

}
