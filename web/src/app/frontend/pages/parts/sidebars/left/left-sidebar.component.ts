import {
    CookieService,
} from 'ngx-cookie-service';
import {
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
    Compiler,
} from '@angular/core';
import {
    ActivatedRoute,
    Router,
} from '@angular/router';
import {
    cancelSubscription,
} from '../../../../../providers/cancelSubscription';
import {
    Subscription,
} from 'rxjs/Subscription';
import {
    Subject,
} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import {
    SocketService,
} from '../../../../../services/socket.service';
import {
    UsersService,
} from '../../../../../services/users.service';
import {
    UserSettingsComponent,
} from './user-settings/user-settings.component';
import {
    MyAcademiesComponent,
} from './my-academies/my-academies.component';
import {
    AcademiesService,
} from '../../../../../services/academies.service';
import { ViewPostComponent } from 'app/frontend/pages/components/posts/view-post/view-post.component';

@Component({
    selector: 'opn-left-sidebar',
    templateUrl: './left-sidebar.component.html',
    styleUrls: ['./left-sidebar.component.scss'],
    providers: [UsersService],
})
export class LeftSidebarComponent implements OnInit, OnDestroy {
    private user: any;
    private academy: any;
    private belt: String = '';
    public subscrips: Subscription[] = [];
    public profileLink: String = '';
    public load = false;
    private isManager: Boolean = false;
    private academyList: any = [];
    private userAvatar: any;

    private modalAcademy = false;
    private changeAvatar = false;

    private serverUrl: String;

    @ViewChild(UserSettingsComponent)
    userSettingsComp: UserSettingsComponent;

    @ViewChild(MyAcademiesComponent)
    myAcademimes: MyAcademiesComponent;

    constructor(
        private usersService: UsersService,
        private serviceAcademy: AcademiesService,
        private route: ActivatedRoute,
        private router: Router,
        private socketService: SocketService,
        private cookieSrvc: CookieService,
        private compiler: Compiler,
    ) {
        this.user = [];
        this.serverUrl = this.usersService.getServerUrl();
    }

    ngOnInit() {
        this.getMeProfile();
        this.subscrips.push(
            this.userSettingsComp.profilesaved.subscribe(data => {
                this.changeAvatar = false;
                this.getMeProfile();
            }),
        );
    }

    getMeProfile() {
        this.subscrips.push(
            this.usersService.getUserMe().subscribe(res => {
                this.profileLink = `/profile/${res.id}`;
                if (res.belt) {
                    this.belt = this.serverUrl + res.belt.sourse;
                }
                this.user = res;
                this.userAvatar = res.avatar;
                this.load = true;
                this.getMyacademies(res.id);
                this.changeAvatar = true;
            }),
        );
    }

    getMyacademies(me) {
        this.serviceAcademy.getManagerAcademies(me).toPromise().then(res => {
            this.academyList = res;
            this.isManager = false;
            res.forEach(academy => {
                if (academy.status != 'Not_created') {
                    this.isManager = true;
                }
            });
        });
    }

    onLogout() {
        localStorage.removeItem('token');
        localStorage.clear();
        this.socketService.disconnect();
        this.cookieSrvc.deleteAll();
        window.location.href = '#/auth/login';
        this.compiler.clearCache();
        // this.router.navigate(['/auth/login']);
    }

    public toSearchEvent() {
        this.router.navigate([`/redirect`], {
            queryParams: {
                url: '/search/activity',
                location: true,
            },
        });
    }

    // public toFollowers() {
    //     this.router.navigate(['profile', this.user.id, 'followers']);
    // }

    // public toFollowings() {
    //     this.router.navigate(['profile', this.user.id, 'followings']);
    // }

    openUserSettings() {
        this.userSettingsComp.modalOpen();
    }

    openMyAcademies() {
        if (this.academyList.length == 1) {
            if (this.academyList[0].is_pro) {
                this.router.navigate(['academiy-datail', this.academyList[0].id, 'dashboard']);
                return;
            }
            this.router.navigate(['academiy-datail', this.academyList[0].id]);
        } else if (this.academyList.length > 1) {
            this.myAcademimes.modalOpen();
        }
    }

    onChangeProfile(data) {
        console.log('onChangeProfile ', data);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
