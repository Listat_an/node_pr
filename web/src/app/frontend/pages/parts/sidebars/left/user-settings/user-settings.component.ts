import {
    DatepickerOptions,
} from '../../../../../../../components/ng-datepicker';

import {
    ClolorsBelt,
    Belts,
} from '../../../../../../../components/belts';

import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators,
} from '@angular/forms';

import {
    AuthService,
} from '../../../../../../services/auth/auth.service';
import {
    AcademiesService,
} from '../../../../../../services/academies.service';

import 'rxjs/add/operator/debounceTime';

import {
    indexOf,
    split,
} from 'lodash';
import {
    NotificationsService,
} from '../../../../../../services/notifications.service';
import {
    format,
} from 'date-fns';

import {
    Component,
    OnInit,
    ViewEncapsulation,
    Output,
    EventEmitter,
} from '@angular/core';
import {
    Select2OptionData,
} from 'ng2-select2';
import {
    ToastrService,
} from 'ngx-toastr';

@Component({
    selector: 'opn-user-settings',
    templateUrl: './user-settings.component.html',
    styleUrls: ['./user-settings.component.scss'],
    providers: [AcademiesService, NotificationsService],
    encapsulation: ViewEncapsulation.Emulated,
})
export class UserSettingsComponent implements OnInit {
    public itemsSearchActive: boolean = false;
    public academiesSearch = [];
    public usersSearch = [];
    public eventsSearch = [];
    public baseUrl;
    public defaultUserIcon = '/assets/images/user-dafault.png';
    public defaultAcademyIcon = 'assets/images/academy-logo-transparent.png';
    public today = new Date();
    public notificationSound = new Audio('assets/sounds/definite.mp3');
    public emptySearch: any = true;
    public isLoading: boolean = false;
    public title = 'Registration';
    public belts: Array < Select2OptionData > ;
    public academies: Array < Select2OptionData > ;
    public weights: Array < Select2OptionData > ;
    public optionsBelt: Select2Options;
    public optionsAcademy: Select2Options;
    public optionsWeight: Select2Options;
    public newMessage: boolean = false;
    public openChat: boolean = false;
    public openAcademyChat: boolean = false;
    public dialogs = [];
    public messages = [];
    public myId;
    public isMessages: any;
    public dialogActive;
    public activeDialog;
    public listSearch: Array < any > = [];
    public typeUser = 'user';
    public academyId;
    private selected = {};

    public birthDay: Date;
    public dateOptions: DatepickerOptions;
    public form: FormGroup;
    public chatType = 'user';
    private belt: any = '0';
    private academy: any;
    private academyName: String;
    private weight: any;
    private validErrAcademy: Boolean;

    colorsBelt: Array < ClolorsBelt > ;
    arrBelts: Array < Belts > ;

    private weightCurrent: any;
    private latitude: any;
    private longitude: any;
    private addres: any = '';
    private short_address: any = '';
    private profile: Boolean = false;

    private currentBelt: any;
    private currentAcademy: any;
    private userAvatar: any;

    private avatarFile: any;
    private changeEmail: Boolean = false;
    private changePassword: Boolean = false;
    private status_public: Boolean;

    private gaAutocompleteStings: any = {
        showSearchButton: false,
        currentLocIconUrl: 'https://cdn4.iconfinder.com/data/icons/proglyphs-traveling/512/Current_Location-512.png',
        locationIconUrl: 'http://www.myiconfinder.com/uploads/iconsets/369f997cef4f440c5394ed2ae6f8eecd.png',
        recentStorageName: 'componentData4',
        noOfRecentSearchSave: 8,
        geoTypes: ['address'],
        inputPlaceholderText: '',
        inputString: '',
    };

    private firstLogin: Boolean = false;
    private modalAcademy = false;
    private myAcademies: Array < any > = [];
    private me: any;
    private allSaved: Boolean = false;
    private changeAddres: Boolean = true;
    private changeShortAddres: Boolean = false;
    private addresEmpty: Boolean = false;
    private ageType: any = null;

    profilesaved: EventEmitter < any > = new EventEmitter < any > ();
    private loadData = false;

    constructor(
        private authService: AuthService,
        private serviceAcademy: AcademiesService,
        private toastr: ToastrService,
        private formBuilder: FormBuilder,
    ) {
        this.validErrAcademy = false;
    }

    ngOnInit() {
        this.dateOptions = {
            minYear: 1970,
            maxYear: 2080,
            displayFormat: 'M[/] D[/] YYYY',
            barTitleFormat: 'MMMM YYYY',
            firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
        };
    }

    public modalOpen() {
        this.profile = true;
        this.form = this.formBuilder.group({
            firstName: [null, [Validators.required]],
            lastName: [null, Validators.required],
            weight: [null, Validators.required],
            email: [{
                    value: null,
                },
                [Validators.email, Validators.required]
            ],
            password: [{
                    value: null,
                },
                [Validators.pattern('((?=.*[0-9])(?=.*[a-zA-Z]).{8,50})'), Validators.required]
            ],
        });
        document.body.classList.add('openModal');
        this.firstLogin = true;
        this.loadData = true;
        this.serviceAcademy.getUserMe()
        .finally(() => {
            this.loadData = false;
        })
        .subscribe(res => {
            this.me = res;
            console.log('this user ************************> ', res);
            this.currentBelt = res.belt;
            this.status_public = res.status_public;
            this.form.patchValue({
                firstName: res.first_name,
                lastName: res.last_name,
                weight: res.weight,
                email: res.email,
                password: '1111111a',
            });
            if (res.address != '') {
                this.addres = (res.address) ? res.address : '';
                this.latitude = res.latitude;
                this.longitude = res.longitude;
            }
            this.ageType = res.belt.belt_color.type === 'all' ? 'junior' : res.belt.belt_color.type;
            this.gaAutocompleteStings.inputString = (res.address) ? res.address : '';
            this.weightCurrent = res.weight;
            this.birthDay = new Date(res.date);
            this.authService.getBeltStipiesBycolorId(res.belt.belt_color.id).subscribe(belts => {
                this.arrBelts = belts;
            });
            this.currentAcademy = {
                id: res.academy.id,
                text: res.academy.name,
            };
            this.setAcademy(this.currentAcademy);
            this.userAvatar = res.avatar;

            this.authService.getBeltColors(`all,${this.ageType}`).subscribe(beltColors => {
                this.colorsBelt = beltColors;
            });
        });
    }

    public setAcademy = function (academy) {
        this.academy = academy.id;
        this.academyName = academy.text;
        this.validErrAcademy = false;
    };

    setAvatar(foto) {
        this.userAvatar = foto;
        this.avatarFile = foto;
    }

    displayFieldCss(field: string) {
        return {
            'has-error': this.isFieldValid(field),
        };
    }

    isFieldValid(field: string) {
        return !this.form.get(field).valid && this.form.get(field).touched;
    }


    searchinputCallback(e) {
        this.addres = e.target.value;
        this.changeAddres = false;
        if (this.addres != '') {
            this.addresEmpty = false;
        }
    }

    autoCompleteCallback1(event) {
        console.log('CHOOSEN LOCATION', event);
        if (event.data) {
            this.changeAddres = true;
            this.setShortAddres(event.data);
            this.addres = event.data.formatted_address;
            const location = event.data.geometry.location;
            this.latitude = location.lat;
            this.longitude = location.lng;
        } else {
            this.changeAddres = false;
        }
    }

    setShortAddres(data) {
        data.address_components.forEach(el => {
            if (indexOf(['locality', 'administrative_area_level_1', 'postal_code'], el.types[0]) >= 0) {
                this.short_address += this.formatAddress(this.short_address, el.short_name);
            }
        });
        this.changeShortAddres = true;
        this.changeAddres = true;
    }

    onChangeAge(e) {
        if (this.me.academy_status == 0) {
            this.authService.getBeltColors(`all,${e.value}`).subscribe(res => {
                this.colorsBelt = res;
                this.authService.getBeltStipiesBycolorId(res[0].id).subscribe(belts => {
                    this.arrBelts = belts;
                    this.belt = belts[0].id;
                });
            });
        }
    }

    selectPrivate(event) {
        console.log('change status: ', event);
        this.status_public = event;
    }

    onChangeEmail(event) {
        this.form.controls['email'].reset({
            value: '',
            disabled: false
        }, );
        event.target.style.display = 'none';
        this.changeEmail = true;
    }

    onChangePassword(event) {
        this.form.controls['password'].reset({
            value: '',
            disabled: false
        }, );
        event.target.style.display = 'none';
        this.changePassword = true;
    }

    onSubmit() {
        if (this.form.valid && !this.validErrAcademy) {
            this.saveSettings(this.form);
        } else {
            this.validateAllFormFields(this.form);
        }
        if (this.academy == undefined) {
            this.validErrAcademy = true;
        }
    }

    saveSettings(form) {
        // const [day, month, year] = split(this.form.get('birthDay').value, '/');

        const settings: any = {
            academy_name: this.academyName,
            user: {
                first_name: form.get('firstName').value,
                last_name: form.get('lastName').value,
                belt_id: `${this.belt}`,
                academy_id: this.academy,
                address: this.addres,
                longitude: this.longitude,
                latitude: this.latitude,
                weight: form.get('weight').value,
                date: this.birthDay,
                status_public: this.status_public,
            },
            avatar_base: this.avatarFile,
        };
        if (this.changeEmail) {
            settings.user['email'] = form.get('email').value;
        }

        if (this.changePassword) {
            settings.user['password'] = form.get('password').value;
        }

        if (this.changeShortAddres) {
            settings.user.short_address = this.short_address;
        }


        this.authService.editProfile(settings, this.me.id).subscribe(profile => {
                if (profile) {
                    this.allSaved = true;
                    // window.location.reload();
                    this.profilesaved.emit(profile);
                    this.closeModal();
                }
            },
            err => {
                const errore = JSON.parse(err._body).errors;
                const self = this;
                errore.forEach(function (el, i, arr) {
                    if (el.status == '400') {
                        self.toastr.info(el.description, 'Warning!');
                    }
                });
            });
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({
                    onlySelf: true,
                });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    public changeBeltColor = function (id) {
        if (this.me.academy_status == 0) {
            this.belt = id;
        }
    };

    onChangeColor(e) {
        if (this.me.academy_status == 0) {
            this.authService.getBeltStipiesBycolorId(e).subscribe(res => {
                this.arrBelts = res;
            });
        }
    }


    formatAddress(short_address, short_name): String {
        return (short_address == '') ? short_name : `, ${short_name}`;
    }

    private closeModal() {
        document.body.classList.remove('openModal');
        this.profile = false;
        this.firstLogin = false;
    }

    onSkip() {
        if (this.addres != '') {
            this.closeModal();
        } else {
            this.addresEmpty = true;
        }
    }
}
