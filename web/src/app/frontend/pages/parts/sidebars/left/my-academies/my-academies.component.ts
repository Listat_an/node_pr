import {
    Component,
    EventEmitter,
    OnInit,
} from '@angular/core';
import {
    Location,
} from '@angular/common';

import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    AbstractControl,
} from '@angular/forms';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';

import {
    AcademiesService,
} from '../../../../../../services/academies.service';

@Component({
    selector: 'opn-my-academies',
    templateUrl: './my-academies.component.html',
    styleUrls: ['./my-academies.component.scss'],
    providers: [AcademiesService],
})
export class MyAcademiesComponent implements OnInit {

    public form: FormGroup;
    private latitude: any;
    private longitude: any;
    private addres: any;
    private me: any;
    private created: any = false;
    private academyId: any;
    private done: Boolean = false;

    private step: Number = 1;
    private thisOpen: Boolean = false;

    private myAcademies: Array<any> = [];

    private gaAutocompleteStings: any = {
        showSearchButton: false,
        showCurrentLocation: false,
        currentLocIconUrl: 'https://cdn4.iconfinder.com/data/icons/proglyphs-traveling/512/Current_Location-512.png',
        locationIconUrl: 'http://www.myiconfinder.com/uploads/iconsets/369f997cef4f440c5394ed2ae6f8eecd.png',
        recentStorageName: 'componentData4',
        noOfRecentSearchSave: 8,
        inputPlaceholderText: 'Address',
        inputString: '',
    };

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private serviceAcademy: AcademiesService,
    ) {
        this.me = this.serviceAcademy.getMe();
    }

    ngOnInit() {
        this.step = 1;
    }

    getMyacademies() {
        this.serviceAcademy.getManagerAcademies(this.me.id).toPromise().then(res => {
            this.myAcademies = res;
            console.log('getManagerAcademies ', res);
        });
    }

    autoCompleteCallback1(event) {
        if (event.data) {
            this.addres = event.data.formatted_address;
            const location = event.data.geometry.location;
            this.latitude = location.lat;
            this.longitude = location.lng;
        }
    }

    public modalOpen() {
        this.form = this.formBuilder.group({
            name: [null, Validators.required],
            phone: [null, Validators.required],
            email: [null, [Validators.email, Validators.required]],
            // password: [{ value: null }, [Validators.pattern('((?=.*[0-9])(?=.*[a-zA-Z]).{8,50})'), Validators.required]],
        });
        this.thisOpen = true;
        document.body.classList.add('openModal');
        this.getMyacademies();
    }

    public closeModal() {
        this.thisOpen = false;
        document.body.classList.remove('openModal');
        this.form.reset();
        this.step = 1;
    }

    displayFieldCss(field: string) {
        return {
            'has-error': this.isFieldValid(field),
        };
    }

    isFieldValid(field: string) {
        return !this.form.get(field).valid && this.form.get(field).touched;
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({
                    onlySelf: true,
                });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    onSubmit() {
        if (this.form.valid && this.addres != '') {
            this.serviceAcademy.addAcademies({
                email: this.form.get('email').value,
                name: this.form.get('name').value,
                phone: this.form.get('phone').value,
                location: this.addres,
                user_id: this.me.id,
                latitude: this.latitude,
                longitude: this.longitude,
            }).subscribe(res => {
                this.created = true;
                this.academyId = res.id;
            });
        } else {
            this.validateAllFormFields(this.form);
        }
    }
    onDone() {
        if (this.academyId) {
            this.done = true;
            document.body.classList.remove('openModal');
            this.router.navigate([`/academiy-datail/${this.academyId}`]);
        } else {
            this.closeModal();
        }
    }
    // onNewAcademy() {
    //     this.step = 2;
    // }
}
