import {
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
} from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    Validators,
} from '@angular/forms';
import {
    IOption,
} from 'ng-select';
import {
    Subscription,
} from 'rxjs/Subscription';
import {
    cancelSubscription,
} from '../../../providers/cancelSubscription';
import {
    SubscriptionsService,
} from './subscriptions.service';
import {
    FormErrorsService,
} from '../../../providers/form-errors.service';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import {
    AcademiesService,
} from '../../../services/academies.service';
import {
    SocketService,
} from '../../../services/socket.service';
import * as _ from 'lodash';

import { ModalConfirmComponent } from '../components/academy/modal-confirm/modal-confirm.component';

@Component({
    selector: 'opn-create-subscriptions',
    templateUrl: './create-subscriptions.component.html',
    styleUrls: ['./create-subscriptions.component.scss', '../@theme/scss/theme.scss'],
    providers: [SubscriptionsService, AcademiesService],
})
export class CreateSubscriptionsComponent implements OnInit, OnDestroy {
    public checkbox: boolean = false;
    public isSubscriptionCreation: boolean = false;
    public isLoading: boolean = false;
    public membership: Array < IOption > = [{
            label: 'One day',
            value: '1_day'
        },
        {
            label: 'Month',
            value: '1_month'
        },
        {
            label: '3 Months',
            value: '3_month'
        },
        {
            label: '6 Months',
            value: '6_month'
        },
        {
            label: 'Year',
            value: '1_year'
        },
    ];
    public cardsMembership: Object = {
        '1_day': 'One day',
        '1_month': 'Month',
        '3_month': '3 month',
        '6_month': '6 month',
        '1_year': 'Year',
    };
    public subscriptions = [];
    public idAcademy: number;
    public formSubscription: FormGroup;
    public user;
    public academy;
    public trial: boolean = false;
    public edit: boolean = false;
    public editId;
    public subscrips: Subscription[] = [];


    public formErrors = {
        name: '',
        price: '',
        membership: '',
        registration_fee: '',
        trial_days: '',
    };

    age: any = [5, 20];

    @ViewChild('elMembership') public elMembership;
    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;
    public confirmAction: String;

    ageConfig: any = {
        behaviour: 'drag',
        connect: true,
        margin: 5,
        limit: 50, // NOTE: overwritten by [limit]="10"
        step: 1,
        range: {
            min: 3,
            max: 100,
        },
        tooltips: [true, true],
    };
    public tab = 'subscriptions';

    constructor(private fb: FormBuilder,
        private subService: SubscriptionsService,
        private formErrorsService: FormErrorsService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private academiesService: AcademiesService,
    ) {
        this.activeRoute.params.subscribe(params => {
            this.idAcademy = params['id'];
            this.getAcademy();
        });
    }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.getSubscriptions(this.idAcademy);
    }

    public getSubscriptions(academyId: number) {
        this.subscrips.push(this.subService.getSubscriptions(academyId)
            .subscribe((res) => {
                this.subscriptions = res.data;
            }, (err) => {
                console.log(err);
            })
        )
    }

    public buildForm() {
        this.formSubscription = this.fb.group({
            academy_id: [this.idAcademy],
            name: ['', Validators.required],
            price: ['', [
                Validators.required,
                Validators.pattern('(0|([1-9][0-9]*))(\\.[0-9]+)?$'),
            ]],
            membership: ['', Validators.required],
            registration_fee: [0, [
                Validators.required,
                Validators.pattern('(0|([1-9][0-9]*))(\\.[0-9]+)?$')
            ]],
            trial_days: [0, [Validators.pattern('[0-9]*')]],
            information: ['']
        });

        this.formSubscription.valueChanges.subscribe((data) => this.onValueChange(data));
    }

    public toggleTrial() {
        this.trial = !this.trial;
    }

    public onValueChange(data ? ) {
        this.formErrors = this.formErrorsService.getErrors(this.formSubscription, this.formErrors, data);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }

    public setValue(data) {
        this.editId = data.id;
        this.formSubscription.controls['name'].setValue(data.name);
        this.formSubscription.controls['price'].setValue(data.price);
        this.formSubscription.controls['registration_fee'].setValue(data.registration_fee);
        this.formSubscription.controls['trial_days'].setValue(data.trial_days);
        //   this.formSubscription.controls['age'].setValue(data.age);
        this.formSubscription.controls['information'].setValue(data.information);
        this.trial = !!data.trial_days;
        const membership = data.membership;
        this.elMembership.select(membership);
        this.formSubscription.controls['membership'].setValue(membership);

        this.age = _.split(data.age, '-', 2);
    }

    public editSubscription(subscribe) {
        this.edit = true;
        this.setValue(subscribe);
    }

    public updateSubscription(form) {
        this.isSubscriptionCreation = false;
        this.isLoading = true;
        this.subscrips.push(this.subService.updateSubscription(
                this.editId, {
                    ...form.value,
                    age: _.join(this.age, '-'),
                },
            )
            .subscribe((res) => {
                const data = res.subscription;
                const i = this.subscriptions.findIndex((el) => {
                    return el.id === data.id;
                });
                this.subscriptions[i] = data;
                this.resetForm();
                this.isLoading = false;
            }, (err) => {
                console.log(err);
            })
        );
    }

    public deleteSubscription(id: number) {
        this.modalConfirm.modalTitle = 'Delete your Subscription?';
        this.modalConfirm.modalDescription = 'This action will remove your subscription.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
        this.editId = id;
        this.confirmAction = 'remove';
    }
    onConfirm(confirm) {
        if (confirm) {
            switch (this.confirmAction) {
                case 'remove': {
                    this.subscrips.push(this.subService.deleteSubscription(this.editId)
                        .subscribe((res) => {
                            this.subscriptions = this.subscriptions.filter((el) => {
                                return el.id !== this.editId;
                            });
                        }, (err) => {
                            console.log(err);
                        })
                    );
                    break;
                }

            }
        }
    }

    public resetForm() {
        this.formSubscription.reset();
        this.formSubscription.controls['academy_id'].setValue(this.idAcademy);
        //   this.formSubscription.controls['age'].setValue('5-10');
        this.formSubscription.controls['registration_fee'].setValue(this.academy.reg_price.toString());
        this.edit = false;
        this.trial = false;
        this.editId = '';
    }

    public onSendForm(form) {
        this.isSubscriptionCreation = false;
        this.isLoading = true;
        this.subscrips.push(this.subService.createSubscription({
                ...form.value,
                age: _.join(this.age, '-'),
            })
            .subscribe((res) => {
                this.subscriptions.unshift(res.subscription);
                this.isLoading = false;
                this.resetForm();
            }, (err) => {
                console.log(err);
            })
        );
    }

    private getAcademy() {
        this.academiesService.getAcademyById(this.idAcademy).subscribe(academy => {
            this.academy = academy;
            this.buildForm();
        });
    }

    public onChangeFilterStatus(tab) {
        switch (tab) {
            case 'classes':
                {
                    this.router.navigate(['academiy-datail', this.idAcademy, 'classes']);
                    break;
                }
            case 'subscriptions':
                {
                    this.router.navigate(['academiy-datail', this.idAcademy, 'create-subscriptions']);
                    break;
                }
        }
    }
}
