import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AcademyBeltService} from '../../../services/academy-belt.sevice';
import {ConfigService} from '../../../services/service.config';
import {AcademiesService} from '../../../services/academies.service';

@Component({
    selector: 'belt-settings',
    templateUrl: './belt-settings.html',
    styleUrls: ['./belt-settings.scss','../@theme/scss/theme.scss'],
    providers: [AcademyBeltService, AcademiesService]
})
export class BeltSettingsComponent implements OnInit {
    private isManager: Boolean = false;
    baseUrl = ConfigService.URL_SERVER;
    academyId;
    academy;
    belts;

    constructor(private academyBeltService: AcademyBeltService,
                private academiesService: AcademiesService,
                private activatedRoute: ActivatedRoute) {

    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.academyId = params['id'];
            this.academiesService.getAcademyById(this.academyId).subscribe(academy => {
                this.academy = academy;
            });
            this.academyBeltService.getBeltSettings(this.academyId).subscribe(beltSettings => {
                this.belts = beltSettings;
            });
        });
    }

    editBeltSetting(belt){
        let value = this.academy.promotion_method === 'time' ? belt.time_stripe_count : belt.class_stripe_count;
        this.academyBeltService.editBeltSettings(belt, value).subscribe(res => {
            belt.time_stripe_count = res.time_stripe_count;
            belt.class_stripe_count = res.class_stripe_count;
            belt.isEdit = false;
        })
    }

    onCheckManager(status) {
        this.isManager = status;
    }
}
