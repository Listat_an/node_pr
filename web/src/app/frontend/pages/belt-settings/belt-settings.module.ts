import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PagesModule} from '../pages.module';
import {BeltSettingsComponent} from './belt-settings.component';
import {RouterModule} from '@angular/router';
import {AcademyComponentsModule} from '../components/academy/academy.module';
import {FormsModule} from '@angular/forms';

const routs = [
    {path: '', component: BeltSettingsComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(routs),
        AcademyComponentsModule,
        CommonModule,
        FormsModule,
        PagesModule,
    ],
    declarations: [
        BeltSettingsComponent
    ]
})
export class BeltSettingsModule {

}
