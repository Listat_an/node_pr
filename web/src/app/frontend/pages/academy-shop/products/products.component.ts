import {
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
} from '@angular/core';
import {
    ShopService,
} from '../../../../services/shop.service';
import {
    ConfigService,
} from '../../../../services/service.config';
import {
    ActivatedRoute,
    Router,
} from '@angular/router';
import {
    range,
    ceil,
    pick,
    filter,
} from 'lodash';
import {
    Subscription,
} from 'rxjs/Subscription';
import {
    cancelSubscription,
} from '../../../../providers/cancelSubscription';
import {
    Subject,
} from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import { ModalConfirmComponent } from '../../components/academy/modal-confirm/modal-confirm.component';

@Component({
    selector: 'opn-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss'],
    providers: [ShopService],
})
export class ProductsComponent implements OnInit, OnDestroy {
    academyId;
    productName: string = '';
    products;
    tmpProduct: any;
    public subscrips: Subscription[] = [];
    private nameFilter: Subject < string > = new Subject();

    baseUrl = ConfigService.URL_SERVER;
    noPhoto = '/assets/images/no-photo-product.png';

    limit: any = 5;
    page: any = 1;
    pagintion: any = {
        show: false,
        pages: [],
    };
    perPageOptions = [{
            value: 5,
            title: '5 per page',
        },
        {
            value: 10,
            title: '10 per page',
        },
        {
            value: 20,
            title: '20 per page',
        },
    ];

    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;

    constructor(
        private shopService: ShopService,
        private activeRoute: ActivatedRoute,
        private router: Router,
    ) {
        this.activeRoute.parent.params.subscribe(res => {
            this.academyId = res['id']
        });
    }

    ngOnInit() {
        this.getProducts();
        this.nameFilter.debounceTime(500).subscribe(() => {
            this.getProducts();
        });
    }

    getProducts() {
        this.subscrips.push(
            this.shopService.getAcademyProductsWithPagination(this.academyId, this.productName, this.page, this.limit).subscribe(res => {
                console.log('getAcademyProductsWithPagination ', res);
                this.products = res['data'];
                this.Paginations(res['total']);
            }),
        );
    }

    searchProducts() {
        this.nameFilter.next();
    }

    public onChangePerPage(e) {
        this.limit = e.value;
        this.getProducts();
    }

    Paginations(total) {
        if (total > this.limit) {
            this.pagintion.pages = range(1, ceil(total / this.limit) + 1, 1);
            this.pagintion.show = true;
        } else {
            this.pagintion.show = false;
        }
    }

    onCheckRow(e, i) {
        if (e.target.checked) {

        } else {

        }
    }

    onLoadPage(page) {
        this.page = page;
        this.getProducts();
    }

    toCreateProduct() {
        this.router.navigate(['academiy-datail', this.academyId, 'shop', 'add-product']);
    }

    onChangeShow(product, i) {
        this.shopService.updateProduct(product.id, {
            ...pick(product, ['title', 'description', 'quantity', 'price', 'academy_id']),
            show: +!product.show,
        }).subscribe(res => {
            if (res) {
                this.products[i].show = +!product.show;
            }
        });
    }

    onRemove(product) {
        this.modalConfirm.modalTitle = 'Delete your Product?';
        this.modalConfirm.modalDescription = 'This action will remove your product.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
        this.tmpProduct = product;
    }

    onConfirm(confirm) {
        if (confirm) {
            this.shopService.deleteProductById(this.tmpProduct.id).subscribe(res => {
                if (res.status) {
                    this.products = this.products.filter(iProduct => iProduct !== this.tmpProduct);
                }
            });
        }
      }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
