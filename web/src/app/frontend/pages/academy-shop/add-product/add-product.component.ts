import {Location} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {ShopService} from '../../../../services/shop.service';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'opn-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.scss'],
    providers: [ShopService]
})
export class AddProductComponent implements OnInit {
    academyId;
    createdProductId;
    public addForm: FormGroup;

    mainImage;
    images = ['', '', '', '', '', ''];
    productData = {
        title: '',
        description: '',
        price: 0,
        shipping_type: 'free',
        shipping_price: 0,
        quantity: 0,
        show: 1,
    };

    productSaved: Boolean = false;
    private load: Boolean = false;

    constructor(private _location: Location,
                private shopService: ShopService,
                private route: ActivatedRoute,
                private formBuilder: FormBuilder,
                private router: Router,
                private toastr: ToastrService,
            ) {
        this.route.parent.params.subscribe(params => {
            this.academyId = params['id'];
            this.productData['academy_id'] = this.academyId;
        });
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        };
        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
                this.router.navigated = false;
                window.scrollTo(0, 0);
            }
        });
    }

    ngOnInit() {
        this.buildForm();
    }

    buildForm() {
        this.addForm = this.formBuilder.group({
            title: [null, [Validators.required]],
            description: [null, Validators.required],
            price: ['', Validators.required],
            shipping_price: [{value: 0.00, disabled: true}, Validators.required],
            quantity: [null, Validators.required],
        });
    }

    onMainImageAdded(e) {
        this.mainImage = e;
    }

    onImageAdded(e, i) {
        this.images[i] = e;
    }

    onChangeShipping(event: Boolean) {
        this.productData.shipping_type = (!event) ? 'free'  : 'costing';
        this.productData.shipping_price = 0;
        switch (this.productData.shipping_type) {
            case 'free': {
                this.addForm.controls['shipping_price'].disable();
                break;
            }
            case 'costing': {
                this.addForm.controls['shipping_price'].enable();
            }
        }
    }

    cancelCreation() {
        this._location.back();
    }

    saveProduct() {
        if (this.addForm.valid) {
            this.load = true;
            let images = this.images;
            let filteredImages = [];
            if (this.mainImage) {
                images = [this.mainImage, ...this.images];
            }
            filteredImages = images.filter(v => (v != '' && v.trim().length > 0));
            this.productData.title = this.addForm.get('title').value;
            this.productData.description = this.addForm.get('description').value;
            this.productData.price = this.addForm.get('price').value;
            this.productData.shipping_price = this.addForm.get('shipping_price').value;
            this.productData.quantity = this.addForm.get('quantity').value;
            const productInfo = {
                ...this.productData,
                images: filteredImages,
            };
            delete productInfo.shipping_type;
        
            this.shopService.createProduct(productInfo)
            .finally(() => {
                this.load = false;
            })
            .subscribe(res => {
                this.createdProductId = res['id'];
                this.productSaved = true;
            });
        }else {
            this.toastr.info('You must enter required fields', 'Warning!');
            this.validateAllFormFields(this.addForm);
        }
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({
                    onlySelf: true,
                });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    onChangeVissbility(e) {
        if (e.target.checked) {
            this.productData.show = 0;
        }else {
            this.productData.show = 1;
        }
    }

    goToView() {
        this.router.navigate([`/academiy-datail/${this.academyId}/product/${this.createdProductId}`]);
    }
    goToCreateNew() {
        console.log('goToCreateNew');
        this.router.navigate([`/academiy-datail/${this.academyId}/shop/add-product`]);
    }
}
