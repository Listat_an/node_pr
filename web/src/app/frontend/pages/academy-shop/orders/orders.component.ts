import {
    Component,
    OnInit,
    ViewEncapsulation,
    OnDestroy,
    ViewChild,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import {
    cancelSubscription,
} from '../../../../providers/cancelSubscription';
import {
    Subscription,
} from 'rxjs/Subscription';

import {
    OrdersService,
} from './orders.service';

import * as _ from 'lodash';
import {
    addDays,
    isBefore,
    subMonths,
} from 'date-fns';
import {
    DatepickerOptions,
} from 'ng2-datepicker';

import {
    Subject,
  } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';

import {
    AcademiesService,
} from '../../../../services/academies.service';

@Component({
    selector: 'opn-shop-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss', '../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        AcademiesService,
        OrdersService,
    ],
})
export class ShopOrdersComponent implements OnInit, OnDestroy {
    private academyId: Number;
    public subscrips: Subscription[] = [];
    private orders: Array < any > = [];

    private studentId: Number = null;
    private isAddBelt: Boolean = false;
    listCheckboxes: Array < any > = [];
    row_index: number = -1;
    private per_page: any = 10;
    private page: any = 1;
    private pagintion: any;
    private load: Boolean = true;
    private academyInfo: any;

    private filter = {
        status: [],
        id: '',
        date: {
            fromDate: subMonths(new Date(), 1),
            toDate: addDays(new Date(), 1),
        },
    };

    private sort = {
        id: {
            order: 'DESC',
            field: 'id',
        },
        date: {
            order: '',
            field: 'created_at',
        },
        full_name: {
            order: '',
            field: 'full_name',
        },
        status: {
            order: '',
            field: 'status',
        },
        total: {
            order: '',
            field: 'total',
        },
    };

    protected statusLabels: object = {
        in_process: 'In process',
        new: 'New',
        shipped: 'Shipped',
    };

    @ViewChild('fromDateComponent') fromDateComponent;

    toDateOptions: DatepickerOptions = {
        minYear: 1970,
        maxYear: 2080,
        displayFormat: 'MM[/] DD[/] YYYY',
        barTitleFormat: 'MMMM YYYY',
        firstCalendarDay: 1, // 0 - Sunday, 1 - Monday
        maxDate: addDays(new Date(), 1),
    };

    fromDateOptions: DatepickerOptions = {
        ...this.toDateOptions,
    };

    statusSelectSettings = {
        text: 'Status',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: false,
        badgeShowLimit: 1,
        classes: 'myclass custom-class-search',
    };

    statusOptions = [];

    private subjectFilter: Subject < string > = new Subject();

    constructor(
        private academySrv: AcademiesService,
        private ordersSrv: OrdersService,
        private activeRoute: ActivatedRoute,
        private router: Router,
    ) {
        this.pagintion = {
            show: false,
            pages: [],
        };

        _.forIn(this.statusLabels, (v: string, k: string) => {
            this.statusOptions.push({
                    itemName: v,
                    id: k,
                },
            );
        });
    }



    ngOnInit() {

        this.activeRoute.parent.params.subscribe(params => {
            this.academyId = params['id'];
        });

        this.getAcademyOrders();
        this.getAcademyInfo();

        this.subjectFilter.debounceTime(500).subscribe(() => {
            this.getAcademyOrders();
          });
    }

    onOrderSearch() {
        this.subjectFilter.next();
    }

    private getAcademyInfo() {
        this.subscrips.push(
            this.academySrv.getAcademyById(this.academyId).subscribe(academyInfo => {
                this.academyInfo = academyInfo;
                console.info('academyInfo', academyInfo);
            }),
        );
    }

    onToDateChange(toDate) {
        if (isBefore(toDate, this.filter.date.fromDate)) {
            this.filter.date.fromDate = toDate;
        }
        this.fromDateOptions.maxDate = toDate;
        this.fromDateComponent.init();
        this.getAcademyOrders();
    }

    selectStatus(options) {
        console.log('options ', options);
    }

    private getAcademyOrders() {
        this.load = false;
        this.subscrips.push(
            this.ordersSrv.getAcademyOrders(this.per_page, this.page, this.academyId, this.sort, this.filter)
            .finally(() => {
                this.load = true;
            })
            .subscribe(orders => {
                this.formatData(orders.data);
                this.Paginations(orders.total);
            }),
        );
    }

    private formatData(data) {
        this.orders = data;
        this.listCheckboxes = data.map(el => {
            return {
                name: 'checkbox_' + el.id,
                checked: false,
                value: el.user_id,
            };
        });
    }

    onChangeFilter(nameFilter) {
        if (this.sort[nameFilter].order === '' || this.sort[nameFilter].order === 'DESC') {
            this.sort[nameFilter].order = 'ASC';
            this.getAcademyOrders();
            return;
        }
        if (this.sort[nameFilter].order === 'ASC') {
            this.sort[nameFilter].order = 'DESC';
            this.getAcademyOrders();
            return;
        }
    }

    Paginations(total) {
        if (total > this.per_page) {
            this.pagintion.pages = _.range(1, _.ceil(total / this.per_page) + 1, 1);
            this.pagintion.show = true;
        } else {
            this.pagintion.show = false;
        }
    }

    onLoadPage(page) {
        this.page = page;
        this.getAcademyOrders();
    }

    public onChangePerPage(e) {
        this.per_page = e.value;
        this.getAcademyOrders();
    }

    toOrder(orderId) {
        this.router.navigate(['academiy-datail', this.academyId, 'shop', 'orders', orderId]);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
