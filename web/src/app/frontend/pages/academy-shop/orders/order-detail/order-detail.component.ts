import {
    Component,
    OnInit,
    ViewEncapsulation,
    OnDestroy,
} from '@angular/core';
import {
    Router,
    ActivatedRoute,
} from '@angular/router';
import {
    cancelSubscription,
} from '../../../../../providers/cancelSubscription';

import {
    OrdersService,
} from '../orders.service';

import {DataService} from '../../../../../services/data.service';

import {
    AcademiesService,
} from '../../../../../services/academies.service';
import {ConfigService} from '../../../../../services/service.config';

import * as _ from 'lodash';

@Component({
    selector: 'opn-shop-orders-detail',
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.scss', '../../../@theme/scss/theme.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        AcademiesService,
        OrdersService,
    ],
})
export class ShopOrderDetailComponent implements OnInit, OnDestroy {
    private academyId: Number;
    private orderId: Number;
    private userId: Number;
    private orderData: any = null;
    private load: Boolean = true;
    private academyInfo: any;
    private subscrips = [];

    private baseUrl = ConfigService.URL_SERVER;

    protected statusLabels: object = {
        new: 'New',
        in_process: 'In process',
        shipped: 'Shipped',
    };

    private statusBar = {
        data: [],
        count: 0,
    };

    protected paidLabels: object = {
        paid: 'Paid',
        not_paid: 'Not paid',
    };

    constructor(
        private academySrv: AcademiesService,
        private ordersSrv: OrdersService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private dataServices: DataService,
    ) {
        _.forIn(this.statusLabels, (v, k) => {
            this.statusBar.data.push({
                id: k,
                label: v,
                active: false,
                hover: false,
            });
            this.statusBar.count += 1;
        });
    }



    ngOnInit() {

        this.userId = this.ordersSrv.getMe().id;

        this.activeRoute.parent.params.subscribe(params => {
            this.academyId = params['id'];
        });
        this.activeRoute.params.subscribe(params => {
            this.orderId = params['orderId'];
        });

        this.getOrder();
    }

    private getOrder() {
        this.load = false;
        this.subscrips.push(
            this.ordersSrv.getOrderById(this.orderId, this.academyId).finally(() => {
                this.load = true;
            }).subscribe(order => {
                this.orderData = order;
                this.setActiveStatus(order.status);
            }),
        );
    }

    protected clearActive() {
        this.statusBar.data.forEach(el => {
            el.active = false;
        });
    }

    protected setActiveStatus(statusId) {
        this.clearActive();
        this.statusBar.data.some(element => {
            element.active = true;
            return element.id === statusId;
        });
    }

    onEnterStatus(statusId) {
        this.statusBar.data.some(element => {
            element.hover = true;
            return element.id === statusId;
        });
    }

    onLeaveStatus() {
        this.statusBar.data.forEach(element => {
            element.hover = false;
        });
    }

    private onSetStatus(statusId, index) {
        this.subscrips.push(
            this.ordersSrv.setOrderStatus(this.orderId, {status: statusId}).subscribe(res => {
                if (res) {
                    this.setActiveStatus(statusId);
                }
            }),
        );
    }

    public onEventMessage(userId) {
        this.dataServices.eventMessage.emit({
            type: 'user',
            user_id: userId,
        });
    }

    toOrders() {
        this.router.navigate(['academiy-datail', this.academyId, 'shop', 'orders']);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
