import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AcademyShopComponent} from './academy-shop.component';
import {AddProductComponent} from './add-product/add-product.component';
import {ProductsComponent} from './products/products.component';
import {ShopOrdersComponent} from './orders/orders.component';
import {ShopOrderDetailComponent} from './orders/order-detail/order-detail.component';
import {EditProductComponent} from './edit-product/edit-product.component';


const routes: Routes = [{
    path: '',
    component: AcademyShopComponent,
    children: [
        {
            path: 'add-product',
            component: AddProductComponent,
        },
        {
            path: 'orders',
            component: ShopOrdersComponent,
        },
        {
            path: 'orders/:orderId',
            component: ShopOrderDetailComponent,
        },
        {
            path: 'products',
            component: ProductsComponent,
        },
        {
            path: 'products/:productId',
            component: EditProductComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AcademyShopRouting {
}
