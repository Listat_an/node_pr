import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UiBackBtnModule} from '../../../../components/ui/ui-back-btn/ui-back-btn.module';
import {AcademyShopComponent} from './academy-shop.component';
import {PagesModule} from '../pages.module';
import {AcademyComponentsModule} from '../components/academy/academy.module';
import {AcademyShopRouting} from './academy-shop.routing';
import {AddProductComponent} from './add-product/add-product.component';
import {AddImgBtnModule} from '../components/add-img-btn/add-img-btn.module';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import { ProductsComponent } from './products/products.component';
import { ShopOrdersComponent } from './orders/orders.component';
import {UiCheckbox2Module} from '../../../../components/ui/ui-checkbox-2/ui-checkbox-2.module';
import {UiPerPageModule} from '../../../../components/ui/ui-per-page/ui-per-page.module';
import {NgDatepickerModule} from 'ng2-datepicker';
import {AngularMultiSelectModule} from '../../../../components/angular2-multiselect-dropdown';
import {ShopOrderDetailComponent} from './orders/order-detail/order-detail.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ModalConfirmModule } from '../components/academy/modal-confirm/modal-confirm.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PagesModule,
        AcademyComponentsModule,
        AcademyShopRouting,
        UiBackBtnModule,
        UiPerPageModule,
        UiCheckbox2Module,
        AddImgBtnModule,
        CurrencyMaskModule,
        NgDatepickerModule,
        AngularMultiSelectModule,
        ModalConfirmModule,
    ],
    declarations: [
        AcademyShopComponent,
        AddProductComponent,
        ProductsComponent,
        ShopOrdersComponent,
        ShopOrderDetailComponent,
        EditProductComponent,
    ],
})
export class AcademyShopModule {
}
