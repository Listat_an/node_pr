import {Location} from '@angular/common';
import {Component, OnInit, ViewChild} from '@angular/core';
import {ShopService} from '../../../../services/shop.service';
import {ConfigService} from '../../../../services/service.config';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';

import { ModalConfirmComponent } from '../../components/academy/modal-confirm/modal-confirm.component';

@Component({
    selector: 'opn-add-product',
    templateUrl: './edit-product.component.html',
    styleUrls: ['./edit-product.component.scss'],
    providers: [
        ShopService,
    ],
})
export class EditProductComponent implements OnInit {
    productId;
    createdProductId;

    mainImage;
    images = ['', '', '', '', '', ''];
    productData: any = {};
    baseUrl = ConfigService.URL_SERVER;

    productSaved: Boolean = false;
    private load: Boolean = false;

    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;
    public confirmAction: String;

    constructor(private _location: Location,
                private shopService: ShopService,
                private route: ActivatedRoute,
                private router: Router,
                private toastr: ToastrService,
            ) {
        this.route.params.subscribe(params => {
            this.productId = params['productId'];
        });
    }

    ngOnInit() {
        this.getProduct();
    }

    getProduct() {
        this.shopService.getProductById(this.productId).subscribe(productData => {
            this.productData = _.omit(productData, ['created_at', 'updated_at', 'images', 'id']);
            this.parseImages(productData.images);
            // this.mainImage = productData.images[0].url;
            //this.images = _.assign(this.images, _.slice(productData.images, 0, 1));
            console.log('images: ', this.images);
        });
    }

    parseImages(images) {
        _.forIn(images, (v, k: any) => {
            if (k == 0) {
                this.convertToDataURLviaCanvas(this.baseUrl + v.url, 'image/png')
                .then( base64Img => {
                    this.mainImage = base64Img;
                });
            }else {
                this.convertToDataURLviaCanvas(this.baseUrl + v.url, 'image/png')
                .then( (base64Img: any) => {
                    this.images[ k - 1 ] = base64Img;
                });
            }
        });
    }

    onMainImageAdded(e) {
        this.mainImage = e;
    }

    onImageAdded(e, i) {
        this.images[i] = e;
    }

    convertToDataURLviaCanvas(url, outputFormat) {
        return new Promise( (resolve, reject) => {
            const img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function(){
                let canvas = <HTMLCanvasElement> document.createElement('CANVAS'),
                ctx = canvas.getContext('2d'),
                dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                canvas = null;
                resolve(dataURL);
            };
            img.src = url;
        });
    }

    onChangeShipping(event: Boolean) {
        this.productData.shipping_type = (!event) ? 'free' : 'costing';
        this.productData.shipping_price = 0;
    }

    cancelCreation() {
        this._location.back();
    }

    saveProduct() {
        let images = this.images;
        let filteredImages = [];
        if (this.mainImage) {
            images = [this.mainImage, ...this.images];
        }
        filteredImages = images.filter(v => (v != '' && v.trim().length > 0));

        const productInfo = {
            ...this.productData,
            images: filteredImages,
        };
        delete productInfo.shipping_type;
        delete productInfo.academy;
        this.load = true;
        this.shopService.updateProduct(this.productId, productInfo).subscribe(res => {
            this.createdProductId = res['id'];
            this.toastr.info('The product updated!');
            this.load = false;
        });
    }

    onChangeVissbility(e) {
        if (e.target.checked) {
            this.productData.show = 0;
        }else {
            this.productData.show = 1;
        }
    }

    removeProduct() {
        this.confirmAction = 'remove';
        this.modalConfirm.modalTitle = 'Delete your Product?';
        this.modalConfirm.modalDescription = 'This action will remove your product.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
    }

    onConfirm(confirm) {
        if (confirm) {
            switch (this.confirmAction) {
                case 'remove': {
                    this.shopService.deleteProductById(this.productId).subscribe(res => {
                        if (res.status) {
                            this._location.back();
                        }
                    });
                    break;
                }

            }
        }
    }
}
