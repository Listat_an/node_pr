import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'opn-academy-shop',
    templateUrl: './academy-shop.component.html',
    styleUrls: ['./academy-shop.component.scss', '../@theme/scss/theme.scss'],
})
export class AcademyShopComponent implements OnInit {
    isManager: Boolean = false;
    academyId;

    constructor(private activeRoute: ActivatedRoute,) {
        this.activeRoute.params.subscribe(params => {
            this.academyId = params['id'];
        });
    }

    ngOnInit() {
    }

    onCheckManager(status) {
        this.isManager = status;
    }
}
