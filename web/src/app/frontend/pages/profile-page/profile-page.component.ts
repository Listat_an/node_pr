import {
    Component,
    OnInit,
    OnDestroy,
    ElementRef,
    ViewChild,
} from '@angular/core';
import {
    Title,
} from '@angular/platform-browser';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    AbstractControl,
} from '@angular/forms';
import {
    Router,
    ActivatedRoute,
    NavigationEnd,
} from '@angular/router';
import {
    Select2TemplateFunction,
    Select2OptionData,
} from 'ng2-select2';
import {
    Observable,
} from 'rxjs/Observable';

import {
    PostService,
} from '../../../services/posts.service';
import {
    ProfileService,
} from '../../../services/profile.service';
import {
    cancelSubscription,
} from '../../../providers/cancelSubscription';
import {
    Subscription,
} from 'rxjs/Subscription';
import {
    DataService,
} from '../../../services/data.service';
import {
    ModalConfirmComponent,
} from '../components/academy/modal-confirm/modal-confirm.component';

import * as dfn from 'date-fns';

@Component({
    selector: 'opn-profile-page',
    templateUrl: './profile-page.component.html',
    styleUrls: ['./profile-page.component.scss', '../@theme/scss/theme.scss'],
    providers: [ProfileService],
})
export class ProfilePageComponent implements OnInit, OnDestroy {
    title = 'Profile';

    public subscrips: Subscription[] = [];

    private userId: Number;
    private posts: Array < any > = [];
    private limit: any = 3;
    private page: any = 1;
    private maxPage: Number;
    private total: Number;

    private me: Number;
    private userProfile: any = [];
    private userAvatar: any;
    private academyName: String = '';
    private belt: String = '';

    private profileTab: String = 'posts';
    private isFolow: Boolean = false;

    private academyInfo: String = '';
    private validErrAcademy: Boolean = false;
    private addingAcademy: Boolean = false;
    private infoAcademies: Array < any > = [];

    private addingPrizes: Boolean = false;
    public formPrizes: FormGroup;
    private infoPrizes: Array < any > = [];
    private prizesText: String = '';

    private popupPost: any = {};
    private showModal: Boolean = false;
    private modalIndex: Number = 0;

    private coverBg: Object = {};
    private cropper: any;
    private serverUrl: String;
    private viewProfile: Boolean = true;

    public yearsF: Array < Select2OptionData > ;
    public optionsYearsF: Select2Options;
    public yearsT: Array < Select2OptionData > ;
    public optionsYearsT: Select2Options;
    private academiesInfo: any = {
        from: Observable.create(obs => {
            obs.next('0');
            obs.complete();
        }),
        to: Observable.create(obs => {
            obs.next('0');
            obs.complete();
        }),
        action: 'add',
    };
    private currentAcademy: any;
    private load: Boolean = true;

    private profileFound: Boolean = true;
    private profileload: Boolean = false;

    private viewUser: any = {};
    private tmpData: any = {
        academy: {},
        prizes: {},
    };

    @ViewChild('cropCover', {
        read: ElementRef,
    }) cropCoverRef: ElementRef;
    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;

    private academyToDate;
    private academyFromDate;

    constructor(
        private formBuilder: FormBuilder,
        private titleService: Title,
        private postService: PostService,
        private profileService: ProfileService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private dataServices: DataService,
    ) {
        this.serverUrl = this.profileService.getServerUrl();

        this.titleService.setTitle('Profile');
        this.me = this.profileService.getMe().id;
        this.activeRoute.params.subscribe(params => {
            this.userId = params['id'];
        });

        // this.router.routeReuseStrategy.shouldReuseRoute = function () {
        //     return false;
        // };
        // this.router.events.subscribe((evt) => {
        //     if (evt instanceof NavigationEnd) {
        //         this.router.navigated = false;
        //         window.scrollTo(0, 0);
        //     }
        // });
    }

    ngOnInit() {
        this.cropCoverRef.nativeElement.style.display = 'none';
        this.load = false;
        this.subscrips.push(
            this.postService.getUser().subscribe(res => {
                this.viewUser = res;
                this.subscrips.push(
                    this.postService.getProfile(this.userId)
                    .finally(() => {
                        this.profileload = true;
                    })
                    .subscribe(profile => {
                            this.userProfile = profile;
                            this.userAvatar = profile.avatar;
                            this.academyName = profile.academy ? profile.academy.name : '';
                            this.belt = profile.belt.sourse_round;

                            if (profile.cover_photo) {
                                this.coverBg = {
                                    background: `url(${this.serverUrl}${profile.cover_photo}) center/cover no-repeat`,
                                };
                            } else {
                                this.coverBg = {
                                    background: `url(/assets/images/profile_cover.jpg) center/cover no-repeat`,
                                };
                            }
                            if (this.viewUser.id != this.userProfile.id && this.userProfile.status_public == 0) {
                                this.viewProfile = false;
                            }
                        },
                        err => {
                            this.profileFound = false;
                        }),
                );
            }),
            this.postService.getPostsById(this.userId, this.limit, this.page).subscribe(res => {
                this.total = res.total;
                this.maxPage = Math.ceil(res.total / this.limit);
                this.posts = this.posts.concat(res.data);
                this.load = true;
            }),
            this.profileService.getAademiesInfo(this.userId).subscribe(res => {
                this.infoAcademies = res.data;
            }),
            this.profileService.getPrizesInfo(this.userId).subscribe(res => {
                this.infoPrizes = res.data;
            }),
        );

        this.formPrizes = this.formBuilder.group({
            text: [null, [Validators.required]],
        });
        this.subscrips.push(
            this.profileService.isFollower(this.userId).subscribe(res => {
                this.isFolow = res.status;
            }),
        );

        let endY = dfn.getYear(new Date());
        let startY = dfn.getYear(dfn.subYears(new Date(), 90));
        this.generateFromYears(startY, endY);
        this.generateToYears(startY, endY);

        this.optionsYearsF = {
            minimumResultsForSearch: 1,
            dropdownCssClass: 'academy-dropdown',
            placeholder: {
                id: '0', // the value of the option
                text: 'From',
            },
        };
        this.optionsYearsT = {
            minimumResultsForSearch: 1,
            dropdownCssClass: 'academy-dropdown',
            placeholder: {
                id: '0', // the value of the option
                text: 'To',
            },
        };
    }

    generateFromYears(start, end) {
        this.yearsF = Observable.create((obs) => {
            let array = [{
                    id: '0',
                    text: 'From',
                },
            ];

            for (let i = start; i <= end; i++) {
                array.push({
                    id: `${i}`,
                    text: `${i}`,
                });
            }
            obs.next(array);
            obs.complete();
        });
    }

    generateToYears(start, end) {
        this.yearsT = Observable.create((obs) => {
            let array = [{
                    id: '0',
                    text: 'From',
                },
                {
                    id: 'Present',
                    text: 'Present',
                },
            ];

            for (let i = start; i <= end; i++) {
                array.push({
                    id: `${i}`,
                    text: `${i}`,
                });
                if (i == end) {
                    obs.next(array);
                    obs.complete();
                }
            }
        });
    }


    changeFromYear(year) {
        this.academyFromDate = year.value;
        let endY = dfn.getYear(new Date());
        let startY = dfn.getYear(new Date(`${year.value}-01-01`));
        this.generateToYears(startY, endY);
    }

    changeToYear(year) {
        this.academyToDate = year.value;
    }

    onScrollDown() {
        if (this.page <= this.maxPage) {
            this.load = false;
            this.page += 1;
            this.subscrips.push(
                this.postService.getPostsById(this.userId, this.limit, this.page).subscribe(res => {
                    this.posts = this.posts.concat(res.data);
                    this.load = true;
                }),
            );
        }
    }
    onPostAdded(postData) {
        if (postData) {
            const post = postData;
            post.user = this.userProfile;
            this.posts.unshift(post);
        }
    }

    onRemovePost(id) {
        document.getElementById('post_' + id).style.display = 'none';
    }

    onChangeTab(event, tab: String) {
        this.profileTab = tab;
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({
                    onlySelf: true,
                });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    onConfirm(confirm) {
        if (confirm) {
            if (this.tmpData.academy.action == 'remove') {
                this.subscrips.push(
                    this.profileService.removeAademiesInfo(this.tmpData.academy.id).subscribe(res => {
                        this.infoAcademies = this.infoAcademies.filter(Iinfo => Iinfo.id !== this.tmpData.academy.id);
                        this.tmpData.academy = {};
                    }),
                );
            }
            if (this.tmpData.prizes.action == 'remove') {
                this.subscrips.push(
                    this.profileService.removePrizesInfo(this.tmpData.prizes.id).subscribe(res => {
                        this.infoPrizes = this.infoPrizes.filter(IPrize => IPrize.id !== this.tmpData.prizes.id);
                    }),
                );
            }
        }
    }

    removeAcademyInfo(info) {
        this.tmpData.academy = {
            id: info.id,
            action: 'remove',
        };
        this.modalConfirm.modalTitle = 'Delete your Academy?';
        this.modalConfirm.modalDescription = 'This action will remove your academy.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
    }

    removeInfoPrize(prize) {
        this.tmpData.prizes = {
            id: prize.id,
            action: 'remove',
        };
        this.modalConfirm.modalTitle = 'Delete your Prize?';
        this.modalConfirm.modalDescription = 'This action will remove your prize.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
    }

    i = 0;

    public onEventMessage() {
        if (!this.viewProfile) {
            return false;
        }
        this.dataServices.eventMessage.emit({
            type: 'user',
            user_id: this.userId,
        });
    }

    private hideAcademyInfoForm() {
        this.addingAcademy = false;
        this.academiesInfo = {
            from: 0,
            to: 0,
        };
        this.academyInfo = '';
        this.validErrAcademy = false;
    }

    private hidePrizeInfoForm() {
        this.addingPrizes = false;
        this.formPrizes.reset();
        this.prizesText = '';
    }

    onNewAcademyInfo(event) {
        this.currentAcademy = {
            text: ''
        };
        this.academiesInfo.action = 'create';
        this.addingAcademy = true;
    }

    editAcademyInfo(event, i) {
        this.currentAcademy = {
            text: event.name,
        };

        this.academyFromDate = event.begin_date;
        this.academyToDate = event.end_date;

        let endY = dfn.getYear(new Date());
        let startY = dfn.getYear(dfn.subYears(new Date(), 90));
        this.generateFromYears(startY, endY);
        this.generateToYears(event.begin_date, endY);

        this.academiesInfo = {
            from: Observable.create(obs => {
                obs.next(event.begin_date);
                obs.complete();
            }),
            to: Observable.create(obs => {
                obs.next(event.end_date);
                obs.complete();
            }),
            action: 'save',
        };
        this.tmpData.academy = {
            id: event.id,
            name: event.name,
            index: i,
        };
        this.addingAcademy = true;
        this.validErrAcademy = false;
    }

    editInfoPrize(event, i) {
        this.formPrizes.controls['text'].setValue(event.content);
        this.tmpData.prizes = {
            id: event.id,
            action: 'save',
            index: i,
        };
        this.addingPrizes = true;
    }

    setAcademy(event) {
        this.academyInfo = event.text;
    }

    onResetAcademy() {
        this.hideAcademyInfoForm();
    }

    onNewPrizesInfo() {
        this.tmpData.prizes.action = 'create';
        this.addingPrizes = true;
    }

    onResetPrizes() {
        this.hidePrizeInfoForm();
    }

    onCreateAcademyInfo() {
        if (this.academyFromDate != 0 && this.academyToDate != 0 && !this.validErrAcademy) {
            this.subscrips.push(
                this.profileService.saveAademyInfo({
                    name: this.academyInfo,
                    begin_date: this.academyFromDate,
                    end_date: this.academyToDate,
                }).subscribe(res => {
                    if (res.status) {
                        this.infoAcademies.unshift(res.data);
                        this.hideAcademyInfoForm();
                    }
                }),
            );
        }
        if (this.academyInfo == '') {
            this.validErrAcademy = true;
        } else {
            this.validErrAcademy = false;
        }
    }

    onEditAcademyInfo() {
        console.log('onEditAcademyInfo ');
        if (this.academyFromDate != 0 && this.academyToDate != 0 && !this.validErrAcademy) {
            this.subscrips.push(
                this.profileService.editAademyInfo({
                    name: this.academyInfo || this.tmpData.academy.name,
                    begin_date: this.academyFromDate,
                    end_date: this.academyToDate,
                }, this.tmpData.academy.id).subscribe(res => {
                    if (res.status) {
                        this.infoAcademies[this.tmpData.academy.index] = res.data;
                        this.hideAcademyInfoForm();
                        this.tmpData.academy = {};
                    }
                }),
            );
        }
        if (this.academyInfo == '') {
            this.validErrAcademy = true;
        } else {
            this.validErrAcademy = false;
        }
    }

    onSubmitAcademy() {
        switch (this.academiesInfo.action) {
            case 'create':
                {
                    this.onCreateAcademyInfo();
                    break;
                }
            case 'save':
                {
                    this.onEditAcademyInfo();
                    break;
                }
        }
    }

    onCreatePrizes() {
        if (this.formPrizes.valid) {
            this.subscrips.push(
                this.profileService.savePrizesInfo({
                    content: this.formPrizes.get('text').value,
                }).subscribe(res => {
                    if (res.status) {
                        this.infoPrizes.unshift(res.data);
                        this.hidePrizeInfoForm();
                    }
                }),
            );
        }
    }

    onEditPrizes() {
        if (this.formPrizes.valid) {
            this.subscrips.push(
                this.profileService.editPrizesInfo({
                    content: this.formPrizes.get('text').value,
                }, this.tmpData.prizes.id).subscribe(res => {
                    if (res.status) {
                        this.infoPrizes[this.tmpData.prizes.index] = res.data;
                        this.hidePrizeInfoForm();
                        this.tmpData.prizes = {};
                    }
                }),
            );
        }
    }

    onSubmitPrizes() {
        switch (this.tmpData.prizes.action) {
            case 'create':
                {
                    this.onCreatePrizes();
                    break;
                }
            case 'edit':
                {
                    this.onEditPrizes();
                    break;
                }
        }

    }

    onFollowUser(id) {
        this.subscrips.push(
            this.profileService.addFollowingMe({
                friend_id: id,
            }).subscribe(res => {
                this.userProfile.count_followers += 1;
                this.isFolow = !this.isFolow;
            }),
        );
    }

    onFollowRemove(id) {
        this.subscrips.push(
            this.profileService.removeFollowingMe({
                friend_id: id,
            }).subscribe(res => {
                this.userProfile.count_followers -= 1;
                this.isFolow = !this.isFolow;
            }),
        );
    }

    onOpenModal(obj) {
        this.popupPost = obj.post;
        this.showModal = true;
        this.modalIndex = obj.fileIndex;
    }

    onCloseModal() {
        this.popupPost = [];
        this.showModal = false;
        this.modalIndex = 0;
    }

    onChangeCover(data) {
        if (data.cover_photo) {
            this.coverBg = {
                background: `url(${this.serverUrl}${data.cover_photo}?${Date.now()}) center no-repeat`,
                'background-size': 'cover',
            };
        }
        this.cropCoverRef.nativeElement.style.display = 'none';
    }

    onCloseCoverModal() {
        this.cropCoverRef.nativeElement.style.display = 'none';
    }

    onEditCoverPhoto($event) {
        const image: any = new Image();
        const file: File = $event.target.files[0];
        const myReader: FileReader = new FileReader();
        const self = this;
        myReader.onloadend = function (loadEvent: any) {
            image.src = loadEvent.target.result;
            self.cropper = image;
            self.cropCoverRef.nativeElement.style.display = 'block';
        };

        myReader.readAsDataURL(file);
    }

    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }

    toAcademy(academy) {
        this.router.navigate([`/academiy-datail/${academy}`]);
    }
    onRemovePostFiles(postId) {
        this.posts = this.posts.filter(IPost => IPost.id != postId);
    }
}
