import { PostService } from './../../../services/posts.service';
import {
    Component,
    OnInit,
    ElementRef,
    ViewChild,
    OnDestroy,
} from '@angular/core';
import {
    Title,
} from '@angular/platform-browser';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    AbstractControl,
} from '@angular/forms';
import {
    Router,
    ActivatedRoute,
    NavigationEnd,
} from '@angular/router';
import {
    ActivitiesService,
} from '../../../services/activities.service';
import {
    PaymentStripeComponent,
} from '../../../../components/payment-stripe/payment-stripe.component';

import {
    mapValues,
    find,
    intersection,
    findIndex,
    take,
    reverse,
} from 'lodash';

import {
    differenceInMinutes,
} from 'date-fns';

import {
    ModalService,
} from '../components/modal/modal.service';
import {
    filter,
} from '../../../../../node_modules/rxjs/operator/filter';

import {
    ModalConfirmComponent,
} from '../components/academy/modal-confirm/modal-confirm.component';

import {
    cancelSubscription,
} from '../../../providers/cancelSubscription';
import {
    Subscription,
} from 'rxjs/Subscription';

export interface ListData {
    name ? : String;
    amount ? : Number;
}

@Component({
    selector: 'opn-activity-view',
    templateUrl: './activity-view.component.html',
    styleUrls: ['./activity-view.component.scss', '../@theme/scss/theme.scss'],
    providers: [ActivitiesService, PostService],
})
export class ActivityViewPageComponent implements OnInit, OnDestroy {

    public subscrips: Subscription[] = [];
    private coverBg: Object = {};
    private cropper: any;
    private serverUrl: String;

    private activity: any = {};
    private userAvatar: any;
    private commentText: String = '';
    private activityId: Number;

    private types: Object = {
        'class': 'Class',
        'seminar': 'Seminar',
        'mat_event': 'Opn Mat',
        'others': 'Others',
    };
    private comments: any;
    private instructors: any = '';
    private address: any = '';
    private techniques: any = '';
    private differenceDate = 0;
    private userSabscriptions: Array < Number > = [];
    private activitySabscriptions: Array < Number > = [];
    private isMember: any;
    private user: any;
    private join_activities: any;
    private all_joined: any = [];
    private maxJoinAvatar: any = 5;
    private rightCount = 0;
    private joinedCount = 0;
    private eventLoaded = false;
    private subscribeLoaded = false;
    private joinLoader = false;
    private unjoinLoader = false;
    private joinId: any = null;

    private modalAlert = '';
    private userView;

    private addComment: Boolean = true;
    private totalData: Array < ListData > ;

    public tabSelect = 'timline';

    @ViewChild('cropCover', {
        read: ElementRef,
    }) cropCoverRef: ElementRef;
    @ViewChild(PaymentStripeComponent)
    public paymentStripe: PaymentStripeComponent;
    @ViewChild(ModalConfirmComponent)
    public modalConfirm: ModalConfirmComponent;
    public confirmAction: String;

    private posts: Array < any > = [];
    private limit: any = 3;
    private page: any = 1;
    private maxPage: Number;
    private total: Number;
    private load: Boolean = true;

    constructor(
        private titleService: Title,
        private activityService: ActivitiesService,
        private activeRoute: ActivatedRoute,
        private router: Router,
        private modalSrv: ModalService,
        private postService: PostService,
    ) {
        this.serverUrl = this.activityService.getServerUrl();
        this.activityService.getMe().subscribe(user => {
            this.user = user;
            this.userView = user;
        });
    }
    ngOnInit() {
        this.cropCoverRef.nativeElement.style.display = 'none';
        this.activityService.getMe().subscribe(user => {
            this.user = user;
            this.userView = user;
            this.activeRoute.params.subscribe(param => {
                this.activityId = param.id;
                this.getActivity(param.id);
                this.subscrips.push(
                    this.postService.getActivityPosts(this.activityId, this.limit, this.page).subscribe(res => {
                        this.total = res.total;
                        this.maxPage = Math.ceil(res.total / this.limit);
                        this.posts = this.posts.concat(res.data);
                        this.load = true;
                    }),
                );
            });
        });
        
        this.activityService.getUserMe().subscribe(user => {
            this.userAvatar = user.avatar;
        });
        this.activityService.getComments(this.activityId).subscribe(comments => {
            this.comments = comments.data;
        });
    }

    onScrollDown() {
        if (this.page <= this.maxPage) {
            this.load = false;
            this.page += 1;
            this.subscrips.push(
                this.postService.getActivityPosts(this.activityId, this.limit, this.page).subscribe(res => {
                    this.posts = this.posts.concat(res.data);
                    this.load = true;
                }),
            );
        }
    }

    onPostAdded(postData) {
        if (postData) {
            const post = postData;
            post.user = this.user;
            this.posts.unshift(post);
        }
    }

    getActivity(activity_id) {
        this.activityService.getActivityById(activity_id).subscribe(activity => {
            console.log('activity ', activity);
            if (activity.cover_photo) {
                this.coverBg = {
                    background: `url(${this.serverUrl}${activity.cover_photo}) center no-repeat`,
                    'background-size': 'cover',
                };
            } else {
                this.coverBg = {
                    background: `url(/assets/images/activity-bg.jpg) center no-repeat`,
                    'background-size': 'cover',
                };
            }
            this.address = activity.academy.location;
            this.titleService.setTitle(activity.name);
            this.activity = activity;
            this.differenceDate = differenceInMinutes(new Date(activity.end_date), new Date());
            if (activity.join_activities) {
                this.parseJoinId(activity.join_activities);
                this.isMember = findIndex(activity.join_activities, {
                    user_id: this.activityService.userId(),
                });
                this.all_joined = activity.join_activities;
                this.joinedCount = activity.join_activities.length;
                if (activity.join_activities.length > this.maxJoinAvatar) {
                    this.rightCount = activity.join_activities.length - this.maxJoinAvatar;
                }
                this.join_activities = take(reverse(activity.join_activities), this.maxJoinAvatar);
            }
            this.eventLoaded = true;
        });
    }

    parseJoinId(join_activities) {
        if (join_activities.length > 0) {
            let join = find(join_activities, {
                user_id: this.userView.id,
            });
            this.joinId = (join) ? join.id : -1;
        }
    }

    onUnJoin(activity) {
        this.modalConfirm.modalTitle = 'Unjoin your activity?';
        this.modalConfirm.modalDescription = 'This action will unjoined your activity.';
        this.modalConfirm.btnsTitle = {
            accept: 'Yes',
            declide: 'no',
        };
        this.modalConfirm.open();
        this.confirmAction = 'unJoin';
    }

    onConfirm(confirm) {
        if (confirm) {
            switch (this.confirmAction) {
                case 'unJoin':
                    {
                        if (this.joinId >= 0) {
                            this.unjoinLoader = true;
                            this.activityService.unJoinActivity(this.joinId).subscribe(res => {
                                if (res.status) {
                                    this.getActivity(this.activity.id);
                                }
                                this.unjoinLoader = false;
                            });
                        }
                        break;
                    }

            }
        }
    }

    onSaveComment(event) {
        if (this.commentText !== '') {
            this.activityService.addComment({
                activity_id: this.activityId,
                content: this.commentText,
            }).subscribe(res => {
                if (res.status) {
                    this.commentText = '';
                    this.comments.unshift(res.comment);
                    this.addComment = true;
                }
            });
        }
    }

    onRemoveComment(modalConfirm) {
        modalConfirm.style.display = 'block';
    }
    onConfirmComment(comment, confirm, modal) {
        if (confirm) {
            this.activityService.removeComment(comment.id).subscribe(res => {
                if (res.status) {
                    this.comments = this.comments.filter(iComment => iComment !== comment);
                }
            });
        } else {
            modal.style.display = 'none';
        }
    }

    onJoin(activity) {
        this.joinLoader = true;
        if (activity.payment_status == 'free') {
            this.activityService.joinActivity({
                activity_id: activity.id,
            }).subscribe(res => {
                    if (res.status) {
                        this.join_activities.push({
                            user: this.user,
                        });
                        this.isMember = 1;
                        this.joinedCount += 1;
                        this.getActivity(activity.id);
                    }
                    this.joinLoader = false;
                },
                err => {
                    this.modalAlert = err.description;
                    this.modalSrv.open('join-fail');
                    this.joinLoader = false;
                });
        } else if (activity.payment_status == 'fee') {
            this.totalData = [].concat({
                name: 'Price',
                amount: this.activity.price_all,
            });
            this.paymentStripe.showPayment();
            this.joinLoader = false;
        }
    }
    toProfile(user_id) {
        this.router.navigate(['profile', user_id]);
    }
    onRePay() {
        this.totalData = [].concat({
            name: 'Price',
            amount: this.activity.price_all,
        });
        this.paymentStripe.showPayment();
        this.modalSrv.close('payment-fail');
    }
    onPay(token) {
        this.joinLoader = true;
        this.activityService.payActivity({
                activity_id: this.activityId,
                source: token.id,
                type: token.type,
            })
            .finally(() => {
                this.joinLoader = false;
            })
            .subscribe(
                res => {
                    if (res.status) {
                        this.join_activities.push({
                            user: this.user,
                        });
                        this.isMember = 1;
                        this.joinedCount += 1;
                        this.modalAlert = `The payment for "${this.activity.name}" was successful`;
                        this.modalSrv.open('payment-success');
                        this.getActivity(this.activityId);
                    }
                },
                err => {
                    if (err.status == 400) {
                        this.modalAlert = err.description;
                        this.modalSrv.open('payment-fail');
                    }
                });
    }
    toAcademy(academy) {
        this.router.navigate([`/academiy-datail/${academy}`]);
    }
    onChangeCover(data) {
        if (data.cover_photo) {
            this.coverBg = {
                background: `url(${this.serverUrl}${data.cover_photo}?${Date.now()}) center no-repeat`,
                'background-size': 'cover',
            };
        }
        this.cropCoverRef.nativeElement.style.display = 'none';
    }

    onCloseCoverModal() {
        this.cropCoverRef.nativeElement.style.display = 'none';
    }

    onEditCoverPhoto($event) {
        const image: any = new Image();
        const file: File = $event.target.files[0];
        const myReader: FileReader = new FileReader();
        const self = this;
        myReader.onloadend = function (loadEvent: any) {
            image.src = loadEvent.target.result;
            self.cropper = image;
            self.cropCoverRef.nativeElement.style.display = 'block';
        };

        myReader.readAsDataURL(file);
        $event.target.value = '';
    }
    onRemovePost(id) {
        document.getElementById('post_' + id).style.display = 'none';
    }
    ngOnDestroy() {
        cancelSubscription(this.subscrips);
    }
}
