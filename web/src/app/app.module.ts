/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {APP_BASE_HREF} from '@angular/common';
import {BrowserModule, Title} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule, ErrorHandler } from '@angular/core';
import {HttpModule} from '@angular/http';
// import {ToastModule, ToastsManager, ToastOptions} from 'ng2-toastr/ng2-toastr';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {AuthService} from './services/auth/auth.service';
import {AuthGuardService as AuthGuard} from './services/auth/auth-guard.service';
import {RoleGuardService as RoleGuard} from './services/auth/role-guard.service';
import {AcademiesService} from './services/academies.service';
import {AcademyGuardService as AcademyGuard} from './services/auth/academy-guard.service';
import {AcademyProService as AcademyPro} from './services/auth/academy-pro.service';
import {JwtModule, JWT_OPTIONS, JwtHelperService} from '@auth0/angular-jwt';
import {DataService} from './services/data.service';
import {ConfigService} from './services/service.config';
// import {CustomToastOptions} from './providers/toast-config.provider';
import { ToastrModule } from 'ngx-toastr';
import { SocketService } from './services/socket.service';
import * as Raven from 'raven-js';

Raven
  .config('https://cf2d8579178746f8845a968d8840ea47@sentry.io/1359943')
  .install();

export class RavenErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    Raven.captureException(err);
  }
}


export function jwtOptionsFactory() {
    return {
        tokenGetter: () => {
            return localStorage.getItem('token');
        },
    };
}

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        AppRoutingModule,
        ToastrModule.forRoot({
            preventDuplicates: true,
        }),
        JwtModule.forRoot({
            jwtOptionsProvider: {
                provide: JWT_OPTIONS,
                useFactory: jwtOptionsFactory,
            },
        }),
    ],
    bootstrap: [AppComponent],
    providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
        AuthGuard,
        RoleGuard,
        AuthService,
        AcademyPro,
        AcademyGuard,
        AcademiesService,
        JwtHelperService,
        Title,
        // ToastsManager,
        // {provide: ToastOptions, useClass: CustomToastOptions},
        DataService,
        ConfigService,
        SocketService,
        { provide: ErrorHandler, useClass: RavenErrorHandler },
    ],
})
export class AppModule {
}
