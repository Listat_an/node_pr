import { Directive, Input, OnInit, ElementRef, HostListener} from '@angular/core';

@Directive({
   selector: '[opnActiveRoute]',
})



export class OpnActiveRouteDirective implements OnInit {
  @Input() routerLink: any;
  @Input() opnActiveRoute: any;
  @Input() step?: any;
  @Input() box?: any;

  @HostListener('click', ['$event'])
  onClick() {
    
  }

  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.checkRoute();
  }

  private checkRoute() {
    const path = document.location.hash;

    if (this.routerLink === '/' && document.location.hash === '#/') {
      this.el.nativeElement.classList.add('active-link');
    }
    console.log('path ', path);
    if (this.routerLink !== '/' && path.lastIndexOf((this.routerLink) ? this.routerLink : this.opnActiveRoute) >= 0 ) {
      const _step = path.split('/');
      if (_step.length == this.step) {
        this.el.nativeElement.classList.add('active-link');
      }
    }
  }
}
