import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'opn-ui-slider',
  templateUrl: './ui-slider.component.html',
  styleUrls: ['./ui-slider.component.scss'],
})
export class UiSliderComponent implements OnInit {

  @Input() check: Boolean = false;

  @Output()
  changeCheckbox: EventEmitter < Boolean > = new EventEmitter < Boolean > ();


  constructor() {}

  ngOnInit() {
  }
  onChange(event) {
    this.changeCheckbox.emit(this.check);
  }
}
