import {
    NgModule,
} from '@angular/core';
import {
    CommonModule,
} from '@angular/common';
import {
    FormsModule,
} from '@angular/forms';
import {
    UiTimeComponent,
} from './ui-time.component';


@NgModule({
    entryComponents: [
        UiTimeComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
    ],
    declarations: [
        UiTimeComponent,
    ],
    exports: [
        UiTimeComponent,
    ],
})
export class UiTimeModule {

}
