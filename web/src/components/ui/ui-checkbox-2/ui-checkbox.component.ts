import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  SimpleChanges,
  SimpleChange,
} from '@angular/core';

import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  AbstractControl,
} from '@angular/forms';

@Component({
  selector: 'opn-ui-checkbox-2',
  templateUrl: './ui-checkbox.component.html',
  styleUrls: ['./ui-checkbox.component.scss'],
})
export class UiCheckbox2Component implements OnInit, OnChanges {

  @Input() check: Boolean = false;
  @Input() value: any;

  @Output()
  changeCheckbox: EventEmitter < Boolean > = new EventEmitter < Boolean > ();


  private id: any;

  constructor() {
    this.id = Date.now();
  }

  ngOnInit() {
  }
  onChange(event) {
    this.changeCheckbox.emit(event);
  }

  ngOnChanges(changes: SimpleChanges) {
    const check: SimpleChange = changes.check;
    this.check = check.currentValue;
  }
}
