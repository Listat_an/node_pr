import {NgModule} from '@angular/core';
import {UiCheckbox2Component} from './ui-checkbox.component';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports:[
        FormsModule
    ],
    declarations: [
        UiCheckbox2Component
    ],
    exports: [
        UiCheckbox2Component
    ],
})
export class UiCheckbox2Module {
}
