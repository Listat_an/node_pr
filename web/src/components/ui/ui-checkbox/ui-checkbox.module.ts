import {NgModule} from '@angular/core';
import {UiCheckboxComponent} from './ui-checkbox.component';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        FormsModule,
    ],
    declarations: [
        UiCheckboxComponent,
    ],
    exports: [
        UiCheckboxComponent,
    ],
})
export class UiCheckboxModule {
}
