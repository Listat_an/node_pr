import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {UiQtyInpComponent} from './ui-qty-inp.component';

@NgModule({
    imports: [
        FormsModule
    ],
    exports: [
        UiQtyInpComponent
    ],
    declarations: [
        UiQtyInpComponent
    ],
})
export class UiQtyInpModule {
}
