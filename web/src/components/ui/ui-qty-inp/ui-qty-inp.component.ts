import {
    Component,
    Input,
    EventEmitter,
    Output,
    OnChanges,
    SimpleChanges,
    SimpleChange,
} from '@angular/core';

@Component({
    selector: 'opn-ui-qty-inp',
    templateUrl: './ui-qty-inp.html',
    styleUrls: ['./ui-qty-inp.scss'],
})
export class UiQtyInpComponent implements OnChanges {
    @Input() quantity: number = 0;
    @Input() minVal: number = 0;
    @Input() errore?: any;

    @Output() onQuantityChange: EventEmitter < number > = new EventEmitter < number > ();

    constructor() {}

    minusVal() {
        if (this.quantity > this.minVal) {
            this.quantity--;
            this.onQuantityChange.emit(this.quantity)
        }
    }

    plusVal() {
        this.quantity += 1;
        this.onQuantityChange.emit(this.quantity)
    }

    emitChange(e) {
        if (e) {
            if (e < this.minVal) this.quantity = this.minVal;
            this.onQuantityChange.emit(this.quantity)
        }
    }
    ngOnChanges(changes: SimpleChanges) {
        const errore: SimpleChange = changes.errore;
        if (!errore.firstChange && errore && errore.currentValue) {
            this.quantity = errore.currentValue.details.quantity;
            this.onQuantityChange.emit(this.quantity);
        }
    }
}
