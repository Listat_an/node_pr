import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpnCalendarHoursComponent } from './opn-calendar-hours.component';

describe('OpnCalendarHoursComponent', () => {
  let component: OpnCalendarHoursComponent;
  let fixture: ComponentFixture<OpnCalendarHoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpnCalendarHoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpnCalendarHoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
