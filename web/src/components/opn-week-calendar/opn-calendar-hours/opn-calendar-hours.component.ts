import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'opn-calendar-hours',
  templateUrl: './opn-calendar-hours.component.html',
  styleUrls: ['./opn-calendar-hours.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class OpnCalendarHoursComponent implements OnInit {
  @Input() public dataSourse: Array<any>;
  @Input() width?: Number = 120;

  public hours: Array<any> = [];

  constructor() { }

  ngOnInit() {
    if (this.dataSourse) {
      this.dataSourse.map(el => {
        this.hours.push({
          v: el,
        });
      });
    }
  }

}
