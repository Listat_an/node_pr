import {
    Component,
    Input,
    Output,
    OnInit,
    OnDestroy,
    AfterViewInit,
    ViewEncapsulation,
    EventEmitter,
    OnChanges,
    SimpleChanges,
    SimpleChange,
    ViewChild,
    ElementRef,
} from '@angular/core';
import { ScheduleService } from './providers/schedule.service';

import {
    PerfectScrollbarDirective,
} from 'ngx-perfect-scrollbar';

import * as helper from './providers/helper';
import * as _ from 'lodash';
import * as _is from 'is_js';
import * as _df from 'date-fns';
import { Subject } from 'rxjs/Subject';

@Component({
    selector: 'opn-week-calendar',
    templateUrl: './opn-week-calendar.component.html',
    styleUrls: ['./opn-week-calendar.component.scss'],
    encapsulation: ViewEncapsulation.Emulated,
})

export class OpnWeekCalendarComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

    @Input() colors: any = {
        seminar: {
            primary: '#fdb44d',
            secondary: 'rgba(253, 180, 77, 0.6)',
        },
        others: {
            primary: '#fd4df7',
            secondary: 'rgba(253, 77, 247, 0.6)',
        },
        mat_event: {
            primary: '#11e66b',
            secondary: 'rgba(17, 230, 107, 0.6)',
        },
    };
    @Input() nowDate?: Date = new Date();
    @Input() hoursRange?: Array < any > = [8, 20];
    @Input() dataSource?: Array < any > = [];
    @Input() isHoliday?: Boolean = false;
    @Input() activityType?: String = 'class';
    @Input() forStudent?: Boolean = false;
    @Output() onWeek: EventEmitter<any> = new EventEmitter<any> ();
    @Output() opnClassClick: EventEmitter<any> = new EventEmitter<any> ();
    @Output() opnHolidayClick: EventEmitter<any> = new EventEmitter<any> ();

    @ViewChild(PerfectScrollbarDirective) directiveRef?: PerfectScrollbarDirective;
    @ViewChild('daysScroll') daysScroll: ElementRef;
    @ViewChild('scrollBox') scrollBox: ElementRef;
    @ViewChild('hoursScroll') hoursScroll: ElementRef;

    ngUnsubscribe: Subject < void > = new Subject < void > ();

    public scrollConfig: any = {
        wheelSpeed: 0.5,
    };

    public toolbarHours: Array<any> = [];
    public toolbarDays: any = [];
    public options: any = {
        celWidth: 120,
        celHeight: 70,
        celPadding: 5,
    };

    private schedule: any = {
        Mon: [],
        Tue: [],
        Wed: [],
        Thu: [],
        Fri: [],
        Sat: [],
        Sun: [],
    };



    public days: Array<any> = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    public dayRows: any = {};
    public viewToolbarDays = false;

    constructor(
        private scheduleSrvc: ScheduleService,
    ) {
        scheduleSrvc.scheduleClick
        .takeUntil(this.ngUnsubscribe)
        .subscribe(resp => {
            if (resp.type == 'class') {
                this.opnClassClick.emit(resp);
            }
            if (resp.type == 'holiday') {
                this.opnHolidayClick.emit(resp);
            }
        });
    }

    ngOnInit() {
        if (this.dataSource &&  _is.not.empty(this.dataSource)) {
            if (this.isHoliday) {
                this.initWeekHoliday();
                this.parseHolidays(this.dataSource);
            }else {
                this.initWeek();
                this.parseShedules(this.dataSource);
            }
        }
    }

    private parseHoursRange(schedule) {
        let s_h: any = _.first(this.hoursRange),
        e_h: any = _.last(this.hoursRange);
        schedule.forEach(_shedule => {
            if (_shedule.time_start < s_h) {
                this.hoursRange[0] = s_h = _.floor(_shedule.time_start);
            }
            if (_shedule.time_end > e_h) {
                this.hoursRange[1] = e_h = (_.ceil(_shedule.time_end) <= 24) ? _.ceil(_shedule.time_end) : 24;
            }
        });

        this.initHours();
    }

    private setDurations() {
        _.map(this.schedule, (days: Array<any>, key) => {
            if (_is.not.empty(days)) {
                this.schedule[key] = days.map((el) => {
                    const [h_s, m_s] = el.time_start.split('.').map( t => {
                        return parseInt(t, 10);
                    });
                    const [h_e, m_e] = el.time_end.split('.').map( t => {
                        return parseInt(t, 10);
                    });

                    let _h, _m;
                    if (h_e > h_s) {
                        _h = h_e - h_s;
                    }else {
                        _h = 0;
                    }

                    if (m_e > m_s) {
                        _m = m_e - m_s;
                    } else if (m_e < m_s) {
                        if (_h > 0 ) {
                            _h--;
                        }
                        if ( m_e == 0) {
                            _m = 60 - m_s;
                        }else {
                            _m = (60 - m_s) + m_e;
                        }
                    }else if (m_e === m_s) {
                        _m = 0;
                    }

                    return {
                        ...el,
                        duration: [_h, _m].join('.'),
                    };
                });
            }
        });

        this.sordDurationDESC();
    }

    private sordDurationDESC() {
        _.map(this.schedule, (days: Array<any>, key) => {
            if (_is.not.empty(days)) {
                this.schedule[key] = _.sortBy(days, [(o) => parseFloat(o.time_start), 'duration']);
            }
        });

        this.createRows();
    }

    private createRows() {
        _.map(this.schedule, (days: Array<any>, key) => {
            if (_is.not.empty(days)) {
                days.forEach((event) => {
                    let row = 0;
                    if (_is.undefined(this.dayRows[key])) {
                        this.dayRows[key] = {
                            [row]: [],
                        };
                        this.dayRows[key][row].push(event);
                    }else {
                        for (let i = 0; i < _.keys(this.dayRows[key]).length; i++) {
                            const t1 = _.toNumber(_.last(this.dayRows[key][i])['time_start']);
                            const t2 = _.toNumber(_.last(this.dayRows[key][i])['time_end']);
                            const t  = _.toNumber(event.time_start);
                            
                            if (t >= t2) {
                                this.dayRows[key][i].push(event);
                                break;
                            }else if (t < t2 && t >= t1 ) {
                                row ++;
                                if (_is.undefined(this.dayRows[key][row])) {
                                    this.dayRows[key][row] = [];
                                    this.dayRows[key][row].push(event);
                                    break;
                                }else {
                                    continue;
                                }
                            }
                        }
                    }
                });
            }else {
                this.dayRows[key] = {
                    [0]: [],
                };
            }
        });

        if (this.isHoliday) {
            console.log('createRows after', this.dayRows);
            this.days = _.keys(this.dayRows);
            this.toolbarDays = helper.getFormatDates(this.dayRows);
            this.viewToolbarDays = true;
        }else {
            this.toolbarDays = helper.getFormatDays(this.toolbarDays, this.dayRows);
            this.viewToolbarDays = true;
        }
    }
    private checkHoliday(schedyle, date_2): Boolean {
        const date_1 = new Date(schedyle.date);
        if (_df.isEqual(date_1, date_2)) {
            return true;
        }
        return false;
    }
    private parseShedules(data) {
        const self = this;
        let schedule: Array<any> = [];
        let _holidayDays: Array<any> = [];
        _.forEach(data, (classes, index) => {
            if (this.activityType == 'class') {
                classes.forEach(_class => {

                    _class.shedules.forEach(_shedule => {
                        if (!this.forStudent && _shedule.holiday == 0) {
                            schedule.push({
                                ..._shedule,
                                name: _class.name,
                                subscriptions: _class.subscriptions,
                                colorBorder: _class.color_border,
                                colorBg: _class.color_bg,
                                meta: {
                                    type: 'class',
                                    role: 'manager',
                                    id: _class.id,
                                },
                            });
                        }

                        if (this.forStudent) {
                            if (_shedule.holiday == 1 && this.checkHoliday(_shedule, new Date(index))) {
                                _holidayDays.push({day: _shedule.day, date: index});
                            }
                            schedule.push({
                                ..._shedule,
                                name: _class.name,
                                subscriptions: _class.subscriptions,
                                colorBorder: _class.color_border,
                                colorBg: _class.color_bg,
                                meta: {
                                    type: 'class',
                                    role: 'student',
                                    id: _class.id,
                                },
                            });
                        }
                    });
                });
            }else if (this.activityType != 'class') {
                const _date = _df.format(new Date(index), 'YYYY-MM-DD');
                classes.forEach(_class => {
                    const start = _df.format(new Date(_class.start_date), 'YYYY-MM-DD');
                    const end = _df.format(new Date(_class.end_date), 'YYYY-MM-DD');
                    const dif = _df.differenceInDays(end, start);
                    if (dif == 0) {
                        schedule.push({
                            class_id: _class.id,
                            day: _df.format(_date, 'ddd'),
                            time_start: _df.format(new Date(_class.start_date), 'H.mm'),
                            time_end: _df.format(new Date(_class.end_date), 'H.mm'),
                            name: _class.name,
                            subscriptions: null,
                            colorBorder: this.colors[`${this.activityType}`].primary,
                            colorBg: this.colors[`${this.activityType}`].secondary,
                            payment: _class.payment_status,
                            price: _class.price_all,
                            meta: {
                                type: this.activityType,
                                role: 'student',
                                id: _class.id,
                            },
                        });
                    }
                    
                    if (dif > 0) {
                        let _time_start, _time_end;
                        const _difS = _df.differenceInDays(_date, start);
                        const _difE = _df.differenceInDays(_date, end);
                        if (Math.abs(_difS) == 0) {
                            _time_start = _df.format(new Date(_class.start_date), 'H.mm');
                        }else if (_difS > 0) {
                            _time_start = '1.00';
                        }

                        if (Math.abs(_difE) == 0) {
                            _time_end = _df.format(new Date(_class.end_date), 'H.mm');
                        }else if (_difE < 0) {
                            _time_end = '24.59';
                        }


                        schedule.push({
                            class_id: _class.id,
                            day: _df.format(_date, 'ddd'),
                            time_start: _time_start,
                            time_end: _time_end,
                            name: _class.name,
                            subscriptions: null,
                            colorBorder: this.colors[`${this.activityType}`].primary,
                            colorBg: this.colors[`${this.activityType}`].secondary,
                            payment: _class.payment_status,
                            price: _class.price_all,
                            meta: {
                                type: this.activityType,
                                role: 'student',
                                id: _class.id,
                            },
                        });
                    }
                });
            }
        });

        // filter by holiday for students

        if (this.forStudent) {
            let _schedule;
            _holidayDays = _.uniqBy(_holidayDays, 'day');
            _holidayDays.forEach(_day => {
                console.log('_day ', _day.day);
                _schedule = _.union(_schedule, schedule.filter(ISchedule => ISchedule.day == _day.day && (ISchedule.holiday == 0 || !this.checkHoliday(ISchedule, new Date(_day.date)))));
            });
            schedule = _.difference(schedule, _schedule);
        }
        this.parseHoursRange(schedule);

        this.schedule = _.assign(this.schedule, _.groupBy(schedule, 'day'));

        this.setDurations();
    }

    private parseHolidays(data) {
        const self = this;
        let holidays: Array<any> = [];
        _.forEach(data, (classes, index) => {
            classes.forEach(_class => {
                _class.shedules.forEach(_shedule => {
                    holidays.push({
                        ..._shedule,
                        name: _class.name,
                        subscriptions: _class.subscriptions,
                        colorBorder: _class.color_border,
                        colorBg: _class.color_bg,
                        meta: {
                            type: 'holiday',
                            role: 'manager',
                            id: _class.id,
                            index: index,
                        },
                    });
                });
            });
        });

        this.parseHoursRange(holidays);
    
        this.schedule = _.groupBy(holidays, 'date');

        this.setDurations();
    }

    ngAfterViewInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    private initWeek() {
        this.toolbarDays = helper.getWeekOfDate(this.nowDate);
        this.onWeek.emit(this.toolbarDays);
    }

    private initWeekHoliday() {
        _.forEach(this.dataSource, (el, index) => {
            if (_is.not.empty(el)) {
                this.toolbarDays.push(index);
            }
        });
        console.log(this.toolbarDays);
        // this.toolbarDays = helper.getWeekOfDate(this.nowDate);
        // this.onWeek.emit(this.toolbarDays);
    }

    private initHours() {
        this.toolbarHours = helper.getHoursRange(this.hoursRange);
    }

    public psScrollY() {
        this.daysScroll.nativeElement.scrollTop = this.scrollBox.nativeElement.scrollTop;
    }

    public psScrollX() {
        this.hoursScroll.nativeElement.scrollLeft = this.scrollBox.nativeElement.scrollLeft;
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

}
