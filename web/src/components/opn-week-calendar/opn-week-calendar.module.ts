import {
    NgModule,
} from '@angular/core';
import {
    CommonModule,
} from '@angular/common';
import {
    PerfectScrollbarModule,
    PERFECT_SCROLLBAR_CONFIG,
    PerfectScrollbarConfigInterface,
} from 'ngx-perfect-scrollbar';

import {
    OpnWeekCalendarComponent,
} from './opn-week-calendar.component';
import {
    MomentModule,
} from 'angular2-moment';
import {
    OpnCalendarDaysComponent,
} from './opn-calendar-days/opn-calendar-days.component';
import {
    OpnCalendarHoursComponent,
} from './opn-calendar-hours/opn-calendar-hours.component';
import {
    OpnDayEventsComponent,
} from './opn-calendar-days/opn-day-events/opn-day-events.component';
import {
    OpnViewEventComponent,
} from './opn-calendar-days/opn-day-events/opn-view-event/opn-view-event.component';
import {OpnViewSubscriptionsComponent} from './opn-calendar-days/opn-day-events/opn-view-event/subscriptions-list/subscriptions-list.component'
import { ScheduleService } from './providers/schedule.service';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: false,
    suppressScrollY: false,
};

@NgModule({
    declarations: [
        OpnWeekCalendarComponent,
        OpnCalendarDaysComponent,
        OpnCalendarHoursComponent,
        OpnDayEventsComponent,
        OpnViewEventComponent,
        OpnViewSubscriptionsComponent,
    ],
    imports: [
        CommonModule,
        MomentModule,
        PerfectScrollbarModule,
    ],
    exports: [
        OpnWeekCalendarComponent,
        OpnCalendarDaysComponent,
        OpnCalendarHoursComponent,
        OpnDayEventsComponent,
        OpnViewEventComponent,
        OpnViewSubscriptionsComponent,
    ],
    entryComponents: [
        OpnWeekCalendarComponent,
        OpnCalendarDaysComponent,
        OpnCalendarHoursComponent,
        OpnDayEventsComponent,
        OpnViewEventComponent,
        OpnViewSubscriptionsComponent,
    ],
    providers: [
        ScheduleService,
        {
        provide: PERFECT_SCROLLBAR_CONFIG,
        useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    }],
})
export class OpnWeekCalendarModule {}
