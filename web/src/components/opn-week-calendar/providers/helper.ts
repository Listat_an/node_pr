import * as _d from 'date-fns';
import * as _ from 'lodash';

export function getWeekOfDate(date: Date): any {
    const s_w = _d.startOfWeek(date, {
        weekStartsOn: 1,
    });

    const e_w = _d.endOfWeek(date, {
        weekStartsOn: 1,
    });

   return _d.eachDay(s_w, e_w);
}

export function getHoursRange(range: Array<any>) {
    let c_h = _.first(range);
    let e_h = _.last(range);
    let hours: Array<any> = [];
    while (c_h <= e_h) {
        hours.push(_d.format(_d.setHours(new Date(), c_h), 'h A'));
        c_h++;
    }
    return hours;
}

export function getFormatDays(range, rows): any {
    let data: any = {};
    _.forEach(range, day => {
        data = _.assign(data, {
            [_d.format(day, 'ddd')]: {
                day: _d.format(day, 'dddd'),
                date: _d.format(day, 'MMM D'),
                rows: (_.values(rows[ _d.format(day, 'ddd')]).length) ? _.values(rows[ _d.format(day, 'ddd')]).length : 1,
            },
        });
    });
    return data;
}

export function getFormatDates(rows): any {
    let data: any = {};
    _.forEach(_.keys(rows), day => {
        let m = new Date(day);
        data = _.assign(data, {
            [day]: {
                mont: _d.format(m, 'MMMM D'),
                day: _d.format(m, 'dddd'),
                rows: (_.values(rows[ day ]).length) ? _.values(rows[ day ]).length : 1,
            },
        });
    });
    return data;
}

export function differenceInDays(date, date2): any {
    return _d.differenceInDays(date, date2);
}

export function eachDay(date, date2): any {
    return _d.eachDay(date, date2);
}


