import {
    Injectable, EventEmitter,
} from '@angular/core';

import {
    Observable,
} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import * as _ from 'lodash';
import { format } from 'date-fns';

@Injectable()
export class ScheduleService {
    scheduleClick: EventEmitter < any > = new EventEmitter < any > ();
    constructor() {}
}
