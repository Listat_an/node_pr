import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import * as _ from 'lodash';
import * as _is from 'is_js';
import { ScheduleService } from '../../../providers/schedule.service';

@Component({
  selector: 'opn-view-event',
  templateUrl: './opn-view-event.component.html',
  styleUrls: ['./opn-view-event.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class OpnViewEventComponent implements OnInit, AfterViewInit {

  @Input() events?: any;
  @Input() options?: any;
  @Input() hours?: any;

  @ViewChild('rowEvent', {read: ElementRef}) rowEvent: ElementRef;

  public alertPos: String;

  public stEvent: {};


  constructor(
    private cd: ChangeDetectorRef,
    private scheduleSrvc: ScheduleService,
  ) {}

  ngOnInit() {
    if (_is.not.undefined(this.hours) && _is.not.undefined(this.events)) {
      const start = parseInt(_.first(this.hours)['v'], 10);
      this.events = this.events.map(el => this.parseEvent(el, start));
    }
    this.cd.detectChanges();
  }

  public onClickEvent(meta) {
    this.scheduleSrvc.scheduleClick.emit(meta);
  }

  ngAfterViewInit() {
    const h = this.rowEvent.nativeElement.offsetParent.offsetParent.firstElementChild.offsetHeight;
    let _h = 500;
    const t = this.rowEvent.nativeElement.offsetParent.offsetTop;
    if (h > _h) {
      _h = h;
    }
    if (_h / 2 < t) {
      setTimeout(() => {
        this.alertPos = 'top';
      }, 0);
    }else {
      setTimeout(() => {
        this.alertPos = 'bot';
      }, 0);
    }
  }

  private parseEvent(event, s): any {
    const [s_h, s_m] = event.time_start.split('.');
    const [d_h, d_m] = event.duration.split('.');
    const left = this.options.celWidth * (s_h - s) + (this.options.celWidth / 60 * s_m) + 'px';
    const width = this.options.celWidth * d_h + (this.options.celWidth / 60 * d_m) + 'px';
    return {
      ...event,
      isOpen: false,
      stEvent: {
        left,
        width,
      },
    };
  }
}
