import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpnViewEventComponent } from './opn-view-event.component';

describe('OpnViewEventComponent', () => {
  let component: OpnViewEventComponent;
  let fixture: ComponentFixture<OpnViewEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpnViewEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpnViewEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
