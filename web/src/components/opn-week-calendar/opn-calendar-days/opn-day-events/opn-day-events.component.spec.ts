import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpnDayEventsComponent } from './opn-day-events.component';

describe('OpnDayEventsComponent', () => {
  let component: OpnDayEventsComponent;
  let fixture: ComponentFixture<OpnDayEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpnDayEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpnDayEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
