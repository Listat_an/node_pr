import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'opn-day-events',
  templateUrl: './opn-day-events.component.html',
  styleUrls: ['./opn-day-events.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class OpnDayEventsComponent implements OnInit {
  @Input() dataSours?: any;
  @Input() hoursSours?: any;
  @Input() options?: any;


  public hours: Array<any> = [];
  public rows: Array<any> = [];

  constructor() { }

  ngOnInit() {
    if (this.hoursSours) {
      this.hoursSours.map((el, i) => {
        this.hours.push({
          v: el,
        });
      });
    }

    if (this.dataSours) {
      _.map(this.dataSours, el => {
        this.rows.push(el);
      });
    }
  }

}
