import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpnCalendarDaysComponent } from './opn-calendar-days.component';

describe('OpnCalendarDaysComponent', () => {
  let component: OpnCalendarDaysComponent;
  let fixture: ComponentFixture<OpnCalendarDaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpnCalendarDaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpnCalendarDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
