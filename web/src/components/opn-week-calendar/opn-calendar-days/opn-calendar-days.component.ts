import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'opn-calendar-days',
  templateUrl: './opn-calendar-days.component.html',
  styleUrls: ['./opn-calendar-days.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class OpnCalendarDaysComponent implements OnInit {

  @Input() dataSourse?: Array<any>;
  @Input() options?: any;
  @Input() isHoliday?: Boolean;

  public days: Array<any> ;
  constructor() { }

  ngOnInit() {
    if (this.isHoliday) {
      this.days = _.keys(this.dataSourse);
    }else {
      this.days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    }
    console.log('this.isHoliday ', this.isHoliday);
    console.log('this.dataSourse ', this.dataSourse);
    console.log('this.days ', this.days);
  }

}
