import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpnWeekCalendarComponent } from './opn-week-calendar.component';

describe('OpnWeekCalendarComponent', () => {
  let component: OpnWeekCalendarComponent;
  let fixture: ComponentFixture<OpnWeekCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpnWeekCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpnWeekCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
