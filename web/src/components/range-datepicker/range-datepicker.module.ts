import {RangeDatepickerComponent} from './range-datepicker.component';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import { ClickOutsideModule } from 'ng4-click-outside';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        FormsModule,
        ClickOutsideModule,
    ],
    declarations: [
        RangeDatepickerComponent,
    ],
    exports: [
        RangeDatepickerComponent,
    ],
})
export class RangeDatepickerModule {
}
