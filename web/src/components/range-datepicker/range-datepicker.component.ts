import {NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
    one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day ?
        false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day ?
        false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
    selector: 'opn-range-datepicker',
    templateUrl: './range-datepicker.html',
    styleUrls: ['./range-datepicker.scss'],
})
export class RangeDatepickerComponent implements OnInit {
    public hoveredDate: NgbDateStruct;
    public fromDate: NgbDateStruct;
    public toDate: NgbDateStruct;
    public selectedDays: string;

    public showWeekNumbers = false;
    public displayMonths = 2;

    @ViewChild('datePicker') public datePicker;
    @Output() selectRange: EventEmitter <any>;


    constructor(public calendar: NgbCalendar) {
        this.selectRange = new EventEmitter <any> ();
        this.fromDate = calendar.getToday();
        this.toDate = calendar.getNext(calendar.getToday(), 'm', 1);
        this.selectedDays = `${this.fromDate.day}.${this.fromDate.month}.${this.fromDate.year}-${this.toDate.day}.${this.toDate.month}.${this.toDate.year}`;
    }

    ngOnInit() {
    }


    public onDateChange(date: NgbDateStruct) {
        let start = '',
            end = '';
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
        } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
            this.toDate = date;
            let endM = this.toDate.month < 10 ? '0' + this.toDate.month : this.toDate.month;
            let endD = this.toDate.day < 10 ? '0' + this.toDate.day : this.toDate.day;
            end = `${endD}.${endM}.${this.toDate.year}`;
            //  this.datePicker.close();
        } else {
            this.toDate = null;
            this.fromDate = date;
        }
        let startM = this.fromDate.month < 10 ? '0' + this.fromDate.month : this.fromDate.month;
        let startD = this.fromDate.day < 10 ? '0' + this.fromDate.day : this.fromDate.day;
        start = `${startD}.${startM}.${this.fromDate.year}`;
        this.selectedDays = `${start}-${end}`;
        if (start && end) {
            this.selectRange.emit({
                start: start,
                end: end,
            });
        }
    }

    public isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);
    public isInside = date => after(date, this.fromDate) && before(date, this.toDate);
    public isFrom = date => equals(date, this.fromDate);
    public isTo = date => equals(date, this.toDate);

    public togglePicker() {
        if (this.toDate) {
            this.datePicker.close();
        } else {
            this.datePicker.open();
        }
    }
}
