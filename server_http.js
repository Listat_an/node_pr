'use strict';
let fs = require('fs');
const express = require('express'),
    app = express(),
    serverHttp = require('http').createServer(app),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    router = express.Router(),
    ejwt = require('express-jwt'),
    jwt = require('jsonwebtoken'),
    cors = require('cors'),
    config = require('./api/config'),
    // passport = require('./api/lib/passport'),
    database = require('./api/lib/db'),
    io = require('socket.io')(serverHttp),
    path = require('path');
global.users_online = {};
global.user_login = null;
const Notification = require('./api/models/notification');
const Message = require('./api/models').message;
const User = require('./api/models').user;
const Helper = require('./api/lib/helper');
var schedule = require('node-schedule');

app.set('database', database);

app.set('trust proxy', true)

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('login', function (data) {
        if (data.id){
            if(global.users_online[data.id]){
                global.users_online[data.id].push(socket.id);
            } else {
                global.users_online[data.id] = [socket.id]
            }
        }
        socket.broadcast.emit('online', {id: data.id});
    });

    socket.on('get-unread', function (data) {
        Promise.all([
            Notification.where({user_id: data.id, view: 0, type: 'user'}).count(),
            Notification.where({user_id: data.id, view: 0, type: 'manager'}).count(),
        ]).then(([user, manager]) => {
            socket.emit('notification_count', {user, manager})
        });
    })

    socket.on('write-group', function (data) {
        Promise.all([
            Notification.where({user_id: data.id, view: 0, type: 'user'}).count(),
            Notification.where({user_id: data.id, view: 0, type: 'manager'}).count(),
        ]).then(([user, manager]) => {
            socket.emit('notification_count', {user, manager})
        });
    });

    socket.on('post_unread_conversation_academy', function (data) {
        Message.where({view: 0, academy_id: data.academy_id}).fetchAll().then(messages => {
            let conversation_id = [];
            messages.map(item => {
              if(!~conversation_id.indexOf(item.get('conversation_id'))){
                conversation_id.push(item.get('conversation_id'));
              }
            });
            if(users_online.hasOwnProperty(data.user_id)){ 
                users_online[data.user_id].forEach(id => {
                  io.sockets.to(id).emit('unread_conversation_academy', {count: conversation_id.length});
                })
            } else {
                socket.emit('unread_conversation_academy', {count: conversation_id.length, academy_id: data.academy_id})
            }
            
        });
    });

    socket.on('disconnect', function (data) {
        let user_id;

        Object.keys(global.users_online).some(key => {
            if(~global.users_online[key].indexOf(socket.id)){
                if(global.users_online[key].lenght > 1){
                    global.users_online[key].splice(global.users_online[key].indexOf(socket.id),1);
                } else {
                    delete global.users_online[key];
                }
                user_id = key;
                return true;
            }
        });
        socket.broadcast.emit('offline', {id: user_id});
    })
});



app.use(cors({
    origin: true,
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true
}));

app.use(bodyParser.urlencoded({
    limit: '500mb',
    extended: true,
    type: 'application/x-www-form-urlencoded'
}));

app.use(bodyParser.json({
    limit: '500mb',
    type: 'application/*'
}));

// app.use(require('skipper')());

app.use(cookieParser());

app.use(express.static('public'));

app.use(ejwt({
    secret: config.token_secret,
    credentialsRequired: true,
    getToken: function fromHeaderOrQuerystring(req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1]
        } else if (req.query && req.query.token) {
            return req.query.token
        }
        return null
    }
}).unless({
    path: [
      {
        url: '/api/auth/signin',
        methods: ['POST']
      },
      {
        url: '/api/auth/signup',
        methods: ['POST']
      },{
        url: '/api/auth/signup_app',
        methods: ['POST']
      },{
        url: '/api/auth/login_or_create_academy',
        methods: ['POST']
      },
      {
        url: '/api/auth/forgot',
        methods: ['POST']
      },
      {
        url: '/api/auth/forgot_app',
        methods: ['POST']
      },
      {
        url: '/api/auth/valid_forgot_kode',
        methods: ['POST']
      },
      {
        url: /api\/auth\/activate_user\/.*$/,
        methods: ['GET']
      },
      {
        url: /api\/auth\/reset_password\/.*$/,
        methods: ['POST']
      },
      {
        url: /uploads\/.*$/,
        methods: ['GET']
      },
      {
        url: '/api/academies',
        methods: ['GET']
      },
      {
        url: '/api/belts',
        methods: ['GET']
      },
      {
        url: '/api/belt_colors',
        methods: ['GET']
      },
      {
        url: '/api/app/stripe_webhook',
        methods: ['GET', 'POST']
      },
      {
        url: '/api/metascraper',
        methods: ['POST']
      },
    ]
  }))

// app.use(passport.initialize());

app.use(function(req,res,next){
    req.io = io;
    user_login = req.user;
    next();
});

fs.readdirSync('./api/routes').forEach((file) => {
    router.use(`/api/${path.parse(file).name}`, require(`./api/routes/${file}`)(
        express.Router()
    ));
});

app.use(router);

var j = schedule.scheduleJob('0 * * * *', function(fireDate){
    User.where({academy_status: 1}).fetchAll({withRelated: ['academy']}).then(users => {
        if(users){
            let _users = users.toJSON();
            _users.forEach(user => {
                if(user.academy.promotion_method == 'time'){
                    Helper.userPromotion(user.id, user.academy_id);
                }
            })
        }
    })
});



serverHttp.listen(3001, () => {
    console.info(`Started server on 3001 `);
});


module.exports = app;