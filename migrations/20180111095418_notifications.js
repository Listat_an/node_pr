exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('notifications', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.text('content');
      t.string('type', 255).defaultTo('notification');
      t.boolean('view').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('notifications')
  ]);
};
