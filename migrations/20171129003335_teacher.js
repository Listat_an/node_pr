exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('teachers', (t) => {
      t.increments().primary();
      t.string('first_name', 255);
      t.string('last_name', 255);
      t.integer('academy_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('teachers')
  ]);
};