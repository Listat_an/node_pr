
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('user_timeline', function (t) {
      t.integer('transaction_id');
      t.boolean('is_change_refund').defaultTo(false);
      t.boolean('is_refund').defaultTo(false);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('user_timeline', function (table) {
      table.dropColumn('transaction_id');
      table.dropColumn('is_change_refund');
      table.dropColumn('is_refund');
    })
  ])
};