exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('post_like', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('post_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('post_like')
  ]);
};