exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('post_files', (t) => {
      t.increments().primary();
      t.integer('post_id');
      t.string('type', 255);
      t.string('url', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('post_files')
  ]);
};