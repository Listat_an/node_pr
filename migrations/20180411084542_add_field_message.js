exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('messages', function (t) {
      t.integer('academy_id').defaultTo(0);;
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('messages', function (table) {
      table.dropColumn('academy_id');
    })
  ])
};