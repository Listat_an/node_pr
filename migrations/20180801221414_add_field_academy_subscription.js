exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academy_subscription', function (t) {
      t.string('stripe_plan_id', 255);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academy_subscription', function (table) {
      table.dropColumn('stripe_plan_id');
    })
  ])
};