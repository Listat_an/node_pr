exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('products', (t) => {
      t.increments().primary();
      t.string('title', 255);
      t.text('description');
      t.double('price');
      t.integer('quantity');
      t.integer('shipping_id');
      t.integer('academy_id');
      t.boolean('show');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('products')
  ]);
};
