
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('user_academy_info', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.string('name', 255);
      t.string('begin_date', 255);
      t.string('end_date', 255);
      t.string('now_date', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_academy_info')
  ]);
};
