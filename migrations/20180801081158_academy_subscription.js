
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('academy_subscription', (t) => {
      t.increments().primary();
      t.string('name', 255);
      t.string('title', 255);
      t.text('description');
      t.double('price');
      t.double('sale');
      t.string('membership', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('academy_subscription')
  ]);
};