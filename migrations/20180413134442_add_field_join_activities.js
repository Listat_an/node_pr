exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('join_activity', function (t) {
      t.double('amount');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('join_activity', function (table) {
      table.dropColumn('amount');
    })
  ])
};