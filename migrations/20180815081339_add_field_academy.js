
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (t) {
      t.string('current_period_end', 255);
      t.string('billing_cycle_anchor', 255);
      t.string('trial_end', 255);
      t.boolean('is_cancel_time').defaultTo(false);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (table) {
      table.dropColumn('current_period_end');
      table.dropColumn('billing_cycle_anchor');
      table.dropColumn('trial_end');
      table.dropColumn('is_cancel_time');
    })
  ])
};