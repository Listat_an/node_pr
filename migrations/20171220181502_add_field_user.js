exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.string('address', 255);
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.dropColumn('address');
    })
  ])
};
