exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('user_revenue', (t) => {
      t.increments().primary();
      t.string('full_name', 255);
      t.string('subscription_name', 255);
      t.integer('user_id');
      t.integer('academy_id');
      t.integer('subscription_id');
      t.double('class').defaultTo(0);
      t.double('event').defaultTo(0);
      t.double('shop').defaultTo(0);
      t.double('subscription').defaultTo(0);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_revenue')
  ]);
};