exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('activity_technique', (t) => {
      t.increments().primary();
      t.integer('technique_id');
      t.integer('activity_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('activity_technique')
  ]);
};

