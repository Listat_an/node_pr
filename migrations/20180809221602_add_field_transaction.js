
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('transactions', function (t) {
      t.string('charge_id', 255);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('transactions', function (table) {
      table.dropColumn('charge_id');
    })
  ])
};