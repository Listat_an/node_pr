exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.dropColumn('academy')
      table.dropColumn('weight')
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.string('academy', 255);
      table.string('weight', 255);
    })
  ])
};
