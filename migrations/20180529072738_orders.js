
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('orders', (t) => {
      t.increments().primary();
      t.string('order_name', 255);
      t.string('first_name', 255);
      t.string('last_name', 255);
      t.string('full_name', 255);
      t.string('email', 255);
      t.string('type', 255).defaultTo('user');
      t.string('phone', 255);
      t.string('state', 255);
      t.string('sity', 255);
      t.string('street', 255);
      t.string('payment_status', 255).defaultTo('not_paid');
      t.string('shipping_status', 255).defaultTo('pick_&_pack');
      t.string('status', 255).defaultTo('in_process');
      t.double('tax');
      t.double('shipping_cost');
      t.double('total');
      t.double('subtotal');
      t.integer('academy_id');
      t.integer('shipping_id');
      t.integer('user_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('orders')
  ]);
};
