exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('user_subscriptions', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('subscription_id');
      t.boolean('status');
      t.string('stripe_subscription', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_subscriptions')
  ]);
};

