
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('checkin', function (t) {
      t.integer('schedule_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('checkin', function (table) {
      table.dropColumn('schedule_id');
    })
  ])
};