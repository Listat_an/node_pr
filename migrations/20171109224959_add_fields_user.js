exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.string('avatar', 255)
      table.string('cover_photo', 255)
      table.string('role', 255)
      table.integer('status').defaultTo(0)
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.dropColumn('avatar')
      table.dropColumn('cover_photo')
      table.dropColumn('role')
      table.dropColumn('status')
    })
  ])
};
