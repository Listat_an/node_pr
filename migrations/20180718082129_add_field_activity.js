exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', (t) => {
      t.string('cover_photo', 255);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (t) {
      t.dropColumn('tax');
      t.dropColumn('cover_photo');
    })
  ])
};