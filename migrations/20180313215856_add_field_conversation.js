exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('conversations', function (t) {
      t.string('type', 255).defaultTo('users');
      t.integer('academy_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('conversations', function (table) {
      table.dropColumn('type');
      table.dropColumn('academy_id');
    })
  ])
};