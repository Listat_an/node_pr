exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('tax_state', (t) => {
      t.increments().primary();
      t.string('state', 255);
      t.string('state_code', 255);
      t.double('procent').defaultTo(0);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('tax_state')
  ]);
};