
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (t) {
      t.double('latitude');
      t.double('longitude');
      t.string('cover_photo', 255);
      t.string('promotion_method', 255);
      t.boolean('is_pro').defaultTo(false);
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (table) {
      table.dropColumn('latitude');
      table.dropColumn('longitude');
      table.dropColumn('cover_photo');
      table.dropColumn('promotion_method');
      table.boolean('is_pro');
    })
  ])
};