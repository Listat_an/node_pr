
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('belts', function (table) {
      table.integer('belt_color_id');
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('belts', function (table) {
      table.dropColumn('belt_color_id')
    })
  ])
};