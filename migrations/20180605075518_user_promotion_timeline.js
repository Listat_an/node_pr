exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('user_promotion_timeline', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('academy_id');
      t.integer('belt_id');
      t.integer('subscription_id');
      t.integer('student_level');
      t.integer('class_attended');
      t.integer('time_attended');
      t.integer('time_stripe_count').defaultTo(10);
      t.integer('class_stripe_count').defaultTo(10);
      t.string('next_promotion', 255);
      t.string('full_name', 255);
      t.string('subscription_name', 255);
      t.boolean('status').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_promotion_timeline')
  ]);
};