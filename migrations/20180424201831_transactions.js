
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('transactions', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('academy_id');
      t.integer('activity_id');
      t.integer('subscription_id');
      t.string('type', 255);
      t.double('amount');
      t.double('amount_app');
      t.double('amount_academy');
      t.double('amount_stripe');
      t.double('procent');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('transactions')
  ]);
};