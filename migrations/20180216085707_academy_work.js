exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('academy_work', (t) => {
      t.increments().primary();
      t.integer('academy_id');
      t.string('day', 255);
      t.string('start', 255);
      t.string('end', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('academy_work')
  ]);
};

