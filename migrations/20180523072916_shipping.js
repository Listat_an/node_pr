exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('shipping', (t) => {
      t.increments().primary();
      t.string('title', 255);
      t.text('description');
      t.double('price');
      t.integer('academy_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('shipping')
  ]);
};