
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('calendar_subscription', (t) => {
      t.increments().primary();
      t.integer('subscription_id');
      t.integer('user_id');
      t.datetime('pay_start');
      t.datetime('pay_end');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('calendar_subscription')
  ]);
};
