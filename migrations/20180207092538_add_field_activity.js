exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (t) {
      t.text('about');
      t.double('price_all');
      t.date('start_date');
      t.date('end_date');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (table) {
      table.dropColumn('about');
      table.dropColumn('price_all');
      table.dropColumn('start_date');
      table.dropColumn('end_date');
    })
  ])
};