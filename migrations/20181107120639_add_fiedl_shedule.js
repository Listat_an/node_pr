
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('schedule', function (t) {
      t.integer('academy_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('schedule', function (table) {
      table.dropColumn('academy_id');
    })
  ])
};