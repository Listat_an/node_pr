
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('academies', (t) => {
      t.increments().primary();
      t.string('name', 255);
      t.integer('user_id');
      t.string('status', 255);
      t.string('location', 255);
      t.string('email', 255);
      t.string('phone', 255);
      t.string('photo', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('academies')
  ]);
};