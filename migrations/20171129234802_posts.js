exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('posts', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.string('title', 255);
      t.text('content');
      t.integer('count_like').defaultTo(0);
      t.boolean('public').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('posts')
  ]);
};