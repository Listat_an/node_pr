exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('subscriptions', function (t) {
      t.string('name', 255);
      t.string('age', 255);
      t.string('plan_id', 255);
      t.double('registration_fee');
      t.text('information');
      t.integer('trial_days');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('subscriptions', function (table) {
      table.dropColumn('name');
      table.dropColumn('information');
      table.dropColumn('trial_days');
      table.dropColumn('registration_fee');
      table.dropColumn('age');
      table.dropColumn('plan_id');
    })
  ])
};