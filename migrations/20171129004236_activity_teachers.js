exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('activity_teachers', (t) => {
      t.increments().primary();
      t.integer('activity_id');
      t.integer('teacher_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('activity_teachers')
  ]);
};