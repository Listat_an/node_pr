
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('subscription_acacdemies', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('academy_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('subscription_acacdemies')
  ]);
};