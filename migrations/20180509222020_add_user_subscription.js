exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('user_subscriptions', (t) => {
      t.boolean('is_cancel').defaultTo(false);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('user_subscriptions', function (t) {
      t.dropColumn('is_cancel');
    })
  ])
};