exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('post_files', function (t) {
      t.integer('user_id');
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('post_files', function (table) {
      table.dropColumn('user_id');
    })
  ])
};