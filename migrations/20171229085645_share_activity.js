exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('share_activity', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('activity_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('share_activity')
  ]);
};