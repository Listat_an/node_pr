
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('subscription_activity', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('activity_id');
      t.integer('subscription_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('subscription_activity')
  ]);
};