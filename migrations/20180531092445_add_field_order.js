exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('orders', (t) => {
      t.json('details');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('orders', function (t) {
      t.dropColumn('details');
    })
  ])
};