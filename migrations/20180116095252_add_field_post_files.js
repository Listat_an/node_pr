
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('post_files', function (t) {
      t.string('post_type', 255).defaultTo('post');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('post_files', function (table) {
      table.dropColumn('post_type');
    })
  ])
};