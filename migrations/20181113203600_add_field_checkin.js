
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('checkin', function (t) {
      t.date('checkin_date');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('checkin', function (table) {
      table.dropColumn('checkin_date');
    })
  ])
};