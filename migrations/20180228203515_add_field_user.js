exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.string('stripe_customer_id', 255);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.dropColumn('stripe_customer_id');
    })
  ])
};
