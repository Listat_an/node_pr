exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('user_active', (t) => {
      t.increments().primary();
      t.string('full_name', 255);
      t.string('subscription_name', 255);
      t.integer('user_id');
      t.integer('academy_id');
      t.integer('subscription_id');
      t.string('type', 255);
      t.boolean('new_user').defaultTo(false);
      t.boolean('status').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_active')
  ]);
};