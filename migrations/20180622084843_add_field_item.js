exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('order_items', (t) => {
      t.double('tax');
      t.double('shipping_price');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('order_items', function (t) {
      t.dropColumn('tax');
      t.dropColumn('shipping_price');
    })
  ])
};