exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('join_activity', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('activity_id');
      t.string('type',255);
      t.boolean('status').defaultTo(true);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('join_activity')
  ]);
};

