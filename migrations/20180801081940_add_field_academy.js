exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (t) {
      t.integer('academy_subscription_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (table) {
      table.dropColumn('academy_subscription_id');
    })
  ])
};