
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('user_prizes_info', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.string('content', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_prizes_info')
  ]);
};