exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (t) {
      t.double('amount');
      t.string('teacher_name', 255);
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (table) {
      table.dropColumn('amount');
      table.dropColumn('teacher_name');
    })
  ])
};
