exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('academy_subscription_permition', (t) => {
      t.increments().primary();
      t.string('name', 255);
      t.integer('subscription_id');
      t.string('title', 255);
      t.text('description');
      t.boolean('status').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('academy_subscription_permition')
  ]);
};