exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('teachers', function (t) {
      t.string('full_name', 255);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('teachers', function (table) {
      table.dropColumn('full_name');
    })
  ])
};