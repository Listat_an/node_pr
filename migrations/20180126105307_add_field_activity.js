exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (t) {
      t.integer('user_count');
      t.boolean('status_public_map').defaultTo(true);
      t.boolean('status_public').defaultTo(true);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (table) {
      table.dropColumn('user_count');
      table.dropColumn('status_public_map');
      table.dropColumn('status_public');
    })
  ])
};