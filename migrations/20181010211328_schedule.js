exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('schedule', (t) => {
      t.increments().primary();
      t.integer('class_id');
      t.string('day', 255);
      t.string('time_start', 255);
      t.string('time_end', 255);
      t.date('date');
      t.date('new_start_date');
      t.boolean('holiday').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('schedule')
  ]);
};
