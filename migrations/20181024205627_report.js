
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('report', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('tmp_id');
      t.string('type', 255);
      t.text('content');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('report')
  ]);
};
