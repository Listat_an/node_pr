

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('schedule_week', (t) => {
      t.increments().primary();
      t.integer('class_id');
      t.string('day', 255);
      t.string('time', 255);
      t.string('only_day', 255);
      t.string('only_month', 255);
      t.json('details');
      t.boolean('status').defaultTo(false);
      t.boolean('is_correct').defaultTo(false);
      t.boolean('include').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('schedule_week')
  ]);
};
