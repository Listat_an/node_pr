exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('transactions', (t) => {
      t.integer('order_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('transactions', function (t) {
      t.dropColumn('order_id');
    })
  ])
};