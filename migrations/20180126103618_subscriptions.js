
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('subscriptions', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('academy_id');
      t.double('price');
      t.string('membership', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('subscriptions')
  ]);
};