exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('product_images', (t) => {
      t.increments().primary();
      t.text('url');
      t.integer('product_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('product_images')
  ]);
};