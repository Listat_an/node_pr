exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('messages', (t) => {
      t.boolean('is_deleted').defaultTo(false);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('messages', function (t) {
      t.dropColumn('is_deleted');
    })
  ])
};