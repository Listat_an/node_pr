exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('belt_user', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('belt_id');
      t.string('status', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('belt_user')
  ]);
};
