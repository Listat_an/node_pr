
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('managers', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('academy_id');
      t.boolean('status').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('managers')
  ]);
};