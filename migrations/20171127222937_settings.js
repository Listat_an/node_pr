
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('settings', (t) => {
      t.increments().primary();
      t.string('name', 255);
      t.text('data');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('settings')
  ]);
};
