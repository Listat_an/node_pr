
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('followers', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('friend_id');
      t.boolean('status').defaultTo(false);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('followers')
  ]);
};