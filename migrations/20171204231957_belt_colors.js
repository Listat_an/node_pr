exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('belt_colors', (t) => {
      t.increments().primary();
      t.string('name', 255);
      t.string('hex', 255);
      t.string('type', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('belt_colors')
  ]);
};