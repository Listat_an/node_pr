exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('activity_comments', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.integer('activity_id');
      t.text('content');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('activity_comments')
  ]);
};
