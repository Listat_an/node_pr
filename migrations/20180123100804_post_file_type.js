
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('post_files', function (t) {
      t.integer('academy_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('post_files', function (table) {
      table.dropColumn('academy_id');
    })
  ])
};