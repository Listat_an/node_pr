exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('products', (t) => {
      t.double('shipping_price').defaultTo(0);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('products', function (t) {
      t.dropColumn('shipping_price');
    })
  ])
};