exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('orders', (t) => {
      t.string('address', 255);
      t.string('billing_address', 255);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('orders', function (t) {
      t.dropColumn('address');
      t.dropColumn('billing_address');
    })
  ])
};