exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('orders', (t) => {
      t.integer('user_order');
      t.string('billing_first_name', 255);
      t.string('billing_last_name', 255);
      t.string('billing_email', 255);
      t.string('billing_phone', 255);
      t.string('billing_state', 255);
      t.string('billing_sity', 255);
      t.string('billing_street', 255);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('orders', function (t) {
      t.dropColumn('user_order');
      t.dropColumn('billing_last_name');
      t.dropColumn('billing_email');
      t.dropColumn('billing_phone');
      t.dropColumn('billing_state');
      t.dropColumn('billing_sity');
      t.dropColumn('billing_street');
    })
  ])
};