
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('activity', (t) => {
      t.increments().primary();
      t.integer('academy_id');
      t.string('type', 255);
      t.string('name', 255);
      t.string('location', 255);
      t.string('user_age_group', 255);
      t.string('user_ranking', 255);
      t.string('payment_status', 255);
      t.timestamp('date');
      t.double('latitude');
      t.double('longitude');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('activity')
  ]);
};