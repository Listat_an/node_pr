
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('cms', (t) => {
      t.increments().primary();
      t.string('title', 255);
      t.text('content');
      t.string('slug', 255);
      t.string('seo_title', 255);
      t.string('seo_desc', 255);
      t.string('seo_key', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('cms')
  ]);
};
