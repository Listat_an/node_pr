exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('carts', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.string('name', 255);
      t.string('photo', 255);
      t.double('price');
      t.integer('product_id');
      t.integer('quantity');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('carts')
  ]);
};