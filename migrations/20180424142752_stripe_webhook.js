exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('stripe_webhook', (t) => {
      t.increments().primary();
      t.text('data');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('stripe_webhook')
  ])
};