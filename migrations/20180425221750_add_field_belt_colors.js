exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('belt_colors', function (t) {
      t.integer('position').defaultTo(0);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('belt_colors', function (table) {
      table.dropColumn('position');
    })
  ])
};