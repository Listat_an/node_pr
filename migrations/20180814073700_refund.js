
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('refund', (t) => {
      t.increments().primary();
      t.integer('manager_id');
      t.integer('transaction_id', 255);
      t.double('amount');
      t.boolean('status').defaultTo(false);;
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('refund')
  ]);
};