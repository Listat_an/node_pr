
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (t) {
      t.string('color_border').defaultTo('rgba(159, 231, 173)');
      t.string('color_bg').defaultTo('rgba(159, 231, 173, 0.65)');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('activity', function (table) {
      table.dropColumn('color_border');
      table.dropColumn('color_bg');
    })
  ])
};
