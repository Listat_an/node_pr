exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('order_items', (t) => {
      t.increments().primary();
      t.string('title', 255);
      t.string('photo', 255);
      t.double('price');
      t.integer('product_id');
      t.integer('order_id');
      t.integer('quantity');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('order_items')
  ]);
};