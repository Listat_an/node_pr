exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.integer('academy_id');
      table.decimal('weight');
      table.string('active_kode', 255);
      table.boolean('status_public').defaultTo(true);
    })
  ])
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.dropColumn('academy_id')
      table.dropColumn('weight')
      table.dropColumn('active_kode')
      table.dropColumn('status_public')
    })
  ])
};
