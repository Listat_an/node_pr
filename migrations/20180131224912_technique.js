exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('technique', (t) => {
      t.increments().primary();
      t.integer('academy_id');
      t.string('name', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('technique')
  ]);
};