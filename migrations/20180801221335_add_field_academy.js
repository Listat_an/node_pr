exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (t) {
      t.boolean('is_payd_subscription').defaultTo(false);
      t.string('stripe_subscription_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (table) {
      table.dropColumn('is_payd_subscription');
      table.dropColumn('stripe_subscription_id');
    })
  ])
};