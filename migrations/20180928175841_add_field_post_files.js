
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('post_files', function (t) {
      t.integer('activity_id').defaultTo(0);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('post_files', function (table) {
      table.dropColumn('activity_id');
    })
  ])
};
