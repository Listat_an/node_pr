
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('users', (t) => {
      t.increments().primary();
      t.string('first_name', 255);
      t.string('last_name', 255);
      t.string('email').unique().notNullable();
      t.string('password', 128).notNullable();
      t.string('ranking', 255);
      t.string('academy', 255);
      t.string('weight', 255);
      t.string('date', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('users')
  ]);
};
