exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('order_items', (t) => {
      t.integer('academy_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('order_items', function (t) {
      t.dropColumn('academy_id');
    })
  ])
};