
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('join_activity', function (t) {
      t.integer('subscription_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('join_activity', function (table) {
      table.dropColumn('subscription_id');
    })
  ])
};