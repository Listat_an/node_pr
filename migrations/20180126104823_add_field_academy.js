
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (t) {
      t.boolean('status_public').defaultTo(true);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('academies', function (table) {
      table.dropColumn('status_public');
    })
  ])
};