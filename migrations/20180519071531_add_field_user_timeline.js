exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('user_timeline', (t) => {
      t.integer('academy_id');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('user_timeline', function (t) {
      t.dropColumn('academy_id');
    })
  ])
};