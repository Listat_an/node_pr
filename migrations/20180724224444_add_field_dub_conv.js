exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('subs_conversations', (t) => {
      t.boolean('is_deleted').defaultTo(false);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('subs_conversations', function (t) {
      t.dropColumn('is_deleted');
    })
  ])
};