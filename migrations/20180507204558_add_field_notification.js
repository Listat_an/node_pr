exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('notifications', function (t) {
      t.json('details');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('notifications', function (table) {
      table.dropColumn('details');
    })
  ])
};