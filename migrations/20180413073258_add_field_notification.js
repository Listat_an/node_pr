exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('notifications', function (t) {
      t.string('notification_type', 255).defaultTo('standart');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('notifications', function (table) {
      table.dropColumn('notification_type');
    })
  ])
};