exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('user_timeline', (t) => {
      t.dropColumn('parent_id');
      t.json('details');
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('user_timeline', function (t) {
      t.dropColumn('details');
      t.integer('parent_id');
    })
  ])
};