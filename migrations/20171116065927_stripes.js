exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('stripes', (t) => {
      t.increments().primary();
      t.string('name', 255);
      t.integer('belt_user_id');
      t.string('status', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('stripes')
  ]);
};
