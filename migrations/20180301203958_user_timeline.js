exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('user_timeline', (t) => {
      t.increments().primary();
      t.integer('user_id');
      t.string('type', 255);
      t.text('data');
      t.integer('parent_id');
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('user_timeline')
  ]);
};

