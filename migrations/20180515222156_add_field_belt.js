exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('belts', (t) => {
      t.dropColumn('age_begin');
      t.dropColumn('age_end');
      t.integer('time_stripe_count').defaultTo(10);
      t.integer('class_stripe_count').defaultTo(10);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('belts', function (t) {
      t.dropColumn('time_stripe_count');
      t.dropColumn('class_stripe_count');
    })
  ])
};