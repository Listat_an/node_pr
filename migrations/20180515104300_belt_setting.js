exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('belt_setting', (t) => {
      t.increments().primary();
      t.integer('belt_id');
      t.integer('academy_id');
      t.integer('time_stripe_count').defaultTo(0);
      t.integer('class_stripe_count').defaultTo(0);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('belt_setting')
  ]);
};