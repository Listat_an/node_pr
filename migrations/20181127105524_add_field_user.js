exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (t) {
      t.boolean('is_blocked').defaultTo(false);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('users', function (table) {
      table.dropColumn('is_blocked');
    })
  ])
};