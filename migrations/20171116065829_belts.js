exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('belts', (t) => {
      t.increments().primary();
      t.string('name', 255);
      t.integer('age_begin');
      t.integer('age_end');
      t.integer('stripe');
      t.string('type', 255);
      t.string('sourse', 255);
      t.string('sourse_round', 255);
      t.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists('belts')
  ]);
};
