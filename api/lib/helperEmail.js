const fs = require('fs');

var template = fs.readFileSync(__dirname + '/emails/template_email.html');
var type_template = {
  confirmation: fs.readFileSync(__dirname + '/emails/confirmation.html').toString(),
  change_email: fs.readFileSync(__dirname + '/emails/change_email.html').toString(),
  change_password: fs.readFileSync(__dirname + '/emails/change_password.html').toString(),
  reset_password: fs.readFileSync(__dirname + '/emails/reset_password.html').toString(),
  reset_password_ok: fs.readFileSync(__dirname + '/emails/reset_password_ok.html').toString(),
  reset_password_app: fs.readFileSync(__dirname + '/emails/reset_password_app.html').toString(),
  verified: fs.readFileSync(__dirname + '/emails/verified.html').toString(),
};

const helper = {
  getTemplateEmail(type, data){
    let result = type_template[type];
    Object.keys(data).map(key => {
      result = result.replace('{{' + key + '}}',data[key])
    });
    return template.toString().replace('{{content}}', result);
  }
}
module.exports = helper;