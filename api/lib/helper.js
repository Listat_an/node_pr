const joi = require('joi');
const _ = require('lodash');
const crypto = require('crypto');
const config = require('../config');
const AppError = require('./app_error');
const jwt = require('jsonwebtoken');
const url = require('url');
const Bookshelf = require('./db');
const Conversation = require('../models').conversation;
const CalendarSubscription = require('../models').calendar_subscription;
const Notification = require('../models').notification;
const SubsConversation = require('../models').subs_conversation;
const Message = require('../models').message;
const UserSubscription = require('../models').user_subscription;
const Subscription = require('../models').subscription;
const Checkin = require('../models').checkin;
const Academy = require('../models').academy;
const Manager = require('../models').manager;
const User = require('../models').user;
const UserPromotion = require('../models').user_promotion_timeline;
const BeltSetting = require('../models').belt_setting;
const Belt = require('../models').belt;
const BeltUser = require('../models').belt_user;
const UserRevenue = require('../models').user_revenue;
const UserActive = require('../models').user_active;
const JoinActivity = require('../models').join_activity;
const UserTimeline = require('../models').user_timeline;
const moment = require('moment');

let helper = {
  validateData(data, schema, options = {}) {
    return new Promise((resolve, reject) => {
      joi.validate(data, schema, options, function(err, value) {
        if (err) {
          return reject(err);
        }
        resolve(value);
      });
    });
  },

  normalizeQuery2({query, model, attributes, includeAttributes}){
    return helper.normalizeQuery(
      query,
      attributes ? attributes : model.getAttributes(),
      includeAttributes ? includeAttributes : model.getInclideAttributes())
  },

  normalizeQuery(query, attributes, includeAttributes = []){
    attributes.push('id');
    let filter = _.isPlainObject(query.filter) ? query.filter : {};
    filter = _.pick(filter, attributes);

    let filterOr = _.isPlainObject(query.filterOr) ? query.filterOr : {};
    filterOr = _.pick(filterOr, attributes);

    let page = _.isPlainObject(query.page) ? query.page : {};
    page = Object.assign({number: 1, size: 20}, _.pick(page, ['number', 'size']));

    if (page.number < 1) {
      page.number = 1;
    }
    if (page.size < 1) {
      page.size = 20;
    }

    let sort = [];
    if (query.sort){
      query.sort.split(',').forEach(field => {
        let direction = 'ASC';
        if (field.length > 0 && field[0] == '-'){
          direction = 'DESC';
          field = field.substr(1);
        }
        if (attributes.indexOf(field) > -1){
          sort.push({field, direction});
        }
      });
    }
    if (sort.length == 0){
      sort.push({field: 'created_at', direction: 'DESC'});
    }

    let include = [];
    if (query.include){
      include = query.include.split(',');
    }

    if(Array.isArray(include)){
      include.forEach((item, index) => {

        let sub_include = item.split('.');

        if(!~includeAttributes.indexOf(sub_include[0])){
          include.splice(index, 1)
        }
      })
    }
    

    // if (includeAttributes){
    //   include = _.intersection(include, includeAttributes);
    // }

    return {
      filterOr,
      filter,
      page,
      sort,
      include
    }
  },

  fetchPaginationData(query, Model, withRelated, whereArr = [], user){
    let comparison_keys = {'eq': '=', 'like': 'LIKE', 'not': '<>', 'lt': '<', 'lte': '<=', 'gt': '>', 'gte': '>=', 'in': 'in'};
    let preparedFetch = Model.forge();
    _.mapKeys(query.filter, (value, key) => {
      if (_.isPlainObject(value)){
        _.mapKeys(value, (plainValue, comp) => {
          if (!_.isUndefined( comparison_keys[ comp ] )) {
            if (comp == 'in'){
              if (_.isString(plainValue)){
                plainValue = plainValue.split(',');
              }
            }
            whereArr.push({ attr: key, comp: comparison_keys[ comp ], value: plainValue });
          }
        });
        delete query.filter[key];
      }
    });

    console.log('whereArr',whereArr)

    preparedFetch.where(query.filter);

    whereArr.forEach((where) => {
      preparedFetch.where(where.attr, where.comp, tryParseDate(where.value));
    });

    whereArrOr = [];

    _.mapKeys(query.filterOr, (value, key) => {
      if (_.isPlainObject(value)){
        _.mapKeys(value, (plainValue, comp) => {
          if (!_.isUndefined( comparison_keys[ comp ] )) {
            if (comp == 'in'){
              if (_.isString(plainValue)){
                plainValue = plainValue.split(',');
              }
            }
            whereArrOr.push({ attr: key, comp: comparison_keys[ comp ], value: plainValue });
          }
        });
        delete query.filterOr[key];
      }
    });

    // preparedFetch.orWhere(query.filterOr);

    // whereArrOr.forEach((where) => {
    //   preparedFetch.orWhere(where.attr, where.comp, tryParseDate(where.value));
    // });

    preparedFetch.query(function (db) {
      db.orWhere(query.filterOr);
      whereArrOr.forEach((where) => {
        db.orWhere(where.attr, where.comp, tryParseDate(where.value));
      });
    });

    query.sort.forEach(sortItem => {
      preparedFetch.orderBy(sortItem.field, sortItem.direction);
    });


    // query.include.push('subscriptions.subscription')
    // console.log(' query.include',  query.include)
    return preparedFetch.fetchPage({
      page: query.page.number,
      pageSize: query.page.size,
      withRelated: withRelated ? withRelated : query.include
    });
  },

  findAllModelData(query, Model, withRelated, whereArr = []){
    let comparison_keys = {'eq': '=', 'like': 'LIKE', 'not': '<>', 'lt': '<', 'lte': '<=', 'gt': '>', 'gte': '>=', 'in': 'in'};
    let preparedFetch = Model.forge();
    _.mapKeys(query.filter, (value, key) => {
      if (_.isPlainObject(value)){
        _.mapKeys(value, (plainValue, comp) => {
          if (!_.isUndefined( comparison_keys[ comp ] )) {
            if (comp == 'in'){
              if (_.isString(plainValue)){
                plainValue = plainValue.split(',');
              }
            }
            whereArr.push({ attr: key, comp: comparison_keys[ comp ], value: plainValue });
          }
        });
        delete query.filter[key];
      }
    });

    console.log('whereArr',whereArr)

    preparedFetch.where(query.filter);

    whereArr.forEach((where) => {
      preparedFetch.where(where.attr, where.comp, tryParseDate(where.value));
    });

    whereArrOr = [];

    _.mapKeys(query.filterOr, (value, key) => {
      if (_.isPlainObject(value)){
        _.mapKeys(value, (plainValue, comp) => {
          if (!_.isUndefined( comparison_keys[ comp ] )) {
            if (comp == 'in'){
              if (_.isString(plainValue)){
                plainValue = plainValue.split(',');
              }
            }
            whereArrOr.push({ attr: key, comp: comparison_keys[ comp ], value: plainValue });
          }
        });
        delete query.filterOr[key];
      }
    });

    // preparedFetch.orWhere(query.filterOr);

    // whereArrOr.forEach((where) => {
    //   preparedFetch.orWhere(where.attr, where.comp, tryParseDate(where.value));
    // });

    preparedFetch.query(function (db) {
      db.orWhere(query.filterOr);
      whereArrOr.forEach((where) => {
        db.orWhere(where.attr, where.comp, tryParseDate(where.value));
      });
    });

    query.sort.forEach(sortItem => {
      preparedFetch.orderBy(sortItem.field, sortItem.direction);
    });


    // query.include.push('subscriptions.subscription')
    // console.log(' query.include',  query.include)
    return preparedFetch.fetchAll({withRelated: withRelated ? withRelated : query.include});
  },

  indexResponse(data, pickAttr){
    return {
      total: data.pagination.rowCount,
      limit: data.pagination.pageSize,
      skip: data.pagination.pageSize * data.pagination.page - data.pagination.pageSize,
      data: (Array.isArray(pickAttr)) ? data.toJSON().map(item => _.pick(item, pickAttr)) : data
    }
  },

  errorResponse(res, errors){

    if (!_.isArray(errors)) {
      errors = [ errors ];
    }

    let response = { errors: [] };
    errors.forEach((err) => {
      let error  = {"status": "500"};
      console.log('eeee', err.type)
      if (typeof err == 'number') {
        switch (err) {
        case 401:
          error = {
            "status": "401",
            "title": "Unauthorized"
          };
          break;
        case 403:
          error = {
            "status": "403",
            "title": "Forbidden"
          };
          break;
        case 404:
          error = {
            "status": "404",
            "title": "Not Found"
          };
          break;
        default:
          error = {
            "status": err.toString()
          };
        }
      } else if (err.type == 'StripeInvalidRequestError') {
        error = {
          status: "400",
          description: err.message,
          param: err.param
        };
      } else if (err.isJoi == true) {
        _errors = [];

        if(err.details){
          if(Array.isArray(err.details)){
            err.details.map(item => {
              _errors.push({
                status: "400",
                description: item.message,
                param: item.path
              })
            })
          }
        }
        response.errors = response.errors.concat(_errors);
        return;
      } else if (err instanceof Error) {
        error = {
          "status": "400",
          "description": err.message
        };
      } else if (err instanceof AppError) {
        error = err;
      } else if (_.isPlainObject(err)) {
        error = err;
      } else {
        error = {
          "status": err.status || "500",
          "description": err.message
        };
      }
      if(err.details){
        error.details = err.details
      }
      response.errors.push(error);
    });

    if (response.errors.length == 1) {
      res.status(response.errors[0].status);
    } else {
      res.status(400);
    }
    res.json(response);
  },

  getS3Link(key, bucket) {
    return `https://s3-${config.s3.region}.amazonaws.com/${bucket}/${key}`
  },

  getS3Key(link, prefix = '', filename = null) {
    let key
    if (link) {
      let pathname = url.parse(link).pathname.substr(1)
      if (pathname) {
        key = pathname.split('/').slice(1).join('/')
      }
    }
    if (!key) {
      if(filename){
        key = prefix + crypto.randomBytes(20).toString('hex')+ "." + filename.split('.').pop().toLowerCase();
      } else {
        key = prefix + crypto.randomBytes(20).toString('hex') + ".jpg"
      }
    }
    return key
  },

  makeAuth(req){
    if (req.cookies && req.cookies.connect_auth) {
      try {
        let decoded = jwt.verify(req.cookies.connect_auth, config.token_secret);
        req.user = decoded;
      } catch (err) { }
    }
    return req;
  },

  createNotification(req, user_id, content, type = 'user', notification_type='standart', details={}){
    return Notification.forge({user_id: user_id || req.user.id, type, content, view: 0, notification_type, details: JSON.stringify(details)}).save().then(notification => {
      console.log('req', Object.keys(req))
      if(users_online.hasOwnProperty(user_id)){ 
        users_online[user_id].forEach(id => {
          req.io.sockets.to(id).emit('notification', notification);
        })
      }
      return Promise.all([
        Notification.where({user_id, view: 0, type: 'user'}).count(),
        Notification.where({user_id, view: 0, type: 'manager'}).count(),
      ]).then(([user, manager]) => {
        if(users_online.hasOwnProperty(user_id)){ 
          users_online[user_id].forEach(id => {
            req.io.sockets.to(id).emit('notification_count', {user, manager});
          })
        }
        return true;
      });
    }).catch(err => {
      console.log('err', err);
      return Promise.resolve(null)
    })
  },


  getConversation(user_id, sender_id, academy_id = null){
    return Promise.resolve(true).then(() => {
      if(academy_id){
        return Conversation.where({academy_id, type: 'academy'}).fetchAll().then(conversations => {
          let ids = conversations.map(item => item.id);
          if (!ids.length){
            return [[]]
          }
          return Bookshelf.knex.raw('SELECT conversation_id FROM `subs_conversations` WHERE conversation_id IN (' + ids.toString() + ') AND user_id = ' + sender_id)
        })
      }
      console.log('getConversation', academy_id)
      return Bookshelf.knex.raw('SELECT t1.conversation_id FROM ( SELECT A.conversation_id FROM `subs_conversations` A WHERE A.user_id = ' + user_id +  ' GROUP BY A.conversation_id ) t1 INNER JOIN ( SELECT C.conversation_id FROM `subs_conversations` C WHERE C.user_id = ' + sender_id + ' GROUP BY C.conversation_id ) t2 ON t2.conversation_id = t1.conversation_id');
    }).then(conv => {
      if(conv[0].length){
        return Conversation.forge({id: conv[0][0].conversation_id}).fetch();
      } 
      let data = {
        status: true
      }
      if(academy_id){
        data.academy_id = academy_id;
        data.type = 'academy';
      }
      return Conversation.forge(data).save().then(conversation => {
        return Promise.all([
          !academy_id ? SubsConversation.forge({conversation_id: conversation.id, user_id}).save() : Manager.where({academy_id: academy_id}).fetchAll().then(menegars => {
            menegars.forEach(manager => {
              // if(user_id != manager.get('user_id')){
                SubsConversation.forge({conversation_id: conversation.id, user_id: manager.get('user_id')}).save();
              // }
            });
            return null;
          }),
          SubsConversation.forge({conversation_id: conversation.id, user_id: sender_id}).save(),
        ]).then(() => {
          return conversation;
        })
      });
    })
  },

  nextLevelTime(count, academy_id, user_id){
    return Subscription.where({academy_id}).fetchAll().then(subs => {
      return UserSubscription.where({user_id: user_id, status: 1}).where('subscription_id', 'in', subs.map(item => item.id)).fetch()
    }).then(sub => {
      if(!sub){
        return [0, count];
      }
      var hours = Math.floor(Math.abs(new Date() - new Date(sub.get('created_at'))) / 36e5);

      var hours = Math.floor(hours/24); //TODO DAY

      let coef = Math.floor(hours/count);
      let res = hours - coef*count ? hours - coef*count : count;
     
      return [res, count]
    });
  },

  nextLevelClass(count, academy_id, user_id){
    return Checkin.where({user_id, academy_id}).fetchAll().then(joins => {
      if(!joins.length){
        return [0, count];
      }
      let coef = Math.floor(joins.length/count);
      return [ joins.length - coef*count ? joins.length - coef*count : count, count];
    });
  },

  sendMessage(req, message){
    return Message.forge({id: message.id}).fetch({withRelated: ['user', 'user_sender']}).then(mess => {
      req.io.sockets.emit('message', mess);
      return mess.toJSON();
    })
  },

  getBeltInfo(user_id, academy_id){
    let current_user_belt;
    return BeltUser.forge({user_id}).fetch().then(belt => {
      if(!belt){
        return Promise.reject(new Error(''));
      }
      return Belt.where({id: belt.get('belt_id')}).fetch();
    }).then(belt => {
      if(!belt){
        return Promise.reject(new Error(''));
      }
      current_user_belt = belt.toJSON();
      return Belt.where({name: belt.get('name'), stripe: 0}).fetch();
    }).then(belt => {
      if(!belt){
        return Promise.reject(new Error(''));
      }
      return Promise.all([
        BeltSetting.where({belt_id: belt.get('id'), academy_id}).fetch(),
        belt
      ]);
    }).then((belts) => {
      let belt_j = belts[0] ? belts[0].toJSON() : belts[1].toJSON();
      return {
        time_stripe_count: belt_j.time_stripe_count,
        class_stripe_count: belt_j.class_stripe_count,
        next_belt: current_user_belt.stripe == 4,
        belt_id: current_user_belt.id
      }
    }).catch((err) => {
      return Promise.resolve(null);
    });
  },


  userPromotion(user_id, academy_id){
    var data = {
      user_id,
      academy_id,
      next_promotion: 'new_belt',
      time_stripe_count: 0,
      class_stripe_count: 0,
      student_level: 0,
    };
    var created = true;
    var promotion_method;
    return Promise.all([
      Academy.where({id: academy_id}).fetch(),
      
      this.getBeltInfo(user_id, academy_id),
      Checkin.where({user_id, academy_id}).fetchAll(),
      UserPromotion.where({user_id, academy_id}).fetch(),
      User.where({id: user_id}).fetch(),
      this.getSubscription(user_id)
    ]).then(([academy, belt_info, checkins, user_promotion, user, subscription]) => {
      if(!academy){
        return null;
      }
      if(!user){
        return null;
      }
      data.full_name = user.get('first_name') + ' ' + user.get('last_name');
      if(user_promotion){
        created = false;
      }

      if(!belt_info){
        return null
      }

      if(!subscription){
        return UserPromotion.where({user_id, academy_id}).destroy().then(() => {
          return Promise.reject(new Error(''));
        })
      }
      data.subscription_id = subscription.id;
      data.subscription_name = subscription.name;
      data.class_attended = checkins.length;
      data.time_stripe_count = belt_info.time_stripe_count;
      data.class_stripe_count = belt_info.class_stripe_count;
      data.next_promotion = belt_info.next_belt ? 'belt' : 'stripe';
      data.belt_id = belt_info.belt_id;
      promotion_method = academy.get('promotion_method');
      return academy.get('promotion_method') == 'time' ? helper.nextLevelTime(belt_info.time_stripe_count, academy_id, user_id) : helper.nextLevelClass(belt_info.class_stripe_count, academy_id, user_id)
    }).then((info) => {
      if(!info){
        return null;
      }

      if(promotion_method == 'time'){
        data.time_attended = info[0]
      }
      data.student_level = info[0];
      data.status = info[0] == info[1];
      return null;
    }).then(() => {
      if(created){
        return UserPromotion.forge(data).save();
      } else {
        return UserPromotion.where({user_id, academy_id}).save(data, { method: 'update'});
      }
    }).catch(err => {
      return Promise.resolve(null);
    })
  },

  userRevenue(user_id, academy_id, type, amount){
    let data = {
      user_id,
      academy_id
    };
    let revenue_data;
    let created = true;
    return Promise.all([
      User.where({id:user_id}).fetch(),
      UserRevenue.where({user_id, academy_id}).fetch(),
      this.getSubscription(user_id)
    ]).then(([user, revenue, subscription]) => {
      if(!user){
        return Promise.reject(new Error(''));
      }
      if(subscription){
        data.subscription_id = subscription.id;
        data.subscription_name = subscription.name;
      }
      data.full_name = user.get('first_name') + ' ' + user.get('last_name');
      data[type] = amount;
      if(revenue){
        created=false;
        revenue_data = revenue.toJSON();
        data[type] = amount + revenue_data[type];
      }

      if (created){
        return UserRevenue.forge(data).save()
      } else {
        return revenue.save(data, { method: 'update'})
      }

    }).catch(err => {
      Promise.resolve(false)
    });
  },

  getSubscription(user_id){
    return UserSubscription.where({user_id: user_id, status: 1}).fetchAll().then(subscriptions => {
      if (!subscriptions.length){
        return null
      }
      let _subscriptions = subscriptions.toJSON();
      return Subscription.where({id:  _subscriptions[0].subscription_id}).fetch().then(sub => {
        if (!sub){
          return null
        }
        return sub.toJSON();
      })
    });
  }, 

  userActive(user_id, academy_id, type, new_user=0){
    let data = {
      user_id,
      academy_id,
      type,
      status: 1,
      new_user
    };
    return Promise.all([
      User.where({id:user_id}).fetch(),
      this.getSubscription(user_id)
    ]).then(([user, subscription]) => {
      if(!user){
        return Promise.reject(new Error(''));
      }
      if(subscription){
        data.subscription_id = subscription.id;
        data.subscription_name = subscription.name;
      }
      data.full_name = user.get('first_name') + ' ' + user.get('last_name');
      
      return UserActive.where({user_id, academy_id, status: 1}).fetchAll();
    }).then(actives => {
      actives.forEach(item => {
        item.save({status: 0}, { method: 'update'});
      })
      return UserActive.forge(data).save();
    }).catch(err => {
      return Promise.resolve(null)
    })
  },

  paginationArray(array, page_size, page_number = 1) {
    --page_number;
    return array.slice(page_number * page_size, (page_number + 1) * page_size);
  },

  canceled_subscription(req, subscription_id, user_id, status_sub = 0){
    let subscription_user, subscription;
    return Promise.all([
      UserSubscription.where({user_id, subscription_id}).fetch(),
      Subscription.where({id: subscription_id}).fetch(),
    ]).then(([sub_user, sub]) => {
      if (!sub_user){
        return Promise.reject(new Error('Not found Subscription user'))
      }
      if (!sub){
        return Promise.reject(new Error('Not found Subscription'))
      }
      subscription = sub;
      subscription_user = sub_user;
      return require('./external_services').cenceledStripeSubscription(subscription_user.get('stripe_subscription'));
    }).then(() => {
      return subscription_user.save({status: status_sub, is_cancel_time: status_sub}, { method: 'update'});
    }).then(() => {
      this.createNotification(req, user_id, 'Your Subscription ' + subscription.get('name') +' was cancelled on  ' + moment().format('Do MMMM YYYY'));
      UserTimeline.forge({
        data: 'Cancelled subscription ' + subscription.get('name'),
        user_id: user_id,
        type: 'standart',
        details: '',
        academy_id: subscription.get('academy_id')
      }).save();
      this.userActive(user_id, subscription.get('academy_id'), 'Cancelled');
      CalendarSubscription.where({
        subscription_id, 
        user_id,
        pay_end: null
      }).fetch().then(calendar_data => {
        if(calendar_data){
          calendar_data.set('pay_end', moment().format('YYYY-MM-DD HH:mm:ss'))
          calendar_data.save();
        }
      });
      return Promise.all([
        JoinActivity.where({subscription_id}).destroy(),
        // UserSubscription.where({user_id, subscription_id}).destroy() //TODO
      ]);
    })
  }



}


module.exports = helper;

function tryParseDate(date){
  if (/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z/.test(date)){
    return new Date(Date.parse(date));
  } else {
    return date
  }
}
