let knexfile = require('../../knexfile')[process.env.NODE_ENV || 'development'],
  knex = require('knex')(knexfile),
  bookshelf = require('bookshelf')(knex);
let jsonColumns = require('bookshelf-json-columns');

bookshelf.plugin('registry');
bookshelf.plugin('pagination');
bookshelf.plugin('virtuals')
bookshelf.plugin(jsonColumns);

module.exports = bookshelf;