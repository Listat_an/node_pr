const _ = require('lodash');
const config = require('../config');
// let AWS = require('aws-sdk');
const fs = require('fs');
var nodemailer = require('nodemailer');
const stripe = require("stripe")(
  config.stripe_key
);

// AWS.config.update({ accessKeyId: config.s3.accessKey, secretAccessKey: config.s3.secretKey });

var transporter = nodemailer.createTransport({

  host: 'mail.adm.tools',
  // port: 587,
  port: 465,
  secure: true, // true for 465, false for other ports
  // secure: false, // true for 465, false for other ports
  auth: {
      user: config.smpt.login, // generated ethereal user
      pass: config.smpt.password // generated ethereal password
  }
  // service: 'gmail',
  // auth: {
  //   user: config.gmail.login,
  //   pass: config.gmail.password
  // }
});

module.exports = {
  sendMail(data) {
    var mailOptions = {
      from: 'youremail@gmail.com',
      to: 'myfriend@yahoo.com',
      subject: 'Sending Email using Node.js',
      html: '<h1>Welcome</h1><p>That was easy!</p>'
    }
    return new Promise((resolve, reject) => {
      transporter.sendMail(data, function(error, info){
        if (error) {
					console.log("​sendMail -> error", error)
          return reject(error)
        } else {
          console.log('Email sent: ' + info.response);
          return resolve(info.response)
        }
      });
    })
  },
  /*
  UploadFileS3(file, key, bucket, isBase64 = true) {
    if(isBase64){
      return new Promise((resolve, reject) => {
        var params = {
          Bucket: bucket,
          Key: key,
          ACL: 'public-read',
          ContentDisposition: 'inline',
          ContentEncoding: 'base64',
          ContentType: 'image/jpeg',
          StorageClass: 'STANDARD'
        }
        var s3obj = new AWS.S3({params: params})
        buf = new Buffer(file.replace(/^data:image\/\w+;base64,/, ""),'base64')
        s3obj.putObject({Body: buf}, function (err, data) {
          if (err)
            return reject(err)
          return resolve(data)
        })
      })
    } else {
      return new Promise((resolve, reject) => {
        var params = {
          Bucket: bucket,
          Key: key,
          ACL: 'public-read',
          ContentDisposition: 'inline',
          ContentType: file.type,
          StorageClass: 'STANDARD'
        }
        var s3obj = new AWS.S3({params: params})
        s3obj.upload({Body: fs.createReadStream(file.fd), ContentDisposition: 'inline', StorageClass: 'STANDARD'}, function (err, data) {
          if (err)
            return reject(err)
          return resolve(data)
        })
      })
    }
  },

  DeleteFileS3(key, bucket) {

    let keys = _.isArray(key) ? key : [ key ]
    let Objects = keys.map(key => ({Key: key}))

    return new Promise((resolve, reject) => {
      let s3 = new AWS.S3()
      var params = {
        Bucket: bucket, 
        Delete: { 
          Objects
        },
      }

      s3.deleteObjects(params, function(err, data) {
        if (err) 
          return reject(err)
        return resolve(data)
      })
    })

  }
  */

  createStripePlan({amount, interval, name, interval_count, trial_period_days, metadata={}}){
    var data = {
      amount: amount || 0,
      interval: interval || "month",
      product: {
        name
      },
      currency: "usd",
      metadata
    };
    if (interval_count){
      data['interval_count'] = interval_count;
    }
    if (trial_period_days){
      data['trial_period_days'] = trial_period_days;
    }
    return new Promise((resolve, reject) => {
      stripe.plans.create(data, function(err, plan) {
        if(err){
          return reject(err)
        }
        return resolve(plan);
        // asynchronously called
      });
    })
  },

  deleteStripePlan(plan_id){
    if (!plan_id){
      return Promise.reject(new Error('Not found plan id'));
    }
   
    return new Promise((resolve, reject) => {
      stripe.plans.del(plan_id, function(err, confirmation) {
        if(err){
          return reject(err)
        }
        return resolve(confirmation);
        // asynchronously called
      });
    })
  },

  deleteStripeSubscription(subscription_id){
    if (!subscription_id){
      return Promise.reject(new Error('Not found subscription_id'));
    }
   
    return new Promise((resolve, reject) => {
      stripe.subscriptions.del(
        subscription_id,
        function(err, confirmation) {
          // asynchronously called
        }
      );
    })
  },

  updateStripeCustomer(customer, data){   
    return new Promise((resolve, reject) => {
      stripe.customers.update(customer, data, function(err, data) {
        if(err){
          console.log('err customer', err);
          return reject(err)
        }
        return resolve(data);
      });
    })
  },

  createStripeCustomer(data){   
    return new Promise((resolve, reject) => {
      stripe.customers.create(data, function(err, data) {
        if(err){
          console.log('err customer', err);
          return reject(err)
        }
        return resolve(data);
      });
    })
  },
  
  
  invoiceStripeRetrieve(invoice){   
    return new Promise((resolve, reject) => {
      stripe.invoices.retrieve(
        invoice,
        function(err, invoice) {
          if(err){
            console.log('err customer', err);
            return reject(err)
          }
          return resolve(invoice);
        }
      );
    })
  },

  createStripeSubscription({customer, plan, source, metadata}){   
    console.log('data', {customer, plan, source})
    return new Promise((resolve, reject) => {
      stripe.subscriptions.create({
        customer,
        items: [
          {
            plan,
          },
        ],
        source,
        metadata,
        trial_from_plan: true
      }, function(err, subscription) {
        if(err){
          console.log('err subscription', err);
          return reject(err)
        }
        return resolve(subscription);
        }
      );
    })
  },


  getStripeCards(customer){   
    return new Promise((resolve, reject) => {
      stripe.customers.listCards(customer, function(err, cards) {
        if(err){
          console.log('err cards', err);
          return reject(err)
        }
        return resolve(cards);
        }
      );
    })
  },

  createStripeCard(customer, source){   
    return new Promise((resolve, reject) => {
      stripe.customers.createSource(
        customer,
        { source }, function(err, card) {
        if(err){
          console.log('err cards', err);
          return reject(err)
        }
        return resolve(card);
        }
      );
    })
  },

  deleteStripeCard(customer, card){   
    return new Promise((resolve, reject) => {
      stripe.customers.deleteCard(
        customer,
        card, function(err, card) {
        if(err){
          console.log('err cards', err);
          return reject(err)
        }
        return resolve(card);
        }
      );
    })
  },

  cenceledStripeSubscription(subscription_id){   
    return new Promise((resolve, reject) => {
      stripe.subscriptions.del(
        subscription_id,
        function(err, card) {
        if(err){
          console.log('err cards', err);
          return reject(err)
        }
        return resolve(card);
        }
      );
    })
  },

  paymentStripe({customer, source, price, desc, type}) {
    let data = {
      amount: Math.round(price * 100),
      currency: 'usd',
      description: desc || '',
      source,
      // customer
    }
    if(type == 'card'){
      data.customer = customer
    }
    return new Promise((resolve, reject) => {
      stripe.charges.create(data, function(err, charge) {
        if (err) return reject(err);
        resolve(charge);
      });
    });
  },

  retrieveStripeCustomer(customer){   
    return new Promise((resolve, reject) => {
      stripe.customers.retrieve(customer, function(err, data) {
        if(err){
          console.log('err customer', err);
          return reject(err)
        }
        return resolve(data);
      });
    })
  },

  updateStripeCustomer(customer, data){   
    return new Promise((resolve, reject) => {
      stripe.customers.update(customer, data,  function(err, data) {
        if(err){
          console.log('err customer', err);
          return reject(err)
        }
        return resolve(data);
      });
    })
  },

  createStripeRefund(data){   
    return new Promise((resolve, reject) => {
      stripe.refunds.create(data, function(err, cards) {
        if(err){
          console.log('err cards', err);
          return reject(err)
        }
        return resolve(cards);
        }
      );
    })
  },

  getStripeSubscription(id){   
    return new Promise((resolve, reject) => {
      stripe.subscriptions.retrieve(id, function(err, data) {
        if(err){
          console.log('err cards', err);
          return reject(err)
        }
        return resolve(data);
        }
      );
    })
  },


}