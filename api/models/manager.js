let bookshelf = require('../lib/db'),
Manager = bookshelf.Model.extend({
  tableName: 'managers',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'academy_id',
      'status',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'academy',
    ]
  }
});

module.exports = bookshelf.model('Manager', Manager);