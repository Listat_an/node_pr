let bookshelf = require('../lib/db'),
SubscriptionActivity = bookshelf.Model.extend({
  tableName: 'subscription_activity',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  activity: function() {
    return this.belongsTo('Activity', 'activity_id');
  },
  subscription: function() {
    return this.belongsTo('Subscription', 'subscription_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'subscription_id',
      'activity_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'activity',
      'subscription'
    ]
  }
});

module.exports = bookshelf.model('SubscriptionActivity', SubscriptionActivity);