let bookshelf = require('../lib/db'),
StripeWebhook = bookshelf.Model.extend({
  tableName: 'stripe_webhook',
  hasTimestamps: true,
}, {
  jsonColumns: ['data'],
  getAttributes: () => {
    return [
      'data',
    ];
  },
  getInclideAttributes: () => {
    return [
    ]
  }
});

module.exports = bookshelf.model('stripe_webhook', StripeWebhook);