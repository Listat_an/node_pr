let bookshelf = require('../lib/db'),
Message = bookshelf.Model.extend({
  tableName: 'messages',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  user_sender: function() {
    return this.belongsTo('User', 'sender_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  initialize: function() {
    this.on('saving', this.afteSave, this);
  },
  afteSave: function(model, attrs, options) {
    // bcrypt.hash(model.attributes.password, 10, function(err, hash) {
    //   if( err ) throw err;
    //   model.set('password', hash);
    // });
    console.log('saved')
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'sender_id',
      'conversation_id',
      'content',
      'view',
      'academy_id',
      'is_deleted',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'user_sender',
      'academy',
    ]
  }
});

module.exports = bookshelf.model('Message', Message);