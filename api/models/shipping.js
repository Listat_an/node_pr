let bookshelf = require('../lib/db'),
Shipping = bookshelf.Model.extend({
  tableName: 'shipping',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'title',
      'description',
      'price',
      'academy_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
    ]
  }
});

module.exports = bookshelf.model('Shipping', Shipping);