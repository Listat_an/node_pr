let bookshelf = require('../lib/db'),
SharePost = bookshelf.Model.extend({
  tableName: 'share_post',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'post_id');
  },
  post: function() {
    return this.belongsTo('Post', 'post_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'post_id',
      'user_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'post',
      'user'
    ]
  }
});

module.exports = bookshelf.model('SharePost', SharePost);