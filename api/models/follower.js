let bookshelf = require('../lib/db'),
Follower = bookshelf.Model.extend({
  tableName: 'followers',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  friend: function() {
    return this.belongsTo('User', 'friend_id');
  },
  initialize: function() {
    this.on("created", this.afteCreate, this);
  },
  afteCreate: function(model, attrs, options) {
    
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'friend_id',
      'status',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'friend',
    ]
  }
});

module.exports = bookshelf.model('Follower', Follower);