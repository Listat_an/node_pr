let bookshelf = require('../lib/db'),
PostComment = bookshelf.Model.extend({
  tableName: 'post_comments',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  post: function() {
    return this.belongsTo('Post', 'post_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'post_id',
      'content',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'post'
    ]
  }
});

module.exports = bookshelf.model('PostComment', PostComment);