let bookshelf = require('../lib/db'),
BeltSetting = bookshelf.Model.extend({
  tableName: 'belt_setting',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  belt: function() {
    return this.belongsTo('Belt', 'belt_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'academy_id',
      'belt_id',
      'time_stripe_count',
      'class_stripe_count',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy',
      'belt',
    ]
  }
});

module.exports = bookshelf.model('BeltSetting', BeltSetting);