let bookshelf = require('../lib/db'),
Product = bookshelf.Model.extend({
  tableName: 'products',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  shipping: function() {
    return this.belongsTo('Shipping', 'shipping_id');
  },
  images: function() {
    return this.hasMany('ProductImage', 'product_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'title',
      'description',
      'price',
      'quantity',
      'show',
      'shipping_price',
      'shipping_id',
      'academy_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy',
      'shipping',
      'images'
    ]
  }
});

module.exports = bookshelf.model('Product', Product);