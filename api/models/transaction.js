let bookshelf = require('../lib/db'),
Transaction = bookshelf.Model.extend({
  tableName: 'transactions',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  activity: function() {
    return this.belongsTo('Activity', 'activity_id');
  },
  subscription: function() {
    return this.belongsTo('Subscription', 'subscription_id');
  },
  order: function() {
    return this.belongsTo('Order', 'order_id');
  },
  refunds: function() {
    return this.hasMany('Refund', 'transaction_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'academy_id',
      'activity_id',
      'subscription_id',
      'type',
      'amount',
      'amount_app',
      'amount_academy',
      'procent',
      'order_id',
      'charge_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'academy',
      'activity',
    ]
  }
});

module.exports = bookshelf.model('Transaction', Transaction);