let bookshelf = require('../lib/db'),
UserPromition = bookshelf.Model.extend({
  tableName: 'user_promotion_timeline',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  belt: function() {
    return this.belongsTo('Belt', 'belt_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  subscription: function() {
    return this.belongsTo('Subscription', 'subscription_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'belt_id',
      'academy_id',
      'subscription_id',
      'student_level',
      'class_attended',
      'time_attended',
      'time_stripe_count',
      'class_stripe_count',
      'next_promotion',
      'full_name',
      'subscription_name',
      'status',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'belt',
      'subscription',
      'academy',
    ]
  }
});

module.exports = bookshelf.model('UserPromition', UserPromition);