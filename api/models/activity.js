let bookshelf = require('../lib/db'),
Activity = bookshelf.Model.extend({
  tableName: 'activity',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  techniques: function() {
    return this.hasMany('ActivityTechnique', 'activity_id');
  },
  comments: function() {
    return this.hasMany('ActivityComment', 'activity_id');
  },
  teachers: function() {
    return this.hasMany('ActivityTeacher', 'activity_id');
  },
  // subscriptions: function() {
  //   return this.hasMany('SubscriptionActivity', 'activity_id');
  // },
  subscriptions: function() {
    return this.hasMany('SubscriptionActivity', 'activity_id');
  },
  join_activities: function() {
    return this.hasMany('JoinActivity', 'activity_id');
  },
  checkins: function () {
    return this.hasMany('Checkin', 'activity_id');
  },
  shedules: function() {
    return this.hasMany('Schedule', 'class_id');
  } 
}, {
  getAttributes: () => {
    return [
      'id',
      'academy_id',
      'name',
      'type',
      'location',
      'user_age_group',
      'user_ranking',
      'payment_status',
      'latitude',
      'longitude',
      'amount', // count user
      'teacher_name',
      'user_count',
      'status_public_map',
      'status_public',
      'start_date',
      'end_date',
      'about',
      'price_all', // price activity
      'cover_photo',
      'created_at',
      'updated_at',
      'color_border',
      'color_bg'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy',
      'techniques',
      'comments',
      'subscriptions',
      'teachers',
      'join_activities',
      'checkins',
      'shedules',
    ]
  }
});

module.exports = bookshelf.model('Activity', Activity);