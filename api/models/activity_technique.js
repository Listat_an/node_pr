let bookshelf = require('../lib/db'),
ActivityTechnique = bookshelf.Model.extend({
  tableName: 'activity_technique',
  hasTimestamps: true,
  activity: function() {
    return this.belongsTo('Activity', 'activity_id');
  },
  technique: function() {
    return this.belongsTo('Technique', 'technique_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'technique_id',
      'activity_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'activity',
      'technique'
    ]
  }
});

module.exports = bookshelf.model('ActivityTechnique', ActivityTechnique);