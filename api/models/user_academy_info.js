let bookshelf = require('../lib/db'),
UserAcademyInfo = bookshelf.Model.extend({
  tableName: 'user_academy_info',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'name',
      'begin_date',
      'end_date',
      'now_date',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
    ]
  }
});

module.exports = bookshelf.model('UserAcademyInfo', UserAcademyInfo);