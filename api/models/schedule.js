let bookshelf = require('../lib/db'),
Schedule = bookshelf.Model.extend({
  tableName: 'schedule',
  hasTimestamps: true,
  class: function() {
    return this.belongsTo('Activity', 'class_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
}, {
  jsonColumns: ['details'],
  getAttributes: () => {
    return [
      'id',
      'class_id',
      'day',
      'time_start',
      'time_end',
      'date',
      'new_start_date',
      'holiday',
      'academy_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'class',
      'academy',
    ]
  }
});

module.exports = bookshelf.model('Schedule', Schedule);