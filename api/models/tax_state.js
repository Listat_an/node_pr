let bookshelf = require('../lib/db'),
TaxState = bookshelf.Model.extend({
  tableName: 'tax_state',
}, {
  getAttributes: () => {
    return [
      'id',
      'state',
      'state_code',
      'procent',
    ];
  },
  getInclideAttributes: () => {
    return [
    ]
  }
});

module.exports = bookshelf.model('TaxState', TaxState);