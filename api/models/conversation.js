let bookshelf = require('../lib/db'),
Conversation = bookshelf.Model.extend({
  tableName: 'conversations',
  hasTimestamps: true,
  users: function() {
    return this.hasMany('SubsConversation', 'conversation_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  }
  
}, {
  getAttributes: () => {
    return [
      'id',
      'status',
      'type',
      'academy_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'users',
      'academy',
    ]
  }
});

module.exports = bookshelf.model('Conversation', Conversation);