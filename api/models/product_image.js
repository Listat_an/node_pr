let bookshelf = require('../lib/db'),
ProductImage = bookshelf.Model.extend({
  tableName: 'product_images',
  hasTimestamps: true,
  product: function() {
    return this.belongsTo('Product', 'product_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'url',
      'product_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'product',
    ]
  }
});

module.exports = bookshelf.model('ProductImage', ProductImage);