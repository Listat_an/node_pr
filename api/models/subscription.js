let bookshelf = require('../lib/db'),
Subscription = bookshelf.Model.extend({
  tableName: 'subscriptions',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  activities: function() {
    return this.hasMany('SubscriptionActivity', 'subscription_id');
  }, 
  users_subscription: function() {
    return this.hasMany('UserSubscription', 'subscription_id');
  }, 
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'academy_id',
      'price',
      'membership',
      'name',
      'age',
      'registration_fee',
      'information',
      'trial_days',
      'plan_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'academy',
      'activities',
      'users_subscription'
    ]
  }
});

module.exports = bookshelf.model('Subscription', Subscription);