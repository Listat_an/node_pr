let bookshelf = require('../lib/db'),
UserSubscription = bookshelf.Model.extend({
  tableName: 'user_subscriptions',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  subscription: function() {
    return this.belongsTo('Subscription', 'subscription_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'subscription_id',
      'status',
      'stripe_subscription',
      'current_period_end',
      'billing_cycle_anchor',
      'trial_end',
      'is_cancel',
      'is_cancel_time',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'subscription'
    ]
  }
});

module.exports = bookshelf.model('UserSubscription', UserSubscription);