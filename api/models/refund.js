let bookshelf = require('../lib/db'),
Refund = bookshelf.Model.extend({
  tableName: 'refund',
  hasTimestamps: true,
  manager: function() {
    return this.belongsTo('User', 'manager_id');
  },
  transaction: function() {
    return this.belongsTo('Transaction', 'transaction_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'manager_id',
      'transaction_id',
      'amount',
      'status',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'manager',
      'transaction',
    ]
  }
});

module.exports = bookshelf.model('Refund', Refund);