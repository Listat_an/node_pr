let bookshelf = require('../lib/db'),
Checkin = bookshelf.Model.extend({
  tableName: 'checkin',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  activity: function() {
    return this.belongsTo('Activity', 'activity_id');
  },
  schedule: function() {
    return this.belongsTo('Schedule', 'schedule_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'academy_id',
      'activity_id',
      'schedule_id',
      'checkin_date',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'academy',
      'activity',
      'schedule',
    ]
  }
});

module.exports = bookshelf.model('Checkin', Checkin);