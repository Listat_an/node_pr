let bookshelf = require('../lib/db'),
BeltColor = bookshelf.Model.extend({
  tableName: 'belt_colors',
  hasTimestamps: true,
  belts: function() {
    return this.hasMany('Belt', 'belt_color_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'name',
      'hex',
      'type',
      'position',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'belts'
    ]
  }
});

module.exports = bookshelf.model('BeltColor', BeltColor);