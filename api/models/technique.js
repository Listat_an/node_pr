let bookshelf = require('../lib/db'),
Technique = bookshelf.Model.extend({
  tableName: 'technique',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'academy_id',
      'name',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy',
    ]
  }
});

module.exports = bookshelf.model('Technique', Technique);