let bookshelf = require('../lib/db'),
AcademySubscriptionPermition = bookshelf.Model.extend({
  tableName: 'academy_subscription_permition',
  hasTimestamps: true,
  academy_subscription: function() {
    return this.belongsTo('AcademySubscription', 'subscription_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'name',
      'subscription_id',
      'title',
      'description',
      'status'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy_subscription',
    ]
  }
});

module.exports = bookshelf.model('AcademySubscriptionPermition', AcademySubscriptionPermition);