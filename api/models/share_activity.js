let bookshelf = require('../lib/db'),
ShareActivity = bookshelf.Model.extend({
  tableName: 'share_activity',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'post_id');
  },
  activity: function() {
    return this.belongsTo('Activity', 'activity_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'activity_id',
      'user_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'post',
      'user'
    ]
  }
});

module.exports = bookshelf.model('ShareActivity', ShareActivity);