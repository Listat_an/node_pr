let bookshelf = require('../lib/db'),
OrderItem = bookshelf.Model.extend({
  tableName: 'order_items',
  hasTimestamps: true,
  product: function() {
    return this.belongsTo('Product', 'product_id');
  },
  order: function() {
    return this.belongsTo('Order', 'order_id');
  },
  academy: function () {
    return this.belongsTo('Academy', 'academy_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'title',
      'photo',
      'price',
      'quantity',
      'product_id',
      'academy_id',
      'tax',
      'shipping_price',
      'order_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'product',
    ]
  }
});

module.exports = bookshelf.model('OrderItem', OrderItem);