let bookshelf = require('../lib/db'),
Teacher = bookshelf.Model.extend({
  tableName: 'teachers',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'first_name',
      'last_name',
      'full_name',
      'academy_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy'
    ]
  }
});

module.exports = bookshelf.model('Teacher', Teacher);