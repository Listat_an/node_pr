let bookshelf = require('../lib/db'),
UserPrizesInfo = bookshelf.Model.extend({
  tableName: 'user_prizes_info',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'content',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
    ]
  }
});

module.exports = bookshelf.model('UserPrizesInfo', UserPrizesInfo);