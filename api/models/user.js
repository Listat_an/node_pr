let bookshelf = require('../lib/db'),
User = bookshelf.Model.extend({
  tableName: 'users',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  user_academy_info: function() {
    return this.belongsTo('UserAcademyInfo', 'user_id');
  },
  user_prizes_info: function() {
    return this.belongsTo('UserPrizesInfo', 'user_id');
  },
  files: function() {
    return this.hasMany('PostFile', 'user_id');
  },
  belt_user: function() {
    return this.hasOne('BeltUser', 'user_id');
  },
  followers: function() {
    return this.hasMany('Follower', 'friend_id');
  },
  followings: function() {
    return this.hasMany('Follower', 'user_id');
  },
  checkins: function() {
    return this.hasMany('Checkin', 'user_id');
  },
  subscriptions: function() {
    return this.hasMany('UserSubscription', 'user_id');
  },
  cart: function() {
    return this.hasMany('Cart', 'user_id');
  },
  transactions: function() {
    return this.hasMany('Transaction', 'user_id');
  },
  virtuals: {
    online: function() {
        return users_online.hasOwnProperty(this.id);
    }
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'first_name',
      'last_name',
      'full_name',
      'email',
      'ranking',
      'academy_id',
      'weight',
      'date',
      'role',
      'avatar',
      'cover_photo',
      'status_public',
      'status',
      'latitude',
      'longitude',
      'address',
      'online',
      'short_address',
      'academy_status',
      'stripe_customer_id',
      'age_type',
      'is_student',
      'is_blocked',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy',
      'user_academy_info',
      'user_prizes_info',
      'files',
      'belt_user',
      'followers',
      'followings',
      'subscriptions',
      'checkins',
      'transactions',
      'cart'
    ]
  }
});

module.exports = bookshelf.model('User', User);