let bookshelf = require('../lib/db'),
UserTimeline = bookshelf.Model.extend({
  tableName: 'user_timeline',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  transaction: function() {
    return this.belongsTo('Transaction', 'transaction_id');
  },
}, {
  jsonColumns: ['details'],
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'details',
      'type',
      'data',
      'academy_id',
      'transaction_id',
      'is_change_refund',
      'is_refund',
      'transaction_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'transaction',
    ]
  }
});

module.exports = bookshelf.model('UserTimeline', UserTimeline);