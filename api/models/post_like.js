let bookshelf = require('../lib/db'),
PostLike = bookshelf.Model.extend({
  tableName: 'post_like',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  post: function() {
    return this.belongsTo('Post', 'post_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'post_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'post'
    ]
  }
});

module.exports = bookshelf.model('PostLike', PostLike);