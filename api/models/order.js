let bookshelf = require('../lib/db'),
Order = bookshelf.Model.extend({
  tableName: 'orders',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  shipping: function() {
    return this.belongsTo('Shipping', 'shipping_id');
  },
  items: function() {
    return this.hasMany('OrderItem', 'order_id');
  },
  academy_orders: function() {
    return this.hasMany('Order', 'user_order');
  },
}, {
  jsonColumns: ['details'],
  getAttributes: () => {
    return [
      'id',
      'order_name',
      'first_name',
      'last_name',
      'full_name',
      'email',
      'phone',
      'state',
      'sity',
      'street',
      'payment_status',
      'shipping_status',
      'status',
      'tax',
      'total',
      'subtotal',
      'academy_id',
      'shipping_id',
      'user_id',
      'type',
      'shipping_cost',
      'user_order',
      'details',
      'created_at',
      'updated_at',
      'billing_first_name',
      'billing_last_name',
      'billing_email',
      'billing_phone',
      'billing_state',
      'billing_sity',
      'billing_street',
      'billing_address',
      'address',
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy',
      'user',
      'shipping',
      'items',
      'academy_orders',
    ]
  }
});

module.exports = bookshelf.model('Order', Order);