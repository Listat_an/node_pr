let bookshelf = require('../lib/db'),
PostFile = bookshelf.Model.extend({
  tableName: 'post_files',
  hasTimestamps: true,
  post: function() {
    return this.belongsTo('Post', 'post_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'post_id',
      'user_id',
      'type',
      'url',
      'post_type',
      'academy_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'post'
    ]
  }
});

module.exports = bookshelf.model('PostFile', PostFile);