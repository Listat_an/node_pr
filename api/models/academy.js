let bookshelf = require('../lib/db'),
Academy = bookshelf.Model.extend({
  tableName: 'academies',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  academy_subscription: function() {
    return this.belongsTo('AcademySubscription', 'academy_subscription_id');
  },
  followers: function() {
    return this.hasMany('AcademyFollower', 'academy_id');
  },
  subscriptions: function() {
    return this.hasMany('Subscription', 'academy_id');
  },
  managers: function() {
    return this.hasMany('Manager', 'academy_id');
  },
  academy_works: function() {
    return this.hasMany('AcademyWork', 'academy_id');
  },
  students: function() {
    return this.hasMany('User', 'academy_id');
  },
  activities: function() {
    return this.hasMany('Activity', 'academy_id');
  },
  products: function() {
    return this.hasMany('Product', 'academy_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'name',
      'user_id',
      'email',
      'location',
      'status',
      'phone',
      'photo',
      'cover_photo',
      'promotion_method',
      'latitude',
      'longitude',
      'is_pro',
      'status_public',
      'information',
      'app_procent',
      'academy_subscription_id',
      'is_payd_subscription',
      'stripe_subscription_id',
      'current_period_end',
      'billing_cycle_anchor',
      'trial_end',
      'is_cancel_time',
      'is_blocked',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'followers',
      'subscriptions',
      'managers',
      'academy_works',
      'students',
      'academy_subscription',
      'activities',
      'products',
    ]
  }
});

module.exports = bookshelf.model('Academy', Academy);