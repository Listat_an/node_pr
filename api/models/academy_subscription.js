let bookshelf = require('../lib/db'),
AcademySubscription = bookshelf.Model.extend({
  tableName: 'academy_subscription',
  hasTimestamps: true,
  permitions: function() {
    return this.belongsTo('AcademySubscriptionPermition', 'subscription_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'name',
      'title',
      'description',
      'price',
      'sale',
      'membership',
      'stripe_plan_id',
    ];
  },
  getInclideAttributes: () => {
    return [
      'permitions',
    ]
  }
});

module.exports = bookshelf.model('AcademySubscription', AcademySubscription);