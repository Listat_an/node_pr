let bookshelf = require('../lib/db'),
Carts = bookshelf.Model.extend({
  tableName: 'carts',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  product: function() {
    return this.belongsTo('Product', 'product_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'name',
      'product_id',
      'price',
      'photo',
      'quantity',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'product',
    ]
  }
});

module.exports = bookshelf.model('Carts', Carts);