let bookshelf = require('../lib/db'),
Stripe = bookshelf.Model.extend({
  tableName: 'stripes',
  hasTimestamps: true,
  belt_user: function() {
    return this.belongsTo('BeltUser', 'belt_user_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'name',
      'belt_user_id',
      'status',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'belt_user'
    ]
  }
});

module.exports = bookshelf.model('Stripe', Stripe);