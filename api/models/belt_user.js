let bookshelf = require('../lib/db'),
BeltUser = bookshelf.Model.extend({
  tableName: 'belt_user',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  belt: function() {
    return this.belongsTo('Belt', 'belt_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'belt_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'belt',
    ]
  }
});

module.exports = bookshelf.model('BeltUser', BeltUser);