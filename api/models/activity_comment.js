let bookshelf = require('../lib/db'),
ActivityComment = bookshelf.Model.extend({
  tableName: 'activity_comments',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  activity: function() {
    return this.belongsTo('Activity', 'activity_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'activity_id',
      'content',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'activity'
    ]
  }
});

module.exports = bookshelf.model('ActivityComment', ActivityComment);