let bookshelf = require('../lib/db'),
AcademyWork = bookshelf.Model.extend({
  tableName: 'academy_work',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'academy_id',
      'day',
      'start',
      'end'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy',
    ]
  }
});

module.exports = bookshelf.model('AcademyWork', AcademyWork);