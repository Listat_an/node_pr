let bookshelf = require('../lib/db'),
Report = bookshelf.Model.extend({
  tableName: 'report',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'tmp_id');
  },
  post: function() {
    return this.belongsTo('Post', 'tmp_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'tmp_id');
  },
  comment: function() {
    return this.belongsTo('PostComment', 'tmp_id');
  },
  user_created:  function() {
    return this.belongsTo('User', 'user_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'tmp_id',
      'type',
      'content',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'user_created',
      'academy',
      'comment',
      'post'
    ]
  }
});

module.exports = bookshelf.model('Report', Report);