let bookshelf = require('../lib/db'),
UserActive = bookshelf.Model.extend({
  tableName: 'user_active',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  subscription: function() {
    return this.belongsTo('Subscription', 'subscription_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'academy_id',
      'subscription_id',
      'full_name',
      'subscription_name',
      'type',
      'status',
      'new_user',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'subscription',
      'academy',
    ]
  }
});

module.exports = bookshelf.model('UserActive', UserActive);