let bookshelf = require('../lib/db'),
Setting = bookshelf.Model.extend({
  tableName: 'settings',
  hasTimestamps: true,
}, {
  getAttributes: () => {
    return [
      'id',
      'title',
      'content',
      'seo_title',
      'seo_desc',
      'seo_key',
      'slug',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
    ]
  }
});

module.exports = bookshelf.model('Setting', Setting);