let bookshelf = require('../lib/db'),
SubscriptionAcademy = bookshelf.Model.extend({
  tableName: 'subscription_acacdemies',
  hasTimestamps: true,
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'academy_id',
      'user_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'academy',
      'user'
    ]
  }
});

module.exports = bookshelf.model('SubscriptionAcademy', SubscriptionAcademy);