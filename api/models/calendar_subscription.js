let bookshelf = require('../lib/db'),
CalendarSubscription = bookshelf.Model.extend({
  tableName: 'calendar_subscription',
  hasTimestamps: true,
}, {
  getAttributes: () => {
    return [
      'id',
      'subscription_id',
      'user_id',
      'pay_start',
      'pay_end',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
    ]
  }
});

module.exports = bookshelf.model('CalendarSubscription', CalendarSubscription);