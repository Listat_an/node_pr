let bookshelf = require('../lib/db'),
Belt = bookshelf.Model.extend({
  tableName: 'belts',
  hasTimestamps: true,
  belt_color: function() {
    return this.belongsTo('BeltColor', 'belt_color_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'name',
      'stripe',
      'type',
      'sourse',
      'belt_color_id',
      'sourse_round',
      'time_stripe_count',
      'class_stripe_count',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'belt_color'
    ]
  }
});

module.exports = bookshelf.model('Belt', Belt);