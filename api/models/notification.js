let bookshelf = require('../lib/db'),
Notification = bookshelf.Model.extend({
  tableName: 'notifications',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  initialize: function() {
    this.on("saving", this.savingModel, this);
  },
  savingModel: function(model, attrs, options) {
    // var self = this;
    // if (self.attributes.details){
    //   self.set('details', JSON.stringify(self.details));
    // }
  }
}, {
  jsonColumns: ['details'],
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'type',
      'content',
      'view',
      'details',
      'notification_type',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user'
    ]
  }
});

module.exports = bookshelf.model('Notification', Notification);