let bookshelf = require('../lib/db'),
SubsConversation = bookshelf.Model.extend({
  tableName: 'subs_conversations',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'conversation_id',
      'is_deleted',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user'
    ]
  }
});

module.exports = bookshelf.model('SubsConversation', SubsConversation);