let bookshelf = require('../lib/db'),
ActivityTeacher = bookshelf.Model.extend({
  tableName: 'activity_teachers',
  hasTimestamps: true,
  activity: function() {
    return this.belongsTo('Activity', 'activity_id');
  },
  teacher: function() {
    return this.belongsTo('Teacher', 'teacher_id');
  },
}, {
  getAttributes: () => {
    return [
      'id',
      'teacher_id',
      'activity_id',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'activity',
      'teacher'
    ]
  }
});

module.exports = bookshelf.model('ActivityTeacher', ActivityTeacher);