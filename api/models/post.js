let bookshelf = require('../lib/db'),
Post = bookshelf.Model.extend({
  tableName: 'posts',
  hasTimestamps: true,
  user: function() {
    return this.belongsTo('User', 'user_id');
  },
  created_post_user: function() {
    return this.belongsTo('User', 'created_post_user_id');
  },
  academy: function() {
    return this.belongsTo('Academy', 'academy_id');
  },
  files: function() {
    return this.hasMany('PostFile', 'post_id');
  },
  comments: function() {
    return this.hasMany('PostComment', 'post_id');
  },
  likes: function() {
    return this.hasMany('PostLike', 'post_id');
  },
  virtuals: {
    distance_post: function() {

        if(user_login && user_login.data){
          var lat1 = user_login.data.latitude;
          var long1 = user_login.data.longitude;
          var lat2 = this.attributes.latitude;
          var long2 = this.attributes.longitude;

          // var R = 6372795;
          var R = 3963.1676;
          //перевод коордитат в радианы
          lat1 *= Math.PI / 180;
          lat2 *= Math.PI / 180;
          long1 *= Math.PI / 180;
          long2 *= Math.PI / 180;
          //вычисление косинусов и синусов широт и разницы долгот
          var cl1 = Math.cos(lat1);
          var cl2 = Math.cos(lat2);
          var sl1 = Math.sin(lat1);
          var sl2 = Math.sin(lat2);
          var delta = long2 - long1;
          var cdelta = Math.cos(delta);
          var sdelta = Math.sin(delta);
          //вычисления длины большого круга
          var y = Math.sqrt(Math.pow(cl2 * sdelta, 2) + Math.pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
          var x = sl1 * sl2 + cl1 * cl2 * cdelta;
          var ad = Math.atan2(y, x);
          var dist = ad * R; //расстояние между двумя координатами в метрах
          return dist || 0;

        } else {
          return 0;
        }
          
        
    }
  }
}, {
  getAttributes: () => {
    return [
      'id',
      'user_id',
      'title',
      'content',
      'public',
      'count_like',
      'count_comments',
      'latitude',
      'longitude',
      'share_from',
      'name_user_like',
      'academy_id',
      'type_post_customer',
      'created_post_user_id',
      'distance_post',
      'created_at',
      'updated_at'
    ];
  },
  getInclideAttributes: () => {
    return [
      'user',
      'academy',
      'files',
      'comments',
      'likes',
      'created_post_user',
    ]
  }
});

module.exports = bookshelf.model('Post', Post);