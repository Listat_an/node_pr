
'use strict';
const Shipping = require('../models').shipping;
const Academy = require('../models').academy;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Shipping});

    return helper.fetchPaginationData(normalizedQuery, Shipping, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Shipping.getAttributes(), Shipping.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Shipping.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Shipping"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        title: Joi.string().required(),
        price: Joi.number().required(),
        // academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        req.body.academy_id ? Academy.where({id: req.body.academy_id}).fetch() : null
      ]);
    }).then(findModel => {
      if(!findModel[0] && req.body.academy_id){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      
      return Shipping.forge(req.body).save();
    }).then((data) => {
      res.json(data.toJSON())
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        title: Joi.string().required(),
        price: Joi.number().required(),
        // academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Shipping.forge({id: req.params.id}).fetch(),
        req.body.academy_id ? Academy.where({id: req.body.academy_id}).fetch() : null
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Shipping', 404))
      }
      if(!findModel[1] && req.body.academy_id){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      return findModel[0].save(req.body, { method: 'update'})
    }).then(data => {
      res.json(data.toJSON())
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Shipping.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}