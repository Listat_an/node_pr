
'use strict';
const Notification = require('../models').notification;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Notification});

    return helper.fetchPaginationData(normalizedQuery, Notification, null, []).then((data) => {
      res.json(helper.indexResponse(data, Notification.getAttributes()))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.post('/view_all', (req, res) => {
    if(!req.body.type){
      req.body.type = 'user'
    }
    Notification.where({user_id: req.user.id, type: req.body.type}).save({view: true}, { method: 'update'}).then((data) => {
      res.json({status: true});
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Notification.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let notification;
    helper.validateData(req.body,
      Joi.object().keys({
        notification_type: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Notification.where({id: req.params.id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Notification', 404))
      }
      return findModel.save({notification_type: req.body.notification_type}, { method: 'update'})
    }).then(data => {
      return Notification.where({id: req.params.id}).fetch();
    }).then(data => {
      res.json(data.toJSON())
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/test', (req, res) => {
    helper.createNotification(req, req.user.id, 'Test conversation').then(() => {
      res.json({status: true})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  return router;
}