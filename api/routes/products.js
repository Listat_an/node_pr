
'use strict';
const Product = require('../models').product;
const Academy = require('../models').academy;
const Shipping = require('../models').shipping;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const Bluebird = require('bluebird');

const ProductImage = require('../models').product_image;

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Product});

    const Bookshelf = req.app.get('database');
    let ids=[];

    return Promise.resolve(true).then(() => {
      
      if (req.query.distantion){

        let latitude = req.query.latitude || -1;
        let longitude = req.query.longitude || 1;

        var distance_select = 'CEILING(' +
        '3963.1676 * acos ( ' +
        'cos ( radians(' + latitude + ') )' +
        '* cos( radians( academies.`latitude` ) )' +
        '* cos( radians( academies.`longitude` ) - radians(' + longitude + ') )' +
        '+ sin ( radians(' + latitude + ') )' +
        '* sin( radians( academies.`latitude` ) ) )' +
        ') AS distance ';
        return Bookshelf.knex.raw('SELECT id, ' + distance_select + '  FROM academies HAVING CEILING(distance) <= ' + req.query.distantion)
        // return Bookshelf.knex.raw('SELECT *, 69.0 * DEGREES(ACOS(COS(RADIANS(' + user.get('latitude') + ')) * COS(RADIANS(activity.latitude)) * COS(RADIANS(' + user.get('longitude') + ' - activity.longitude)) + SIN(RADIANS(' + user.get('latitude') + ')) * SIN(RADIANS(activity.latitude)))) AS distance FROM activity HAVING distance < ' + req.query.distantion )
      } else {
        return [[]];
      }
    
    }).then((data) => {

      if (req.query.distantion){        
        ids = data[0].map(item => item.id);
        normalizedQuery.filter.academy_id = {in: ids.toString()}
      }

      return helper.fetchPaginationData(normalizedQuery, Product, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Product.getAttributes(), Product.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Product});
    Product.where({id: req.params.id}).fetch({withRelated: normalizedQuery.include}).then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Product"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    
    let images = req.body.images || [];
    let product;
    helper.validateData(req.body,
      Joi.object().keys({
        title: Joi.string().required(),
        quantity: Joi.number().required(),
        price: Joi.number().required(),
        // shipping_id: Joi.number().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Academy.where({id: req.body.academy_id}).fetch(),
        // Shipping.where({id: req.body.shipping_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      // if(!findModel[1]){
      //   return Promise.reject(new AppError('Not found Shipping', 404))
      // }
     if (req.body.hasOwnProperty('images')){
       delete req.body.images;
     }
      return Product.forge(req.body).save();
    }).then((data) => {
      product = data;
      return Bluebird.map(images, (image, index) => {
        let dir = 'public/uploads/products/' + product.id+ '/';
        if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
        }
        let name = 'product_'+ product.id + '_' + Date.now() + index +'.png';
        return new Promise(function(resolve, reject) {
          fs.writeFile(dir + name, new Buffer(image.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
             if (err) reject(err);
             else resolve(dir + name);
          });
        }).then(url => {
          return ProductImage.forge({url: url.replace('public/', ''), product_id: product.id}).save();
        });
      })
    }).then((images) => {
      let data = product.toJSON();
      data['images'] = images;
      res.json(data)
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let images = req.body.images || [];
    let product;
    helper.validateData(req.body,
      Joi.object().keys({
        title: Joi.string().required(),
        quantity: Joi.number().required(),
        price: Joi.number().required(),
        // shipping_id: Joi.number().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Product.forge({id: req.params.id}).fetch(),
        Academy.where({id: req.body.academy_id}).fetch(),
        // Shipping.where({id: req.body.shipping_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Product', 404))
      }
      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      // if(!findModel[2]){
      //   return Promise.reject(new AppError('Not found Shipping', 404))
      // }
      if (req.body.hasOwnProperty('images')){
        delete req.body.images;
      }
      return findModel[0].save(req.body, { method: 'update'})
    }).then(data => {
      product = data;
      return ProductImage.where({product_id: product.id}).destroy()
    }).then(() => {
      return Bluebird.map(images, (image, index) => {
        let dir = 'public/uploads/products/' + product.id+ '/';
        if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
        }
        let name = 'product_'+ product.id + '_' + Date.now() + index +'.png';
        return new Promise(function(resolve, reject) {
          fs.writeFile(dir + name, new Buffer(image.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
             if (err) reject(err);
             else resolve(dir + name);
          });
        }).then(url => {
          return ProductImage.forge({url: url.replace('public/', ''), product_id: product.id}).save();
        });
      })
    }).then((images) => {
      let data = product.toJSON();
      data['images'] = images;
      res.json(data)
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Product.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}