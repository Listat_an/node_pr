
'use strict';
const Conversation = require('../models').conversation;
const SubsConversation = require('../models').subs_conversation;
const Message = require('../models').message;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const Manager = require('../models').manager;
const User = require('../models').user;
const Academy = require('../models').academy;

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/:id/messages', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Message});

    normalizedQuery.filter.conversation_id = req.params.id;
    normalizedQuery.include = ['user', 'user_sender', 'academy'];

    return helper.fetchPaginationData(normalizedQuery, Message, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Message.getAttributes(), Message.getInclideAttributes())))
    }).catch((err) => {
      console.log('err', err);
      return res.status(500).json(err)
    })
  });

  router.post('/add_message', (req, res) => {
    req.body.sender_id = req.user.id;
    let send_message;
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().integer().required(),
        content: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      if (req.body.conversation_id){
        return {id: req.body.conversation_id};
      }
      let academy_id = null;
      if(req.body.academy_id){
        academy_id = req.body.academy_id;
      }
      return helper.getConversation(req.body.user_id, req.body.sender_id, academy_id);
    }).then(conversation => {
      return Message.forge({
        user_id: req.body.user_id,
        sender_id: req.user.id,
        content: req.body.content,
        conversation_id: conversation.id,
        academy_id: req.body.academy_id || 0
      }).save();
    }).then(message => {
      return helper.sendMessage(req, message);
    }).then(message => {
      send_message = message;
      return Promise.all([
        Conversation.where({id: message.conversation_id}).fetch(),
        SubsConversation.where({conversation_id: message.conversation_id}).fetchAll(),
        Conversation.where({id: message.conversation_id}).save({updated_at: Date.now()}, { method: 'update'}),
      ]);
    }).then(([_conversation, sub_con]) => {
      
      let conversation = _conversation.toJSON();
      let user_ids = sub_con.map(item => item.get('user_id'));

      sub_con.forEach(item => {
        item.save({is_deleted: 0}, {method: 'update'});
      })

      if(conversation.academy_id && ~user_ids.indexOf(req.user.id)){
        return Promise.all([
         Manager.where({academy_id: conversation.academy_id}).fetchAll(),
          User.where({id: req.user.id}).fetch(),
          Academy.where({id: conversation.academy_id}).fetch()
        ]).then(([managers, _user, academy]) => {
          let user = _user.toJSON(0)
          managers.map(manager => {
            helper.createNotification(req, manager.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.first_name + ' ' + user.last_name + '</a> sent message by academy <a href="/#/academiy-datail/' + academy.id + '">' + academy.get('name') + '</a> <a class="btn-action" href="/#/academiy-datail/' + academy.id + '/messages">check message</a>', 'manager');
          });
          return null;
        })
      }
      return null;
    }).then(() => {
      res.json(send_message);
    }).catch(err => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Conversation});
    let conversations;
    SubsConversation.where({user_id: req.user.id, is_deleted: 0}).fetchAll().then(data => {
      let subs_conversation = data.toJSON();
      let conversation_ids = subs_conversation.map(item => item.conversation_id);

      if (normalizedQuery.filter.hasOwnProperty('academy_id')){
        normalizedQuery.filter.type = 'academy';
        normalizedQuery.filter.id = {in: conversation_ids.toString()};
      } else {
        normalizedQuery.filter.id = {in: conversation_ids.toString()};
      }
      
      return helper.fetchPaginationData(normalizedQuery, Conversation, null, [])
    }).then((data) => {
      conversations = data;
      let whereMessage = [];
      if(!data){
        return [];
      }
      conversations.map(item => {
        whereMessage.push(Message.query(function(qb){
          qb.where('conversation_id','in', [item.id]); 
          qb.orderBy('id','DESC'); 
        }).fetch({withRelated: ['user', 'user_sender']}));
      });

      return Promise.all(whereMessage);
    }).then(messages => {

      let result = helper.indexResponse(conversations, _.concat(Conversation.getAttributes(), Conversation.getInclideAttributes()))

      result.data = result.data.map(item => {
        item.last_message = null;
        return item;
      });

      messages.map(message => {
        if(message){
          let ms =  message.toJSON();
          let index = _.findIndex(result.data, ['id', ms.conversation_id]);
          if(~index){
            result.data[index]['last_message'] = ms;
          }
        }
      })
      
      res.json(result);
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  });

  router.post('/by_user', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return helper.getConversation(req.user.id, req.body.user_id);
    }).then(conversation => {
      return Promise.all([
        Conversation.where({id: conversation.id}).fetch({withRelated: ['users.user']}),
        Message.query(function(qb){
          qb.where('conversation_id','in', [conversation.id]); 
          qb.orderBy('id','DESC'); 
        }).fetch({withRelated: ['user', 'user_sender']})
      ]) 
    }).then(([conversation, message]) => {
      conversation = conversation.toJSON();
      conversation.last_message = null;
      if(message)
        conversation.last_message = message.toJSON();
        
      return res.json(conversation);
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/academy/by_user', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().integer().required(),
        academy_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return helper.getConversation(req.user.id, req.body.user_id, req.body.academy_id);
    }).then(conversation => {
      return Promise.all([
        Conversation.where({id: conversation.id}).fetch({withRelated: ['users.user', 'academy']}),
        Message.query(function(qb){
          qb.where('conversation_id','in', [conversation.id]); 
          qb.orderBy('id','DESC'); 
        }).fetch({withRelated: ['user', 'user_sender']})
      ]) 
    }).then(([conversation, message]) => {
      conversation = conversation.toJSON();
      conversation.last_message = null;
      if(message)
        conversation.last_message = message.toJSON();
        
      return res.json(conversation);
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/views', (req, res) => {
    let user_not_view = [], user_ids=[];
    let type = 'user';
    helper.validateData(req.body,
      Joi.object().keys({
        conversation_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        // SubsConversation.where({conversation_id: req.body.conversation_id}).fetchAll(),
        null,
        Conversation.where({id: req.body.conversation_id}).fetch()
      ]);
    }).then(([subs_con, conv]) => {
      console.log('​module.exports -> conv', conv);
      if(!conv.get('academy_id')){
        return null;
      }
      return  Manager.where({academy_id: conv.get('academy_id')}).fetchAll()
    }).then((managers) => {
      let where = {user_id: req.user.id, conversation_id: req.body.conversation_id, view: 0};
      // let user_ids= subs_con.map(item => item.get('user_id'));
      // if(conv.get('academy_id') && !~user_ids.findIndex(req.user.id)){
      //   where = {conversation_id: req.body.conversation_id};
      //   user_not_view = user_ids;
      // }



      user_ids = [req.user.id];

      if(managers){
        managers.forEach(item => { 
          if(item.get('user_id') == req.user.id){
            type = 'manager';
          }
        })
        user_ids = managers.map(item => item.get('user_id'));
        if(~user_ids.indexOf(req.user.id)){
          where = {conversation_id: req.body.conversation_id};
        } else {
          user_ids = [req.user.id]
        }
      }

      return Message.where(where).fetchAll();
    }).then(message => {
      if(type == 'manager'){
        user_ids.push(0);
      }
      message.forEach(element => {
        if(~user_ids.indexOf(element.get('user_id'))){
          element.set('view', 1);
          element.save()
        }
      });
      return res.json({
        status: true, 
      }) 
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })


  router.post('/unread_messages', (req, res) => {
    let user_id = req.user.id;
    if(req.body.user_id){
      user_id = req.body.user_id;
    }
    Message.where({user_id: user_id, view: 0}).fetchAll().then(messages => {
      res.json({count: messages.length})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/academy/unread_messages', (req, res) => {
    let user_id = req.user.id;
    if(req.body.user_id){
      user_id = req.body.user_id;
    }
    if(!req.body.academy_id){
      return res.status(400).json({message: 'Academy_id required'})
    }
    Message.where({view: 0, academy_id: req.body.academy_id}).fetchAll().then(messages => {
      res.json({count: messages.length})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/academy/unread_conversation', (req, res) => {
    let user_id = req.user.id;
    if(req.body.user_id){
      user_id = req.body.user_id;
    }
    if(!req.body.academy_id){
      return res.status(400).json({message: 'Academy_id required'})
    }
    Message.where({view: 0, academy_id: req.body.academy_id}).fetchAll().then(messages => {
      let conversation_id = [];
      messages.map(item => {
        if(!~conversation_id.indexOf(item.get('conversation_id'))){
          conversation_id.push(item.get('conversation_id'));
        }
      });
      res.json({count: conversation_id.length})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/create', (req, res) => {
    let conversation;
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Conversation});
    Conversation.forge({type: req.body.academy_id ? 'academy' : 'user', academy_id: req.body.academy_id}).save().then(conv => {
      conversation = conv;
      return Promise.all([
        SubsConversation.forge({conversation_id: conv.id, user_id: req.body.user_id}).save(),
        !req.body.academy_id ? SubsConversation.forge({conversation_id: conv.id, user_id: req.user.id}).save() : Manager.where({academy_id: req.body.academy_id}).fetchAll().then(menegars => {
          menegars.forEach(manager => {
            // if(user_id != manager.get('user_id')){
              SubsConversation.forge({conversation_id: conversation.id, user_id: manager.get('user_id')}).save();
            // }
          });
          return null;
        })
      ]);
    }).then(() => {
      return Conversation.where({id: conversation.id}).fetch({withRelated: normalizedQuery.include});
    }).then((data) => {
      res.json(data)
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.delete('/:id', (req, res) => {
    Conversation.where({ id: req.params.id }).fetch().then((conv) => {
      if(!conv){
        return Promise.reject(new AppError('Not found Conversation', 404))
      }
      return SubsConversation.where({user_id: req.user.id, conversation_id: req.params.id}).save({is_deleted: 1}, {method: 'update'})
    }).then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  })

  router.delete('/message/:id', (req, res) => {
    Message.where({ id: req.params.id }).fetch().then((message) => {
      if(!message){
        return Promise.reject(new AppError('Not found Message', 404))
      }
      return message.save({is_deleted: 1}, {method: 'update'})
    }).then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  })

  return router;
}