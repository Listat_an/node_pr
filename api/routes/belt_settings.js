
'use strict';
const Belt = require('../models').belt;
const Academy = require('../models').academy;
const BeltSetting = require('../models').belt_setting;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: BeltSetting});

    return helper.fetchPaginationData(normalizedQuery, BeltSetting, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(BeltSetting.getAttributes(), BeltSetting.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    BeltSetting.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found BeltSetting"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().integer().required(),
        belt_id: Joi.number().integer().required(),
        time_stripe_count: Joi.number().integer().required(),
        class_stripe_count: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Academy.forge({id: req.body.academy_id}).fetch(),
        Belt.forge({id: req.body.belt_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Belt', 404))
      }
      return BeltSetting.forge(req.body).save();
    }).then((belt) => {
      res.json(belt.toJSON())
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().integer().required(),
        belt_id: Joi.number().integer().required(),
        time_stripe_count: Joi.number().integer().required(),
        class_stripe_count: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        BeltSetting.forge({id: req.params.id}).fetch(),
        Academy.forge({id: req.body.academy_id}).fetch(),
        Belt.forge({id: req.body.belt_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found BeltSetting', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      if(!findModel[2]){
        return Promise.reject(new AppError('Not found Belt', 404))
      }
      return findModel[0].save(req.body, { method: 'update'})
    }).then((belt) => {
      res.json(belt.toJSON())
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    BeltSetting.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}