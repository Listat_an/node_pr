
'use strict';
const Stripe = require('../models').stripe;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Stripe});

    return helper.fetchPaginationData(normalizedQuery, Stripe, null, []).then((data) => {
      res.json(helper.indexResponse(data, Stripe.getAttributes()))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Stripe.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Stripe"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        belt_user_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return BeltUser.forge({id: req.body.belt_user_id}).fetch();
    }).then(findModel =>{

      if(!findModel){
        return Promise.reject(new AppError('Not found belt_user', 404))
      }

      return Stripe.forge(req.body).save()
    }).then((data) => {
      res.json(data.toJSON())
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        belt_user_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return BeltUser.forge({id: req.body.belt_user_id}).fetch();
    }).then(findModel =>{

      if(!findModel){
        return Promise.reject(new AppError('Not found belt_user', 404))
      }
      return Page.forge({id: req.params.id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Stripe', 404))
      }
      return findModel.save(req.body, { method: 'update'})
    }).then(data => {
      res.json(data.toJSON())
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Stripe.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}