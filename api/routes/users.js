
'use strict';
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const helperEmail = require('../lib/helperEmail');
const Stripe = require('../models').stripe;
const Academy = require('../models').academy;
const PostFile = require('../models').post_file;
const BeltSetting = require('../models').belt_setting;
const Belt = require('../models').belt;
const BeltUser = require('../models').belt_user;
const Follower = require('../models').follower;
const AcademyFollower = require('../models').academy_follower;
const SubscriptionAcademy = require('../models').subscription_academy;
const JoinActivity = require('../models').join_activity;
const Checkin = require('../models').checkin;
const UserPromotion = require('../models').user_promotion_timeline;
const Joi = require('joi');
const fs = require('fs');
const moment = require('moment');
const _ = require('lodash');
const ExternalServices = require('../lib/external_services');
const bcrypt = require('bcrypt'),
  config = require('../config'),
  jwt = require('jsonwebtoken'),
  crypto = require('crypto');

const UserTimeline = require('../models').user_timeline;
const UserSubscription = require('../models').user_subscription;
const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    const Bookshelf = req.app.get('database');
    let ids=[];
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: User});
    return Promise.resolve(true).then(() => {
      
      if (req.query.distantion){

        let latitude = req.query.latitude || -1;
        let longitude = req.query.longitude || 1;

        var distance_select = 'CEILING(' +
        '3963.1676 * acos ( ' +
        'cos ( radians(' + latitude + ') )' +
        '* cos( radians( users.`latitude` ) )' +
        '* cos( radians( users.`longitude` ) - radians(' + longitude + ') )' +
        '+ sin ( radians(' + latitude + ') )' +
        '* sin( radians( users.`latitude` ) ) )' +
        ') AS distance ';
        return Bookshelf.knex.raw('SELECT id, ' + distance_select + '  FROM users HAVING CEILING(distance) <= ' + req.query.distantion)
        // return Bookshelf.knex.raw('SELECT *, 69.0 * DEGREES(ACOS(COS(RADIANS(' + user.get('latitude') + ')) * COS(RADIANS(activity.latitude)) * COS(RADIANS(' + user.get('longitude') + ' - activity.longitude)) + SIN(RADIANS(' + user.get('latitude') + ')) * SIN(RADIANS(activity.latitude)))) AS distance FROM activity HAVING distance < ' + req.query.distantion )
      } else {
        return [[]];
      }
    
    }).then((data) => {

      if (req.query.distantion){        
        ids = data[0].map(item => item.id);
        normalizedQuery.filter.id = {in: ids.toString()}
      }
      return helper.fetchPaginationData(normalizedQuery, User, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(User.getAttributes(), User.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/me', (req, res) => {
    let user;
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: User});
    if(normalizedQuery.include.indexOf('academy') < 0){
      normalizedQuery.include.push('academy');
    }
    console.log('include', normalizedQuery.include)
    User.where({id: req.user.id}).fetch({withRelated: normalizedQuery.include}).then(findUsers => {
      if(!findUsers){
        return res.status(404).json({message: "Not found user"})
      }
      user = findUsers.toJSON();

      UserSubscription.where({user_id: req.user.id, is_cancel_time: 1}).fetchAll().then(subs_user => {
        subs_user.forEach(item => {
          if(moment() > moment.unix(item.get('billing_cycle_anchor'))){
            item.save({status: 0, is_cancel_time: 0}, {method: 'update'});
          }
        });
      })

      let updateAcademy = {
        academy_subscription_id: null, 
        stripe_subscription_id: null, 
        is_payd_subscription: 0,
        promotion_method: null,
        is_pro: 0,
        promotion_time: null,
        promotion_attendance: null,
        current_period_end: '',
        billing_cycle_anchor: '',
        trial_end: '',
        is_cancel_time: 0,
      }

      if(user.academy && user.academy.is_cancel_time){
        if(moment() > moment.unix(user.academy.billing_cycle_anchor)){
          Object.assign(user.academy, updateAcademy);
          User.where({academy_id: user.academy_id, is_student: 1}).fetchAll().then(students => {
            students.forEach(student => {
              helper.createNotification(req, student.get('id'), 'Academy PRO account canceled and all premium features are no longer active for you');
            });
          })
          Academy.where({id: user.academy_id}).save(updateAcademy, {method: 'update'})
        }
      }

      return BeltUser.where({user_id: user.id}).fetch();
    }).then((findBeltUser) => {
      if(!findBeltUser){
        return null;
      }

      return Belt.forge({id: findBeltUser.get('belt_id')}).fetch({withRelated: ['belt_color']});
    }).then((_belt) => {
      if(_belt){
        user.belt = _belt.toJSON();
      }
      return SubscriptionAcademy.where({user_id: user.id}).fetchAll({withRelated: ['academy']});
    }).then((academies) => {
      if(academies){
        user.subscription_academies = academies.toJSON();
      }
      return Promise.all([
        PostFile.where({user_id: req.user.id, type: 'image'}).fetchAll(),
        PostFile.where({user_id: req.user.id, type: 'video'}).fetchAll(),
        Follower.where({user_id: req.user.id}).fetchAll(),
        Follower.where({friend_id: req.user.id}).fetchAll(),
        AcademyFollower.where({user_id: req.user.id}).fetchAll()
      ])
    }).then(([images, videos, followings, followers, followings_cademy]) => {
      user.count_images = images ? images.length : 0;
      user.count_videos = videos ? videos.length : 0;
      user.count_followers = followers ? followers.length : 0;
      user.count_followings = followings ? followings.length : 0;
      user.count_followings = followings_cademy ? user.count_followings + followings_cademy.length : user.count_followings;
      res.json(_.omit(user, ['password', 'reset_token_password']));
    }).catch(err => {
      console.log('err', err)
      res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    let user;
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: User});
    if(normalizedQuery.include.indexOf('academy') < 0){
      normalizedQuery.include.push('academy');
    }
    console.log('include', normalizedQuery.include)
    User.where({id: req.params.id}).fetch({withRelated: normalizedQuery.include}).then(findUsers => {
      if(!findUsers){
        return res.status(404).json({message: "Not found user"})
      }
      user = findUsers.toJSON();
      return BeltUser.where({user_id: user.id}).fetch();
    }).then((findBeltUser) => {
      if(!findBeltUser){
        return null;
      }

      return Belt.forge({id: findBeltUser.get('belt_id')}).fetch({withRelated: ['belt_color']});
    }).then((_belt) => {
      if(_belt){
        user.belt = _belt.toJSON();
      }
      return SubscriptionAcademy.where({user_id: user.id}).fetchAll({withRelated: ['academy']});
    }).then((academies) => {
      if(academies){
        user.subscription_academies = academies.toJSON();
      }
      return Promise.all([
        PostFile.where({user_id: req.params.id, type: 'image'}).fetchAll(),
        PostFile.where({user_id: req.params.id, type: 'video'}).fetchAll(),
        Follower.where({user_id: req.params.id}).fetchAll(),
        Follower.where({friend_id: req.params.id}).fetchAll(),
        AcademyFollower.where({user_id: req.params.id}).fetchAll()
      ])
    }).then(([images, videos, followings, followers, followings_cademy]) => {
      user.count_images = images ? images.length : 0;
      user.count_videos = videos ? videos.length : 0;
      user.count_followers = followers ? followers.length : 0;
      user.count_followings = followings ? followings.length : 0;
      user.count_followings = followings_cademy ? user.count_followings + followings_cademy.length : user.count_followings;
      res.json(_.omit(user, ['password', 'reset_token_password']));
    }).catch(err => {
      console.log('err', err)
      res.status(500).json(err)
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let userData = req.body.user || {};
    let user, updateUser, userId, findUser;
    let belt_id = null;

    let avatar_base = null;
    let cover_photo_base = null;
    if(req.body.avatar_base){
      avatar_base = req.body.avatar_base;
    }
    if(req.body.cover_photo_base){
      cover_photo_base = req.body.cover_photo_base;
    }

    if(req.body.user && req.body.user.belt_id){
      belt_id = req.body.user.belt_id
      delete req.body.user.belt_id;
    }

    Promise.all([
      User.forge({id: req.params.id}).fetch(),
      (userData.password) ? bcrypt.hash(userData.password, 12) : null
    ])
   .then(([_user, hashedPsw]) => {
      if(!_user){
        return Promise.reject(new AppError("Not found user", 404));
      }
      findUser = _user.toJSON();
      if (hashedPsw){
        userData.password = hashedPsw;
      }
      return _user.save(userData, { method: 'update'});
    }).then(user => {
      updateUser = user; 

      if(updateUser.get('email') != findUser.email){
        return ExternalServices.sendMail({
          from: config.from_email,
          to: findUser.email,
          subject: 'Change email',
          html: helperEmail.getTemplateEmail('change_email', {})
        });
      }
      return null;
    }).then(() => {
      if(updateUser.get('password') != findUser.password){
        return ExternalServices.sendMail({
          from: config.from_email,
          to: updateUser.get('email'),
          subject: 'Change password',
          html: helperEmail.getTemplateEmail('change_password', {})
        });
      }
      return null;
    }).then(() => {
      if (!belt_id) {
          return [null];
      }
      return Promise.all([
        Belt.forge({ id: belt_id }).fetch(),
        BeltUser.where({ user_id: updateUser.id }).fetch(),
      ]) 
    }).then((findModels) => {
        if (!findModels[0]) {
            return null;
        }
        if(!findModels[1]){
          return BeltUser.forge({ user_id: updateUser.id, belt_id }).save();
        } else {
          return findModels[1].save({belt_id}, { method: 'update'});
        }
        
    }).then((beltUser) => {
        if (!beltUser) {
            return null;
        }
        let promiseAllModel = [];
        if (req.body.user.stripe) {
            for (var index = 0; index < req.body.user.stripe; index++) {
                promiseAllModel.push(Stripe.forge({ belt_user_id: beltUser.id, name: '' }).save());
            }
        }
        if (promiseAllModel.length > 0)
            return Promise.all(promiseAllModel)
        else
            return null;
    }).then(() => {
      return new Promise((resolve, reject) => {
        var storage = multer.diskStorage({
          destination: function (req, file, cb) {
            let dir = 'public/uploads/users/' + req.params.id + '/';
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            cb(null, dir)
          },
          filename: function (req, file, cb) {
            cb(null, file.originalname)
          }
        });
        
        var upload = multer({ storage: storage }).fields([{ name: 'avatar', maxCount: 1 }, { name: 'cover_photo', maxCount: 1 }]);
        upload(req, res, function (err) {
          if (err) {
            return reject(err)
          }
          return resolve(true)
        })
      });
    }).then(() => {
      if (!req.files){
        return null;
      }

      let data = {};
      if(req.files.avatar){
        data.avatar = (req.files.avatar[0].destination + req.files.avatar[0].originalname).replace('public/', '');
      }

      if(req.files.cover_photo){
        data.cover_photo = (req.files.cover_photo[0].destination + req.files.cover_photo[0].originalname).replace('public/', '');
      }

      return updateUser.save(data, { method: 'update'})
    }).then(() => {

      if (!avatar_base){
        return null;
      }
      let dir = 'public/uploads/users/' + req.params.id+ '/';
      if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
      }

      return new Promise(function(resolve, reject) {
        fs.writeFile(dir + 'avatar.png', new Buffer(avatar_base.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
           if (err) reject(err);
           else resolve(dir + 'avatar.png');
        });
      });
    }).then(avatar_url => {
      if(!avatar_url){
        return null;
      }
      return updateUser.save({avatar: avatar_url.replace('public/', '')}, { method: 'update'})

    }).then(() => {
      if (!cover_photo_base){
        return null;
      }
      let dir = 'public/uploads/users/' + req.params.id + '/';
      if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
      }

      return new Promise(function(resolve, reject) {
        fs.writeFile(dir + 'cover_photo.png', new Buffer(cover_photo_base.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
           if (err) reject(err);
           else resolve(dir + 'cover_photo.png');
        });
      });
    }).then(cover_photo_url => {
      if(!cover_photo_url){
        return null;
      }
      return updateUser.save({cover_photo: cover_photo_url.replace('public/', '')}, { method: 'update'})
    }).then(() => {
      if(req.body.user && req.body.user.academy_id){
        return SubscriptionAcademy.where({}).fetch().then(s_a => {
          if(!s_a){
            return SubscriptionAcademy.forge({user_id: updateUser.id, academy_id: req.body.user.academy_id}).save();
          }
          return s_a.save({academy_id: req.body.user.academy_id}, { method: 'update'})
        }) 
      }
      if(!req.body.academy_name){
          return null;
      }

      return Academy.forge({name: req.body.academy_name, status: 'Not_created'}).save().then(academyCreate => {
        return SubscriptionAcademy.forge({user_id: updateUser.id, academy_id: academyCreate.id}).save();
      });
    }).then(() => {
      res.json(_.omit(updateUser.toJSON(), ['password', 'reset_token_password']));
    }).catch((err) => {
      console.log('err', err)
      return helper.errorResponse(res, [ err ]);
    });
  });

  router.post('/send_confirmation', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        id: Joi.number().required(),
      }), { abortEarly: false }
    ).then(validBody => {
      return User.where({id: req.body.id}).fetch();
    }).then(findUser => {
      if(!findUser){
        return Promise.reject(new AppError("Not found user", 404));
      }
      let active_kode = crypto.randomBytes(25).toString('hex');
      if(findUser.get('active_kode')){
          active_kode = findUser.get('active_kode');
      }
      return findUser.save({active_kode}, { method: 'update'})
    }).then(_user => {
      let user = _user.toJSON();
      return ExternalServices.sendMail({
        from: config.from_email,
        to: user.email,
        subject: 'Confirmation',
        html: helperEmail.getTemplateEmail('confirmation', {
          user_name: user.first_name + ' ' + user.last_name,
          confirmation_url: config.site_front + '/#/auth/login/' + user.active_kode,
          site_url: config.site_front
        })
      });
    }).then(_user => {
      return res.json({status: true, message: 'Email send'})
    }).catch(err => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    });
  })


  router.post('/academy/status', (req, res) => {
    let user;
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().required(),
        academy_id: Joi.number().required(),
        status: Joi.boolean().required(),
      }), { abortEarly: false }
    ).then(validBody => {
      return User.where({id: req.body.user_id}).fetch();
    }).then(findUser => {
      if(!findUser){
        return Promise.reject(new AppError("Not found user", 404));
      }

      if(findUser.get('academy_id') != req.body.academy_id){
        return Promise.reject(new AppError("Not found academy in user", 404));
      }
      user = findUser
      return findUser.save({academy_status: req.body.status}, { method: 'update'})
    }).then(user => {
      return res.json({status: true, user})
    }).catch(err => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    });
  })

  router.post('/cards', (req, res) => {
    let user_id = req.body.user_id || req.user.id;
    User.forge({id: req.user.id}).fetch().then(findUser => {
      if(!findUser){
        return Promise.reject(new AppError("Not found user", 404));
      }

      if (findUser.get('stripe_customer_id')){
        return findUser;
      }
      return ExternalServices.createStripeCustomer({email: findUser.get('email')}).then(data => {
        findUser.set('stripe_customer_id', data.id);
        return findUser.save();
      });
    }).then(user => {
      return ExternalServices.retrieveStripeCustomer(user.get('stripe_customer_id'));
    }).then(customer => {

      let data = customer && customer.sources && customer.sources.data ? customer.sources.data  : [];

      data.forEach(element => {
        element.defaul = false;
        if(customer.default_source == element.id){
          element.defaul = true;
        }
      });

      return res.json(data);
    }).catch(err => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    });
  })

  router.post('/belt', (req, res) => {
    let belt, user;
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().required(),
        belt_id: Joi.number().required(),
      }), { abortEarly: false }
    ).then(validBody => {
      return Promise.all([
        User.where({id: req.body.user_id}).fetch(),
        Belt.where({id: req.body.belt_id}).fetch({withRelated: ['belt_color']}),
      ]);
    }).then(([findUser, findBelt]) => {
      if(!findUser){
        return Promise.reject(new AppError("Not found user", 404));
      }
      if(!findBelt){
        return Promise.reject(new AppError("Not found belt", 404));
      }
      user = findUser.toJSON();
      belt = findBelt.toJSON();
      return BeltUser.where({user_id: req.body.user_id}).destroy()
    }).then(() => {
      return BeltUser.forge(req.body).save();
    }).then(() => {
      UserTimeline.forge({
        data: 'New belt received',
        user_id: req.body.user_id,
        type: 'belt',
        details: belt,
        academy_id: user.academy_id
      }).save()
      UserPromotion.where({user_id: req.body.user_id, academy_id: user.academy_id}).destroy();
      helper.createNotification(req, req.body.user_id, 'Your Academy has been promoted you. You receive new ' + (belt.stripe > 0 ? 'stripe' : 'belt'));
      res.json({status: true, belt})
    }).catch(err => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    });
  })

  router.all('/next_level', (req, res) =>{
    let academy;
    let user;
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().integer().required(),
        user_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Academy.forge({id: req.body.academy_id}).fetch(),
        User.forge({id: req.body.user_id}).fetch(),
        BeltUser.forge({user_id: req.body.user_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found User', 404))
      }
      
      if(!findModel[2]){
        return Promise.reject(new AppError('Not found BeltUser', 404))
      }

      academy = findModel[0].toJSON();
      user = findModel[1].toJSON();

      return Belt.where({id: findModel[2].get('belt_id')}).fetch();
    }).then((belt) => {
      if(!belt){
        return Promise.reject(new AppError('Not found belt', 404))
      }
      return Belt.where({name: belt.get('name'), stripe: 0}).fetch();
    }).then((belt) => {
      if(!belt){
        return Promise.reject(new AppError('Not found belt', 404))
      }
      return Promise.all([
        BeltSetting.where({belt_id: belt.get('id'), academy_id: academy.id}).fetch(),
        belt
      ]);
    }).then((belt) => {
      if(!belt[0] && !belt[1]){
        return Promise.reject(new AppError('Not found belt', 404))
      }
      let belt_j = belt[0] ? belt[0].toJSON() : belt[1].toJSON();
      
      if(academy.promotion_method == 'time'){
        return helper.nextLevelTime(belt_j.time_stripe_count, academy.id, req.body.user_id);
      } else {
        return helper.nextLevelClass(belt_j.class_stripe_count, academy.id, req.body.user_id);
      }

      return 0;
    }).then(([level, total_level]) => {
      res.json({next_level: level, total_level: total_level})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.all('/change_card', (req, res) =>{
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().required(),
        source: Joi.string().required(),
      }), { abortEarly: false }
    ).then(validBody => {
      return User.where({id: req.body.user_id}).fetch();
    }).then(user => {
      if(!user){
        return Promise.reject(new AppError("Not found user", 404));
      }
      let _user = user.toJSON();
      if(_user.stripe_customer_id){
        return {id: _user.stripe_customer_id};
      }
      return ExternalServices.createStripeCustomer({email: _user.email}).then(data => {
        user.set('stripe_customer_id', data.id);
        user.save();
        return data;
      });
    }).then(user => {
      return ExternalServices.updateStripeCustomer(user.id, {source: req.body.source});
    }).then((customer) => {    
      let data = customer && customer.sources && customer.sources.data ? customer.sources.data  : [];

      data.forEach(element => {
        element.defaul = false;
        if(customer.default_source == element.id){
          element.defaul = true;
        }
      });  
      res.json({status: true, card: data})
    }).catch(err => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    });
  })

  router.delete('/:id', async (req, res) => {
    try {
      await User.forge({ id: req.params.id }).destroy();
      await require('../models/academy_follower').where({user_id: req.params.id}).destroy();
      res.json({status: true});
    } catch (error) {
      helper.errorResponse(res, [ err ]);
    }
  })

  router.post('/blocked', async (req, res) => {
    try {
      await helper.validateData(req.body,
        Joi.object().keys({
          user_id: Joi.number().integer().required(),
          status: Joi.boolean().required(),
        }), { abortEarly: false, allowUnknown: true }
      );

      let user = await User.where({id: req.body.user_id}).fetch();

      if(!user){
        throw new AppError('Not found user', 404)
      }

      await User.where({id: req.body.user_id}).save({is_blocked: req.body.status}, {method: 'update'})

      res.json({status: true});
    } catch (error) {
      helper.errorResponse(res, [ err ]);
    }
  });

  return router;
}