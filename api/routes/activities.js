
'use strict';
const Activity = require('../models').activity;
const ActivityTeacher = require('../models').activity_teacher;
const JoinActivity = require('../models').join_activity;
const ActivityTechnique = require('../models').activity_technique;
const Academy = require('../models').academy;
const ShareActivity = require('../models').share_activity;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const Bluebird = require('bluebird');
const SubscriptionActivity = require('../models').subscription_activity;
const Teacher = require('../models').teacher;
const Subscription = require('../models').subscription;
const Technique = require('../models').technique;
const AcademyFollower = require('../models').academy_follower;
const ExtarnalService = require('../lib/external_services');
const moment = require('moment');
const Manager = require('../models').manager;
const UserSubscription = require('../models').user_subscription;
const Transaction = require('../models').transaction;
const Checkin = require('../models').checkin;
const UserTimeline = require('../models').user_timeline;
const Schedule = require('../models').schedule;
const CalendarSubscription = require('../models').calendar_subscription;

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    const Bookshelf = req.app.get('database');
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});

    console.log('normalizedQuery', normalizedQuery)

    let ids = [];

    let idsDistantion = [];
    let idsJoined = [];
    let idsTechers = [];

    return User.forge({id: req.user.id}).fetch().then((user) => {
      if(!user){
        return Promise.reject(new AppError('Not found User', 404))
      }
      if (req.query.distantion){

        let latitude = req.query.latitude || user.get('latitude');
        let longitude = req.query.longitude || user.get('longitude');

        var distance_select = 'CEILING(' +
        '3963.1676 * acos ( ' +
        'cos ( radians(' + latitude + ') )' +
        '* cos( radians( activity.`latitude` ) )' +
        '* cos( radians( activity.`longitude` ) - radians(' + longitude + ') )' +
        '+ sin ( radians(' + latitude + ') )' +
        '* sin( radians( activity.`latitude` ) ) )' +
        ') AS distance ';
        return Bookshelf.knex.raw('SELECT id, ' + distance_select + '  FROM activity HAVING CEILING(distance) <= ' + req.query.distantion)
        // return Bookshelf.knex.raw('SELECT *, 69.0 * DEGREES(ACOS(COS(RADIANS(' + user.get('latitude') + ')) * COS(RADIANS(activity.latitude)) * COS(RADIANS(' + user.get('longitude') + ' - activity.longitude)) + SIN(RADIANS(' + user.get('latitude') + ')) * SIN(RADIANS(activity.latitude)))) AS distance FROM activity HAVING distance < ' + req.query.distantion )
      } else {
        return [[]];
      }
    
    }).then((data) => {
      // console.log('data', data)
      // data[0].map(item => {
      //   console.log(item)
      // })
      if (req.query.distantion){        
        idsDistantion = data[0].map(item => item.id);
        // data[0].map(item => {
        //   console.log(item)
        // });
      }

      if (req.query.join_activity){
        return JoinActivity.where({user_id: req.user.id}).fetchAll().then(activities => {
          return activities.map(item => item.get('activity_id'));
        }).then(joinIds => {
          return UserSubscription.where({user_id: req.user.id, status: 1}).fetchAll().then(subs => {
            return SubscriptionActivity.where('subscription_id', 'in', subs.map(item => item.get('subscription_id'))).fetchAll()
          }).then(subscriptionActivitys => {
            idsJoined =  _.concat(joinIds, subscriptionActivitys.map(item => item.get('activity_id')));
            return _.concat(joinIds, subscriptionActivitys.map(item => item.get('activity_id')));
          });
        })
      }

      return [];
    }).then(idsJoin => {
      if (req.query.teacher_ids){
        return ActivityTeacher.where('teacher_id', 'in', req.query.teacher_ids.split(',')).fetchAll().then(_findModel1 => {
          let findModel = _findModel1.toJSON();
          findModel.forEach(item => {
            idsTechers.push(item.activity_id)
            idsJoin.push(item.activity_id)
          });
          return idsJoin;
        })
      }
      return idsJoin;
    }).then(idsJoin => {

      console.log('idsJoined', idsJoined)
      console.log('idsTechers', idsTechers)
      console.log('idsDistantion', idsDistantion)

      if (req.query.join_activity){
        normalizedQuery.filter.id = {in: idsJoined.toString()}
      } else {

        if(req.query.distantion){
          normalizedQuery.filter.id = {in: idsDistantion.toString()};
          let tempIds = []
          if (req.query.teacher_ids){
            idsTechers.forEach(item => {
              if(~idsDistantion.indexOf(item)){
                tempIds.push(item);
              }
            });
            normalizedQuery.filter.id = {in: tempIds.toString()};
          }
        } else {
          if (req.query.teacher_ids){
            normalizedQuery.filter.id = {in: idsTechers.toString()};
          }
        }
      }

      return Promise.all([
        helper.fetchPaginationData(normalizedQuery, Activity, null, []),
        JoinActivity.where({user_id: req.user.id}).fetchAll(),
        Checkin.where({user_id: req.user.id}).fetchAll()
      ]) 
    }).then(([_data, _joineds, _checkins]) => {
      let data = helper.indexResponse(_data, _.concat(Activity.getAttributes(), Activity.getInclideAttributes()));
      let joineds = _joineds.map(item => item.get('activity_id'));
      let checkins = _checkins.map(item => item.get('activity_id'));
      data.data.map(item => {
        item.isJoined = false;
        item.isCheckined = false;
        if(~joineds.indexOf(item.id)){
          item.isJoined = true;
        }
        if(~checkins.indexOf(item.id)){
          item.isCheckined = true;
        }
      })
      res.json(data);
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  });


  router.get('/classes', (req, res) => {

    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});

    return JoinActivity.where({user_id: req.user.id}).fetchAll().then((shareActivity) => {
      if(!shareActivity){
        return []
      }
      let ids = [];
      shareActivity.forEach(element => {
        ids.push(element.get('activity_id'));
      });
      return ids;
    
    }).then((ids) => {
      normalizedQuery.filter.id = {in: ids.toString()};
      normalizedQuery.filter.type = 'class';
      return helper.fetchPaginationData(normalizedQuery, Activity, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Activity.getAttributes(), Activity.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/classes_shedule_manager', async (req, res) =>{
    try {
      let result = {};
      
      if(!req.query.from || !moment(req.query.from).isValid()){
        throw new AppError('Not params from or valid', 404);
      }

      if(!req.query.to || !moment(req.query.to).isValid()){
        throw new AppError('Not params to or valid', 404);
      }

      if(!req.query.academy_id){
        throw new AppError('Not params academy_id', 404);
      }

      let academy = await Academy.where({id: req.query.academy_id}).fetchAll();

      if(!academy){
        throw new AppError('Not found academy', 404);
      }

      let from = moment(req.query.from);
      let to = moment(req.query.to);

      let classes = await Activity.where({academy_id: req.query.academy_id, type: 'class'}).fetchAll({});

      let shedules = await Schedule.where('class_id', 'in', classes.map(item => item.id)).fetchAll({withRelated: ['class.subscriptions.subscription']});
      if(shedules){
        shedules = shedules.toJSON();
      }
      
      let shedules_day = {};
      let classes_id = {};
      shedules.forEach(item => {
        if(shedules_day[item.day]){
          shedules_day[item.day].push(item);
        } else {
          shedules_day[item.day] = [item]
        }

        if(!classes_id[item.class_id]){
          classes_id[item.class_id] = item.class;
        }

      })


      for (let index = from; index <= to; index.add(1, 'day')) {
        let day = index.format('ddd');

        let tmp_res = {};

        if(shedules_day.hasOwnProperty(day)){
          
          shedules_day[day].forEach(item => {
            if(tmp_res[item.class_id]){
              tmp_res[item.class_id].push(item);
            } else {
              tmp_res[item.class_id] = [item]
            }
          })
        }

        
        result[index.format('YYYY-MM-DD')] = Object.keys(tmp_res).map(class_id => {
          let tmp_class = _.cloneDeep(classes_id[class_id]);
          tmp_class.shedules = _.cloneDeep(tmp_res[class_id]);
          tmp_class.shedules.forEach(item => delete item.class)
          return tmp_class;
        });
      }


      res.json({status: true, data: result});
    } catch (error) {
      helper.errorResponse(res, [ error ]);
    }
  })

  router.get('/classes_shedule_manager_holiday', async (req, res) =>{
    try {
      let result = {};
      
      // if(!req.query.from || !moment(req.query.from).isValid()){
      //   throw new AppError('Not params from or valid', 404);
      // }

      // if(!req.query.to || !moment(req.query.to).isValid()){
      //   throw new AppError('Not params to or valid', 404);
      // }

      if(!req.query.academy_id){
        throw new AppError('Not params academy_id', 404);
      }

      let academy = await Academy.where({id: req.query.academy_id}).fetchAll();

      if(!academy){
        throw new AppError('Not found academy', 404);
      }

      let from = moment(req.query.from);
      let to = moment(req.query.to);

      let classes = await Activity.where({academy_id: req.query.academy_id, type: 'class'}).fetchAll({});

      let shedules = await Schedule.where({holiday: '1'}).where('class_id', 'in', classes.map(item => item.id)).fetchAll({withRelated: ['class.subscriptions.subscription']});
      if(shedules){
        shedules = shedules.toJSON();
      }
      
      let shedules_day = {};
      let classes_id = {};
      shedules.forEach(item => {
        if(shedules_day[moment(item.date).format('YYYY-MM-DD')]){
          shedules_day[moment(item.date).format('YYYY-MM-DD') ].push(item);
        } else {
          shedules_day[moment(item.date).format('YYYY-MM-DD') ] = [item]
        }

        if(!classes_id[item.class_id]){
          classes_id[item.class_id] = item.class;
        }

      });


      Object.keys(shedules_day).forEach(date => {

        let tmp_res = {};

        shedules_day[date].forEach(item => { 
          if(tmp_res[item.class_id]){
            tmp_res[item.class_id].push(item);
          } else {
            tmp_res[item.class_id] = [item]
          }
        });

         result[date] = Object.keys(tmp_res).map(class_id => {
          let tmp_class = _.cloneDeep(classes_id[class_id]);
          tmp_class.shedules = _.cloneDeep(tmp_res[class_id]);
          tmp_class.shedules.forEach(item => delete item.class)
          return tmp_class;
        });       

      })


      // for (let index = from; index <= to; index.add(1, 'day')) {
      //   let day = index.format('ddd');

      //   let tmp_res = {};

      //   if(shedules_day.hasOwnProperty(day)){
      //     shedules_day[day].forEach(item => {
      //       if(moment(item.date).format('YYYY-MM-DD') == index.format('YYYY-MM-DD')){
              
      //         if(tmp_res[item.class_id]){
      //           tmp_res[item.class_id].push(item);
      //         } else {
      //           tmp_res[item.class_id] = [item]
      //         }
      //       }
      //     })
      //   }

        
      //   result[index.format('YYYY-MM-DD')] = Object.keys(tmp_res).map(class_id => {
      //     let tmp_class = _.cloneDeep(classes_id[class_id]);
      //     tmp_class.shedules = _.cloneDeep(tmp_res[class_id]);
      //     tmp_class.shedules.forEach(item => delete item.class)
      //     return tmp_class;
      //   });
      // }


      res.json({status: true, data: result});
    } catch (error) {
      helper.errorResponse(res, [ error ]);
    }
  })

  router.get('/classes_shedule_student', async (req, res) =>{
    try {
      let result = {};
      
      if(!req.query.from || !moment(req.query.from).isValid()){
        throw new AppError('Not params from or valid', 404);
      }

      if(!req.query.to || !moment(req.query.to).isValid()){
        throw new AppError('Not params to or valid', 404);
      }

      // if(!req.query.academy_id){
      //   throw new AppError('Not params academy_id', 404);
      // }

      if(!req.query.type){
        throw new AppError('Not params type', 404);
      }


      let subscription_activitys = null;
      if(req.query.is_join){
        let ModelUserSubscription = UserSubscription.where({user_id: req.user.id, status: 1});
        let subscriptions = await ModelUserSubscription.fetchAll();
        subscriptions = subscriptions.toJSON();
        subscription_activitys = await SubscriptionActivity.where('subscription_id', 'in',  subscriptions.map(item => item.subscription_id)).fetchAll();
        subscription_activitys = subscription_activitys.toJSON();
      } else {
        if(req.query.subscription_ids){
          // ModelUserSubscription.where('subscription_id', 'in', req.query.subscription_ids.split(','))
          subscription_activitys = await SubscriptionActivity.where('subscription_id', 'in',  req.query.subscription_ids.split(',')).fetchAll();
          subscription_activitys = subscription_activitys.toJSON();
        }
      }


      let from = moment(req.query.from);
      let to = moment(req.query.to);

      req.query.to = to.clone().add(1, 'day').format('YYYY-MM-DD')

      let classes = [];
      let activity = [];
      if(req.query.type == 'class'){

        let whereModelActivity = {type: 'class'}

        if(req.query.academy_id){
          whereModelActivity.academy_id = req.query.academy_id;
        }

        let ModelActivity = Activity.where(whereModelActivity);
        if(subscription_activitys){
          ModelActivity = ModelActivity.where('id', 'in', subscription_activitys.map(item => item.activity_id));
        }
        classes = await ModelActivity.fetchAll({});
        console.log('TCL: classes', classes);
      } else {

        let whereActivity = {type: req.query.type}
        if(req.query.academy_id){
          whereActivity.academy_id = req.query.academy_id;
        }
        
        activity = Activity.query(function (db) {
          db.where(whereActivity)
          // db.orWhere(query.filterOr);

          if(req.query.payment_status){
            activity = db.andWhere({payment_status: req.query.payment_status});
          }

          db.andWhere(function(){
            this.where(function(){
              this.where('end_date', '<=', req.query.to).andWhere('end_date', '>=', req.query.from)
            }).orWhere(function(){
              this.where('start_date', '<=', req.query.to).andWhere('start_date', '>=', req.query.from)
            })
          })
        })
        
        activity = await activity.fetchAll()
        
      }
      
      let shedules = await Schedule.where('class_id', 'in', classes.map(item => item.id)).fetchAll({withRelated: ['class.subscriptions.subscription']});
      if(shedules){
        shedules = shedules.toJSON();
      }

      if(activity.length){
        activity = activity.toJSON();
      }
      
      let shedules_day = {};
      let classes_id = {};
      shedules.forEach(item => {
        if(shedules_day[item.day]){
          shedules_day[item.day].push(item);
        } else {
          shedules_day[item.day] = [item]
        }

        if(!classes_id[item.class_id]){
          classes_id[item.class_id] = item.class;
        }

      })


      for (let index = from; index <= to; index.add(1, 'day')) {
        let day = index.format('ddd');
    

        let tmp_res = {};

        
        if(shedules_day.hasOwnProperty(day)){
          
          shedules_day[day].forEach(item => {
            if(tmp_res[item.class_id]){
              tmp_res[item.class_id].push(item);
            } else {
              tmp_res[item.class_id] = [item]
            }
          })
        }

        
        result[index.format('YYYY-MM-DD')] = Object.keys(tmp_res).map(class_id => {
          let tmp_class = _.cloneDeep(classes_id[class_id]);
          tmp_class.shedules = _.cloneDeep(tmp_res[class_id]);
          tmp_class.shedules.forEach(item => delete item.class)
          return tmp_class;
        });
      }


      activity.forEach(item => {
        let data = moment(item.start_date);
        
        while ( data <= moment(item.end_date)) {
          if(result.hasOwnProperty(data.format('YYYY-MM-DD'))){
            result[data.format('YYYY-MM-DD')].push(item);
          }
          data.add(1, 'day');
        }
      })

      res.json({status: true, data: result});
    } catch (error) {
      helper.errorResponse(res, [ error ]);
    }
  })

  router.get('/events', (req, res) => {

    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});

    return ShareActivity.where({user_id: req.user.id}).fetchAll().then((shareActivity) => {
      if(!shareActivity){
        return []
      }
      let ids = [];
      shareActivity.forEach(element => {
        ids.push(element.get('activity_id'));
      });
      return ids;
    
    }).then((ids) => {
      console.log('ids', ids)
      normalizedQuery.filter.id = {in: ids.toString()};
      normalizedQuery.filter.type = {in: 'seminar,mat_event,others'};
      return helper.fetchPaginationData(normalizedQuery, Activity, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Activity.getAttributes(), Activity.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/open_mats', (req, res) => {

    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});

    return ShareActivity.where({user_id: req.user.id}).fetchAll().then((shareActivity) => {
      if(!shareActivity){
        return []
      }
      let data = shareActivity;
      let ids = [];
      shareActivity.forEach(element => {
        ids.push(element.get('activity_id'));
      });
      return ids;
    
    }).then((ids) => {
      normalizedQuery.filter.id = {in: ids.toString()};
      normalizedQuery.filter.type = 'mat_event';
      return helper.fetchPaginationData(normalizedQuery, Activity, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Activity.getAttributes(), Activity.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });


  router.get('/classes/user/:id([0-9]+)', (req, res) => {

    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});

    return ShareActivity.where({user_id: req.params.id}).fetchAll().then((shareActivity) => {
      if(!shareActivity){
        return []
      }
      let ids = [];
      shareActivity.forEach(element => {
        ids.push(element.get('activity_id'));
      });
      return ids;
    
    }).then((ids) => {
      normalizedQuery.filter.id = {in: ids.toString()};
      normalizedQuery.filter.type = 'class';
      return helper.fetchPaginationData(normalizedQuery, Activity, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Activity.getAttributes(), Activity.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/events/user/:id([0-9]+)', (req, res) => {

    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});

    return ShareActivity.where({user_id: req.params.id}).fetchAll().then((shareActivity) => {
      if(!shareActivity){
        return []
      }
      let ids = [];
      shareActivity.forEach(element => {
        ids.push(element.get('activity_id'));
      });
      return ids;
    
    }).then((ids) => {
      console.log('ids', ids)
      normalizedQuery.filter.id = {in: ids.toString()};
      normalizedQuery.filter.type = 'special_event';
      return helper.fetchPaginationData(normalizedQuery, Activity, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Activity.getAttributes(), Activity.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/open_mats/user/:id([0-9]+)', (req, res) => {

    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});

    return ShareActivity.where({user_id: req.params.id}).fetchAll().then((shareActivity) => {
      if(!shareActivity){
        return []
      }
      let data = shareActivity;
      let ids = [];
      shareActivity.forEach(element => {
        ids.push(element.get('activity_id'));
      });
      return ids;
    
    }).then((ids) => {
      normalizedQuery.filter.id = {in: ids.toString()};
      normalizedQuery.filter.type = 'mat_event';
      return helper.fetchPaginationData(normalizedQuery, Activity, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Activity.getAttributes(), Activity.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });


  router.get('/:id([0-9]+)', (req, res) => {
    let activity_class;
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});
    Activity.where({id: req.params.id}).fetch({withRelated: normalizedQuery.include}).then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Activity"})
      }
      activity_class = findModel.toJSON();

      let includes = {
        techniques:{
          field: 'technique_id',
          ids: []
        },
        teachers:{
          field: 'teacher_id',
          ids: []
        },
        subscriptions:{
          field: 'subscription_id',
          ids: []
        },
      }

      Object.keys(includes).forEach(key => {
        if(activity_class.hasOwnProperty(key)){
          if(Array.isArray(activity_class[key])){
            activity_class[key].forEach(item => {
              includes[key].ids.push(item[includes[key].field])
            })
          }
        }
      });

      console.log('includes', includes)

      return Promise.all([
        includes.teachers.ids.length ? Teacher.where('id', 'in', includes.teachers.ids).fetchAll() : null,
        includes.techniques.ids.length ? Technique.where('id', 'in', includes.techniques.ids).fetchAll() : null,
        includes.subscriptions.ids.length ? Subscription.where('id', 'in', includes.subscriptions.ids).fetchAll() : null,
        JoinActivity.where({activity_id: req.params.id}).fetchAll(),
        Checkin.where({activity_id: req.params.id}).fetchAll(),
      ])
    }).then(([teachers, techniques, subscriptions, join_activities, _checkins]) => {
      let includes = {teachers, techniques, subscriptions};

      normalizedQuery.include.forEach(include => {
        if (includes.hasOwnProperty(include)){
          activity_class[include] = includes[include];
        }
      })

      let checkins = _checkins.toJSON();

      if(activity_class.hasOwnProperty('join_activities')){
        activity_class.join_activities.forEach(item => {
          item.isCheckined = false;

          let index = _.findIndex(checkins, ['user_id', item.user_id]);
          if(~index){
            item.isCheckined = true;
            item.checkin_id = checkins[index].id;
          }

          if(item.hasOwnProperty('user')){
            item.user.isCheckined = item.isCheckined;
          }
        });
      }

      let total_price = 0;

      join_activities.toJSON().forEach(item => {
        if(item.amount){
          total_price += item.amount || 0;
        }
      });

      activity_class.total_price = total_price;

      res.json(activity_class);
    }).catch(err => {
      console.log('err', err);

      res.status(500).json(err)
    })
  });

  router.post('/', (req, res) => {
    let activity;
    let teachers;
    let techniques;
    let subscriptions;
    let days;
    if(req.body.teachers){
      teachers = req.body.teachers;
      delete req.body.teachers;
    }

    if(req.body.days){
      days = req.body.days;
      delete req.body.days;
    }

    if(req.body.techniques){
      techniques = req.body.techniques;
      delete req.body.techniques;
    }

    if(req.body.subscriptions){
      subscriptions = req.body.subscriptions;
      delete req.body.subscriptions;
    }

    helper.validateData(req.body,
      Joi.object().keys({
        name: Joi.string().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Academy.forge({id: req.body.academy_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      return Activity.forge(req.body).save();
    }).then((data) => {
      activity = data.toJSON();
      if (!teachers && !Array.isArray(teachers)){
        return null;
      }

      return Bluebird.map(teachers, teacher => {
        if(teacher.id){
          return ActivityTeacher.forge({activity_id: activity.id, teacher_id: teacher.id}).save();
        }
        return null;
      })
    }).then(() => {
      if (!techniques && !Array.isArray(techniques)){
        return null;
      }

      return Bluebird.map(techniques, technique => {
        if(technique.id){
          return ActivityTechnique.forge({activity_id: activity.id, technique_id: technique.id}).save();
        }
        return null;
      })
    }).then(() => {
      if (!subscriptions && !Array.isArray(subscriptions)){
        return null;
      }

      return Bluebird.map(subscriptions, subscription => {
        if(subscription.id){
          return SubscriptionActivity.forge({activity_id: activity.id, subscription_id: subscription.id}).save();
        }
        return null;
      })
    }).then(() => {
      return Promise.all([
        AcademyFollower.where({academy_id: activity.academy_id}).fetchAll(),
        User.where({academy_id: activity.academy_id}).fetchAll(),
        Academy.where({id: activity.academy_id}).fetch(),
        Manager.where({academy_id: activity.academy_id}).fetchAll()
      ]).then((_data) => {
        let data = _data[0].toJSON();
        let users = _data[1].toJSON();
        let academy = _data[2].toJSON();
        let type = '';
        let menagers = _data[3].toJSON();
        switch (activity.type) {
          case 'seminar':
            type = 'Seminar'
            break;
          case 'class':
            type = 'Class'
            break;
          case 'mat_event':
            type = 'Event'
            break;
          case 'others':
            type = 'Others'
            break;
          default:
            break;
        }

        data.map(item => {
          helper.createNotification(req, item.user_id, '<a href="/#/academiy-datail/' + academy.id + '">' + academy.name + '</a>' + ' has new '+ type +' <a href="/#/activity/' + activity.id + '">' + activity.name + '</a>');
        });
        users.map(item => {
          helper.createNotification(req, item.id, '<a href="/#/academiy-datail/' + academy.id + '">' + academy.name + '</a>' + ' has new '+ type +' <a href="/#/activity/' + activity.id + '">' + activity.name + '</a>');
        });
        menagers.map(item => {
          helper.createNotification(req, item.user_id, '<a href="/#/activity/' + activity.id + '">' + activity.name + '</a> will start on ' + moment().format('Do MMMM YYYY'), 'manager');
        });
        return true;
      })
    }).then(() => { 
      if(days){
        return Promise.all(days.map(item => Schedule.forge({
          class_id: activity.id,
          day: item.day,
          time_start: item.time_start,
          time_end: item.time_end,
        }).save()));
      }
      return null
    }).then(() => {
      return Activity.where({id: activity.id}).fetch({withRelated: ['subscriptions.subscription']})
    }).then((activityModel) => {
      res.json({status: true, activity: activityModel.toJSON()})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/share', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        ShareActivity.where({activity_id: req.body.activity_id, user_id: req.user.id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found activity', 404))
      }
      if(findModel[1]){
        return null
      }

      return ShareActivity.forge({activity_id: req.body.activity_id, user_id: req.user.id}).save();
    }).then((data) => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/unshare', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return ShareActivity.where({activity_id: req.body.activity_id, user_id: req.user.id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found activity', 404))
      }

      return findModel.destroy();
    }).then((data) => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let activity;
    let teachers;
    let techniques;
    let subscriptions;
    if(req.body.teachers){
      teachers = req.body.teachers;
      delete req.body.teachers;
    }

    if(req.body.techniques){
      techniques = req.body.techniques;
      delete req.body.techniques;
    }

    if(req.body.subscriptions){
      subscriptions = req.body.subscriptions;
      delete req.body.subscriptions;
    }
    helper.validateData(req.body,
      Joi.object().keys({
        name: Joi.string().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Activity.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Activity', 404))
      }
      activity = findModel;
      return Academy.forge({id: req.body.academy_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      return activity.save(req.body, { method: 'update'})
    }).then(data => {
      activity = data.toJSON();
      if (!teachers && !Array.isArray(teachers)){
        return null;
      }

      return ActivityTeacher.where({activity_id: activity.id}).destroy().then(() =>{
        return Bluebird.map(teachers, teacher => {
          if(teacher.id){
            return ActivityTeacher.forge({activity_id: activity.id, teacher_id: teacher.id}).save();
          }
          return null;
        })
      })
    }).then(() => {
      if (!techniques && !Array.isArray(techniques)){
        return null;
      }
      return ActivityTechnique.where({activity_id: activity.id}).destroy().then(() => {
        return Bluebird.map(techniques, technique => {
          if(technique.id){
            return ActivityTechnique.forge({activity_id: activity.id, technique_id: technique.id}).save();
          }
          return null;
        })
      })
    }).then(() => {
      if (!subscriptions && !Array.isArray(subscriptions)){
        return null;
      }

      return  SubscriptionActivity.where({activity_id: activity.id}).destroy().then(() => {
        return Bluebird.map(subscriptions, subscription => {
          if(subscription.id){
            return SubscriptionActivity.forge({activity_id: activity.id, subscription_id: subscription.id}).save();
          }
          return null;
        })
      })
    }).then(() => {
      res.json({status: true, activity})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', async (req, res) => {
    try {
      await Activity.forge({ id: req.params.id }).destroy();
      await Schedule.destroy({class_id: req.params.id});
    } catch (error) {
      helper.errorResponse(res, [ error ]);
    }
  });

  router.post('/pay', (req, res) => {
    let user;
    let activity;
    helper.validateData(req.body,
      Joi.object().keys({
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        User.forge({id: req.user.id}).fetch(),
        JoinActivity.where({user_id: req.user.id, activity_id: req.body.activity_id}).fetch(),
        JoinActivity.where({activity_id: req.body.activity_id}).fetchAll(),
      ]) 
    }).then(modals => {
      if (modals[2]){
        return Promise.reject(new Error('Is you join activity'));
      }
      activity = modals[0];
      user = modals[1];
      if(!activity){
        return Promise.reject(new Error('Not found activity'));
      }

      if(!(activity.get('price_all') > 0)){
        return Promise.reject(new Error('Not found activity price'));
      }
      
      if(!(activity.get('payment_status') == 'fee')){
        return Promise.reject(new Error('Not found activity fee'));
      }

      if((activity.get('amount') < modals[3].length)){
        return Promise.reject(new Error('Is all user joined'));
      }

      if(user.get('stripe_customer_id')){
        return user;
      }
      return ExtarnalService.createStripeCustomer({email: user.get('email')}).then(data => {
        user.set('stripe_customer_id', data.id);
        return user.save();
      })
    }).then(_user => {
      return ExtarnalService.paymentStripe({
        customer: _user.get('stripe_customer_id'), 
        source: req.body.source,
        price: activity.get('price_all'),
        type: req.body.type
      });
    }).then(payment => {
      let type = '';
      activity = activity.toJSON();
      switch (activity.type) {
        case 'seminar':
          type = 'Seminar'
          break;
        case 'class':
          type = 'Class'
          break;
        case 'mat_event':
          type = 'Event'
          break;
        case 'others':
          type = 'Others'
          break;
        default:
          break;
      }
      if (payment.status == 'succeeded') {
        return JoinActivity.forge({user_id: req.user.id, activity_id: activity.id, amount: activity.price_all}).save().then(data => {
          return Promise.all([
            User.where({id: req.user.id}).fetch(),
            Manager.where({academy_id: activity.academy_id}).fetchAll()
          ]);
        }).then(([_user, _managers]) => {
          Transaction.forge({
            type: 'activity',
            activity_id: activity.id,
            academy_id: activity.academy_id,
            amount: activity.price_all,
            amount_app: 0,
            user_id: req.user.id,
            amount_academy: 0,
            amount_stripe: 0,
            procent: 0,
            charge_id: payment.id
          }).save().then(transaction => {
            UserTimeline.forge({
              data: 'Payment processed for the ' + type + ' $'  + activity.price_all,
              user_id: req.user.id,
              type: 'refund',
              details: '',
              transaction_id: transaction.id,
              academy_id: activity.academy_id
            }).save();
          });
          helper.userRevenue(req.user.id, activity.academy_id, (type == 'Class' ? 'class' : 'event'), activity.price_all);
          
          helper.createNotification(req, req.user.id, '<a href="/#/profile/'+ _user.id +'">' + _user.get('first_name') + ' ' + _user.get('last_name') + '</a> joined ' + type + ' "<a href="/#/activity/' + activity.id + '">' + activity.name + '</a>"');
          helper.createNotification(req, req.user.id, 'Your payment ' + activity.price_all + '$ for ' + type + ' <a href="/#/activity/' + activity.id + '">' + activity.name + '</a> was processed. Thank you!');
          let managers = _managers.toJSON();
          managers.map(item => {
            helper.createNotification(req, item.user_id, '<a href="/#/profile/'+ _user.id +'">' + _user.get('first_name') + ' ' + _user.get('last_name') +'</a>  joined to ' + type + ' "<a href="/#/activity/' + activity.id + '">' + activity.name + '</a>"', 'manager');
          })
          return Promise.resolve({status: true});
        });
      } else {
        helper.createNotification(req, req.user.id, 'Your payment for ' + type + ' "<a href="/#/activity/' + activity.id + '">' + activity.name + '</a>"' +  ' was declined on ' + moment().format('Do MMMM YYYY') + ' <a href="/#/activity/' + activity.id + '">PAY NOW</a>');
        return Promise.reject(new Error('Is not pay activity'));
      }
    }).then(status => {
      res.json(status);
    }).catch((err) => {
      let type = '';
      console.log('err', err);
      if(!activity){
        return helper.errorResponse(res, [ err ]);
      }
      activity = activity.toJSON();
      switch (activity.type) {
        case 'seminar':
          type = 'Seminar'
          break;
        case 'class':
          type = 'Class'
          break;
        case 'mat_event':
          type = 'Event'
          break;
        case 'others':
          type = 'Others'
          break;
        default:
          break;
      }
      UserTimeline.forge({
        data: 'Payment was declined for ' + type + ' "<a href="/#/activity/' + activity.id + '">' + activity.name + '</a>"',
        user_id: req.user.id,
        type: 'standart',
        details: '',
        academy_id: activity.academy_id
      }).save();
      return helper.createNotification(req, req.user.id, 'Your payment for ' + type + ' "<a href="/#/activity/' + activity.id + '">' + activity.name + '</a>"' +  ' was declined on ' + moment().format('Do MMMM YYYY') + ' <a href="/#/activity/' + activity.id + '">PAY NOW</a>').then(() => {
        return helper.errorResponse(res, [ err ]); 
      });
      
    })
  });

  router.get('/counts', (req, res) => {

    let normalizedQuery = helper.normalizeQuery(req.query, ['academy_id','user_id','created_at', 'updated_at']);


    normalizedQuery.include = ['activity'];

    let is_academy = 0;

    let queryJoin = _.cloneDeep(normalizedQuery);
    let queryCheckin = _.cloneDeep(normalizedQuery);

    if(queryJoin.filter.hasOwnProperty("academy_id")){
      is_academy= queryJoin.filter.academy_id;
      delete queryJoin.filter.academy_id;
    }

    Promise.all([
      helper.findAllModelData(queryJoin, JoinActivity),
      helper.findAllModelData(queryCheckin, Checkin),
    ]).then(([joins, checkins]) => {
      let counts = {
        join: {
          class: 0,
          mat_event: 0,
          others: 0,
          seminar: 0,
        },
        checkin: {
          class: 0,
          mat_event: 0,
          others: 0,
          seminar: 0,
        }
      }
      let i =0;
      joins.forEach(join => {
        let item = join.toJSON();
        i++;
        if(item.activity){
          if(is_academy){
            if(item.activity.academy_id == is_academy){
              if(!counts.join.hasOwnProperty(item.activity.type)){
                counts.join[item.activity.type] = 1;
              } else {
                counts.join[item.activity.type] = counts.join[item.activity.type] + 1;
              }
            }
          } else {
            if(!counts.join.hasOwnProperty(item.activity.type)){
              counts.join[item.activity.type] = 1;
            } else {
              counts.join[item.activity.type] = counts.join[item.activity.type] + 1;
            }
          }
        }
      });

      checkins.forEach(join => {
        let item = join.toJSON();
        if(item.activity){
          if( !counts.checkin.hasOwnProperty(item.activity.type)){
            counts.checkin[item.activity.type] = 1;
          } else {
            counts.checkin[item.activity.type] = counts.checkin[item.activity.type] + 1;
          }
        }
        
      })
      res.json(counts);
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
    
  });

  router.post('/add_cover_photo', (req, res) => {
    let activity;
    helper.validateData(req.body,
      Joi.object().keys({
        cover_photo: Joi.string().required(),
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Activity.where({id: req.body.activity_id}).fetch();
    }).then(findModel => {

      if(!findModel){
        return Promise.resolve(new Error('Not found activity'));
      }

      activity = findModel;

      let dir = 'public/uploads/activity/' + activity.id + '/';
      if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
      }

      return new Promise(function(resolve, reject) {
        fs.writeFile(dir + 'cover_photo.png', new Buffer(req.body.cover_photo.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
           if (err) reject(err);
           else resolve(dir + 'cover_photo.png');
        });
      });
    }).then(cover_photo_url => {
      if(!cover_photo_url){
        return null;
      }
      return activity.save({cover_photo: cover_photo_url.replace('public/', '')}, { method: 'update'})
    }).then((activity) => {
      res.json(activity);
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/unjoin', (req, res) => {
    let activity;
    let user_id = req.body.user_id || req.user.id;
    helper.validateData(req.body,
      Joi.object().keys({
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Activity.where({id: req.body.activity_id}).fetch();
    }).then(findModel => {

      if(!findModel){
        return Promise.resolve(new Error('Not found activity'));
      }

      return JoinActivity.where({user_id, activity_id: req.body.activity_id}).destroy()
    }).then(() => {
      res.json({status: true});
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/student_checkins', async (req, res) => {
    try {
      await helper.validateData(req.body,
        Joi.object().keys({
          activity_id: Joi.number().integer().required(),
          schedule_id: Joi.number().integer().required(),
          date: Joi.date().required()
        }), { abortEarly: false, allowUnknown: true }
      );

      let activity_subscriptions = await SubscriptionActivity.where({activity_id: req.body.activity_id}).fetchAll();
      activity_subscriptions = activity_subscriptions.toJSON();
      
      let user_subscriptions = await UserSubscription.where('subscription_id', 'in', activity_subscriptions.map(item => item.subscription_id)).where({status: 1}).fetchAll();


      user_subscriptions = user_subscriptions.toJSON();

      let user_sub_calendar = await CalendarSubscription.query(function (db) {
        db.where('pay_start', '<=', req.body.date)
        db.andWhere(function(){
            this.where('pay_end', '>=', req.body.date).orWhere({'pay_end': null})
        })
      }).fetchAll();

      user_sub_calendar = user_sub_calendar.toJSON();

      // let user_ids = user_subscriptions.map(item => item.user_id);
      let user_ids = user_sub_calendar.map(item => item.user_id);

      let users = await User.where('id', 'in', user_ids).fetchAll();
      
      let checkins = await Checkin.where('user_id', 'in', user_ids).where({ activity_id: req.body.activity_id, schedule_id: req.body.schedule_id}).fetchAll();
      
      checkins = checkins.toJSON();
			
      let pickAttr = _.concat(User.getAttributes(), User.getInclideAttributes())

      let result = (Array.isArray(pickAttr)) ? users.toJSON().map(item => _.pick(item, pickAttr)) : users.toJSON();

      let users_data = result.map(user => {
        user.checkin = null;
        return user;
      });

      checkins.forEach(checkin => {
        if( moment(checkin.checkin_date).format('YYYY-MM-DD') == req.body.date){
          let index = _.findIndex(users_data, ['id', checkin.user_id]);
          if(~index){
            users_data[index].checkin = checkin;
          }
        }
      });
      
      res.json({
        status: true,
        data: users_data
      })
    } catch (error) {
      helper.errorResponse(res, [ error ]);
    }
  })

  return router;
}