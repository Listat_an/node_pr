
'use strict';
const Academy = require('../models').academy;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const ExternalServices = require('../lib/external_services');
const Manager = require('../models').manager;
const Belt = require('../models').belt;
const BeltSetting = require('../models').belt_setting;
const AcademyFollower = require('../models').academy_follower;
const config = require('../config')

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    const Bookshelf = req.app.get('database');
    let ids=[];
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Academy});
    console.log('normalizedQuery', normalizedQuery)
    console.log(req.user)
    return Promise.resolve(true).then(() => {
      
      if (req.query.distantion){

        let latitude = req.query.latitude || -1;
        let longitude = req.query.longitude || 1;

        var distance_select = 'CEILING(' +
        '3963.1676 * acos ( ' +
        'cos ( radians(' + latitude + ') )' +
        '* cos( radians( academies.`latitude` ) )' +
        '* cos( radians( academies.`longitude` ) - radians(' + longitude + ') )' +
        '+ sin ( radians(' + latitude + ') )' +
        '* sin( radians( academies.`latitude` ) ) )' +
        ') AS distance ';
        return Bookshelf.knex.raw('SELECT id, ' + distance_select + '  FROM academies HAVING CEILING(distance) <= ' + req.query.distantion)
        // return Bookshelf.knex.raw('SELECT *, 69.0 * DEGREES(ACOS(COS(RADIANS(' + user.get('latitude') + ')) * COS(RADIANS(activity.latitude)) * COS(RADIANS(' + user.get('longitude') + ' - activity.longitude)) + SIN(RADIANS(' + user.get('latitude') + ')) * SIN(RADIANS(activity.latitude)))) AS distance FROM activity HAVING distance < ' + req.query.distantion )
      } else {
        return [[]];
      }
    
    }).then((data) => {

      if (req.query.distantion){        
        ids = data[0].map(item => item.id);
        normalizedQuery.filter.id = {in: ids.toString()}
      }

      return helper.fetchPaginationData(normalizedQuery, Academy, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Academy.getAttributes(), Academy.getInclideAttributes())))
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Academy});
    Academy.where({id: req.params.id}).fetch({withRelated: normalizedQuery.include}).then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Academy"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    req.body.status = 'In_process';
    let academy;

    let photo_base = null;
    let cover_photo_base = null;
    if(req.body.photo_base){
      photo_base = req.body.photo_base;
      delete req.body.photo_base;
    }
    if(req.body.cover_photo_base){
      cover_photo_base = req.body.cover_photo_base;
      delete req.body.cover_photo_base;
    }

    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().integer().required(),
        name: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return User.forge({id: req.body.user_id}).fetch();
    }).then(findModel =>{

      if(!findModel){
        return Promise.reject(new AppError('Not found user', 404))
      }

      return Academy.forge(req.body).save();
    }).then((data) => {
      academy = data;
      AcademyFollower.forge({user_id: req.body.user_id, academy_id: academy.id}).save();
      return Manager.forge({user_id: req.body.user_id, academy_id: academy.id, status: true}).save();
    }).then(() => {

      if (!photo_base){
        return null;
      }
      let dir = 'public/uploads/academies/' + req.params.id+ '/';
      if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
      }

      return new Promise(function(resolve, reject) {
        fs.writeFile(dir + 'photo.png', new Buffer(photo_base.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
           if (err) reject(err);
           else resolve(dir + 'photo.png');
        });
      });
    }).then(photo_url => {
      if(!photo_url){
        return null;
      }
      return academy.save({photo: photo_url.replace('public/', '')}, { method: 'update'})
    }).then(() => {

      if (!cover_photo_base){
        return null;
      }
      let dir = 'public/uploads/academies/' + req.params.id+ '/';
      if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
      }

      return new Promise(function(resolve, reject) {
        fs.writeFile(dir + 'cover_photo.png', new Buffer(cover_photo_base.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
           if (err) reject(err);
           else resolve(dir + 'cover_photo.png');
        });
      });
    }).then(cover_photo_url => {
      if(!cover_photo_url){
        return null;
      }
      return academy.save({cover_photo: cover_photo_url.replace('public/', '')}, { method: 'update'})
    }).then(() => {
      Belt.where({stripe: 0}).fetchAll().then(belts => {
        belts.map(belt => {
          BeltSetting.forge({academy_id: academy.id,  belt_id: belt.get('id'),
            time_stripe_count: belt.get('time_stripe_count'),
            class_stripe_count: belt.get('class_stripe_count')}).save()
        });
      });
      res.json(academy.toJSON())
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/set_status', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        id: Joi.number().integer().required(),
        status: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Academy.forge({id: req.body.id}).fetch();
    }).then(findModel =>{

      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      return findModel.save({status: req.body.status}, { method: 'update'});
    }).then((_data) => {
      let academy = _data.toJSON();
      ExternalServices.sendMail({
        from: config.from_email,
        to: academy.email,
        subject: 'Confirmation',
        html: `<p>Your academy is "${academy.status}".</p>`
        +`<p>Regards, OPNMAT</p>`
      });
      res.json({status: true})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let academy;
    let photo_base = null;
    let cover_photo_base = null;
    if(req.body.photo_base){
      photo_base = req.body.photo_base;
      delete req.body.photo_base;
    }
    if(req.body.cover_photo_base){
      cover_photo_base = req.body.cover_photo_base;
      delete req.body.cover_photo_base;
    }
    helper.validateData(req.body,
      Joi.object().keys({
        // user_id: Joi.number().integer().required(),
        // name: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return User.forge({id: req.body.user_id || req.user.id}).fetch();
    }).then(findModel =>{

      if(!findModel){
        return Promise.reject(new AppError('Not found user', 404))
      }

      return Academy.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      if(req.user.role != 'admin'){
        if(req.body.hasOwnProperty('app_procent'))
        delete req.body.app_procent;
      }

      return findModel.save(req.body, { method: 'update'})
    }).then(data => {
      academy = data;
    }).then(() => {
      return new Promise((resolve, reject) => {
        var storage = multer.diskStorage({
          destination: function (req, file, cb) {
            let dir = 'public/uploads/academies/' + req.params.id + '/';
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            cb(null, dir)
          },
          filename: function (req, file, cb) {
            cb(null, file.originalname)
          }
        });
        
        var upload = multer({ storage: storage }).fields([{ name: 'photo', maxCount: 1 }, { name: 'cover_photo', maxCount: 1 }]);
        upload(req, res, function (err) {
          if (err) {
            return reject(err)
          }
          return resolve(true)
        })
      });
    }).then(() => {
      if (!req.files){
        return null;
      }

      let data = {};
      if(req.files.photo){
        data.photo = (req.files.photo[0].destination + req.files.photo[0].originalname).replace('public/', '');
      }

      if(req.files.cover_photo){
        data.cover_photo = (req.files.cover_photo[0].destination + req.files.cover_photo[0].originalname).replace('public/', '');
      }

      return academy.save(data, { method: 'update'})
    }).then(() => {

      if (!photo_base){
        return null;
      }
      let dir = 'public/uploads/academies/' + req.params.id+ '/';
      if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
      }

      return new Promise(function(resolve, reject) {
        fs.writeFile(dir + 'photo.png', new Buffer(photo_base.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
           if (err) reject(err);
           else resolve(dir + 'photo.png');
        });
      });
    }).then(photo_url => {
      if(!photo_url){
        return null;
      }
      return academy.save({photo: photo_url.replace('public/', '')}, { method: 'update'})
    }).then(() => {

      if (!cover_photo_base){
        return null;
      }
      let dir = 'public/uploads/academies/' + req.params.id+ '/';
      if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
      }

      return new Promise(function(resolve, reject) {
        fs.writeFile(dir + 'cover_photo.png', new Buffer(cover_photo_base.replace(/^data:image\/\w+;base64,/, ""), 'base64'), (err) => {
           if (err) reject(err);
           else resolve(dir + 'cover_photo.png');
        });
      });
    }).then(cover_photo_url => {
      if(!cover_photo_url){
        return null;
      }
      return academy.save({cover_photo: cover_photo_url.replace('public/', '')}, { method: 'update'})
    }).then(() => {
      res.json(academy.toJSON())
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', async (req, res) => {
    try {
      await Academy.forge({ id: req.params.id }).destroy();
      await require('../models/academy_follower').where({academy_id: req.params.id}).destroy();
      await require('../models/academy_work').where({academy_id: req.params.id}).destroy();
      await require('../models/activity').where({academy_id: req.params.id}).destroy();
      await require('../models/belt_setting').where({academy_id: req.params.id}).destroy();
      await require('../models/checkin').where({academy_id: req.params.id}).destroy();
      await require('../models/conversation').where({academy_id: req.params.id}).destroy();
      await require('../models/manager').where({academy_id: req.params.id}).destroy();
      await require('../models/message').where({academy_id: req.params.id}).destroy();
      await require('../models/order_item').where({academy_id: req.params.id}).destroy();
      await require('../models/order').where({academy_id: req.params.id}).destroy();
      await require('../models/post_file').where({academy_id: req.params.id}).destroy();
      await require('../models/post').where({academy_id: req.params.id}).destroy();
      await require('../models/product').where({academy_id: req.params.id}).destroy();
      await require('../models/shedule').where({academy_id: req.params.id}).destroy();
      await require('../models/shipping').where({academy_id: req.params.id}).destroy();
     
      let subscriptions = await require('../models/subscriptions').where({academy_id: req.params.id}).fetchAll();

      subscriptions = subscriptions.toJSON();

      await Promise.all(subscriptions.map(item => ExtarnalService.deleteStripePlan(item.plan_id)));

      await require('../models/subscription_academy').where({academy_id: req.params.id}).destroy();

      await require('../models/subscriptions').where({academy_id: req.params.id}).destroy();
      await require('../models/teacher').where({academy_id: req.params.id}).destroy();
      await require('../models/technique').where({academy_id: req.params.id}).destroy();
      await require('../models/transaction').where({academy_id: req.params.id}).destroy();
      await require('../models/user_active').where({academy_id: req.params.id}).destroy();
      await require('../models/user_promotion_timeline').where({academy_id: req.params.id}).destroy();
      await require('../models/user_revenue').where({academy_id: req.params.id}).destroy();
      await require('../models/user_timeline').where({academy_id: req.params.id}).destroy();
      await require('../models/user').where({academy_id: req.params.id}).save({academy_id: null}, {method: 'update'});
      res.json({status: true});
    } catch (error) {
      helper.errorResponse(res, [ err ]);
    }
  });

  router.post('/blocked', async (req, res) => {
    try {
      await helper.validateData(req.body,
        Joi.object().keys({
          academy_id: Joi.number().integer().required(),
          status: Joi.boolean().required(),
        }), { abortEarly: false, allowUnknown: true }
      );

      let academy = await Academy.where({id: req.body.academy_id}).fetch();

      if(!academy){
        throw new AppError('Not found academy', 404)
      }

      await Academy.where({id: req.body.academy_id}).save({is_blocked: req.body.status}, {method: 'update'})

      res.json({status: true});
    } catch (error) {
      helper.errorResponse(res, [ err ]);
    }
  });

  router.post('/change_student', (req, res) => {
    let user;
    User.forge({id: req.body.user_id || req.user.id}).fetch().then(findModel =>{
      if(!findModel){
        return res.status(400).json({message: 'User not found'})
      }
      user = findModel.toJSON();
      return Promise.all([
        Academy.where({id: findModel.get('academy_id')}).fetch(),
        Manager.where({academy_id: findModel.get('academy_id')}).fetchAll()
      ])
    }).then(([academy, managers]) => {
      managers.map(manager => {
        helper.createNotification(req, manager.get('user_id'), 'User is change academy', 'manager', 'change_academy', {academy_id: academy.id, user_id: user.id, full_name: user.first_name + ' ' + user.last_name});
      });
      res.json({status: true})
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  });

  return router;
}