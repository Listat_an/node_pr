'use strict';
const UserActive = require('../models').user_active;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: UserActive});

    return helper.fetchPaginationData(normalizedQuery, UserActive, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(UserActive.getAttributes(), UserActive.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}