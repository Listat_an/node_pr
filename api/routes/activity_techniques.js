
'use strict';
const ActivityTechnique = require('../models').activity_technique;
const Technique = require('../models').technique;
const Activity = require('../models').activity;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: ActivityTechnique});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, ActivityTechnique, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(ActivityTechnique.getAttributes(), ActivityTechnique.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    ActivityTechnique.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found ActivityTechnique"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        activity_id: Joi.number().integer().required(),
        technique_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        Technique.forge({id: req.body.technique_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Activitu', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Technique', 404))
      }

      return ActivityTechnique.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let activity_teacher;
    helper.validateData(req.body,
      Joi.object().keys({
        activity_id: Joi.number().integer().required(),
        technique_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return ActivityTechnique.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found ActivityTechnique', 404))
      }
      activity_teacher = findModel;
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        Technique.forge({id: req.body.technique_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Activity', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Technique', 404))
      }
      return activity_teacher.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    ActivityTechnique.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}