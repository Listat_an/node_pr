
'use strict';
const AcademySubscrioption = require('../models').academy_subscription;
const Academy = require('../models').academy;
const User = require('../models').user;
const Manager = require('../models').manager;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const moment = require('moment');
const fs = require('fs');
const _ = require('lodash');
const ExtarnalService = require('../lib/external_services');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: AcademySubscrioption});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, AcademySubscrioption, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(AcademySubscrioption.getAttributes(), AcademySubscrioption.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    AcademySubscrioption.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found AcademySubscrioption"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        title: Joi.string().required(),
        name: Joi.string().required(),
        price: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {

      if(req.body.price == 0){
        return {id: null};
      }

      var data_plan ={
        amount: req.body.price*100,
        name: req.body.name,
        interval: 'month'
      }
      return ExtarnalService.createStripePlan(data_plan);
    }).then(plan => {
      if(!plan){
        return Promise.reject(new AppError('Create plan error', 500))
      }
      req.body.stripe_plan_id = plan.id;

      return AcademySubscrioption.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, data})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let academy_subscription;
    helper.validateData(req.body,
      Joi.object().keys({
        title: Joi.string().required(),
        name: Joi.string().required(),
        price: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return AcademySubscrioption.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found AcademySubscrioption', 404))
      }
      academy_subscription = findModel;
    
      return academy_subscription.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    let subscription;
    AcademySubscrioption.forge({ id: req.params.id }).fetch().then((_subscription) => {
      if (!_subscription){
        return Promise.reject(new AppError('Not found subscription', 404))
      }
      subscription = _subscription;
      if(!subscription.get('stripe_plan_id')){
        return null;
      }
      return ExtarnalService.deleteStripePlan(subscription.get('stripe_plan_id'));
    }).then((data) => {
      return subscription.destroy();
    }).then(() => {
      Academy.where({academy_subscription_id: subscription.id}).save({academy_subscription_id: null, is_payd_subscription: 0, stripe_subscription_id: null}, { method: 'update'});
      res.json({ status: true })
    }).catch((err) => {
      console.log('​err', err);
      return helper.errorResponse(res, [ err ]);
    })
  });


  router.post('/pay', (req, res) => {
    let subscription, user, academy, promotion_method;
    let status = false;
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      if(req.body.promotion){
        promotion_method = req.body.promotion;
        delete req.body.promotion;
      }
      return Promise.all([
        AcademySubscrioption.forge({id: req.body.subscription_id}).fetch(),
        User.forge({id: req.user.id}).fetch(),
        Academy.where({id: req.body.academy_id}).fetch(),
      ]) 
    }).then(modals => {
      subscription = modals[0];
      user = modals[1];
      academy = modals[2];

      if(!subscription){
        return Promise.reject(new Error('Not found academy subscription'));
      }

      if (academy.get('academy_subscription_id') == req.body.subscription_id){
        return Promise.reject(new Error('Is you subscription'));
      }
     
      
      if(user.get('stripe_customer_id')){
        return user;
      }
      return ExtarnalService.createStripeCustomer({email: user.get('email')}).then(data => {
        user.set('stripe_customer_id', data.id);
        return user.save();
      })
    }).then(_user => {

      if(!subscription.get('stripe_plan_id')){
        status = true;
        return {
          id: null,
          current_period_end: '',
          billing_cycle_anchor: '',
          trial_end: '',
        }
      }

      return ExtarnalService.createStripeSubscription({
        customer: _user.get('stripe_customer_id'), 
        plan: subscription.get('stripe_plan_id'),
        source: req.body.source,
        metadata: {
          user_id: req.user.id,
          type: 'academy_subscription',
          academy_id: academy.id,
          plan_id: subscription.get('stripe_plan_id')
        }
      })
    }).then((stripe_subscription) => {
      academy.save({
        academy_subscription_id: subscription.id, 
        stripe_subscription_id: stripe_subscription.id, 
        is_payd_subscription: status,
        promotion_method,
        is_pro: status,
        promotion_time: 10,
        promotion_attendance: 10,
        current_period_end: stripe_subscription.current_period_end,
        billing_cycle_anchor: stripe_subscription.billing_cycle_anchor,
        trial_end: stripe_subscription.trial_end,
      }, {method: 'update'});
      res.json({
        status: true
      })
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]); 
    })
  })

  router.post('/canceled', (req, res) => {
    let academy;
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Academy.where({id: req.body.academy_id}).fetch();
    }).then(academyFind => {
      academy = academyFind;
      if(!academy){
        return Promise.reject(new Error('Not found academy'));
      }

      if(!academy.get('academy_subscription_id')){
        return Promise.reject(new Error('Not found academy subscription'));
      }

      if(!academy.get('stripe_subscription_id')){
        return Promise.reject(new Error('Not found stripe subscription'));
      }

      return ExtarnalService.cenceledStripeSubscription(academy.get('stripe_subscription_id'))
    }).then(() => {

      Manager.where({academy_id: academy.get('id')}).fetchAll().then(managers => {
        managers.forEach(manager => {
          helper.createNotification(req, manager.get('user_id'), 'Academy PRO account canceled and will cease to act ' + moment.unix(academy.get('billing_cycle_anchor')).format('Do MMMM YYYY'), 'manager');
        });
      })

      return academy.save({is_cancel_time: true, stripe_subscription_id: null}, {method: 'update'})

      // return academy.save({
      //   academy_subscription_id: null, 
      //   stripe_subscription_id: null, 
      //   is_payd_subscription: 0,
      //   promotion_method: null,
      //   is_pro: 0,
      //   promotion_time: null,
      //   promotion_attendance: null
      // }, {method: 'update'});
    }).then(academyFind => {
      res.json({
        status: true
      })
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]); 
    })
  })

  return router;
}