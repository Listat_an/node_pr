
'use strict';
const ActivityTeacher = require('../models').activity_teacher;
const Teacher = require('../models').teacher;
const Activity = require('../models').activity;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: ActivityTeacher});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, ActivityTeacher, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(ActivityTeacher.getAttributes(), ActivityTeacher.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    ActivityTeacher.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found ActivityTeacher"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        activity_id: Joi.number().integer().required(),
        teacher_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        Teacher.forge({id: req.body.teacher_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Activitu', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Teacher', 404))
      }

      return ActivityTeacher.forge(req.body).save();
    }).then((data) => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let activity_teacher;
    helper.validateData(req.body,
      Joi.object().keys({
        activity_id: Joi.number().integer().required(),
        teacher_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return ActivityTeacher.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found ActivityTeacher', 404))
      }
      activity_teacher = findModel;
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        Teacher.forge({id: req.body.teacher_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Activity', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Teacher', 404))
      }
      return activity_teacher.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    ActivityTeacher.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}