
'use strict';
const AcademyFollower = require('../models').academy_follower;
const Academy = require('../models').academy;
const User = require('../models').user;
const BeltUser = require('../models').belt_user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const Manager = require('../models').manager;

const multer  = require('multer')

module.exports = (router) => {

  router.get('/status/:id([0-9]+)', (req, res) => {
    return AcademyFollower.where({academy_id: req.params.id, user_id: req.user.id}).fetch().then(folower => {
      if (folower){
        return res.json({status: true});
      }
      return res.json({status: false});
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  })

  router.get('/users/:id([0-9]+)', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: User});
    let followers;
    return AcademyFollower.where({academy_id: req.params.id}).fetchAll().then(_followers => {
      followers = _followers.toJSON();
      normalizedQuery.filter.id = {in: followers.map(item => item.user_id)};
      return Promise.all([
        helper.fetchPaginationData(normalizedQuery, User, null, []),
        BeltUser.where('user_id', 'in', followers.map(item => item.user_id)).fetchAll({withRelated: ['belt']})
      ]) 
    }).then(([_data, _belts]) => {
      let users = helper.indexResponse(_data, _.concat(User.getAttributes(), User.getInclideAttributes()));
      let belts = _belts.toJSON();
      users.data = users.data.map(user => {
        user.is_follower = false;
        let index = _.findIndex(followers, ['user_id', user.id]);
        if(index > -1){ 
          user.is_follower = followers[index];
        }

        let index_belt = _.findIndex(belts, ['user_id', user.id]);
        user.belt = null;
        if(index_belt > -1){ 
          user.belt = belts[index_belt].belt;
        }
        return user;
      })
      
      res.json(users);
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  });
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: AcademyFollower});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, AcademyFollower, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(AcademyFollower.getAttributes(), AcademyFollower.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    AcademyFollower.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found AcademyFollower"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/add', (req, res) => {
    var academy;
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Academy.where({id: req.body.academy_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      academy = findModel;
      return AcademyFollower.where({user_id: req.user.id, academy_id: req.body.academy_id}).fetch();
    }).then( follower => {
      if(follower){
        return null;
      }
      return AcademyFollower.forge({user_id: req.user.id, academy_id: req.body.academy_id}).save();
    }).then((data) => {
      Promise.all([
        User.where({id: req.user.id}).fetch(),
        Manager.where({academy_id: academy.id}).fetch(),
      ]).then(([_user, _managers]) => {
        let user = _user.toJSON();
        let managers = _managers.toJSON();
        helper.createNotification(req, req.user.id, '<a href="/#/profile/'+ user.id +'">' + user.first_name + ' ' + user.last_name + '</a> joined <a href="/#/academiy-datail/' + academy.id + '">' + academy.get('name') + '</a>');
        _managers.forEach(manager => {
          helper.createNotification(req, manager.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.first_name + ' ' + user.last_name + '</a> started to follow your <a href="/#/academiy-datail/' + academy.id + '">academy</a>', 'manager');
        });
      })
      
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    
    AcademyFollower.forge({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found AcademyFollower', 404))
      }
      return findModel.save({status: req.body.status}, { method: 'update'})
    }).then(data => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/remove', (req, res) => {
    
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Academy.forge({id: req.body.academy_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      return AcademyFollower.where({user_id: req.user.id, academy_id: req.body.academy_id}).destroy();
    }).then((data) => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  return router;
}