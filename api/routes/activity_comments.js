'use strict';
const ActivityComment = require('../models').activity_comment;
const Activity = require('../models').activity;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: ActivityComment});

    return helper.fetchPaginationData(normalizedQuery, ActivityComment, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(ActivityComment.getAttributes(), ActivityComment.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    ActivityComment.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found ActivityComment"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    req.body.user_id = req.user.id;
    let user;
    helper.validateData(req.body,
      Joi.object().keys({
        content: Joi.string().required(),
        user_id: Joi.number().required(),
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        User.forge({id: req.body.user_id}).fetch(),
      ]) ;
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Activity', 404))
      }
      if(!findModel[1]){
        return Promise.reject(new AppError('Not found User', 404))
      }

      user = findModel[1].toJSON();

      return ActivityComment.forge(req.body).save();
    }).then((_data) => {
      let data = _data.toJSON();
      data.user = user;
      res.json({status: true, comment: data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    req.body.user_id = req.user.id;
    let coment;
    helper.validateData(req.body,
      Joi.object().keys({
        content: Joi.string().required(),
        user_id: Joi.number().required(),
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return ActivityComment.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found ActivityComment', 404))
      }
      coment = findModel;
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        User.forge({id: req.body.user_id}).fetch(),
      ]) ;
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Activity', 404))
      }
      if(!findModel[1]){
        return Promise.reject(new AppError('Not found User', 404))
      }
      return coment.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, comment: data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    ActivityComment.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}