'use strict';
const UserPromotion = require('../models').user_promotion_timeline;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: UserPromotion});

    return helper.fetchPaginationData(normalizedQuery, UserPromotion, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(UserPromotion.getAttributes(), UserPromotion.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}