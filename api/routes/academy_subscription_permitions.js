
'use strict';
const AcademySubscriptionPermition = require('../models').academy_subscription_permition;
const AcademySubscription = require('../models').academy_subscription;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: AcademySubscriptionPermition});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, AcademySubscriptionPermition, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(AcademySubscriptionPermition.getAttributes(), AcademySubscriptionPermition.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    AcademySubscriptionPermition.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found AcademySubscriptionPermition"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().integer().required(),
        name: Joi.string().required(),
        title: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        AcademySubscription.forge({id: req.body.subscription_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found AcademySubscription', 404))
      }

      return AcademySubscriptionPermition.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, data})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let academy_subscription_permition;
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().integer().required(),
        name: Joi.string().required(),
        title: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return AcademySubscriptionPermition.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found AcademySubscriptionPermition', 404))
      }
      academy_subscription_permition = findModel;
      return Promise.all([
        AcademySubscription.forge({id: req.body.subscription_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found AcademySubscription', 404))
      }

      return academy_subscription_permition.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    AcademySubscriptionPermition.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}