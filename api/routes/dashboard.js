
'use strict';
const Activity = require('../models').activity;
const Academy = require('../models').academy;
const Checkin = require('../models').checkin;
const UserPromotion = require('../models').user_promotion_timeline;
const UserActive = require('../models').user_active;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
const Bluebird = require('bluebird');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/total_class', (req, res) => {
    
    let end_date = '';
    let start_date = '';
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Activity});

    if(normalizedQuery.filter.hasOwnProperty('created_at') && normalizedQuery.filter.created_at.hasOwnProperty('lte')){
      end_date = normalizedQuery.filter.created_at.lte;
      start_date = normalizedQuery.filter.created_at.gte;
    }

    if(normalizedQuery.filter.hasOwnProperty('created_at')){
      // delete normalizedQuery.filter.created_at;
    }

    return helper.findAllModelData(normalizedQuery, Activity, null, []).then((data) => {
      console.log('​module.exports -> normalizedQuery', normalizedQuery);
      let result = {
        "others": {
          "total": 0,
          "active_total": 0
        },
        "class": {
            "total": 0,
            "active_total": 0
        },
        "mat_event": {
            "total": 0,
            "active_total": 0,
        },
        "seminar": {
          "total": 0,
          "active_total": 0
        }
      };
      data.forEach(_item => {
        let item = _item.toJSON();

        let active_total = false

        if (moment(item.end_date) <= moment(end_date) && moment() >= moment(item.end_date)){
          active_total= true
        }

        if (result[item.type]){
          result[item.type] = {
            total: result[item.type].total+1,
            active_total: active_total ? result[item.type].active_total + 1 : result[item.type].active_total
          }
        } else {
          result[item.type] = {
            total: 1,
            active_total: active_total ? 1 : 0
          }
        }
      });
      res.json(result);
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/promotion_view', (req, res) => {
    let result = {stripe: 0, belt: 0};
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: UserPromotion});
    normalizedQuery.filter["status"] = 1;
    return helper.findAllModelData(normalizedQuery, UserPromotion, null, []).then((data) => {
      if(data){
        data.toJSON().forEach(item => {
          console.log('​module.exports -> item', item);
          result[item.next_promotion] = result[item.next_promotion] + 1;
        })
      }
      return null;
    }).then(() => {
      res.json(result);
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/revenue', (req, res) => {
    let where = '';
    const Bookshelf = req.app.get('database');
    if (req.query.hasOwnProperty('academy_id')){
      where = 'where academy_id =' + req.query.academy_id;
    }

    if (req.query.hasOwnProperty('date_from')){
      if(where){
        where = where + ' and created_at >= \'' + req.query.date_from + '\'';
      } else {
        where = 'where created_at >= \'' + req.query.date_from + '\'';
      }
    }

    if (req.query.hasOwnProperty('date_to')){
      if(where){
        where = where + ' and created_at <= \'' + req.query.date_to +'\''
      } else {
        where = 'where created_at <= \'' + req.query.date_to + '\'';
      }
    }
    console.log('where', where)
    let sql = "SELECT type, DATE_FORMAT(created_at,'%Y-%m') as date, SUM(amount) as total FROM transactions " + where + " GROUP BY date, type";
    return Bookshelf.knex.raw(sql).then((_data) => {
      let data = _data[0];

      let result = {
        subscription: [],
        shop: [],
        event: [],
      } 
      data.forEach(item => {
        switch (item.type) {
          case 'subscription':
            result.subscription.push(item)
            break;
          case 'activity':
            result.event.push(item)
            break;
          case 'shop_order_academy':
            result.shop.push(item)
            break;
        
          default:
            break;
        }
      });

      res.json(result);
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/marketing', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: UserActive});
    return helper.findAllModelData(normalizedQuery, UserActive, null, []).then((_data) => {
      

      let data = _data.toJSON();
      let result = {
        cancelled: 0,
        inactive: 0,
        free_trial: 0,
        events: 0,
      }

      data.forEach(item => {
        if(item.type == 'Free trial' && item.status == 1){
          result.free_trial += 1;
        }

        if(item.type == 'Cancelled' && item.status == 1){
          result.cancelled += 1;
        }

        if(item.type == 'Inactive' && item.status == 1){
          result.inactive += 1;
        }

        if(item.type == 'Event' || item.type == 'Class' || item.type == 'Seminar' || item.type == 'Others'){
          result.events += 1;
        }
      });

      res.json(result);
    }).catch((err) => {
      return res.status(500).json(err)
    })
  })

  router.post('/report_admin',async(req, res) => {
    try {

      let result = [];

      /*
        {
          tyle: 'academy' || 'student',
          academy_type: 'all' || 'pro' || 'regular',
          academy_id: '' || '{id}'
          activity: 'all' || 'subscription' || 'order' || 'class'
          "subscription_id": "" || "{id}"
          date_from: '2018-10-14',
          date_to: '2018-11-14'
        }

      */

      /*
        {
          tyle: 'academy' || 'student',
          student_status: 'All' || 'Cancelled' || 'Active' || 'Free trial' || 'Others' || 'Event' || 'Class' || 'Seminar',
          student_id: '' || '{id}'
          activity: 'all' || 'subscription' || 'order' || 'class'
          "subscription_id": "" || "{id}"
          date_from: '2018-10-14',
          date_to: '2018-11-14'
        }

      */


      let {type, academy_type, academy_id, date_from, date_to, activity, subscription_id, student_id, student_status} = req.body;

      if(type == 'academy'){
        let academy_where = {};
        switch (academy_type) {
          case 'pro':
            academy_where = {is_pro: 1}
            break;
          case 'regular':
            academy_where = {is_pro: 0}
            break;
        
          default:
            break;
        }

        let academies = Academy.where(academy_where);

        if(academy_id){
          // academy_where = {id: academy_id}
          academies = academies.where('id', 'in', academy_id.split(','))
        }

        academies = await academies.fetchAll({withRelated: [
          'students.transactions.order',
          'students.transactions.subscription',
          'students.transactions.activity',
          'students.transactions.academy',
        ]});

        academies = academies.toJSON();
				
        academies.forEach(academy => {
          academy.students.forEach((student) => {
            student.transactions.forEach(transaction => {
              // console.log("​transaction", transaction);

              let is_valid_date = false;
              if(moment(date_from) <= moment(transaction.created_at) && moment(transaction.created_at) <= moment(date_to) ){
                is_valid_date = true;
              }

              if(~['activity', 'subscription', 'order'].indexOf(transaction.type) && is_valid_date){
                
                let activity_type = transaction.type;
								console.log("​transaction.type", transaction.type)

                if(transaction.type == 'activity'){
                  activity_type = transaction.activity.type;
                }

                if(activity == 'subscription' && transaction.type == 'subscription'){
									console.log("​transaction.type", transaction.type)
                  
                  if(subscription_id && subscription_id != transaction.subscription_id){
                    return;
                  } 
                  result.push({
                    academy_name: academy.name,
                    academy_status: academy.is_pro ? 'pro' : 'regular',
                    date_from: date_from,
                    date_to: date_to,
                    activity_type,
                    activity_name: transaction.subscription.name,
                    student_name: student.full_name,
                    amount: transaction.amount,
                    date_transaction:  transaction.created_at
                  }) 
                } else if('order' == activity){
									console.log("​transaction.type", transaction.type)
                  result.push({
                    academy_name: academy.name,
                    academy_status: academy.is_pro ? 'pro' : 'regular',
                    date_from: date_from,
                    date_to: date_to,
                    activity_type,
                    activity_name: '',
                    student_name: student.full_name,
                    amount: transaction.amount,
                    date_transaction:  transaction.created_at
                  });
                } else if(transaction.type == 'activity' && activity_type == activity){
                  result.push({
                    academy_name: academy.name,
                    academy_status: academy.is_pro ? 'pro' : 'regular',
                    date_from: date_from,
                    date_to: date_to,
                    activity_type,
                    activity_name: transaction.activity.name,
                    student_name: student.full_name,
                    amount: transaction.amount,
                    date_transaction:  transaction.created_at
                  })
                } else if(activity == 'all'){
                  let name = '';
                  if(transaction.type == 'activity'){
                    name = transaction.activity.name || '';
                  }
                  if(transaction.type == 'subscription'){
                    name = transaction.subscription.name || '';
                  }
                  result.push({
                    academy_name: academy.name,
                    academy_status: academy.is_pro ? 'pro' : 'regular',
                    date_from: date_from,
                    date_to: date_to,
                    activity_type,
                    activity_name: name,
                    student_name: student.full_name,
                    amount: transaction.amount,
                    date_transaction:  transaction.created_at
                  })
                }
              }
              
            })
          })

        })


      } else if(type == 'student') {
        let whereStudent = {is_student: 1};


        let students = User.where(whereStudent);

        if(student_id){
          students = students.where('id', 'in', student_id.split(','))
        }

        students = await students.fetchAll({withRelated: [
          'transactions.order',
          'transactions.subscription',
          'transactions.activity',
          'transactions.academy',
        ]});

        students = students.toJSON();

        let users_active = await UserActive.where('user_id', 'in', students.map(item => item.id)).where({status: 1}).fetchAll();
        users_active = users_active.toJSON();

        students.forEach(student => {
          student.student_status = '';
          let index = _.findIndex(users_active, ['user_id', student.id]);
          if(~index){
            student.student_status = users_active[index].type;
          }
        })

        students.forEach(student => {
          let is_pushed = false;

          if(student_status == 'All'){
            is_pushed = true;
          } else if(student_status == student.student_status){
            is_pushed = true;
          }

          if(is_pushed){
            student.transactions.forEach(transaction => {
              let is_valid_date = false;
              if(moment(date_from) <= moment(transaction.created_at) && moment(transaction.created_at) <= moment(date_to) ){
                is_valid_date = true;
              }
  
              if(~['activity', 'subscription', 'order'].indexOf(transaction.type) && is_valid_date){
                
                let activity_type = transaction.type;
  
                if(transaction.type == 'activity'){
                  activity_type = transaction.activity.type;
                }
                if(activity == 'subscription' && transaction.type == 'subscription'){
                  
                  if(subscription_id && subscription_id != transaction.subscription_id){
                    return;
                  } 
                  result.push({
                    student_name: student.full_name,
                    status: student.student_status,
                    date_from: date_from,
                    date_to: date_to,
                    academy_name: transaction.academy.name,
                    activity_type,
                    activity_name: transaction.subscription.name || '',
                    amount: transaction.amount,
                    date_transaction:  transaction.created_at
                  }) 
                } else if('order' == activity){
                  console.log("​transaction.type", transaction.type)
                  result.push({
                    student_name: student.full_name,
                    status: student.student_status,
                    date_from: date_from,
                    date_to: date_to,
                    academy_name: transaction.academy.name,
                    activity_type,
                    activity_name: transaction.order.order_name || '',
                    amount: transaction.amount,
                    date_transaction:  transaction.created_at
                  });
                } else if(transaction.type == 'activity' && activity_type == activity){
                  result.push({
                    student_name: student.full_name,
                    status: student.student_status,
                    date_from: date_from,
                    date_to: date_to,
                    academy_name: transaction.academy.name,
                    activity_type,
                    activity_name: transaction.activity.name || '',
                    amount: transaction.amount,
                    date_transaction:  transaction.created_at
                  })
                } else if(activity == 'all'){
                  let name = '';
                  if(transaction.type == 'activity'){
                    name = transaction.activity.name || '';
                  }
                  if(transaction.type == 'subscription'){
                    name = transaction.subscription.name || '';
                  }
                  result.push({
                    student_name: student.full_name,
                    status: student.student_status,
                    date_from: date_from,
                    date_to: date_to,
                    academy_name: transaction.academy.name,
                    activity_type,
                    activity_name: name,
                    amount: transaction.amount,
                    date_transaction:  transaction.created_at
                  })
                }
              }
            })
          }
          
        })

      }




      res.json({status: true, data: result})


    } catch (error) {
			console.log("​}catch -> error", error)
      helper.errorResponse(res, [ error ]);
    }
  })

  return router;
}