
'use strict';
const Transaction = require('../models').transaction;
const User = require('../models').user;
const BeltUser = require('../models').belt_user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Transaction});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, Transaction, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Transaction.getAttributes(), Transaction.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}