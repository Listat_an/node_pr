
'use strict';
const UserPrizesInfo = require('../models').user_prizes_info;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: UserPrizesInfo});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, UserPrizesInfo, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(UserPrizesInfo.getAttributes(), UserPrizesInfo.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    UserPrizesInfo.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found UserPrizesInfo"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    req.body.user_id = req.user.id;
    helper.validateData(req.body,
      Joi.object().keys({
        content: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return UserPrizesInfo.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    req.body.user_id = req.user.id;
    UserPrizesInfo.forge({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found UserPrizesInfo', 404))
      }
      return findModel.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    UserPrizesInfo.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}