
'use strict';
const Follower = require('../models').follower;
const User = require('../models').user;
const BeltUser = require('../models').belt_user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const AcademyFollower = require('../models').academy_follower;
const Academy = require('../models').academy;

const multer  = require('multer')

module.exports = (router) => {

  router.get('/status/:id([0-9]+)', (req, res) => {
    return Follower.where({friend_id: req.params.id, user_id: req.user.id}).fetch().then(folower => {
      if (folower){
        return res.json({status: true});
      }
      return res.json({status: false});
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  })

  router.get('/users', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: User});
    let followers;
    return Follower.where({user_id: req.user.id}).fetchAll().then(_followers => {
      followers = _followers.toJSON();
      normalizedQuery.filter.id = {in: followers.map(item => item.friend_id)};
      return Promise.all([
        helper.fetchPaginationData(normalizedQuery, User, null, []),
        BeltUser.where('user_id', 'in', followers.map(item => item.friend_id)).fetchAll({withRelated: ['belt']})
      ]) 
    }).then(([_data, _belts]) => {
      let users = helper.indexResponse(_data, _.concat(User.getAttributes(), User.getInclideAttributes()));
      let belts = _belts.toJSON();
      users.data = users.data.map(user => {
        user.is_following = false;
        let index = _.findIndex(followers, ['friend_id', user.id]);
        if(index > -1){ 
          user.is_following = followers[index];
        }

        let index_belt = _.findIndex(belts, ['user_id', user.id]);
        user.belt = null;
        if(index_belt > -1){ 
          user.belt = belts[index_belt].belt;
        }
        return user;
      })
      
      res.json(users);
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  });

  router.get('/users/:id([0-9]+)', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: User});
    let followers;
    return Follower.where({user_id: req.params.id}).fetchAll().then(_followers => {
      followers = _followers.toJSON();
      normalizedQuery.filter.id = {in: followers.map(item => item.friend_id)};
      return Promise.all([
        helper.fetchPaginationData(normalizedQuery, User, null, []),
        BeltUser.where('user_id', 'in', followers.map(item => item.friend_id)).fetchAll({withRelated: ['belt']}),
        Follower.where({friend_id: req.user.id}).fetchAll()
      ]) 
    }).then(([_data, _belts, _followers]) => {
      let users = helper.indexResponse(_data, _.concat(User.getAttributes(), User.getInclideAttributes()));
      let belts = _belts.toJSON();
      let followers_tmp = _followers.toJSON();
      users.data = users.data.map(user => {
        user.is_following = false;
        user.is_my_follower = false;

        let index_follower = _.findIndex(followers_tmp, ['user_id', user.id]);
        if(~index_follower){
          user.is_my_follower = true;
        }

        let index = _.findIndex(followers, ['friend_id', user.id]);
        if(index > -1){ 
          user.is_following = followers[index];
        }

        let index_belt = _.findIndex(belts, ['user_id', user.id]);
        user.belt = null;
        if(index_belt > -1){ 
          user.belt = belts[index_belt].belt;
        }
        return user;
      })
      
      res.json(users);
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  });
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Follower});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, Follower, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Follower.getAttributes(), Follower.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Follower.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Follower"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/add', (req, res) => {
    var user;
    helper.validateData(req.body,
      Joi.object().keys({
        friend_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return User.where({id: req.body.friend_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found User', 404))
      }
      user = findModel;
      return Follower.where({user_id: req.user.id, friend_id: req.body.friend_id}).fetch();
    }).then( follower => {
      if(follower){
        return null;
      }
      return Follower.forge({user_id: req.user.id, friend_id: req.body.friend_id, status: user.get('status_public')}).save();
    }).then((data) => {
      if(!data){
        return null;
      }
      return Promise.all([
        User.where({id: req.body.friend_id}).fetch(),
        User.where({id: req.user.id}).fetch(),
      ]).then(_findUsers => {
        let findUser = _findUsers[0].toJSON();
        let me = _findUsers[1].toJSON();
        if(findUser.status_public){
          helper.createNotification(req, req.body.friend_id, '<a href="/#/profile/'+ me.id +'">'+me.first_name + ' ' + me.last_name + '</a> started to follow your profile');
        } else {
          helper.createNotification(req, req.body.friend_id, '<a href="/#/profile/'+ me.id +'">'+me.first_name + ' ' + me.last_name + '</a> started to follow your profile', 'user', 'follower');
        }
        helper.createNotification(req, req.user.id, 'You started to follow ' + '<a href="/#/profile/'+ findUser.id +'">' + findUser.first_name + ' ' + findUser.last_name + '</a>');
        return true;
      });
    }).then(() => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    
    Follower.forge({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found follower', 404))
      }
      return findModel.save({status: req.body.status}, { method: 'update'})
    }).then(data => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/remove', (req, res) => {
    
    helper.validateData(req.body,
      Joi.object().keys({
        friend_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return User.forge({id: req.body.friend_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found User', 404))
      }

      return Follower.where({user_id: req.user.id, friend_id: req.body.friend_id}).destroy();
    }).then((data) => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.get('/user/:id([0-9]+)/academies', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Academy});
    
    AcademyFollower.where({user_id: req.params.id}).fetchAll().then(_followers => {
      let followers = _followers.toJSON();
      normalizedQuery.filter.id = {in: followers.map(item => item.academy_id)};
      return Promise.all([
        helper.fetchPaginationData(normalizedQuery, Academy, null, []),
        // Academy.where('id', 'in', followers.map(item => item.academy_id)).fetchAll({withRelated: ['belt']})
      ]) 
    }).then(([_data, _academies]) => {
      let data = helper.indexResponse(_data, _.concat(Academy.getAttributes(), Academy.getInclideAttributes()));
      // let academies = _academies.toJSON();
      data.data.map(item => {
        item.is_following = true;
        return item;
      })
      res.json(data);
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  })

  return router;
}