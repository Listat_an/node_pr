'use strict';
const Post = require('../models').post;
const Academy = require('../models').academy;
const Activity = require('../models').activity;
const PostFile = require('../models').post_file;
const Follower = require('../models').follower;
const User = require('../models').user;
const PostLike = require('../models').post_like;
const SharePost = require('../models').share_post;
const DeletePost = require('../models').delete_post;
const AcademyFollower = require('../models').academy_follower;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');

// const app = require('../../app');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Post});

    return Follower.where({user_id: req.user.id}).fetchAll().then(friends => {
      if (!friends){
        return []
      }

      let user_ids = [req.user.id];

      friends.forEach(friend => {
        user_ids.push(friend.get('friend_id'))
      });

      return Promise.all([
        user_ids,
        AcademyFollower.where({user_id: req.user.id}).fetchAll().then(academies => {
          return academies.map(item => item.get('academy_id'));
        })
      ]);

    }).then(([user_ids, academy_ids]) => {
      
      return Promise.all([
        Post.where('share_from','in', ['all','follower']).where('user_id','in', user_ids).where('academy_id','<', 1).fetchAll(),
        Post.where('share_from','in', ['all','follower']).where('academy_id','in', academy_ids).fetchAll()
      ]).then(([usersPost, academiesPost]) => {
        return _.concat(usersPost.map(item => item.id), academiesPost.map(item => item.id))
      });
    }).then((ids) => {
      if (!normalizedQuery.filter.id){
        normalizedQuery.filter.id = {in: ids.toString()};
    }
      // normalizedQuery.filter.share_from = {in: 'all,follower'};

      if(normalizedQuery.include.indexOf('likes') < 0){
        normalizedQuery.include.push('likes')
      }
      if(normalizedQuery.include.indexOf('comments') < 0){
        normalizedQuery.include.push('comments')
      }
      return helper.fetchPaginationData(normalizedQuery, Post, null, [])
    }).then((data) => {
      let posts = helper.indexResponse(data, _.concat(Post.getAttributes(), Post.getInclideAttributes()));
      posts.data.forEach(post => {
        post.time = moment(post.updated_at).fromNow();
        post.is_like = false;
        if(_.findIndex(post.likes, ['user_id', req.user.id]) > -1){
          post.is_like = true;
        }
        post.count_comments = post.comments.length;
        delete post.likes;
        delete post.comments;
      });
      res.json(posts)
    }).catch((err) => {
      console.log('eer', err)
      return res.status(500).json(err)
    })
  });

  router.get('/me', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Post});
    
    return Promise.all([
      SharePost.where({user_id: req.user.id}).fetchAll(),
      Post.where({user_id: req.user.id}).fetchAll()
    ]).then(([sharePost, posts]) => {
      let post_ids = [];
      if(sharePost){
        sharePost.forEach(post => {
          post_ids.push(post.get('post_id'))
        });
      }

      if(posts){
        posts.forEach(post => {
          post_ids.push(post.get('id'))
        });
      }
      

      return post_ids;

    }).then(post_ids => {
      normalizedQuery.filter.id = {in: post_ids.toString()};
      //TODO add ids post share
      if(normalizedQuery.include.indexOf('likes') < 0){
        normalizedQuery.include.push('likes')
      }
      if(normalizedQuery.include.indexOf('comments') < 0){
        normalizedQuery.include.push('comments')
      }
      return helper.fetchPaginationData(normalizedQuery, Post, null, [])
    }).then((data) => {
      let posts = helper.indexResponse(data, _.concat(Post.getAttributes(), Post.getInclideAttributes()));
      posts.data.forEach(post => {
        post.time = moment(post.updated_at).fromNow();
        post.is_like = false;
        if(_.findIndex(post.likes, ['user_id', req.user.id]) > -1){
          post.is_like = true;
        }
        post.count_comments = post.comments.length;
        delete post.likes;
        delete post.comments;
      });
      res.json(posts)
    }).catch((err) => {
      console.log('eer', err)
      return res.status(500).json(err)
    })
  });

  router.get('/user/:id([0-9]+)', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Post});

    console.log('normalizedQuery', normalizedQuery);
    return Promise.all([
      SharePost.where({user_id: req.params.id}).fetchAll(),
      Post.where({user_id: req.params.id}).fetchAll()
    ]).then(([sharePost, posts]) => {
      

      let post_ids = [];
      if(sharePost){
        sharePost.forEach(post => {
          post_ids.push(post.get('post_id'))
        });
      }

      if(posts){
        posts.forEach(post => {
          post_ids.push(post.get('id'))
        });
      }
      

      return post_ids;

    }).then(post_ids => {
      normalizedQuery.filter.id = {in: post_ids.toString()};
      //TODO add ids post share
      if(normalizedQuery.include.indexOf('likes') < 0){
        normalizedQuery.include.push('likes')
      }
      if(normalizedQuery.include.indexOf('comments') < 0){
        normalizedQuery.include.push('comments')
      }
      return helper.fetchPaginationData(normalizedQuery, Post, null, [])
    }).then((data) => {
      let posts = helper.indexResponse(data, _.concat(Post.getAttributes(), Post.getInclideAttributes()));
      posts.data.forEach(post => {
        post.time = moment(post.updated_at).fromNow();
        post.is_like = false;
        if(_.findIndex(post.likes, ['user_id', req.params.id]) > -1){
          post.is_like = true;
        }
        post.count_comments = post.comments.length;
        delete post.likes;
        delete post.comments;
      });
      res.json(posts)
    }).catch((err) => {
      console.log('eer', err)
      return res.status(500).json(err)
    })
  });

  router.get('/academy/:id([0-9]+)', (req, res) => {
    let user_id = req.user.id;
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Post});

    Academy.forge({id: req.params.id}).fetch().then(academy => {
      if(!academy){
        return Promise.reject(new AppError("Not found academy", 404)); 
      }
      return academy;
    }).then(academy => {
      normalizedQuery.filter.academy_id = academy.get('id');
      //TODO add ids post share
      if(normalizedQuery.include.indexOf('likes') < 0){
        normalizedQuery.include.push('likes')
      }
      if(normalizedQuery.include.indexOf('comments') < 0){
        normalizedQuery.include.push('comments')
      }
      return helper.fetchPaginationData(normalizedQuery, Post, null, [])
    }).then((data) => {
      let posts = helper.indexResponse(data, _.concat(Post.getAttributes(), Post.getInclideAttributes()));
      posts.data.forEach(post => {
        post.time = moment(post.updated_at).fromNow();
        post.is_like = false;
        if(_.findIndex(post.likes, ['user_id', user_id]) > -1){
          post.is_like = true;
        }
        post.count_comments = post.comments.length;
        delete post.likes;
        delete post.comments;
      });
      res.json(posts)
    }).catch((err) => {
      console.log('eer', err)
      return helper.errorResponse(res, [err]);
    })
  });

  router.get('/activity/:id([0-9]+)', (req, res) => {
    let user_id = req.user.id;
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Post});

    Activity.forge({id: req.params.id}).fetch().then(activity => {
      if(!activity){
        return Promise.reject(new AppError("Not found activity", 404)); 
      }
      return activity;
    }).then(activity => {
      normalizedQuery.filter.activity_id = activity.get('id');
      //TODO add ids post share
      if(normalizedQuery.include.indexOf('likes') < 0){
        normalizedQuery.include.push('likes')
      }
      if(normalizedQuery.include.indexOf('comments') < 0){
        normalizedQuery.include.push('comments')
      }
      return helper.fetchPaginationData(normalizedQuery, Post, null, [])
    }).then((data) => {
      let posts = helper.indexResponse(data, _.concat(Post.getAttributes(), Post.getInclideAttributes()));
      posts.data.forEach(post => {
        post.time = moment(post.updated_at).fromNow();
        post.is_like = false;
        if(_.findIndex(post.likes, ['user_id', user_id]) > -1){
          post.is_like = true;
        }
        post.count_comments = post.comments.length;
        delete post.likes;
        delete post.comments;
      });
      res.json(posts)
    }).catch((err) => {
      console.log('eer', err)
      return helper.errorResponse(res, [err]);
    })
  });

  router.get('/worldwide', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Post});
    const Bookshelf = req.app.get('database');

    let distance_ids = [];
    let folloving_ids = [];
    let academy_ids = [];

    User.forge({id: req.user.id}).fetch().then(user => {
      if(!user){
        return [[]];
      }

      let distance = 500;

      if(req.query.distance){
        distance = req.query.distance
      }

      var distance_select = 'CEILING(' +
        '3963.1676 * acos ( ' +
        'cos ( radians(' + user.get('latitude') + ') )' +
        '* cos( radians( posts.`latitude` ) )' +
        '* cos( radians( posts.`longitude` ) - radians(' + user.get('longitude') + ') )' +
        '+ sin ( radians(' + user.get('latitude') + ') )' +
        '* sin( radians( posts.`latitude` ) ) )' +
        ') AS distance '
      return Bookshelf.knex.raw('SELECT id, ' + distance_select + '  FROM posts HAVING CEILING(distance) <= ' + distance + ' ORDER BY distance ASC' )
      // return Bookshelf.knex.raw('SELECT *, 69.0 * DEGREES(ACOS(COS(RADIANS(' + user.get('latitude') + ')) * COS(RADIANS(posts.latitude)) * COS(RADIANS(' + user.get('longitude') + ' - posts.longitude)) + SIN(RADIANS(' + user.get('latitude') + ')) * SIN(RADIANS(posts.latitude)))) AS distance FROM posts HAVING distance < 50.0')
    }).then((data) => {
      distance_ids = data[0].map(post => post.id);
      return Promise.all([
        Follower.where({user_id: req.user.id}).fetchAll(),
        AcademyFollower.where({user_id: req.user.id}).fetchAll(),
      ]);
    }).then(([follovings, academies]) => {
      normalizedQuery.filter.id = {in: distance_ids.toString()};
      normalizedQuery.filter.share_from = {in: 'all'};

      if(normalizedQuery.include.indexOf('likes') < 0){
        normalizedQuery.include.push('likes')
      }
      if(normalizedQuery.include.indexOf('comments') < 0){
        normalizedQuery.include.push('comments')
      }

      normalizedQuery.sort = [];

       normalizedQuery.filterOr.user_id = {in: follovings.map(item => item.get('friend_id').toString())}
       normalizedQuery.filterOr.academy_id = {in: academies.map(item => item.get('id').toString())}

      
      return helper.findAllModelData(normalizedQuery, Post, null, [])
    }).then((data) => {
      // console.log('data', data.fetchPage({page: 10, pageSize: 20}));
      let total = data.length;
      data = data.toJSON();
      // let posts = helper.indexResponse(data, _.concat(Post.getAttributes(), Post.getInclideAttributes()));
      let posts = helper.paginationArray(_.orderBy(data, ['distance_post', 'created_at'], ['asc', 'desc']), normalizedQuery.page.size, normalizedQuery.page.number)
      posts.forEach(post => {
        post.time = moment(post.updated_at).fromNow();
        post.is_like = false;
        if(_.findIndex(post.likes, ['user_id', req.user.id]) > -1){
          post.is_like = true;
        }
        post.count_comments = post.comments.length;
        delete post.likes;
        delete post.comments;
      });
      res.json({
        total,
        limit: parseInt(normalizedQuery.page.size),
        skip: ((normalizedQuery.page.number-1) * normalizedQuery.page.size),
        data: posts})
    }).catch((err) => {
      console.log('eer', err)
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Post});
    Post.where({id: req.params.id}).fetch({withRelated: normalizedQuery.include}).then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Post"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    let post;
    req.body.user_id = req.user.id;
    helper.validateData(req.body,
      Joi.object().keys({
        // title: Joi.string().required(),
        // content: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        User.forge({id: req.user.id}).fetch(),
        req.body.academy_id > 0 ? Academy.where({id: req.body.academy_id}).fetch() : null
      ]);
    }).then(([user, academy]) => {
      if(academy){
        req.body.latitude = academy.get('latitude');
        req.body.longitude = academy.get('longitude');
      } else {
        req.body.latitude = user.get('latitude');
        req.body.longitude = user.get('longitude');
      }
      return Post.forge(req.body).save();
    }).then((data) => {
      if(data.get('academy_id') > 0){
        return Promise.all([
          AcademyFollower.where({academy_id: data.get('academy_id')}).fetchAll(),
          User.where({academy_id: data.get('academy_id')}).fetchAll(),
          Academy.where({id: data.get('academy_id')}).fetch()
        ]).then((findModels) => {
          let followers = findModels[0];
          let users = findModels[1];
          let academy = findModels[2].toJSON();
          followers.map(item => {
            if(item.get('user_id') != academy.user_id){
              helper.createNotification(req, item.get('user_id'), '<a href="/#/academiy-datail/' + academy.id + '">' + academy.name + '</a> has new post');
            }
          });
          // users.map(item => {
          //   helper.createNotification(req, item.id, '<a href="/#/academiy-datail/' + academy.id + '">' + academy.name + '</a> has new post');
          // });
          return data;
        })
      }
      return data;
    }).then((data) => {
      return Post.where({id: data.id}).fetch();
    }).then((_data) => {
      let post = _data.toJSON();
      post.time = moment(post.updated_at).fromNow();
      post.is_like = false;
      res.json({status: true, post});
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    req.body.user_id = req.user.id;
    let post;
    helper.validateData(req.body,
      Joi.object().keys({
        // title: Joi.string().required(),
        // content: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Post.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Post', 404))
      }
      return findModel.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, post: data.toJSON()})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Post.forge({ id: req.params.id }).destroy().then(() => {
      PostFile.where({post_id: req.params.id}).fetchAll().then(files => {
        files.forEach(file => {
          if (fs.existsSync('public/'+file.get('url'))){
            fs.unlinkSync('public/'+file.get('url'));
          }
          file.destroy();
        })
      });
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.post('/like', (req, res) => {
    let postFind;
    helper.validateData(req.body,
      Joi.object().keys({
        post_id: Joi.number().required()
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Post.forge({id: req.body.post_id}).fetch(),
        PostLike.where({user_id: req.user.id, post_id: req.body.post_id}).fetch(),
        User.forge({id: req.user.id}).fetch()
      ]) 
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Post', 404))
      }
      postFind = findModel[0].toJSON();

      if(findModel[1]){
        return null;
      }

      return Promise.all([
        findModel[0].save({count_like: findModel[0].get('count_like') + 1, name_user_like: findModel[2].get('first_name') + ' ' + findModel[2].get('last_name') }, { method: 'update'}),
        PostLike.forge({user_id: req.user.id, post_id: req.body.post_id}).save()
      ])
    }).then(data => {
      User.where().fetch({id: postFind.user_id,}).then(_findUser => {
        findUser = _findUser.toJSON();
        helper.createNotification(req, postFind.user_id, '<a href="/#/profile/'+ findUser.id +'">' + findUser.first_name + ' ' + findUser.last_name + '</a> liked your <a href="#/?show-p='+ req.body.post_id +'">post</a>', postFind.academy_id ? 'manager' : 'user');
      })
      res.json({status: true, action: 'like'})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/unlike', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        post_id: Joi.number().required()
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Post.where({id: req.body.post_id}).fetch(),
        PostLike.where({user_id: req.user.id, post_id: req.body.post_id}).fetch()
      ]) 
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Post', 404))
      }

      if(!findModel[1]){
        return null;
      }
      return Promise.all([
        findModel[0].save({count_like: findModel[0].get('count_like') - 1 }, { method: 'update'}),
        findModel[1].destroy()
      ])
    }).then(data => {
      res.json({status: true, action: 'unlike'})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/share', (req, res) => {
    let postFind, filesPost;
    helper.validateData(req.body,
      Joi.object().keys({
        post_id: Joi.number().required()
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Post.forge({id: req.body.post_id}).fetch(),
        PostFile.where({post_id: req.body.post_id}).fetchAll()
      ]);
    }).then(([post, files]) => {
      if(!post){
        return Promise.reject(new AppError('Not found Post', 404))
      }
      postFind = post.toJSON();
      filesPost = files.toJSON();
    //   return SharePost.where({user_id: req.user.id, post_id: req.body.post_id}).fetch();
    // }).then(findShare => {
    //   if(findShare){
    //     return true;
    //   }
    //   return SharePost.forge({user_id: req.user.id, post_id: req.body.post_id}).save(); 

      let postCreaet = Object.assign({}, postFind, {user_id: req.user.id, created_post_user_id: postFind.user_id});

      delete postCreaet.id;
      delete postCreaet.created_at;
      delete postCreaet.updated_at;

      return Post.forge(postCreaet).save();
    }).then((creaetPost) => {
      let arrayCreated = [];

      if(filesPost.length == 0){
        return true
      }

      filesPost.forEach(file => {
        let tmp_file = Object.assign({}, file, {user_id: req.user.id, post_id: creaetPost.id});
        delete tmp_file.id;
        delete tmp_file.created_at;
        delete tmp_file.updated_at;
        arrayCreated.push(PostFile.forge(tmp_file).save());
      });

      return Promise.all(arrayCreated);
    }).then(() => {

      User.where({id: postFind.user_id}).fetch().then(_findUser => {
        findUser = _findUser.toJSON();
        helper.createNotification(req, postFind.user_id, '<a href="/#/profile/'+ findUser.id +'">' + findUser.first_name + ' ' + findUser.last_name + '</a> shared your <a href="/#/?show-p='+ req.body.post_id +'">post</a> to own timeline');
      })
      res.json({status: true, action: 'share'})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/remove', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        post_id: Joi.number().required()
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Post.forge({id: req.body.post_id}).fetch();
    }).then(findPost => {
      if(!findPost){
        return Promise.reject(new AppError('Not found Post', 404))
      }
     
      if(req.user.id == findPost.get('user_id')){
        return Promise.all([
          findPost.destroy().then(() => {
            PostFile.where({post_id: req.body.post_id}).fetchAll().then(files => {
              files.forEach(file => {
                if (fs.existsSync('public/'+file.get('url'))){
                  fs.unlinkSync('public/'+file.get('url'));
                }
               file.destroy();
              })
            });
            return null;
          }),
          SharePost.where({post_id: req.body.post_id}).destroy(),
        ])
      } else {
        return Promise.all([
          SharePost.where({post_id: req.body.post_id, user_id: req.user.id}).destroy(),
          DeletePost.forge({post_id: req.body.post_id, user_id: req.user.id}).save(),
        ])
      }
      
    }).then(() => {
      res.json({status: true, action: 'remove'})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  })

  return router;
}