
'use strict';
const BeltUser = require('../models').belt_user;
const User = require('../models').user;
const Belt = require('../models').belt;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: BeltUser});

    return helper.fetchPaginationData(normalizedQuery, BeltUser, null, []).then((data) => {
      res.json(helper.indexResponse(data, BeltUser.getAttributes()))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    BeltUser.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found BeltUser"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().integer().required(),
        belt_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        User.forge({id: req.body.user_id}).fetch(),
        Belt.forge({id: req.body.belt_id}).fetch()
      ]);
    }).then(([user, belt]) => {

      if(!user){
        return Promise.reject(new AppError('Not found user', 404))
      }
      if(!belt){
        return Promise.reject(new AppError('Not found belt', 404))
      }

      return BeltUser.forge(req.body).save();
    }).then((data) => {
      res.json(data.toJSON())
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().integer().required(),
        belt_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        User.forge({id: req.body.user_id}).fetch(),
        Belt.forge({id: req.body.belt_id}).fetch()
      ]);
    }).then(([user, belt]) => {

      if(!user){
        return Promise.reject(new AppError('Not found user', 404))
      }
      if(!belt){
        return Promise.reject(new AppError('Not found belt', 404))
      }
      return BeltUser.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found BeltUser', 404))
      }
      return findModel.save(req.body, { method: 'update'})
    }).then(data => {
      res.json(data.toJSON())
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    BeltUser.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}