
'use strict';
const Academy = require('../models').academy;
const CalendarSubscription = require('../models').calendar_subscription;
const User = require('../models').user;
const Belt = require('../models').belt;
const Activity = require('../models').activity;
const StripeWebhook = require('../models').stripe_webhook;
const Subscription = require('../models').subscription;
const UserSubscription = require('../models').user_subscription;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const helperEmail = require('../lib/helperEmail');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const ExternalServices = require('../lib/external_services');
const Transaction = require('../models').transaction;
const UserTimeline = require('../models').user_timeline;
const AcademySubscrioption = require('../models').academy_subscription;
const moment = require('moment');
const multer  = require('multer')

module.exports = (router) => {
  
  router.post('/search_academy_event_user', (req, res) => {

    let queryAcademy =  { 
      filterOr: {},
      filter: Object.assign({}, (req.body.academy && req.body.academy.filter ? req.body.academy.filter : {}), { 
        name: { like: '%' + req.body.search + '%' } 
      }),
      page: { number: 1, size: 3 },
      sort: [ { field: 'created_at', direction: 'DESC' } ],
      include: _.concat([], req.body.academy && req.body.academy.include ? req.body.academy.include : [])  
    }

    let queryUser =  { 
      filterOr: {},
      filter: Object.assign({}, (req.body.user && req.body.user.filter ? req.body.user.filter : {}),{ full_name: { like: '%' + req.body.search + '%' } }),
      page: { number: 1, size: 3 },
      sort: [ { field: 'created_at', direction: 'DESC' } ],
      include: _.concat([], req.body.user && req.body.user.include ? req.body.user.include : [])  
    }

    let queryEvent = { 
      filterOr: {},
      filter: Object.assign({}, (req.body.event && req.body.event.filter ? req.body.event.filter : {}), { 
        name: { like: '%' + req.body.search + '%' }, 
        type: {not: 'class'} 
      }),
      page: { number: 1, size: 3 },
      sort: [ { field: 'created_at', direction: 'DESC' } ],
      include: _.concat([], req.body.event && req.body.event.include ? req.body.event.include : [])  
    }

    console.log('queryAcademy', queryAcademy);
    console.log('queryUser', queryUser);
    console.log('queryEvent', queryEvent);

     
  
    return Promise.all([
      helper.fetchPaginationData(queryAcademy, Academy, null, []),
      helper.fetchPaginationData(queryUser, User, null, []),
      helper.fetchPaginationData(queryEvent, Activity, null, []),
    ]).then(([academies, users, events]) => {
      res.json({
        academies,
        users,
        events
      })
    }).catch((err) => {
      console.log('err', err);
      return res.status(500).json(err)
    })
  });

  router.get('/get_template', (req, res) => {
    res.send(helperEmail.getTemplateEmail('confirmation', {
      user_name: 'TEst',
      confirmation_url: 'sdfsdf',
      site_url: 'sdf'
    }));
  });

  router.all('/stripe_webhook', (req, res) =>{
    Promise.resolve(true).then(() => {
      if(req.body.object == 'event' && req.body.type == 'charge.succeeded' && req.body.data 
      && req.body.data.object && req.body.data.object.object == 'charge' && req.body.data.object.invoice){
        return ExternalServices.invoiceStripeRetrieve(req.body.data.object.invoice)
      }
      if(req.body.object == 'event' && req.body.type == 'charge.failed' && req.body.data 
      && req.body.data.object && req.body.data.object.object == 'charge' && req.body.data.object.invoice){
        return ExternalServices.invoiceStripeRetrieve(req.body.data.object.invoice)
      }

      if(req.body.object.object == 'charge' && req.body.object.invoice){
        return ExternalServices.invoiceStripeRetrieve(req.body.object.invoice)
      }

      return null;
    }).then((invoice) => {
      console.log('invoice', invoice)
      if(!invoice)
        return null;

      if(invoice.lines.data.length && invoice.lines.data[0].metadata.type && invoice.lines.data[0].metadata.type && invoice.lines.data[0].metadata.type == 'subscription' ){
        return Subscription.where({plan_id: invoice.lines.data[0].metadata.plan_id}).fetch().then(subscription => {
          if(!subscription){
            return null;
          }
          if(invoice.paid){
            Transaction.forge({
              academy_id: subscription.get('academy_id'),
              type: 'subscription',
              subscription_id: subscription.get('id'),
              user_id: invoice.lines.data[0].metadata.user_id,
              amount: parseInt(invoice.lines.data[0].amount)/100,
              amount_app: 0,
              amount_academy: 0,
              amount_stripe: 0,
              procent: 0,
              charge_id: invoice.charge
            }).save().then(transaction => {
              UserTimeline.forge({
                data: 'Payment processed for subscription $' + parseInt(invoice.lines.data[0].amount)/100,
                user_id: invoice.lines.data[0].metadata.user_id,
                type: 'refund',
                details: '',
                transaction_id: transaction.id,
                is_change_refund: 0,
                academy_id: subscription.get('academy_id')
              }).save();
            });
            helper.userRevenue(invoice.lines.data[0].metadata.user_id, subscription.get('academy_id'), 'subscription', parseInt(invoice.lines.data[0].amount)/100);
            helper.createNotification(req, invoice.lines.data[0].metadata.user_id, 'Your payment ' + (parseInt(invoice.lines.data[0].amount)/100) + '$ subscription ' + subscription.get('name') + ' was processed. Thank you!');
            
            helper.userActive(invoice.lines.data[0].metadata.user_id, subscription.get('academy_id'), 'Active');

            CalendarSubscription.where({subscription_id:  subscription.get('id'), user_id: invoice.lines.data[0].metadata.user_id, pay_end: null}).fetch().then(calendar_data => {
              if(!calendar_data){
                CalendarSubscription.forge({
                  user_id: invoice.lines.data[0].metadata.user_id, 
                  subscription_id: subscription.get('id'),
                  pay_start: moment().format('YYYY-MM-DD HH:mm:ss')
                }).save();
              }
            });

            return ExternalServices.getStripeSubscription(invoice.subscription).then(stripeSubscription => {
              let data = {
                status: 1
              }

              if(stripeSubscription){
                data.billing_cycle_anchor = stripeSubscription.billing_cycle_anchor;
                data.current_period_end = stripeSubscription.current_period_end;
              }
                            
              return UserSubscription.where({
                user_id: invoice.lines.data[0].metadata.user_id, 
                subscription_id: subscription.get('id')
              }).save(data, { method: 'update'})
            });
          } else {
            UserTimeline.forge({
              data: 'Payment was declined for subscription ' + subscription.get('name'),
              user_id: invoice.lines.data[0].metadata.user_id,
              type: 'standart',
              details: '',
              academy_id: subscription.get('academy_id')
            }).save();
            helper.createNotification(req, invoice.lines.data[0].metadata.user_id, 'Your payment for Subscription ' + subscription.get('name') + ' was declined on ' + moment().format('Do MMMM YYYY') + '.')
            CalendarSubscription.where({
              subscription_id:  subscription.get('id'), 
              user_id: invoice.lines.data[0].metadata.user_id, 
              pay_end: null
            }).fetch().then(calendar_data => {
              if(calendar_data){
                calendar_data.set('pay_end', moment().format('YYYY-MM-DD HH:mm:ss'))
                calendar_data.save();
              }
            });
            return UserSubscription.where({user_id: invoice.lines.data[0].metadata.user_id, subscription_id: subscription.get('id')}).save({status: 0}, { method: 'update'});
          }
        })
      } else if(invoice.lines.data.length && invoice.lines.data[0].metadata.type && invoice.lines.data[0].metadata.type && invoice.lines.data[0].metadata.type == 'academy_subscription') {
        return AcademySubscrioption.where({stripe_plan_id: invoice.lines.data[0].metadata.plan_id}).fetch().then(subscription => {
          if(!subscription){
            return null;
          }

          if(invoice.paid){
            Transaction.forge({
              academy_id: invoice.lines.data[0].metadata.academy_id,
              type: 'academy_subscription',
              subscription_id: subscription.get('id'),
              user_id: invoice.lines.data[0].metadata.user_id,
              amount: parseInt(invoice.lines.data[0].amount)/100,
              amount_app: 0,
              amount_academy: 0,
              amount_stripe: 0,
              procent: 0,
              charge_id: invoice.charge
            }).save();
            ExternalServices.getStripeSubscription(invoice.subscription).then(stripeSubscription => {
              let data = {
                is_payd_subscription: 1,
                is_pro: 1
              }

              if(stripeSubscription){
                data.billing_cycle_anchor = stripeSubscription.billing_cycle_anchor;
                data.current_period_end = stripeSubscription.current_period_end;
              }
              Academy.where({id: invoice.lines.data[0].metadata.academy_id}).save(data, {method: 'update'});
            })
          } else {
            Academy.where({id: invoice.lines.data[0].metadata.academy_id}).save({is_payd_subscription: 0, is_pro: 1}, {method: 'update'});
          }
        })
      }
      // }
      return null
    }).then(() => {
      res.json({received: true});
    }).catch(err => {
      console.log('err', err)
      res.status(500).json(err)
    })

    // StripeWebhook.forge({data: JSON.stringify(req.body)}).save().then(data => {
    //   res.json({received: true});
    // }).catch(err => {
    //   res.status(500).json(err)
    // })


  })

  router.all('/get_stripe_webhook', (req, res) =>{
    console.log('Test webhook');
    StripeWebhook.forge().fetchAll().then(data => {
      res.json(data.toJSON().map(item => {
        item.data = JSON.parse(item.data);
        return item;
      }));
    }).catch(err => {
      res.status(500).json(err)
    })
  })

  return router;
}