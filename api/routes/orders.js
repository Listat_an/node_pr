
'use strict';
const Order = require('../models').order;
const Academy = require('../models').academy;
const Shipping = require('../models').shipping;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Order});

    return helper.fetchPaginationData(normalizedQuery, Order, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Order.getAttributes(), Order.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Order});
    Order.where({id: req.params.id}).fetch({withRelated: normalizedQuery.include}).then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Order"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let order;
    Order.where({id: req.params.id}).fetch().then(_order => {
      if(!_order){
        return Promise.reject(new AppError('Not found Order', 404))
      }
      order = _order.toJSON();
      order = Object.assign({}, order, req.body);

      let pickAttr = [
        'order_name',
        'first_name',
        'last_name',
        'full_name',
        'email',
        'phone',
        'state',
        'sity',
        'street',
        'billing_first_name',
        'billing_last_name',
        'billing_email',
        'billing_phone',
        'billing_state',
        'billing_sity',
        'billing_street',
        'billing_address',
      ]

      return _order.save(_.pick(req.body, pickAttr), { method: 'update'});
    }).then(_order => {
      res.json(order);
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/status/:id([0-9]+)', (req, res) => {
    let order;
    helper.validateData(req.body,
      Joi.object().keys({
        status: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Order.where({id: req.params.id}).fetch();
    }).then(_order => {
      if(!_order){
        return Promise.reject(new AppError('Not found Order', 404))
      }
      order = _order.toJSON();
      order.status = req.body.status;
      return _order.save({status: req.body.status}, { method: 'update'});
    }).then(_order => {
      if(req.body.status == 'in_process'){
        Order.where({id: order.user_order}).save({status: 'in_process'}, { method: 'update'});
      } else if(req.body.status == 'shipped') {
        Order.where({user_order: order.user_order}).fetchAll().then(_orders => {
          let orders = _orders.toJSON();
          let is_shipped = true;
          orders.some(order => {
            if(order.status != 'shipped'){
              is_shipped = false;
              return true;
            }
          });
          if(is_shipped){
            Order.where({id: order.user_order}).save({status: 'shipped'}, { method: 'update'});
          }
        })
      }
      helper.createNotification(req, order.user_id, 'Order #' + order.user_order + ' has new status', 'user', 'new_status', {order_id: order.user_order});
      res.json(order);
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });



  return router;
}