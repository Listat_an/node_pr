'use strict';
const UserTimeline = require('../models').user_timeline;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: UserTimeline});

    return helper.fetchPaginationData(normalizedQuery, UserTimeline, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(UserTimeline.getAttributes(), UserTimeline.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}