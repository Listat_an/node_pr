
'use strict';
const SubscriptionActivity = require('../models').subscription_activity;
const Subscription = require('../models').subscription;
const Activity = require('../models').activity;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const UserSubscription = require('../models').user_subscription;
const JoinActivity = require('../models').join_activity;

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: SubscriptionActivity});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, SubscriptionActivity, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(SubscriptionActivity.getAttributes(), SubscriptionActivity.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    SubscriptionActivity.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found SubscriptionActivity"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().required(),
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Subscription.forge({id: req.body.subscription_id}).fetch(),
        Activity.forge({id: req.body.activity_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Subscription', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Activity', 404))
      }

      return SubscriptionActivity.forge({user_id: req.user.id, activity_id: req.body.activity_id, subscription_id: req.body.subscription_id}).save();
    }).then((data) => {
      return UserSubscription.where({
        subscription_id: req.body.subscription_id,
        status: 1
      }).fetchAll().then(userSubscriptions => {
        userSubscriptions.map(item => {
          JoinActivity.forge({
            activity_id: req.body.activity_id,
            user_id: item.get('user_id'),
            status: true,
            amount: 0,
            subscription_id: req.body.subscription_id
          }).save();
        })
        return null;
      })
    }).then(() => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let subscription;
    SubscriptionActivity.forge({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found SubscriptionActivity', 404))
      }
      subscription = findModel;
      return Promise.all([
        Subscription.forge({id: req.body.subscription_id}).fetch(),
        Activity.forge({id: req.body.activity_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Subscription', 404))
      }
      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Activity', 404))
      }
      return subscription.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    SubscriptionActivity.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}