
'use strict';
const Report = require('../models').report;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    

    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Report});
    normalizedQuery.include = _.concat(normalizedQuery.include, Report.getInclideAttributes());
		console.log("​normalizedQuery.include", normalizedQuery.include)
    return helper.fetchPaginationData(normalizedQuery, Report, null, []).then((data) => {

      let reports = helper.indexResponse(data, _.concat(Report.getAttributes(), Report.getInclideAttributes()))

      reports.data = reports.data.map(item => {
        return _.pick(item, _.concat(Report.getAttributes(), [item.type] ));
      });

      res.json(reports)
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Report.where({id: req.params.id}).fetch({withRelated: normalizedQuery.include}).then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Report"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    req.body.user_id = req.user.id;
    helper.validateData(req.body,
      Joi.object().keys({
        tmp_id: Joi.number().integer().required(),
        type: Joi.string().required(),
        content: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Report.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, data: data.toJSON()});
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Report.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}