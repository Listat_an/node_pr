'use strict';
const User = require('../models').user;
const Stripe = require('../models').stripe;
const Academy = require('../models').academy;
const Belt = require('../models').belt;
const BeltSetting = require('../models').belt_setting;
const UserTimeline = require('../models').user_timeline;
const BeltUser = require('../models').belt_user;
const AppError = require('../lib/app_error');
const ExternalServices = require('../lib/external_services');
const SubscriptionAcademy = require('../models').subscription_academy;
const helperEmail = require('../lib/helperEmail');
const helper = require('../lib/helper');
const Joi = require('joi');
const _ = require('lodash');
const Bluebird = require('bluebird');
const UserAcademyInfo = require('../models').user_academy_info;
const Manager = require('../models').manager;
const AcademyFollower = require('../models').academy_follower;
const bcrypt = require('bcrypt'),
    config = require('../config'),
    jwt = require('jsonwebtoken'),
    crypto = require('crypto');

module.exports = (router) => {
    router.post('/signin', (req, res) => {
        let user;
        if (!req.body.email) {
            // return Promise.reject(new AppError("email required", 400));
            return res.status(400).json(new AppError("email required", 400))
        }
        if (!req.body.password) {
            return res.status(400).json(new AppError("password required", 400));
        }

        req.body.email = req.body.email.toLowerCase();

        User.where({
            email: req.body.email
        }).fetch().then((_user) => {
            if (_user === null) {
                return Promise.reject(new AppError("Incorrect credentials", 401));
            }
            user = _user.toJSON()
            return bcrypt.compare(req.body.password, user.password)
        }).then(matches => {
            if (!matches) {
                return Promise.reject(new AppError("Incorrect credentials", 401));
            }
            res.json({
                access_token: jwt.sign({
                    id: user.id,
                    role: user.role,
                    data: _.pick(user, ['latitude', 'longitude'])
                }, config.token_secret),
                user: _.omit(user, ['password', 'reset_token_password', 'role'])
            });
        }).catch((err) => {
            helper.errorResponse(res, [err]);
        });
    });

    router.post('/signup', (req, res) => {
        let user;
        let updateUser;

        let belt_id = null;

        if (!req.body.user) {
            return res.status(400).json({ message: 'user is requerd' })
        }

        helper.validateData(req.body.user,
            Joi.object().keys({
                first_name: Joi.string().alphanum().max(30).required(),
                last_name: Joi.string().alphanum().max(30).required(),
                password: Joi.string().required(),
                email: Joi.string().email().required(),
                academy_id: Joi.number().integer(),
                weight: Joi.number().required(),
                date: Joi.string().required()
            }), { abortEarly: false, allowUnknown: true }
        ).then(validBody => {
            return Promise.all([
                User.where({ email: validBody.email.toLowerCase() }).fetch(),
                bcrypt.hash(validBody.password, 12)
            ])
        }).then(([user, hash]) => {
            if (user) {
                return Promise.reject(new AppError("this email already exists ", 401));
            }
            req.body.user.password = hash;
            req.body.user.email = req.body.user.email.toLowerCase();
            req.body.user.role = 'user';
            req.body.user.active_kode = crypto.randomBytes(25).toString('hex');
            if (req.body.user.belt_id) {
                belt_id = req.body.user.belt_id;
                delete req.body.user.belt_id;
            }
            req.body.user.full_name = req.body.user.first_name + ' ' + req.body.user.last_name;
            return User.forge(req.body.user).save();
        }).then(createUser => {
            updateUser = createUser;
            user = createUser.toJSON();
            return ExternalServices.sendMail({
                from: config.from_email,
                to: user.email,
                subject: 'Confirmation',
                html: helperEmail.getTemplateEmail('confirmation', {
                    user_name: user.first_name + ' ' + user.last_name,
                    confirmation_url: config.site_front + '/#/auth/login/' + user.active_kode,
                    site_url: config.site_front
                })
            });
        }).then(() => {
            if (!belt_id) {
                return null;
            }
            return Belt.forge({ id: belt_id }).fetch();
        }).then((beltFind) => {
            if (!beltFind) {
                return null;
            }
            return BeltUser.forge({ user_id: user.id, belt_id }).save();
        }).then((beltUser) => {
            if (!beltUser) {
                return null;
            }
            let promiseAllModel = [];
            if (req.body.user.stripe) {
                for (var index = 0; index < req.body.user.stripe; index++) {
                    promiseAllModel.push(Stripe.forge({ belt_user_id: beltUser.id, name: '' }).save());
                }
            }
            if (promiseAllModel.length > 0)
                return Promise.all(promiseAllModel)
            else
                return null;
        }).then(() => {
            if (!req.body.academy) {
                return null;
            }
            let academy = req.body.academy;
            academy.user_id = user.id;
            academy.status = 'In_process';
            return Academy.forge(academy).save().then(academyCreate => {
                Manager.forge({user_id: user.id, academy_id: academyCreate.id}).save();
                Belt.where({stripe: 0}).fetchAll().then(belts => {
                    belts.map(belt => {
                      BeltSetting.forge({academy_id: academyCreate.id,  belt_id: belt.get('id'),
                        time_stripe_count: belt.get('time_stripe_count'),
                        class_stripe_count: belt.get('class_stripe_count')}).save()
                    });
                  });
                return academyCreate;
            });
        }).then((academyCreate) => {
            if (!academyCreate) {
                return null;
            }

            return updateUser.save({ academy_id: academyCreate.id }, { method: 'update' })
        }).then((_user) => {
            if (_user) {
                user = _user.toJSON();
            }
            if(!req.body.academy_name || user.academy_id){
                AcademyFollower.forge({user_id:  user.id, academy_id: user.academy_id}).save();
                return null;
            }

            return Academy.forge({name: req.body.academy_name, status: 'Not_created'}).save().then(createAcademy => {
                Manager.forge({user_id: user.id, academy_id: createAcademy.id}).save();
                Belt.where({stripe: 0}).fetchAll().then(belts => {
                    belts.map(belt => {
                      BeltSetting.forge({academy_id: createAcademy.id,  belt_id: belt.get('id'),
                        time_stripe_count: belt.get('time_stripe_count'),
                        class_stripe_count: belt.get('class_stripe_count')}).save()
                    });
                  });
                return updateUser.save({academy_id: createAcademy.id}, { method: 'update'});
            });
        }).then(() => {
            let data = user;
            helper.createNotification(req, data.id, '<a href="/#/profile/'+ data.id +'">' + data.first_name + ' ' + data.last_name + '</a> signed up to OPN MAT');
            UserTimeline.forge({
                data: 'Signed up to OPNMAT.com',
                user_id: data.id,
                type: 'standart',
                details: ''
            }).save()
            Academy.where({id: user.academy_id}).fetch().then(academy => {
                if(!academy){
                    return null;
                }
                UserAcademyInfo.forge({user_id: user.id, name: academy.get('name'), begin_date: '', end_date: 'Present' }).save();
            });
            
            res.json({
                access_token: jwt.sign({
                    id: data.id,
                    role: 'user',
                    data: _.pick(data, ['latitude', 'longitude'])
                }, config.token_secret),
                user: _.omit(data, ['password', 'reset_token_password', 'role'])
            });
        }).catch((err) => {
            console.log('err', err)
            helper.errorResponse(res, [err]);
        })
    });

    router.post('/signup_app', (req, res) => {
        let user;
        let updateUser;

        let belt_id = null;

        if (!req.body.user) {
            return res.status(400).json({ message: 'user is requerd' })
        }

        helper.validateData(req.body.user,
            Joi.object().keys({
                first_name: Joi.string().alphanum().max(30).required(),
                last_name: Joi.string().alphanum().max(30).required(),
                password: Joi.string().required(),
                email: Joi.string().email().required(),
                academy_id: Joi.number().integer(),
                weight: Joi.number().required(),
                date: Joi.string().required()
            }), { abortEarly: false, allowUnknown: true }
        ).then(validBody => {
            return Promise.all([
                User.where({ email: validBody.email.toLowerCase() }).fetch(),
                bcrypt.hash(validBody.password, 12)
            ])
        }).then(([user, hash]) => {
            if (user) {
                return Promise.reject(new AppError("this email already exists ", 401));
            }
            req.body.user.password = hash;
            req.body.user.email = req.body.user.email.toLowerCase();
            req.body.user.role = 'user';
            req.body.user.active_kode = ''; // 
            req.body.user.status = 10;
            if (req.body.user.belt_id) {
                belt_id = req.body.user.belt_id;
                delete req.body.user.belt_id;
            }
            req.body.user.full_name = req.body.user.first_name + ' ' + req.body.user.last_name;
            return User.forge(req.body.user).save();
        }).then(createUser => {
            updateUser = createUser;
            user = createUser.toJSON();
            if (!belt_id) {
                return null;
            }
            return Belt.forge({ id: belt_id }).fetch();
        }).then((beltFind) => {
            if (!beltFind) {
                return null;
            }
            return BeltUser.forge({ user_id: user.id, belt_id }).save();
        }).then((beltUser) => {
            if (!beltUser) {
                return null;
            }
            let promiseAllModel = [];
            if (req.body.user.stripe) {
                for (var index = 0; index < req.body.user.stripe; index++) {
                    promiseAllModel.push(Stripe.forge({ belt_user_id: beltUser.id, name: '' }).save());
                }
            }
            if (promiseAllModel.length > 0)
                return Promise.all(promiseAllModel)
            else
                return null;
        }).then(() => {
            if (!req.body.academy) {
                return null;
            }
            let academy = req.body.academy;
            academy.user_id = user.id;
            academy.status = 'In_process';
            return Academy.forge(academy).save().then(academyCreate => {
                Manager.forge({user_id: user.id, academy_id: academyCreate.id}).save();
                Belt.where({stripe: 0}).fetchAll().then(belts => {
                    belts.map(belt => {
                      BeltSetting.forge({academy_id: academyCreate.id,  belt_id: belt.get('id'),
                        time_stripe_count: belt.get('time_stripe_count'),
                        class_stripe_count: belt.get('class_stripe_count')}).save()
                    });
                  });
                return academyCreate;
            });
        }).then((academyCreate) => {
            if (!academyCreate) {
                return null;
            }

            return updateUser.save({ academy_id: academyCreate.id }, { method: 'update' })
        }).then((_user) => {
            if (_user) {
                user = _user.toJSON();
            }
            if(!req.body.academy_name || user.academy_id){
                return null;
            }

            return Academy.forge({name: req.body.academy_name, status: 'Not_created'}).save().then(createAcademy => {
                Manager.forge({user_id: user.id, academy_id: createAcademy.id}).save();
                Belt.where({stripe: 0}).fetchAll().then(belts => {
                    belts.map(belt => {
                      BeltSetting.forge({academy_id: createAcademy.id,  belt_id: belt.get('id'),
                        time_stripe_count: belt.get('time_stripe_count'),
                        class_stripe_count: belt.get('class_stripe_count')}).save()
                    });
                  });
                return updateUser.save({academy_id: createAcademy.id}, { method: 'update'});
            });
        }).then(() => {
            let data = user;
            helper.createNotification(req, data.id, '<a href="/#/profile/'+ data.id +'">' + data.first_name + ' ' + data.last_name + '</a> signed up to OPN MAT');
            UserTimeline.forge({
                data: 'Signed up to OPNMAT.com',
                user_id: data.id,
                type: 'standart',
                details: ''
            }).save()
            Academy.where({id: user.academy_id}).fetch().then(academy => {
                if(!academy){
                    return null;
                }
                UserAcademyInfo.forge({user_id: user.id, name: academy.get('name'), begin_date: '', end_date: 'Present' }).save();
            });
            AcademyFollower.forge({user_id:  user.id, academy_id: user.academy_id}).save();
            res.json({
                access_token: jwt.sign({
                    id: data.id,
                    role: 'user',
                    data: _.pick(data, ['latitude', 'longitude'])
                }, config.token_secret),
                user: _.omit(data, ['password', 'reset_token_password', 'role'])
            });
        }).catch((err) => {
            console.log('err', err)
            helper.errorResponse(res, [err]);
        })
    });

    router.post('/send_confirmation', (req, res) => {
      helper.validateData(req.body,
        Joi.object().keys({
          email: Joi.string().email().required(),
        }), { abortEarly: false }
      ).then(validBody => {
        return User.where({id: req.user.id}).fetch();
      }).then(findUser => {
        if(!findUser){
          return Promise.reject(new AppError("Not found user", 404));
        }
        let active_kode = crypto.randomBytes(25).toString('hex');
        if(findUser.get('active_kode')){
            active_kode = findUser.get('active_kode');
        }
        return findUser.save({email: req.body.email, active_kode}, { method: 'update'})
      }).then(_user => {
        let user = _user.toJSON();
        return ExternalServices.sendMail({
            from: config.from_email,
          to: user.email,
          subject: 'Confirmation',
          html: helperEmail.getTemplateEmail('confirmation', {
            user_name: user.first_name + ' ' + user.last_name,
            confirmation_url: config.site_front + '/#/auth/login/' + user.active_kode,
            site_url: config.site_front
          })
        });
      }).then(_user => {
        return res.json({status: true, message: 'Email send'})
      }).catch(err => {
        console.log('err', err)
        helper.errorResponse(res, [ err ]);
      });
    })
    
    router.get('/activate_user/:kode', (req, res) => {
        let user;
        if (!req.params.kode) {
            return res.status(400).json(new AppError("is not kode activate", 400))
        }

        User.where({ active_kode: req.params.kode }).fetch().then(findUser => {
            if (!findUser) {
                return Promise.reject(new AppError("Not found user", 404));
            }

            return findUser.save({ active_kode: '', status: 10 })
        }).then(_user => {
            user = _user.toJSON();
            return ExternalServices.sendMail({
                from: config.from_email,
                to: user.email,
                subject: 'Confirmation',
                html: helperEmail.getTemplateEmail('verified', {
                    user_name: user.first_name + ' ' + user.last_name
                })
            });
        }).then(() => {
            return res.json({ status: true, user: _.omit(user, ['password', 'reset_token_password', 'role']) })
        }).catch(err => {
            console.log('err', err)
            helper.errorResponse(res, [err]);
        });
    })

    router.post('/forgot', (req, res) => {
        let token = crypto.randomBytes(25).toString('hex');
        helper.validateData(req.body,
            Joi.object().keys({
                email: Joi.string().email().required()
            }), { abortEarly: false }).then(validBody => {
            return User.where({
                email: validBody.email
            }).fetch();
        }).then((user) => {
            if (!user) {
                return Promise.reject(new AppError("Email not found", 400));
            }
            user.set('reset_token_password', token)
            return user.save()
        }).then((user) => {
            let data = {
                from: config.from_email,
                to: user.get('email'),
                subject: "Forgot",
                html: helperEmail.getTemplateEmail('reset_password', {
                    reset_url: config.site_front + '/#/auth/reset/' + token
                })
            };
            return ExternalServices.sendMail(data);
        }).then(() => {
            return res.json({ status: true, reset_token_password: token })
        }).catch((err) => {
            helper.errorResponse(res, [err]);
        })
    })


    router.post('/forgot_app', async (req, res) => {
        let token = crypto.randomBytes(3).toString('hex');
        try {
            let validBody = await helper.validateData(req.body,
                Joi.object().keys({
                    email: Joi.string().email().required()
                }), { abortEarly: false });

            let user = await User.where({
                email: validBody.email
            }).fetch();    

            if (!user) {
                throw new AppError("Email not found", 400);
            }

            user.set('reset_token_password', token);
            user = await user.save();

            let data = {
                from: config.from_email,
                to: user.get('email'),
                subject: "Forgot",
                html: helperEmail.getTemplateEmail('reset_password_app', {
                    kode: token
                })
            };
            await ExternalServices.sendMail(data);

            res.json({ status: true })
        } catch (error) {
			console.log("​}catch -> error", error)
            return helper.errorResponse(res, [error]);
        }
    })

    router.post('/valid_forgot_kode', async (req, res) => {
        try {
            let validBody = await helper.validateData(req.body,
                Joi.object().keys({
                    kode: Joi.string().required()
                }), { abortEarly: false });

            let user = await User.where({
                reset_token_password: validBody.kode
            }).fetch();    

            if (!user) {
                throw new AppError("Kode is not valid", 400);
            }
            res.json({ status: true })
        } catch (error) {
            return helper.errorResponse(res, [error]);
        }
    })

    router.post('/reset_password/:kode', (req, res) => {
        if (!req.params.kode) {
            return res.status(400).json(new AppError("is not kode reset password", 400))
        }

        let userUpdate;

        helper.validateData(req.body,
            Joi.object().keys({
                password: Joi.string().required()
            }), { abortEarly: false }).then(validBody => {
            return Promise.all([
                User.where({
                    reset_token_password: req.params.kode
                }).fetch(),
                bcrypt.hash(validBody.password, 12)
            ]);
        }).then(([user, hach]) => {
            if (!user) {
                return Promise.reject(new AppError("User not found", 400));
            }
            user.set('reset_token_password', '');
            user.set('password', hach);
            return user.save()
        }).then((user) => {
            userUpdate = user.toJSON();
            let data = {
                from: config.from_email,
                to: user.get('email'),
                subject: "Reset password",
                html: helperEmail.getTemplateEmail('reset_password_ok', {})
            };
            return ExternalServices.sendMail(data);
        }).then(() => {
            return res.json({
                status: true,
                access_token: jwt.sign({
                    id: userUpdate.id,
                    role: userUpdate.role,
                    data: _.pick(userUpdate, ['latitude', 'longitude'])
                }, config.token_secret),
                user: _.omit(userUpdate, ['password', 'reset_token_password', 'role'])
            })
        }).catch((err) => {
            helper.errorResponse(res, [err]);
        })
    })

    router.post('/login_or_create_academy', (req, res) => {
			let user, academy;
			helper.validateData(req.body.user,
				Joi.object().keys({
					email: Joi.string().email().required(),
					password: Joi.string().required()
				}), { abortEarly: false }
			).then(validBody => {
				return User.where({email: req.body.user.email}).fetch()
			}).then((_user) => {
				if (_user === null) {
						return Promise.reject(new AppError("Incorrect email", 401));
				}
				user = _user.toJSON()
				return bcrypt.compare(req.body.user.password, user.password)
			}).then(matches => {
				if (!matches) {
					return Promise.reject(new AppError("Incorrect password", 401));
				}
				if (!req.body.academy && !req.body.academy_name){
					return null
				}

				let academy = {};

				if(req.body.academy){
					academy = req.body.academy;
				}

				if(req.body.academy_name){
					academy = {name: req.body.academy_name, status: 'Not_created'}
				}

				return Academy.forge(academy).save().then(createAcademy => {
					Manager.forge({user_id: user.id, academy_id: createAcademy.id}).save();
					Belt.where({stripe: 0}).fetchAll().then(belts => {
						belts.map(belt => {
							BeltSetting.forge({academy_id: createAcademy.id,  belt_id: belt.get('id'),
								time_stripe_count: belt.get('time_stripe_count'),
								class_stripe_count: belt.get('class_stripe_count')}).save()
						});
					});
					academy = createAcademy.toJSON();
                    user.academy_id = createAcademy.id;
                    AcademyFollower.forge({user_id: user.id, academy_id: createAcademy.id}).save();
					return User.where({id: user.id}).save({academy_id: createAcademy.id}, { method: 'update'});
				})
			}).then(userUpdate => {			
                Academy.where({id: user.academy_id}).fetch().then(academy => {
                    if(!academy){
                        return null;
                    }
                    UserAcademyInfo.forge({user_id: user.id, name: academy.get('name'), begin_date: '', end_date: 'Present' }).save();
                })
                
				res.json({
					access_token: jwt.sign({
							id: user.id,
                            role: user.role,
                            data: _.pick(user, ['latitude', 'longitude'])
					}, config.token_secret),
					user: _.omit(user, ['password', 'reset_token_password', 'role']),
					academy
				});
			}).catch((err) => {
				helper.errorResponse(res, [err]);
			})
    })


    return router;
}