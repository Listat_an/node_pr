
'use strict';
const Belt = require('../models').belt;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Belt});

    return helper.fetchPaginationData(normalizedQuery, Belt, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Belt.getAttributes(), Belt.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Belt.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found belt"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    let belt;
    Belt.forge(req.body).save().then((data) => {
      belt = data;
      return new Promise((resolve, reject) => {
        var storage = multer.diskStorage({
          destination: function (req, file, cb) {
            let dir = 'public/uploads/belts/';
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            cb(null, dir)
          },
          filename: function (req, file, cb) {
            cb(null, file.originalname)
          }
        });
        
        var upload = multer({ storage: storage }).fields([{ name: 'sourse', maxCount: 1 }]);
        upload(req, res, function (err) {
          if (err) {
            return reject(err)
          }
          return resolve(true)
        })
      });
    }).then(() => {
      if (!req.files){
        return null;
      }

      let data = {};
      if(req.files.sourse){
        data.sourse = req.files.sourse[0].destination + req.files.sourse[0].originalname;
      }

      return belt.save(data, { method: 'update'})
    }).then(() => {
      res.json(belt.toJSON())
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let belt;
    Belt.forge({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found belt', 404))
      }
      return findModel.save(req.body, { method: 'update'})
    }).then(data => {
      belt = data;
      return new Promise((resolve, reject) => {
        var storage = multer.diskStorage({
          destination: function (req, file, cb) {
            let dir = 'public/uploads/belts/' + belt.id;
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            cb(null, dir)
          },
          filename: function (req, file, cb) {
            cb(null, file.originalname)
          }
        });
        
        var upload = multer({ storage: storage }).fields([{ name: 'sourse', maxCount: 1 }]);
        upload(req, res, function (err) {
          if (err) {
            return reject(err)
          }
          return resolve(true)
        })
      });
    }).then(() => {
      if (!req.files){
        return null;
      }

      let data = {};
      if(req.files.sourse){
        data.sourse = (req.files.sourse[0].destination + req.files.sourse[0].originalname).replace('public/', '') ;
      }

      return belt.save(data, { method: 'update'})
    }).then(() => {
      res.json(belt.toJSON())
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Belt.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}