
'use strict';
const SubscriptionAcademy = require('../models').subscription_academy;
const Academy = require('../models').academy;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: SubscriptionAcademy});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, SubscriptionAcademy, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(SubscriptionAcademy.getAttributes(), SubscriptionAcademy.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    SubscriptionAcademy.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found SubscriptionAcademy"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Academy.forge({id: req.body.academy_id}).fetch(),
        User.forge({id: req.body.academy_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      if(!findModel[1]){
        return Promise.reject(new AppError('Not found User', 404))
      }

      return SubscriptionAcademy.forge({user_id: req.user_id, academy_id: req.body.academy_id}).save();
    }).then((data) => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let subscription;
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return SubscriptionAcademy.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found SubscriptionAcademy', 404))
      }
      subscription = findModel;
      return Promise.all([
        Academy.forge({id: req.body.academy_id}).fetch(),
        User.forge({id: req.body.academy_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      if(!findModel[1]){
        return Promise.reject(new AppError('Not found User', 404))
      }
      return subscription.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    SubscriptionAcademy.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}