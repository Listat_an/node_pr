
'use strict';
const Subscription = require('../models').subscription;
const Academy = require('../models').academy;
const UserSubscription = require('../models').user_subscription;
const CalendarSubscription = require('../models').calendar_subscription;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const config = require('../config')
const _ = require('lodash');
const ExtarnalService = require('../lib/external_services');
const Manager = require('../models').manager;
const moment = require('moment');
const multer  = require('multer');
const SubscriptionActivity = require('../models').subscription_activity;
const JoinActivity = require('../models').join_activity;
const UserTimeline = require('../models').user_timeline;
const Transaction = require('../models').transaction;

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Subscription});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, Subscription, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Subscription.getAttributes(), Subscription.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Academy});
    Subscription.where({id: req.params.id}).fetch({withRelated: normalizedQuery.include}).then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Subscription"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().required(),
        price: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Academy.forge({id: req.body.academy_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      req.body.user_id = req.user.id;

      var data_plan ={
        amount: req.body.price*100,
        name: req.body.name
      }

      // req.body.membership

      data_plan.interval = 'month';

      // switch (req.body.membership) {
      //   case '1_day':
      //     data_plan.interval = 'day';
      //     break;
      //   case '1_month':
      //     data_plan.interval = 'month';
      //     break;
      //   case '3_month':
      //     data_plan.interval = 'month';
      //     data_plan.interval_count = 3;
      //   case '6_month':
      //     data_plan.interval = 'month';
      //     data_plan.interval_count = 6;
      //     break; 
      //   case '1_year':
      //     data_plan.interval = 'year';
      //     data_plan.interval_count = 1;
      //     break;
      //   default:
      //     break;
      // }

      if(req.body.trial_days){
        data_plan['trial_period_days'] = req.body.trial_days;
      }

      return ExtarnalService.createStripePlan(data_plan);
    }).then(plan => {
      if(!plan){
        return Promise.reject(new AppError('Create plan error', 500))
      }
      req.body.plan_id = plan.id;
      console.log('plan', plan)
      return Subscription.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, subscription: data})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let subscription;
    return Subscription.forge({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Subscription', 404))
      }
      subscription = findModel;
      return Promise.all([
        Academy.forge({id: req.body.academy_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      // req.body.academy_id = req.params.id;
      return subscription.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, subscription: data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    var subscription;
    Subscription.forge({ id: req.params.id }).fetch().then(_subscription => {
      if (!_subscription){
        return Promise.reject(new AppError('Not found subscription', 404))
      }
      subscription = _subscription;
      if(!subscription.get('plan_id')){
        return null;
      }
      return ExtarnalService.deleteStripePlan(subscription.get('plan_id'));
    }).then((data) => {
      console.log('data', data)
      return subscription.destroy();
    }).then(() => {
      return UserSubscription.where({subscription_id: req.params.id}).destroy();
    }).then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.post('/canceled', (req, res) => {
    let subscription;
    let subscriptionD;
    let user_id;
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().required(),
        user_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        UserSubscription.where({user_id: req.body.user_id, subscription_id: req.body.subscription_id}).fetch(),
        Subscription.forge({id:  req.body.subscription_id}).fetch()
      ]);
    }).then((modelFind) => {
      if (!modelFind[0]){
        return Promise.reject(new Error('Not found Subscription'))
      }

      if (!modelFind[1]){
        return Promise.reject(new Error('Not found Subscription'))
      }
      subscription = modelFind[0];
      subscriptionD = modelFind[1];
      console.log('stripe_subscription', modelFind[0].toJSON())
      return ExtarnalService.cenceledStripeSubscription(modelFind[0].get('stripe_subscription'));
    }).then(() => {
      return subscription.save({status: 0}, { method: 'update'});
    }).then(() => {
      helper.createNotification(req, req.user.id, 'Your Subscription ' + subscriptionD.get('name') +' was cancelled on  ' + moment().format('Do MMMM YYYY'));
      UserTimeline.forge({
        data: 'Cancelled subscription ' + subscriptionD.get('name'),
        user_id: req.user.id,
        type: 'standart',
        details: '',
        academy_id: subscriptionD.get('academy_id')
      }).save();
      helper.userActive(req.user.id, subscriptionD.get('academy_id'), 'Cancelled');
      CalendarSubscription.where({
        subscription_id:  req.body.subscription_id, 
        user_id: req.user.id, 
        pay_end: null
      }).fetch().then(calendar_data => {
        if(calendar_data){
          calendar_data.set('pay_end', moment().format('YYYY-MM-DD HH:mm:ss'))
          calendar_data.save();
        }
      });
      return Promise.all([
        JoinActivity.where({subscription_id: req.body.subscription_id}).destroy(),
        UserSubscription.where({user_id: req.user.id, subscription_id: req.body.subscription_id}).destroy() //TODO
      ]);
    }).then(() => {
      res.json({status: true});
    }).catch(err => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  })

  router.post('/canceled_notification', (req, res) => {
    let subscription;
    let subscriptionD;
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        UserSubscription.where({user_id: req.user.id, subscription_id: req.body.subscription_id}).fetch(),
        Subscription.forge({id:  req.body.subscription_id}).fetch()
      ]);
    }).then((modelFind) => {
      if (!modelFind[0]){
        return Promise.reject(new Error('Not found Subscription'))
      }

      if (!modelFind[1]){
        return Promise.reject(new Error('Not found Subscription'))
      }
      subscription = modelFind[0];
      subscriptionD = modelFind[1];
      return Promise.all([
        Manager.where({academy_id: subscriptionD.get('academy_id')}).fetchAll(),
        User.where({id: req.user.id}).fetch(),
        subscription.save({is_cancel: 1}, { method: 'update'})
      ]);
    }).then(([managers, user]) => {

      if(subscriptionD.get('trial_days')){
        if(moment() < moment.unix(subscription.get('trial_end'))){
          helper.canceled_subscription(req, req.body.subscription_id, req.user.id)
        } else {
          managers.map(item => {
            helper.createNotification(req, item.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.get('first_name') + ' ' + user.get('last_name') + '</a> has canceled subscription ' + subscriptionD.get('name'), 'manager', 'canceled_subscription', {subscription_id: req.body.subscription_id, user_id: req.user.id});
          })
        }
      } else {
        managers.map(item => {
          helper.createNotification(req, item.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.get('first_name') + ' ' + user.get('last_name') + '</a> has canceled subscription ' + subscriptionD.get('name'), 'manager', 'canceled_subscription', {subscription_id: req.body.subscription_id, user_id: req.user.id});
        })
      }
      res.json({status: true});
    }).catch(err => {
      console.log('err', err)
      return helper.errorResponse(res, [ err ]); 
    })
  })

  router.post('/fee_notification', (req, res ) => {
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().required(),
        user_id: Joi.number().required(),
        fee: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      if(req.body.fee == 0){
        return helper.canceled_subscription(req, req.body.subscription_id, req.body.user_id, 1);  // canceled 
      }
      return helper.createNotification(req, req.body.user_id, 'You need to pay a fee to cancel your subscription', 'user', 'canceled_subscription_fee', {fee: req.body.fee, subscription_id: req.body.subscription_id, user_id: req.body.user_id});
    }).then(() => {
      res.json({status: true});
    }).catch(err => {
      console.log('err', err)
      return helper.errorResponse(res, [ err ]); 
    })
  })

  router.post('/pay_fee_canceled_subscription', (req, res) => {

    let user, subscription, subscription_user;

    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().required(),
        user_id: Joi.number().required(),
        fee: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        UserSubscription.where({user_id: req.body.user_id, subscription_id: req.body.subscription_id}).fetch(),
        Subscription.forge({id:  req.body.subscription_id}).fetch(),
        User.forge({id:  req.body.user_id}).fetch(),
      ]);
    }).then((modelFind) => {
      if (!modelFind[0]){
        return Promise.reject(new Error('Not found Subscription user'))
      }

      if (!modelFind[1]){
        return Promise.reject(new Error('Not found Subscription'))
      }

      if (!modelFind[2]){
        return Promise.reject(new Error('Not found User'))
      }

      user = modelFind[2];
      subscription = modelFind[1];
      subscription_user = modelFind[0];

      if(user.get('stripe_customer_id')){
        return user;
      }
      return ExtarnalService.createStripeCustomer({email: user.get('email')}).then(data => {
        user.set('stripe_customer_id', data.id);
        return user.save();
      })
    }).then(_user => {

      return ExtarnalService.paymentStripe({
        customer: _user.get('stripe_customer_id'), 
        source: req.body.source,
        price: subscription.get('price'),
        type: req.body.type
      }).then(payment =>{
        if (payment.status == 'succeeded') {
          Transaction.forge({
            type: 'fee_canceled_subscription',
            activity_id: null,
            user_id: req.user.id,
            subscription_id: subscription.get('id'),
            academy_id: subscription.get('academy_id'),
            amount: subscription.get('price'),
            amount_app: 0,
            amount_academy: 0,
            amount_stripe: 0,
            procent: 0,
            charge_id: payment.id
          }).save()
          return true;
        } else {
          return Promise.reject(new Error('Not pay'))
        }
      });
    }).then(() => {
      return helper.canceled_subscription(req, req.body.subscription_id, req.body.user_id, 1)
    }).then(() => {

      Manager.where({academy_id: subscription.get('academy_id')}).fetchAll().then(managers => {
        managers.map(item => {
          helper.createNotification(req, item.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.get('first_name') + ' ' + user.get('last_name') + '</a> paid canceled subscription ' + subscriptionD.get('name'), 'manager');
        })
      });

      res.json({
        status: true
      })
    }).catch(err => {
      console.log('err', err)
      return helper.errorResponse(res, [ err ]); 
    })
  })

  router.post('/decline_subscription', (req, res) => {
    let subscription;
    let subscriptionD;
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        UserSubscription.where({user_id: req.user.id, subscription_id: req.body.subscription_id}).fetch(),
        Subscription.forge({id:  req.body.subscription_id}).fetch()
      ]);
    }).then((modelFind) => {
      if (!modelFind[0]){
        return Promise.reject(new Error('Not found Subscription'))
      }

      if (!modelFind[1]){
        return Promise.reject(new Error('Not found Subscription'))
      }
      subscription = modelFind[0];
      subscriptionD = modelFind[1];
      return Promise.all([
        // Manager.where({academy_id: subscriptionD.get('academy_id')}).fetchAll(),
        // User.where({id: req.user.id}).fetch(),
        subscription.save({is_cancel: false}, { method: 'update'})
      ]);
    }).then(() => {

      // managers.map(item => {
      //   helper.createNotification(req, item.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.get('first_name') + ' ' + user.get('last_name') + '</a> has canceled subscription ' + subscriptionD.get('name'), 'manager', 'canceled_subscription', {subscription_id: req.body.subscription_id});
      // })
      helper.userActive(req.user.id, subscriptionD.get('academy_id'), 'Active');
      res.json({status: true});
    }).catch(err => {
      console.log('err', err)
      return helper.errorResponse(res, [ err ]); 
    })
  })

  router.post('/pay', (req, res) => {
    let subscription, user, subscription_user;
    let registration_fee_payd = true;
    helper.validateData(req.body,
      Joi.object().keys({
        subscription_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Subscription.forge({id: req.body.subscription_id}).fetch(),
        User.forge({id: req.user.id}).fetch(),
        UserSubscription.where({user_id: req.user.id, subscription_id: req.body.subscription_id}).fetch(),
      ]) 
    }).then(modals => {

      if (modals[2] && modals[2].get('status')){
        return Promise.reject(new Error('Is you subscription'));
      }
      if (!modals[1]){
        return Promise.reject(new Error('Not found subscription User'));
      }
      subscription = modals[0];
      user = modals[1];
      subscription_user = modals[2];
      if(!subscription){
        return Promise.reject(new Error('Not found subscription'));
      }
      if(user.get('stripe_customer_id')){
        return user;
      }
      return ExtarnalService.createStripeCustomer({email: user.get('email')}).then(data => {
        user.set('stripe_customer_id', data.id);
        return user.save();
      })
    }).then(_user => {
      return UserSubscription.where({user_id: req.user.id}).fetchAll().then(u_s => {
        return Subscription.where('id', 'in', u_s.map(item => item.get('subscription_id'))).fetchAll().then((_subs) => {
          _subs.some(item => {
            if(item.get('registration_fee') > 0){
              registration_fee_payd = false;
              return true;
            }
          });
          return _user;
        });
      })
    }).then(_user => {
      if(subscription.get('membership') == '1_day'){
        return ExtarnalService.paymentStripe({
          customer: _user.get('stripe_customer_id'), 
          source: req.body.source,
          price: subscription.get('price'),
          type: req.body.type
        }).then(payment =>{
          if (payment.status == 'succeeded') {
            Transaction.forge({
              type: 'subscription',
              activity_id: null,
              user_id: req.user.id,
              subscription_id: subscription.get('id'),
              academy_id: subscription.get('academy_id'),
              amount: subscription.get('price'),
              amount_app: 0,
              amount_academy: 0,
              amount_stripe: 0,
              procent: 0,
              charge_id: payment.id
            }).save().then(transaction => {
              UserTimeline.forge({
                data: 'Payment processed for subscription $' + subscription.get('price'),
                user_id: req.user.id,
                type: 'refund',
                details: '',
                transaction_id: transaction.id,
                academy_id: subscription.get('academy_id')
              }).save();
            });
            helper.userRevenue(req.user.id, subscription.get('academy_id'), 'subscription', subscription.get('price'));
            helper.userActive(req.user.id, subscription.get('academy_id'), 'Active');
            return true;
          } else {
            return Promise.reject(new Error('Not pay'))
          }
        });
      }

      return ExtarnalService.createStripeSubscription({
        customer: _user.get('stripe_customer_id'), 
        plan: subscription.get('plan_id'),
        source: req.body.source,
        metadata: {
          user_id: req.user.id,
          type: 'subscription',
          plan_id: subscription.get('plan_id')
        }
      }).then(stripe_subscription => {
        if(subscription_user){
          UserSubscription.where({id: subscription_user.id}).destroy();
        }
        User.where({id: req.user.id}).save({academy_id: subscription.get('academy_id'), is_student: 1}, { method: 'update'});
        return UserSubscription.forge({
          user_id: user.get('id'),
          subscription_id: subscription.get('id'),
          stripe_subscription: stripe_subscription.id,
          status: true,
          current_period_end: stripe_subscription.current_period_end,
          billing_cycle_anchor: stripe_subscription.billing_cycle_anchor,
          trial_end: stripe_subscription.trial_end,
        }).save();
      })
    }).then(() => {
      return Promise.all([
        Manager.where({academy_id: subscription.get('academy_id')}).fetchAll()
      ]).then(([managers]) => {
        managers.map(item => {
          helper.createNotification(req, item.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.get('first_name') + ' ' + user.get('last_name') +'</a>  subscribed to ' + subscription.get('name'), 'manager');
          helper.createNotification(req, item.get('user_id'), 'Payment from ' + '<a href="/#/profile/'+ user.id +'">'+  user.get('first_name') + ' ' + user.get('last_name') +'</a>  was received for subscription ' + subscription.get('name'), 'manager');
        })
        if(subscription.get('trial_days') > 0){
          helper.userActive(req.user.id, subscription.get('academy_id'), 'Free trial');
          helper.createNotification(req, req.user.id, 'Success! You have subscribed to ' + subscription.get('name') + '. Your trial ends in ' + subscription.get('trial_days') + ' days!')
        } else {
          helper.createNotification(req, req.user.id, 'Your Subscription ' + subscription.get('name') +' started on  ' + moment().format('Do MMMM YYYY'))
        }
        UserTimeline.forge({
          data: 'signed up for subscription ' + subscription.get('name'),
          user_id: req.user.id,
          type: 'standart',
          details: '',
          academy_id: subscription.get('academy_id')
        }).save();
        User.where({id: req.user.id}).save({academy_id: subscription.get('academy_id'), is_student: 1}, { method: 'update'});
        return true;
      });
    }).then(() => {
      return SubscriptionActivity.where({subscription_id: subscription.get('id')}).fetchAll().then(subs_act => {
        subs_act.forEach(item => {
          JoinActivity.forge({
            activity_id: item.get('activity_id'),
            user_id: req.user.id,
            status: true,
            subscription_id: subscription.get('id'),
            amount: 0
          }).save();
        });
        return null;
      })
    }).then(() => {
      res.json({
        status: true,
        registration_fee_payd
      })
    }).catch((err) => {
      console.log('err', err)
      if(!subscription){
        return helper.errorResponse(res, [ err ]);
      }
      return helper.createNotification(req, req.user.id, 'Your payment for Subscription ' + subscription.get('name') + ' was declined on ' + moment().format('Do MMMM YYYY')).then(() => {
        return helper.errorResponse(res, [ err ]); 
      });
    })
  })

  return router;
}