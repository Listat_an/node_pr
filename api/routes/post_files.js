
'use strict';
const PostFile = require('../models').post_file;
const Post = require('../models').post;
const Academy = require('../models').academy;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: PostFile});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, PostFile, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(PostFile.getAttributes(), PostFile.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    PostFile.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found PostFile"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/:post_id([0-9]+)', (req, res) => {
    let post_file;
    let post;
    helper.validateData(req.params,
      Joi.object().keys({
        post_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Post.forge({id: req.params.post_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Post', 404))
      }

      post = findModel;
      return new Promise((resolve, reject) => {
        var storage = multer.diskStorage({
          destination: function (req, file, cb) {
            let dir = 'public/uploads/posts/' + post.get('id') + '/';
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            cb(null, dir)
          },
          filename: function (req, file, cb) {
            cb(null, file.originalname)
          }
        });
        
        var upload = multer({ storage: storage }).fields([{ name: 'photos', maxCount: 3 }, { name: 'videos', maxCount: 1 }]);
        upload(req, res, function (err) {
          if (err) {
            return reject(err)
          }
          return resolve(true)
        })
      });
    }).then(() => {

      if(!req.files){
        return null;
      }

      let created_file = [];

      if (req.files.photos && Array.isArray(req.files.photos)){
        req.files.photos.forEach(item => {
          let url = (item.destination + item.originalname).replace('public/', '');
          created_file.push(PostFile.forge({type: 'image', post_id: post.get('id'), academy_id: post.get('academy_id'), user_id: req.user.id, url}).save());
        });
      }

      if (req.files.videos && Array.isArray(req.files.videos)){
        req.files.videos.forEach(item => {
          let url = (item.destination + item.originalname).replace('public/', '');
          created_file.push(PostFile.forge({type: 'video', post_id: post.get('id'), academy_id: post.get('academy_id'), user_id: req.user.id, url}).save());
        });
      }

      return Promise.all(created_file);
    }).then((files_created) => {
      res.json({status: true, files: files_created.map(item => item.toJSON())})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    PostFile.forge({ id: req.params.id }).fetch().then((_file) => {
      let file = _file.toJSON();
      if (file.url && fs.existsSync('public/'+file.url)){
        fs.unlinkSync('public/'+file.url);
      }
      return _file.destroy();
    }).then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}