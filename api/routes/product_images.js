
'use strict';
const ProductImage = require('../models').product_image;
const Product = require('../models').product;
const Academy = require('../models').academy;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: ProductImage});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, ProductImage, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(ProductImage.getAttributes(), ProductImage.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    ProductImage.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found ProductImage"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/:product_id([0-9]+)', (req, res) => {
    let post_file;
    let product;
    helper.validateData(req.params,
      Joi.object().keys({
        product_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Product.forge({id: req.params.product_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found product', 404))
      }

      product = findModel;
      return new Promise((resolve, reject) => {
        var storage = multer.diskStorage({
          destination: function (req, file, cb) {
            let dir = 'public/uploads/products/' + product.get('id') + '/';
            if (!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            cb(null, dir)
          },
          filename: function (req, file, cb) {
            cb(null, file.originalname)
          }
        });
        
        var upload = multer({ storage: storage }).fields([{ name: 'images', maxCount: 7 }]);
        upload(req, res, function (err) {
          if (err) {
            return reject(err)
          }
          return resolve(true)
        })
      });
    }).then(() => {

      if(!req.files){
        return null;
      }

      let created_file = [];

      if (req.files.images && Array.isArray(req.files.images)){
        req.files.images.forEach(item => {
          let url = (item.destination + item.originalname).replace('public/', '');
          created_file.push(ProductImage.forge({product_id: product.get('id'), url}).save());
        });
      }

      return Promise.all(created_file);
    }).then((files_created) => {
      res.json({status: true, files: files_created.map(item => item.toJSON())})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    ProductImage.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}