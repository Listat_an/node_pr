
'use strict';
const Manager = require('../models').manager;
const Academy = require('../models').academy;
const User = require('../models').user;
const BeltUser = require('../models').belt_user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {

  router.get('/status/:id([0-9]+)', (req, res) => {
    return Manager.where({academy_id: req.params.id, user_id: req.user.id}).fetch().then(manager => {
      if (manager){
        return res.json({status: true});
      }
      return res.json({status: false});
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  })

  router.get('/users/:id([0-9]+)', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: User});
    let managers;
    return Manager.where({academy_id: req.params.id}).fetchAll().then(_managers => {
      managers = _managers.toJSON();
      normalizedQuery.filter.id = {in: managers.map(item => item.user_id)};
      return Promise.all([
        helper.fetchPaginationData(normalizedQuery, User, null, []),
        BeltUser.where('user_id', 'in', managers.map(item => item.user_id)).fetchAll({withRelated: ['belt']})
      ]) 
    }).then(([_data, _belts]) => {
      let users = helper.indexResponse(_data, _.concat(User.getAttributes(), User.getInclideAttributes()));
      let belts = _belts.toJSON();
      users.data = users.data.map(user => {
        user.is_manager = false;
        let index = _.findIndex(managers, ['user_id', user.id]);
        if(index > -1){ 
          user.is_manager = managers[index];
        }

        let index_belt = _.findIndex(belts, ['user_id', user.id]);
        user.belt = null;
        if(index_belt > -1){ 
          user.belt = belts[index_belt].belt;
        }
        return user;
      })
      
      res.json(users);
    }).catch((err) => {
      console.log('err', err)
      return res.status(500).json(err)
    })
  });
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Manager});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, Manager, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Manager.getAttributes(), Manager.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Manager.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Manager"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/add', (req, res) => {
    var academy;
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().required(),
        user_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Academy.where({id: req.body.academy_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      academy = findModel;
      return Manager.where({user_id: req.body.user_id, academy_id: req.body.academy_id}).fetch();
    }).then( manager => {
      if(manager){
        return null;
      }
      return Manager.forge({user_id: req.body.user_id, academy_id: req.body.academy_id}).save();
    }).then((data) => {
      return Promise.all([
        User.where({id: req.body.user_id}).fetch(),
        Manager.where({academy_id: req.body.academy_id}).fetchAll()
      ]).then(models => {
        let user = models[0].toJSON();
        let managers = models[1];
        helper.createNotification(req, req.body.user_id, '<a href="/#/profile/'+ user.id +'">' + user.first_name + ' ' + user.last_name + '</a> added you as manager of <a href="/#/academiy-datail/' + academy.id + '">' + academy.get('name') + '</a>');
        managers.forEach(manager => {
          if(manager.get('user_id') != req.body.user_id){
            helper.createNotification(req, manager.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.first_name + ' ' + user.last_name + '</a>  became a new manager', 'manager');
          }
        });
      })
    }).then((data) => {
      res.json({status: true})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    
    Manager.forge({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Manager', 404))
      }
      return findModel.save({status: req.body.status}, { method: 'update'})
    }).then(data => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/remove', (req, res) => {
    
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().required(),
        user_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Academy.forge({id: req.body.academy_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      return Manager.where({user_id: req.body.user_id, academy_id: req.body.academy_id}).destroy();
    }).then((data) => {
      Promise.all([
        User.where({id: req.body.user_id}).fetch(),
        Manager.where({acdemy_id: req.body.academy_id}).fetchAll()
      ]).then(models => {
        let user = models[0];
        let managers = models[1];
       
        managers.forEach(manager => {
          helper.createNotification(req, manager.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.first_name + ' ' + user.last_name + '</a> was removed from managers', 'manager');
        });
      })
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/academies', (req, res) => {
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Academy});

    let user_id = req.user.id;

    if(req.body.user_id){
      user_id = req.body.user_id;
    }

    return Promise.all([
      Manager.where({user_id: user_id}).fetchAll(),
      Academy.where({user_id: user_id}).fetchAll()
    ]).then(findModals => {
      let ids = [];
      findModals[0].forEach(item => {
        ids.push(item.get('academy_id'));
      })

      findModals[1].forEach(item => {
        ids.push(item.get('id'));
      })
      return ids;

    }).then(ids => {
      normalizedQuery.filter.id = {in: ids.toString()};
      return helper.fetchPaginationData(normalizedQuery, Academy, null, [])
    }).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Academy.getAttributes(), Academy.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  })

  return router;
}