
'use strict';
const Schedule = require('../models').schedule;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const Activity = require('../models').activity;

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Schedule});
    console.log('​module.exports -> normalizedQuery', normalizedQuery);

    // if(!normalizedQuery.filter.hasOwnProperty('class_id')){
    //   return res.status(400).json({message: 'Filter class_id should be required'})
    // }

    return helper.fetchPaginationData(normalizedQuery, Schedule, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Schedule.getAttributes(), Schedule.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.post('/delete', async (req, res) => {
    try {
      await Schedule.where(req.body).destroy()
      res.json({status: true});
    } catch (error) {
      console.log('TCL: }catch -> error', error);
      helper.errorResponse(res, [ error ]);
    }
  })

  router.post('/:action', (req, res) => {
    let shema;
    if(req.params.action == 'create' || req.params.action == 'update'){
      shema =  Joi.object().keys({
        class_ids: Joi.string().required(),
        days: Joi.array().required()
      });
    } else if(req.params.action == 'create_holiday') {
      shema =  Joi.object().keys({
        class_ids: Joi.string().required(),
        date: Joi.date(),
        days: Joi.array().required()
      });
    }

    if(Array.isArray(req.body)){
      shema = Joi.array().items(shema);
    }

    helper.validateData(req.body, shema, { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      if(req.params.action == 'create' || req.params.action == 'create_holiday'){
        return null;
      }

      let ids = [];
      let holiday = 0;
      if(Array.isArray(req.body)){
        req.body.forEach(item => {
          item.class_ids.split(',').forEach(id => {
            if(!~ids.indexOf(id)){
              ids.push(parseInt(id));
            }
          });
          holiday = item.date ? 1 : 0;
        })
      } else {
        ids = req.body.class_ids.split(',');
        holiday = req.body.date ? 1 : 0;
      }
      return Schedule.where('class_id', 'in', ids).where({holiday}).fetchAll()
    }).then((_all_shedules) => {
      let all_shedules = _all_shedules ? _all_shedules.toJSON() : [];
      let shedules_create = [];
      let shedules_update = [];

      if(Array.isArray(req.body)){
        req.body.forEach(item => {
          genSheduled(item);
        })
      } else {
        genSheduled(req.body);
      }

      function genSheduled(data_body) {
        data_body.class_ids.split(',').forEach(class_id => {
          let data = {
            class_id,
            academy_id: data_body.academy_id
          }
  
          if(req.params.action == 'update'){
            data.new_start_date = new Date();
          } else if(req.params.action == 'create_holiday'){
            data.date = data_body.date;
            data.holiday = true;
          }

          if(data_body.date){
            data.date = data_body.date;
            data.holiday = true;
          }
  
          data_body.days.forEach(item => {
            item = Object.assign({}, item);
            if(item.id){
              shedules_update.push( Object.assign({}, data, {
                day: item.day,
                time_start: item.time_start,
                time_end: item.time_end,
                id: item.id
              }));
            } else {
              shedules_create.push( Object.assign({}, data, {
                day: item.day,
                time_start: item.time_start,
                time_end: item.time_end,
              }));
            }
          });
  
        });
      }

      let shedules_delete = [];

      all_shedules.forEach(item => {
        let index = _.findIndex(shedules_update, ['id', item.id]);
        if(index < 0){
          shedules_delete.push(item);
        }
      })

      return Promise.all([
        Promise.all(shedules_create.map(item => Schedule.forge(item).save())),
        Promise.all(shedules_update.map(item => Schedule.where({id: item.id}).save(item, { method: 'update'}))),
        Promise.all(shedules_delete.map(item => Schedule.where({id: item.id}).destroy()))
      ]);
    }).then((data) => {
      res.json({status: true});
    }).catch((err) => {
      console.log('​module.exports -> err', err);
      helper.errorResponse(res, [ err ]);
    })
  });

  

  return router;
}