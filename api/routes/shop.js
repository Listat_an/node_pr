
'use strict';
const Order = require('../models').order;
const Academy = require('../models').academy;
const TaxState = require('../models').tax_state;
const Cart = require('../models').cart;
const Transaction = require('../models').transaction;
const User = require('../models').user;
const Manager = require('../models').manager;
const Product = require('../models').product;
const OrderItem = require('../models').order_item;
const Shipping = require('../models').shipping;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const Bluebird = require('bluebird');
const ExtarnalService = require('../lib/external_services');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/saved_checkout', (req, res) => {
    Promise.all([
      Order.where({user_id: req.user.id, status: 'draft', type: 'user'}).fetch({withRelated: ["items.academy"]}),
      Cart.where({user_id: req.user.id}).fetchAll().then(items => {
        let _items = items.toJSON();
        return Bluebird.map(_items, item => {
          return Product.where({id: item.product_id}).fetch({withRelated: ["academy"]}).then(product => {
            if(!product){
              Cart.where({id: item.id}).destroy();
              return null
            }
            return product.toJSON();
          })
        })
      })
    ]).then(([_order, products]) => {
      let order = null;
      if(_order){
        order = _order.toJSON();
      }
     
      let data = {
        order: order,
        shipping_details: []
      }

      let group_academy = [];

      products.forEach(item => {
        if(item){
          let index = _.findIndex(group_academy, ['academy_id', item.academy_id]);
          if(~index){
            group_academy[index] = Object.assign({}, group_academy[index], {shipping_cost: group_academy[index].shipping_cost + item.shipping_price })
          } else {
            group_academy.push({
              academy_id: item.academy_id,
              shipping_cost: item.shipping_price,
              academy: item.academy
            })
          }
        }
        
      })
      data.shipping_details = group_academy;
      res.json(data);
    }).catch((err) => {
      console.log('​err', err);
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/prepare_checkout', (req, res) => {
    let order, data_order, carts;
    helper.validateData(req.body,
      Joi.object().keys({
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().required(),
        phone: Joi.string().required(),
        state: Joi.string().required(),
        sity: Joi.string().required(),
        street: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Order.where({user_id: req.user.id, status: 'draft', type: 'user'}).fetch().then(order => {
        if(!order){
          return null
        }
        return Promise.all([
          order.destroy(),
          OrderItem.where({order_id: order.id})
        ])
      }) 
    }).then(() => {

      data_order = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        phone: req.body.phone,
        phone: req.body.phone,
        address: req.body.address,
        sity: req.body.sity,
        state: req.body.state,
        street: req.body.street,
        billing_first_name: req.body.billing_first_name,
        billing_last_name: req.body.billing_last_name,
        billing_email: req.body.billing_email,
        billing_phone: req.body.billing_phone,
        billing_phone: req.body.billing_phone,
        billing_address: req.body.billing_address,
        billing_sity: req.body.billing_sity,
        billing_state: req.body.billing_state,
        billing_street: req.body.billing_street,
        shipping_cost: 0,
        tax: 0,
        full_name: req.body.first_name + ' ' +  req.body.last_name,
        user_id: req.user.id,
        status: 'draft'
      }
      
      return Promise.all([
        Order.forge(data_order).save(),
        Cart.where({user_id: req.user.id}).fetchAll()
      ]);
    }).then(([data, _carts]) => {
      order = data.toJSON();
      carts = _carts.toJSON();
      let ids = carts.map(item => item.product_id);
      return Promise.all([
        Product.where('id', 'in', ids).fetchAll(),
        TaxState.where({state_code: data_order.state}).fetch()
      ]);
    }).then(([products, _tax_state]) => {
      let items = [];
      let tax_state = null;
      if(_tax_state){
        tax_state = _tax_state.toJSON();
      }



      products.forEach(_product => {
        let product = _product.toJSON();
        let index = _.findIndex(carts, ['product_id', product.id]);
        let tax = 0;
        if(tax_state){
          tax = (tax_state.procent/(parseFloat(product.price)))*100;
        }
        let item = {
          product_id: product.id,
          title: product.title,
          price: product.price,
          order_id: order.id,
          academy_id: product.academy_id,
          shipping_price: product.shipping_price,
          tax
        };
        if(~index){
          item['quantity'] = carts[index].quantity;
          item['photo'] = carts[index].photo;
          
          item['tax'] = tax;
          data_order.tax += tax;
          data_order.shipping_cost += parseFloat(product.shipping_price);
          items.push(OrderItem.forge(item).save())
        }
      });
     
      return Promise.all(items);
    }).then((data) => {
      return Order.where({id: order.id}).fetch({withRelated: ["items"]});
    }).then((data) => {
      order = data.toJSON();
      let subtotal = 0;
      let tax = 0;
      let shipping_cost = 0;
      order.items.forEach(item => {
        console.log('​item', item);
        subtotal = subtotal + item.price * item.quantity;
        tax = tax + item.tax;
        shipping_cost = shipping_cost + item.shipping_price;
      });
      order.subtotal = subtotal;
      order.tax = tax;
      order.shipping_cost = shipping_cost;
      order.total = subtotal + tax + shipping_cost;
      return data.save({subtotal, total: order.total, tax, shipping_cost }, { method: 'update'});
    }).then((data) => {
      res.json(order);
    }).catch((err) => {
      console.log('​err', err);
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/shipping_order', (req, res) => {
    let order;
    helper.validateData(req.body,
      Joi.object().keys({
        shipping_id: Joi.number().required(),
        order_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Order.where({id: req.body.order_id}).fetch({withRelated: ["items"]}),
        Shipping.where({id: req.body.shipping_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Order', 404))
      }
      if(!findModel[1]){
        return Promise.reject(new AppError('Not found Shipping', 404))
      }

      order = findModel[0].toJSON();
      order.shipping_id = findModel[1].id;
      order.shipping = findModel[1].toJSON();

      order.total = order.total + findModel[1].get('price');
      order.shipping_cost = findModel[1].get('price');
      return findModel[0].save({shipping_id: req.body.shipping_id, total: order.total + findModel[1].get('price'), shipping_cost: findModel[1].get('price') })
    }).then((data) => {
      res.json(order);
    }).catch((err) => {
      console.log('​err', err);
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/payment_order', (req, res) => {
    let order, user;
    helper.validateData(req.body,
      Joi.object().keys({
        source: Joi.string().required(),
        type: Joi.string().required(),
        order_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Order.where({id: req.body.order_id}).fetch({withRelated: ["items", 'user']});
    }).then(modelFind => {
      if(!modelFind){
        return Promise.reject(new AppError('Not found Order', 404))
      }
      order = modelFind.toJSON();

      return helper.validateData(order,
        Joi.object().keys({
          first_name: Joi.string().required(),
          last_name: Joi.string().required(),
          email: Joi.string().required(),
          phone: Joi.string().required(),
          state: Joi.string().required(),
          sity: Joi.string().required(),
          street: Joi.string().required(),
          user_id: Joi.number().required(),
        }), { abortEarly: false, allowUnknown: true }
      );
    }).then(validBody => {

      if(order.user_id != req.user.id){
        return Promise.reject(new AppError('Is not me order', 400))
      }

      // if(order.payment_status == 'paid'){
      //   return Promise.reject(new AppError('Order is paid', 400))
      // }

      if(order.user.stripe_customer_id){
        return {id: order.user.stripe_customer_id};
      }
      return ExtarnalService.createStripeCustomer({email: user.get('email')}).then(data => {
        return User.where({id: order.user.id}).save({stripe_customer_id: data.id}, { method: 'update'}).then(() => data);
      });
    }).then(customer => {
      return ExtarnalService.paymentStripe({
        customer: customer.id, 
        source: req.body.source,
        price: order.total,
        type: req.body.type
      }); 
    }).then(payment => {
      if (payment.status == 'succeeded') {
        Transaction.forge({
          type: 'shop_order',
          user_id: req.user.id,
          amount: order.total,
          amount_app: 0,
          amount_academy: 0,
          amount_stripe: 0,
          procent: 0,
          charge_id: payment.id
        }).save().then(transaction => {
          UserTimeline.forge({
            data: 'Payment processed for order $' + order.total,
            user_id: req.user.id,
            type: 'refund',
            details: '',
            transaction_id: transaction.id,
          }).save();
        });
        return Order.where({id: order.id}).save({payment_status: 'paid'}, { method: 'update'})
      } else {
        return Promise.reject(new Error('Is not pay order'));
      }
    }).then(() => {
      let academies = {};
      order.items.forEach(item => {
        if(academies[item.academy_id]){
          academies[item.academy_id].push(item);
        } else {
          academies[item.academy_id] = [item];
        }
      });

      let data_order = {
        first_name: order.first_name,
        last_name: order.last_name,
        email: order.email,
        phone: order.phone,
        phone: order.phone,
        sity: order.sity,
        state: order.state,
        street: order.street,
        full_name: order.first_name + ' ' +  order.last_name,
        user_id: req.user.id,
        type: 'academy',
        shipping_cost: 0,
        payment_status: 'paid',
        tax: order.tax,
        status: 'new'
      }

      let order_academies = []

      Object.keys(academies).forEach(academy => {
        data_order.academy_id = academy;
        let items_model = [];
        let subtotal = 0;
        let tax = 0;
        let shipping_cost = 0;
        academies[academy].forEach(item => {
          subtotal = subtotal + item.quantity * item.price;
          tax = tax + item.tax;
          shipping_cost = shipping_cost + item.shipping_price;
        });
        data_order.subtotal = subtotal;
        data_order.tax = tax;
        data_order.shipping_cost = shipping_cost;
        data_order.total = data_order.shipping_cost + subtotal + tax;
        data_order.user_order = order.id;
        order_academies.push(Order.forge(data_order).save().then(_order =>{
          Transaction.forge({
            type: 'shop_order_academy',
            academy_id: academy,
            order_id: _order.id,
            user_id: req.user.id,
            amount: data_order.total,
            amount_app: 0,
            amount_academy: 0,
            amount_stripe: 0,
            procent: 0,
          }).save();
          Manager.where({academy_id: academy}).fetchAll().then(managers => {
            if(!managers){
              return null;
            }
            managers.map(manager => {
              helper.createNotification(req, manager.get('user_id'), 'New order', 'manager', 'new_order', {order_id: _order.id});
            });
          });
          helper.userRevenue(req.user.id, academy, 'shop', data_order.total);
          academies[academy].forEach(item => {
            let tmp_item = Object.assign({}, item, {order_id: _order.id});
            delete tmp_item.id;
            items_model.push(OrderItem.forge(tmp_item).save());
          });
          return Promise.all(items_model).then(() => _order);
        }))
      });

      return Promise.all(order_academies)
    }).then((order_academies) => {
      return Order.where({id: order.id}).save({status: 'new', details: {academies_order: order_academies.map(item => item.id)}}, {method: 'update'});
    }).then((order_academies) => {
      return Cart.where({user_id: req.user.id}).destroy()
    }).then((order_academies) => {
      return Bluebird.map(order.items, item => {
        return Product.where({id: item.product_id}).fetch().then(product => {
          if(!product){
            return null;
          }
          return product.save({quantity: product.get('quantity') - item.quantity}, {method: 'update'})
        })
      })
    }).then(() => {
      res.json({status: true});
    }).catch((err) => {
      console.log('​err', err);
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/decline_order', (req, res) => {
    Promise.all([
      Order.where({user_id: req.user.id, status: 'draft', type: 'user'}).fetch().then(order => {
        if(!order){
          return null;
        }
        return Promise.all([
          order.destroy(),
          OrderItem.where({order_id: order.id}).destroy(),
        ])
      }),
      Cart.where({user_id: req.user.id}).destroy()
    ]).then(() =>{
      res.json({status: true});
    }).catch((err) => {
      console.log('​err', err);
      helper.errorResponse(res, [ err ]);
    })
  })

  return router;
}