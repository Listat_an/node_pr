'use strict';
const JoinActivity = require('../models').join_activity;
const Activity = require('../models').activity;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const Manager = require('../models').manager;

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: JoinActivity});

    return helper.fetchPaginationData(normalizedQuery, JoinActivity, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(JoinActivity.getAttributes(), JoinActivity.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    JoinActivity.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found JoinActivity"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    req.body.user_id = req.user.id;
    let user;
    let activity;
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().required(),
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        User.forge({id: req.body.user_id}).fetch(),
        JoinActivity.where({activity_id: req.body.activity_id}).fetchAll()
      ]) ;
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Activity', 404))
      }
      if(!findModel[1]){
        return Promise.reject(new AppError('Not found User', 404))
      }
      activity = findModel[0].toJSON();
      if(findModel[2].length >= activity.amount){
        return Promise.reject(new AppError('Limit joined user exceeded', 404))
      }

      user = findModel[1].toJSON();
      // req.body.amount = activity.price_all ;
      return JoinActivity.forge(req.body).save();
    }).then((_data) => {
      let data = _data.toJSON();
      data.user = user;
      let type = '';
      switch (activity.type) {
        case 'seminar':
          type = 'Seminar'
          break;
        case 'class':
          type = 'Class'
          break;
        case 'mat_event':
          type = 'Event'
          break;
        case 'others':
          type = 'Others'
          break;
        default:
          break;
      }
      // TODO: link class event seminar

      helper.getSubscription(req.user.id).then(subs => {
        if(!subs){
          helper.userActive(req.user.id, activity.academy_id, type);
        }
      })
      helper.createNotification(req, user.id, '<a href="/#/profile/'+ user.id +'">' + user.first_name + ' ' + user.last_name +'</a>  joined ' + type + ' "<a href="/#/activity/' + activity.id + '">' + activity.name + '</a>"');
      return Manager.where({academy_id: activity.academy_id}).fetchAll().then(_managers => {
        if(!_managers){
          return true;
        }
        // let managers = _managers.toJSON();
        _managers.map(item => {
          helper.createNotification(req, item.get('user_id'), '<a href="/#/profile/'+ user.id +'">' + user.first_name + ' ' + user.last_name +'</a>  joined to ' + type + ' "<a href="/#/activity/' + activity.id + '">' + activity.name + '</a>"', 'manager');
        });
        return true;
      })
    }).then(() => {
      res.json({status: true})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    req.body.user_id = req.user.id;
    let coment;
    helper.validateData(req.body,
      Joi.object().keys({
        user_id: Joi.number().required(),
        activity_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return JoinActivity.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found JoinActivity', 404))
      }
      coment = findModel;
      return Promise.all([
        Activity.forge({id: req.body.activity_id}).fetch(),
        User.forge({id: req.body.user_id}).fetch(),
      ]) ;
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Activity', 404))
      }
      if(!findModel[1]){
        return Promise.reject(new AppError('Not found User', 404))
      }
      req.body.amount = findModel[0].get('amount');
      return coment.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, comment: data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    JoinActivity.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}