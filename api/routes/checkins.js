
'use strict';
const Checkin = require('../models').checkin;
const Academy = require('../models').academy;
const Manager = require('../models').manager;
const User = require('../models').user;
const Activity = require('../models').activity;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const UserTimeline = require('../models').user_timeline;
const Schedule = require('../models').schedule;
const moment = require('moment');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Checkin});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, Checkin, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Checkin.getAttributes(), Checkin.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Checkin.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Checkin"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', async (req, res) => {
    try {
      await helper.validateData(req.body,
        Joi.object().keys({
          // academy_id: Joi.number().integer().required(),
          user_id: Joi.number().integer().required(),
          activity_id: Joi.number().integer().required(),
        }), { abortEarly: false, allowUnknown: true }
      );

      let findModel = await Promise.all([
        // Academy.forge({id: req.body.academy_id}).fetch(),
        null,
        User.forge({id: req.body.user_id}).fetch(),
        Activity.forge({id: req.body.activity_id}).fetch(),
        Checkin.where({user_id: req.body.user_id, activity_id: req.body.activity_id}).fetchAll()
      ]);

      if(!findModel[1]){
        throw new AppError('Not found User', 404)
      }

      if(!findModel[2]){
        throw new AppError('Not found Activity', 404)
      }

      let activity = findModel[2].toJSON();

      if(findModel[3].length && activity.type != 'class'){
        throw new AppError('You checkined in activity', 404)
      }

      let checkins = findModel[3].toJSON();

     
      if(activity.type == 'class'){
        let sheduleds = await Schedule.where({class_id: activity.id, day: moment().format('ddd')}).fetchAll();
        sheduleds = sheduleds.toJSON();
      

        if(!sheduleds.length){
          throw new AppError('Is not sheduled class', 400);
        }

        let sheduled_is = [];

        sheduleds.forEach(item => {
          let start_times = item.time_start.split('.');
          let end_times = item.time_end.split('.');

          let current_times = moment().format('kk.mm').split('.');

          if(start_times.length == 2 && end_times.length == 2){
            let min_start = parseInt(start_times[0]) * 60 + parseInt(start_times[1]) + 10;
            let min_end = parseInt(end_times[0]) * 60 + parseInt(end_times[1]);
            let current = parseInt(current_times[0]) * 60 + parseInt(current_times[1]);
            if(current >= min_start && current <=min_end){
              sheduled_is.push(item);
            }
          }
        });

        if(!sheduled_is.length){
          throw new AppError('Is not sheduled class', 400);
        }

        let current_sheduled = null;

        //TODO sort

        let holideys = sheduled_is.filter(item => item.holiday == 1);
        let updates = sheduled_is.filter(item => !!item.new_start_date);

        if(holideys.length){
          current_sheduled = holideys[0];
        } else if(updates.length){
          current_sheduled = updates[0];
        } else {
          current_sheduled = sheduled_is[0];          
        }

        if(!current_sheduled){
          throw new AppError('Is not sheduled class', 400);
        }


        checkins.forEach(item => {
          if(moment(item.created_at).format('YYYY-MM-DD') == moment().format('YYYY-MM-DD') && item.schedule_id == current_sheduled.id){
            throw new AppError('Is checkined class', 400);
          }
        });

        req.body.schedule_id = current_sheduled.id;
        
      }


      req.body.academy_id = activity.academy_id;
      req.body.checkin_date = moment().format('YYYY-MM-DD HH:mm:ss');

      let data = await Checkin.forge(req.body).save();

      let type = '';
      switch (activity.type) {
        case 'seminar':
          type = 'Seminar'
          break;
        case 'class':
          type = 'Class'
          break;
        case 'mat_event':
          type = 'Event'
          break;
        case 'others':
          type = 'Others'
          break;
        default:
          break;
      }

      UserTimeline.forge({
        data: 'Checked in the ' + type + ' <a href="/#/activity/'+ activity.id +'">' + activity.name + '</a>',
        user_id: req.body.user_id,
        type: 'standart',
        details: '',
        academy_id: activity.academy_id
      }).save();

      Manager.where({academy_id: activity.academy_id}).fetchAll().then(menagers => {
        menagers.map(item => {
          helper.createNotification(req, item.get('user_id'), '<a href="/#/profile/'+ findModel[1].id +'">' + findModel[1].get('first_name') + ' ' + findModel[1].get('last_name') + '</a> checked in to ' + type + ' <a href="/#/activity/'+ activity.id +'">' + activity.name + '</a>', 'manager');
        })
      });

      helper.userPromotion(req.body.user_id, req.body.academy_id)
      
      res.json({status: true, data})
    } catch (error) {
      helper.errorResponse(res, [ error ]);
    }
  });

  router.post('/manager', async (req, res) => {
    try {
      await helper.validateData(req.body,
        Joi.object().keys({
          academy_id: Joi.number().integer().required(),
          user_id: Joi.number().integer().required(),
          activity_id: Joi.number().integer().required(),
          schedule_id: Joi.number().integer().required(),
          checkin_date: Joi.date().required()
        }), { abortEarly: false, allowUnknown: true }
      );

      if(moment(req.body.checkin_date).isValid()){
        req.body.checkin_date = moment(req.body.checkin_date).format('YYYY-MM-DD HH:mm:ss')
      }
      
      let data = await Checkin.forge(req.body).save();

      res.json({status: true, data})

    } catch (error) {
      helper.errorResponse(res, [ error ]);
    }
  })

  router.delete('/:id', (req, res) => {
    Checkin.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}