'use strict';
const UserRevenue = require('../models').user_revenue;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: UserRevenue});

    return helper.fetchPaginationData(normalizedQuery, UserRevenue, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(UserRevenue.getAttributes(), UserRevenue.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}