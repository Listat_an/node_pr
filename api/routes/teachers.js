
'use strict';
const Teacher = require('../models').teacher;
const Academy = require('../models').academy;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Teacher});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, Teacher, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Teacher.getAttributes(), Teacher.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Teacher.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Teacher"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        first_name: Joi.string().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Academy.forge({id: req.body.academy_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      return Teacher.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let teacher;
    helper.validateData(req.body,
      Joi.object().keys({
        first_name: Joi.string().required(),
        academy_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Teacher.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Teacher', 404))
      }
      teacher = findModel;
      return Academy.forge({id: req.body.academy_id}).fetch();
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found Academy', 404))
      }
      return teacher.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Teacher.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}