
'use strict';
const UserAcademyInfo = require('../models').user_academy_info;
const User = require('../models').user;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: UserAcademyInfo});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, UserAcademyInfo, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(UserAcademyInfo.getAttributes(), UserAcademyInfo.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    UserAcademyInfo.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found UserAcademyInfo"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    req.body.user_id = req.user.id;
    helper.validateData(req.body,
      Joi.object().keys({
        name: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return UserAcademyInfo.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    req.body.user_id = req.user.id;
    UserAcademyInfo.forge({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found UserAcademyInfo', 404))
      }
      return findModel.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    UserAcademyInfo.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}