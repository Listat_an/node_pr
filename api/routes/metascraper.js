const metascraper = require('metascraper')([
  require('metascraper-description')(),
  require('metascraper-image')(),
  require('metascraper-logo')(),
  require('metascraper-title')(),
  require('metascraper-url')()
]);

const got = require('got');

module.exports = (router) => {
  router.post('/', async(req, res) => {
    try {
      const {body: html, url} = await got(req.body.url);
      const metadata = await metascraper({ html, url });
      res.status(200).json(metadata);
    } catch (error) {
      res.status(500).json(error);
    }
  });
  return router;
}