
'use strict';
const AcademyWork = require('../models').academy_work;
const Academy = require('../models').academy;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: AcademyWork});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, AcademyWork, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(AcademyWork.getAttributes(), AcademyWork.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    AcademyWork.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found AcademyWork"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Academy.forge({id: req.body.academy_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      return AcademyWork.forge(req.body).save();
    }).then((data) => {
      res.json({status: true, data})
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let academy_work;
    helper.validateData(req.body,
      Joi.object().keys({
        academy_id: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return AcademyWork.forge({id: req.params.id}).fetch()
    }).then(findModel => {
      if(!findModel){
        return Promise.reject(new AppError('Not found AcademyWork', 404))
      }
      academy_work = findModel;
      return Promise.all([
        Academy.forge({id: req.body.academy_id}).fetch(),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Academy', 404))
      }

      return academy_work.save(req.body, { method: 'update'})
    }).then(data => {
      res.json({status: true, data})
    }).catch((err) => {
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    AcademyWork.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}