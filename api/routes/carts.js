'use strict';
const Cart = require('../models').cart;
const Product = require('../models').product;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Cart});

    normalizedQuery.filter.user_id = req.user.id;
    return helper.fetchPaginationData(normalizedQuery, Cart, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Cart.getAttributes(), Cart.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Cart.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Cart"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    helper.validateData(req.body,
      Joi.object().keys({
        product_id: Joi.number().integer().required(),
        quantity: Joi.number().integer().required(),
        // photo: Joi.string().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Product.forge({id: req.body.product_id}).fetch({withRelated: ['images']}),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Product', 404))
      }
      let product = findModel[0].toJSON();
      if(product.quantity < req.body.quantity){
        return Promise.reject(new AppError('Lack of product quantity. Available ' + product.quantity, 404))
      }
      
      return Cart.forge({
        product_id: req.body.product_id,
        photo: req.body.photo || product.images[0] || '',
        quantity: req.body.quantity,
        user_id: req.user.id,
        price: product.price,
        name: product.title
      }).save();
    }).then((product) => {
      res.json(product.toJSON())
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.put('/:id([0-9]+)', (req, res) => {
    let product;
    helper.validateData(req.body,
      Joi.object().keys({
        quantity: Joi.number().integer().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Cart.forge({id: req.params.id}).fetch({withRelated: ['product']}),
      ]);
    }).then(findModel => {
      if(!findModel[0]){
        return Promise.reject(new AppError('Not found Product', 404))
      }
      product = findModel[0].toJSON();
      if(product.product.quantity < req.body.quantity){
        return Promise.reject(new AppError('Lack of product quantity. Available ' + product.product.quantity, 404, null, null, {quantity: product.product.quantity, product_id: product.product_id, id: product.id}))
      }
      return findModel[0].save({quantity: req.body.quantity}, { method: 'update'})
    }).then(() => {
      product.quantity = req.body.quantity;
      res.json(product)
    }).catch((err) => {
      console.log('err', err)
      helper.errorResponse(res, [ err ]);
    })
  });

  router.delete('/:id', (req, res) => {
    Cart.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  return router;
}