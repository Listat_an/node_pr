
'use strict';
const Refund = require('../models').refund;
const User = require('../models').user;
const Transaction = require('../models').transaction;
const AppError = require('../lib/app_error');
const helper = require('../lib/helper');
const Joi = require('joi');
const fs = require('fs');
const _ = require('lodash');
const ExtarnalService = require('../lib/external_services');
const UserTimeline = require('../models').user_timeline;

const multer  = require('multer')

module.exports = (router) => {
  
  router.get('/', (req, res) => {
    
    let normalizedQuery = helper.normalizeQuery2({query: req.query, model: Refund});

    console.log('normalizedQuery', normalizedQuery)
    return helper.fetchPaginationData(normalizedQuery, Refund, null, []).then((data) => {
      res.json(helper.indexResponse(data, _.concat(Refund.getAttributes(), Refund.getInclideAttributes())))
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  router.get('/:id([0-9]+)', (req, res) => {
    Refund.where({id: req.params.id}).fetch().then(findModel => {
      if(!findModel){
        return res.status(404).json({message: "Not found Refund"})
      }
      res.json(findModel.toJSON());
    }).catch(err => res.status(500).json(err))
  });

  router.post('/', (req, res) => {
    req.body.manager_id = req.user.id;
    let user;
    helper.validateData(req.body,
      Joi.object().keys({
        transaction_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => {
      return Promise.all([
        Transaction.where({id: req.body.transaction_id}).fetch(),
        User.where({id: req.user.id}).fetch(),
      ])
    }).then(models => {
      if(!models[0] || !models[0].get('charge_id')){
        return Promise.reject(new AppError('Not found transaction', 404))
      }

      if(!models[1]){
        return Promise.reject(new AppError('Not found User', 404))
      }

      if(!req.body.amount){
        req.body.amount = models[0].get('amount');
      }

      UserTimeline.where({transaction_id: req.body.transaction_id}).save({
        is_change_refund: 1
      }, {method: 'update'});
      
      user = models[1].toJSON();
      return Refund.forge(req.body).save();
    }).then((data) => {
      User.where({role: 'admin'}).fetchAll().then(admins => {
        admins.forEach(admin => {
          helper.createNotification(req, admin.id, 'The manager wants to make a refund', 'admin', 'refund', {refund_id: data.id});
        });
      })
      
      res.json({status: true, data})
    }).catch((err) => {
      console.log('​err', err);
      helper.errorResponse(res, [ err ]);
    })
  });

  router.post('/stripe', (req, res) => {
    let refund;
    helper.validateData(req.body,
      Joi.object().keys({
        refund_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => { 
      return Promise.all([
        Refund.where({id: req.body.refund_id}).fetch({withRelated: [
          'transaction.order', 
          'transaction.subscription', 
          'transaction.activity', 
          'transaction.academy', 
          'transaction.user',  
          'manager'
        ]})
      ])
    }).then(models => { 
      if(!models[0]){
        return Promise.reject(new AppError('Not found refund', 404))
      }

      if(models[0].get('status')){
        return Promise.reject(new AppError('Is refunded', 404))
      }

      refund = models[0].toJSON();

      return ExtarnalService.createStripeRefund({
        charge: refund.transaction.charge_id,
        amount: refund.amount * 100
      })
    }).then(refundStripe => {
      if(refundStripe.status == 'succeeded'){
        Refund.where({id: req.body.refund_id}).save({status: 1}, {method: 'update'})
        UserTimeline.where({transaction_id: refund.transaction_id}).save({
          is_refund: 1,
        }, {method: 'update'});

        helper.createNotification(req, refund.manager_id, `Your refund request for transaction ${refund.transaction.user.first_name} ${refund.transaction.user.last_name} $${refund.amount} ${refund.transaction.type} has been approved. Please allow 3-5 business day to complete the transaction.`, 'manager');
        helper.createNotification(req, refund.transaction.user_id, `Your refund request for transaction ${refund.transaction.user.first_name} ${refund.transaction.user.last_name} $${refund.amount} ${refund.transaction.type} has been approved. Please allow 3-5 business day to complete the transaction.`);

        return res.json({status: true}) 
      } else {
        return res.json({status: false}) 
      }
    }).catch((err) => {
      console.log('​err', err);
      helper.errorResponse(res, [ err ]);
    })
  })

  router.post('/decline', (req, res) => {
    let refund;
    helper.validateData(req.body,
      Joi.object().keys({
        refund_id: Joi.number().required(),
      }), { abortEarly: false, allowUnknown: true }
    ).then(validBody => { 
      return Promise.all([
        Refund.where({id: req.body.refund_id}).fetch({withRelated: [
          'transaction.order', 
          'transaction.subscription', 
          'transaction.activity', 
          'transaction.academy', 
          'transaction.user', 
          'manager'
        ]})
      ])
    }).then(models => { 
      if(!models[0]){
        return Promise.reject(new AppError('Not found refund', 404))
      }

      refund = models[0].toJSON();

      helper.createNotification(req, refund.manager_id, `Your refund request for transaction ${refund.transaction.user.first_name} ${refund.transaction.user.last_name} $${refund.amount} ${refund.transaction.type} has been decline. If you still may have some questions, please feel free to contact OPN-MAT team.`, 'manager');

      models[0].destroy();

      UserTimeline.where({transaction_id: refund.transaction_id}).save({
        is_change_refund: 0
      }, {method: 'update'});

      return res.json({status: true}) 
    }).catch((err) => {
      console.log('​err', err);
      helper.errorResponse(res, [ err ]);
    })
  })


  router.delete('/:id', (req, res) => {
    Refund.forge({ id: req.params.id }).destroy().then(() => {
      res.json({ status: true })
    }).catch((err) => {
      return res.status(500).json(err)
    })
  });

  

  return router;
}